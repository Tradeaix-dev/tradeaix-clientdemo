({
    selectdocuvault : function(component, event) {
        component.set("v.popdocuvault", true);
    },
    canceldocuvault : function(component, event) {
        component.set("v.popdocuvault", false);
    },
    getBidDetailslist: function(component, event, helper){        
        debugger;
        if(localStorage.getItem("FromRaidCHK") && localStorage.getItem("FromRaidUserPWD") =='undefined')
        {
            component.set("v.FromRaidCHK",true);
        }else{
            component.set("v.FromRaidCHK",false);
        }
        if(localStorage.getItem("LoggeduserProfile") == $A.get("{!$Label.c.SMCP_ProfileId}") || localStorage.getItem("LoggeduserProfile") == $A.get("{!$Label.c.AMCP}")){
            component.set("v.IsSellerManager",true);
        }
        else if(localStorage.getItem("LoggeduserProfile") == $A.get("{!$Label.c.BMCP_ProfileID}")){
            component.set("v.IsBuyerManager",true);
        }
        
        var action = component.get('c.BidDetailsList');
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();
        component.set("v.currentDate",yyyy+'-'+mm+'-'+dd);
        var offerId = getUrlParameter('offerId');
        var syndId = getUrlParameter('syndId');
        var username = localStorage.getItem('UserSession');
        component.set("v.Id",syndId);
        component.set("v.syndId",syndId);
        if(getUrlParameter('syndId') == undefined) 
            syndId = "";         
        var self = this; 
        if(syndId == ""){
            component.set("v.createBid",true);
        }
        component.set("v.transactionId",offerId);
        action.setParams({
            "offerId" : offerId,
            "syndId" : syndId,
            "userid" :username
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state == "SUCCESS"){
                component.set("v.offer",result); 
                component.set("v.createBid",false);
                if(syndId !=""){ 
                    component.set("v.TransId",result.Syndication.Transaction__c);
                    if(result.Syndication.Bid_Status__c == 'Quote Closed'){
                        component.set("v.closedbid", true);
                    }
                }
                if(syndId == ""){    
                    component.set("v.offer",result);
                    component.set("v.createBid",true);
<<<<<<< HEAD
                    component.set("v.createcf",true);
                    component.set("v.stringamount",result.TAmount); 
                    component.set("v.TransAMt", result.TAmount);
                }
                if(offerId != '')
                {
                    function addDays(days){
                        var currentDate = new Date();
                        currentDate.setDate(currentDate.getDate() + days); 
                        var date =currentDate.getFullYear()+ "-" +(((currentDate.getMonth() + 1) < 10 ) ? '0' + (currentDate.getMonth() + 1) : (currentDate.getMonth() + 1)) + "-" + ((currentDate.getDate() < 10) ?  '0' + currentDate.getDate() : currentDate.getDate());                return date; 
                    }
                    
                    var currentTime = new Date();
                    var day = currentTime.getDay(); 
                    
                    var addday = 2;
                    if(day == 4 || day == 5){
                        addday = addday + 2; 
                    }
                    
                    var date4 = addDays(addday); 
                    component.find("BidExpirationDate").set("v.value",date4);
                } 
            }
        });
        $A.enqueueAction(action); 
    },
    getBidDetails: function(component, event, helper) {
        debugger;
        var action = component.get('c.BidDetails');
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();
        component.set("v.currentDate",yyyy+'-'+mm+'-'+dd);
        var offerId = getUrlParameter('offerId');
        var syndId = getUrlParameter('syndId');
        component.set("v.Id",syndId);
        if(getUrlParameter('syndId') == undefined) 
            syndId = ""; 
        
        var self = this; 
        action.setParams({
            "offerId" : offerId,
            "syndId" : syndId
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                component.set("v.offer", result);
                component.set("v.offerId", result.Syndication.Offer__c);
                component.set("v.syndId", result.syndId); 
                component.set("v.parentId", result.syndId);  
                component.set("v.LoggeduserCunion", result.CreditUnion.Id);
                component.set("v.CUId", result.OfferCreditUnion.Id);
                component.set("v.Pname", result.OfferCreditUnion.CU_Name__c);
                component.set("v.cpname", result.CreditUnion.CU_Name__c);
                component.set("v.offerId1", result.offerId);
                component.set("v.isSuperUser", result.isSuperUser);
                component.set("v.BuyerUserId", result.Syndication.CreatedById);
                component.set("v.TransAMt", result.TAmount);
                if(result.Syndication.Name == undefined)
                {
                    var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
                    document.title =sitename +' : Quote Detail';
                    
                }else{
                    document.title = result.Syndication.Name;
                }
                var syndication = {
                    'sobjectType': 'Syndications__c',
                    'Id': result.syndId,
                    'Name': result.Syndication.Name == undefined ? 'Create Bid' : result.Syndication.Name,
                    'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                    'Bid_Status__c': result.Syndication.Bid_Status__c,
                    'Buyer_Counter_Price__c': result.Syndication.Buyer_Counter_Price__c,
                    'Buyer_Service_Percentage__c': result.Syndication.Buyer_Service_Percentage__c,
                    'Note__c': result.Syndication.Note__c,
                    'Offer__c': result.Syndication.Offer__c,
                    'Participation__c': result.Syndication.Participation__c,
                    'Portfolio_Price__c': result.Syndication.Portfolio_Price__c,
                    'Seller_Service_Percentage__c': result.Syndication.Seller_Service_Percentage__c,
                    'submitted_Date__c': result.Syndication.submitted_Date__c,
                    'Buyer_Name__c': result.Syndication.Buyer_Name__c,
                    'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                };
                component.set("v.syndication", syndication);  
                
                if(offerId != '')
                {
                    function addDays(days){
                        var currentDate = new Date();
                        currentDate.setDate(currentDate.getDate() + days); 
                        var date =currentDate.getFullYear()+ "-" +(((currentDate.getMonth() + 1) < 10 ) ? '0' + (currentDate.getMonth() + 1) : (currentDate.getMonth() + 1)) + "-" + ((currentDate.getDate() < 10) ?  '0' + currentDate.getDate() : currentDate.getDate());                return date; 
                    }
                    
                    var currentTime = new Date();
                    var day = currentTime.getDay(); 
                    
                    var addday = 2;
                    if(day == 4 || day == 5){
                        addday = addday + 2; 
                    }
                    
                    var date4 = addDays(addday); 
                    component.find("BidExpirationDate").set("v.value",date4);
                    component.set("v.syndication.Buyer_Counter_Price__c",result.TAmount);
                } 
                var nextAction = component.get("c.secondarymarketingflag"); 
                debugger;
                nextAction.setCallback(this, function(actionResult) {
                    var results = actionResult.getReturnValue();
                    if(state === "SUCCESS"){
                        debugger;
                        component.set("v.issecondaryMarket",results)
                    }
                });
                $A.enqueueAction(nextAction);
                
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.offer", emptyTask);
            }
            var menu = document.getElementById('Quotes');
            var classArr = menu.className.split(" ");
            if (classArr.indexOf("current") == -1) {
                menu.className += " " + "current";
            }
        });
        $A.enqueueAction(action);
    },      
    
    registeruser: function(component, event) { 
        component.set("v.Customer", true);
        component.set("v.benefits",true);
        component.set("v.clrbenefits",'active');
        component.set("v.userinfo",false);
        component.set("v.setpwd",false);
        component.set("v.finish",false);
    },
    bennext: function(component, event) { 
        
        
        var action = component.get('c.GetLoggedUserInfo');  
        action.setParams({
            "LoggedUserID" : localStorage.getItem('UserSession')
        });
        debugger;
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.UM",result);
                component.set("v.benefits",false);
                component.set("v.userinfo",true);
                component.set("v.clrbenefits",'completed');
                component.set("v.clrud",'active');
            }
        });
        debugger;
        $A.enqueueAction(action);
        
    },
    backtoben: function(component, event) { 
        component.set("v.benefits",true);
        component.set("v.userinfo",false);
        component.set("v.clrbenefits",'active');
        component.set("v.clrud",'');
    },
    nexttosetpwd: function(component, event) { 
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        
        //var FirstName = component.find("FirstName").get("v.value");
        //var sitename = component.find("sitename").get("v.value");
       // var email = component.find("email").get("v.value");
        //var lastname = component.find("lastname").get("v.value");
        //var swiftcode = component.find("swiftcode").get("v.value");
        //var location = component.find("location").get("v.value");
        var phone = component.find("phone").get("v.value");   
        var Mobile = component.find("Mobile").get("v.value");  
        var countrycode = component.find("auracountrycode").get("v.value");
        var mobilecountrycode = component.find("auramobilecountrycode").get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        
        
        /*if(FirstName == '' || FirstName == 'undefined' || FirstName == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.FirstNameError", 'Input field required'); 
        } else { 
            component.set("v.FirstNameError", ''); 
        } 
        
        if(sitename == '' || sitename == 'undefined' || sitename == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LegalNameError", 'Input field required'); 
        } else { 
            component.set("v.LegalNameError", ''); 
        } 
        
        if(lastname == '' || lastname == 'undefined' || lastname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LastNameError", 'Input field required'); 
        } else { 
            component.set("v.LastNameError", ''); 
        } 
        if(swiftcode == '' || swiftcode == 'undefined' || swiftcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.SwiftCodeError", 'Input field required'); 
        } else { 
            component.set("v.SwiftCodeError", ''); 
        } */
        if(!$A.util.isEmpty(phone))
        {   
            if(countrycode == '' || countrycode =='--Select--' || countrycode == 'undefined' || countrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.countrycodeError", 'Input field required'); 
            } else { 
                component.set("v.countrycodeError", ''); 
            } 
            if(!phone.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please enter valid phone number'); 
            } 
            else if(phone.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.PhoneError", ''); 
                }
        }
        if(!$A.util.isEmpty(Mobile))
        {   
            if(mobilecountrycode == '' || mobilecountrycode =='--Select--' || mobilecountrycode == 'undefined' || mobilecountrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.mobilecountrycodeError", 'Input field required'); 
            } else { 
                component.set("v.mobilecountrycodeError", ''); 
            }
            
            if(!Mobile.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please enter valid Mobile number'); 
            } 
            else if(Mobile.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.MobileError", ''); 
                }
        }
        if(errorFlag == 'false') {
            
            
            
            var action = component.get('c.UpdateRegForm');  
            action.setParams({
                "LoggedUserID" : localStorage.getItem('UserSession'),            
                "UM" : component.get("v.UM")
            });
            debugger;
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.newcounterPopup", false);
                    component.set("v.onboard", true);
                    
                    component.set("v.userinfo",false);
                    component.set("v.setpwd",true);
                    component.set("v.clrbenefits",'completed');
                    component.set("v.clrud",'completed');
                    component.set("v.clrspwd",'active');
                    
                }
            });
            debugger;
            $A.enqueueAction(action);
        }
    },
    backtouser: function(component, event) { 
        debugger;
        component.set("v.userinfo",true);
        component.set("v.setpwd",false);
        component.set("v.clrbenefits",'completed');
        component.set("v.clrud",'active');
        component.set("v.clrspwd",'');
    },
    nexttofinish: function(component, event) { 
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var newPassword = component.find("newPassword").get("v.value");
        var confirmPassword = component.find("confirmPassword").get("v.value"); 
        var re = new RegExp("^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$");
        
        if(newPassword == undefined || newPassword == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.newpassworderror",'Input field required');
        }else{
            component.set("v.newpassworderror",'');
        }
        if(confirmPassword == undefined || confirmPassword == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.confirmpassworderror",'Input field required');
        }
        else if(newPassword != confirmPassword){  
            errorFlag = 'true';
            component.set("v.confirmpassworderror",'');
            component.set("v.changepasswordvalidation",true);
            component.set("v.validation", false);
            component.set("v.combinationvalidation",false);
        }
            else if (!re.test(confirmPassword)) {
                errorFlag = 'true';
                component.set("v.combinationvalidation",true);
                component.set("v.changepasswordvalidation",false);
                component.set("v.validation",false);
            }
        
        
        if(errorFlag == 'false') {
            var action = component.get('c.UpdateLBFROMMain');
            var self = this; 
            action.setParams({
                "ID" : localStorage.getItem("UID"),               
                "newPassword" : newPassword
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                debugger;
                if(state === "SUCCESS"){       
                    component.set("v.setpwd",false);
                    component.set("v.finish",true);
                    component.set("v.clrbenefits",'completed');
                    component.set("v.clrud",'completed');
                    component.set("v.clrspwd",'completed');
                    component.set("v.clrfinish",'active');
                }
            }); 
            $A.enqueueAction(action);    
        } 
    },
    finishben: function(component, event) { 
        debugger;
        component.set("v.clrbenefits",'');
        component.set("v.clrud",'');
        component.set("v.clrspwd",'');
        component.set("v.clrfinish",'');
        component.set("v.Customer",false);
        $A.get('e.force:refreshView').fire();
    },
    closeregistration: function(component, event) { 
      component.set("v.clrbenefits",'');
        component.set("v.clrud",'');
        component.set("v.clrspwd",'');
        component.set("v.clrfinish",'');
        component.set("v.Customer",false);
    },
   
    SubmitBid : function(component, event) { 
        debugger;
        var errorFlag = 'false';
        var buyerCounterPrice11 = component.get("v.stringamount");//component.find("buyerCounterPrice1").get("v.value");
        var buyerServicePrice12 = component.get("v.stringbidfee");//component.find("buyerServicePrice1").get("v.value");
        var Tamt = component.get("v.TransAMt");        
        var peramt = (Tamt/100)*10;
        var Disamt=Tamt-peramt;
        /*alert('#$#$'+peramt);
        alert('#$#$'+buyerCounterPrice11);
        alert('#$#$'+Disamt);
        alert(buyerCounterPrice11 > Tamt);
        alert(buyerCounterPrice11 < Disamt);*/
        if(component.find("auracostfundvalue").get("v.value") == '--Select--'){
            errorFlag = 'true';
            component.set("v.costoffundmessage", 'Input field required');
        }else{
            component.set("v.costoffundmessage", '');
        } 
        if(buyerCounterPrice11 == 0)
        {
            errorFlag = 'true';
            component.set("v.Bid_Port_Price", 'Enter a valid number');
        }
        else if(buyerCounterPrice11 > Tamt || buyerCounterPrice11 < Disamt)
        {
            errorFlag = 'true';
            component.set("v.Bid_Port_Price", 'The quote amount should be greater than 90% OR equal to the transaction amount.');
        }
            else if(isNaN(buyerCounterPrice11) || buyerCounterPrice11 <= 0)
            {
                errorFlag = 'true';
                component.set("v.Bid_Port_Price", 'Enter a valid number');
            } else {
                component.set("v.Bid_Port_Price", '');
            }
        if(buyerServicePrice12 == 0)
        {
            errorFlag = 'true';
            component.set("v.Bid_Ser_Fee", 'Enter a valid number');
        }
        else if(isNaN(buyerServicePrice12) || buyerServicePrice12 <= 0)
        {
            errorFlag = 'true';
            component.set("v.Bid_Ser_Fee", 'Enter a valid number');
        } else {
            component.set("v.Bid_Ser_Fee", '');  
        }        
        function addDays(days){
            var currentDate = new Date();
            var date = currentDate.setDate(currentDate.getDate() + days); 
            var date = (((currentDate.getMonth() + 1) < 10 ) ? '0' + (currentDate.getMonth() + 1) : (currentDate.getMonth() + 1)) + "/" + ((currentDate.getDate() < 10) ?  '0' + currentDate.getDate() : currentDate.getDate()) + "/" + currentDate.getFullYear();
            return date; 
        } 
        
        var dateFormat = $A.get("$Locale.dateFormat");
        //alert('dateFormat-'+dateFormat);
        var date1 =component.find("BidExpirationDate").get("v.value"); 
        // alert('date1-'+date1);
        //var dateString = $A.localizationService.formatDateTime(date1, dateFormat);
        // alert('dateString-'+dateString);
        var selectDate = new Date(date1);
        //alert('selectDate-'+selectDate);
        var date2 = addDays(0);
        var date3 = addDays(30);
        
        var add2 = $A.localizationService.formatDateTime(new Date(), dateFormat);
        var add3 = $A.localizationService.formatDateTime(date3, dateFormat);
        var selectPassDate = new Date(date2);
        var select30Date = new Date(date3); 
        var day1 = selectDate.getDay();
        //alert(day1);
        debugger;
        if (component.find("BidExpirationDate").get("v.value") == "") { 
            component.set("v.BidExpirationDateError", true);
            component.set("v.ExpirationDate", 'Please select the Bid Expiration Date.');
            errorFlag = 'true';
        } 
        else if (selectDate < selectPassDate) {
            component.find("BidExpirationDate").set("v.value", "");
            component.set("v.BidExpirationDateError", true);
            component.set("v.ExpirationDate", 'Bid Expiration Date must be in the future.');
            errorFlag = 'true';
        } 
            else if (selectDate >= select30Date) {
                component.find("BidExpirationDate").set("v.value", "");
                component.set("v.BidExpirationDateError", true);
                component.set("v.ExpirationDate", 'Selected date should be less than 30 days.');
                errorFlag = 'true';
            }
                else if (day1 == 0 || day1 == 6) { 
                    component.find("BidExpirationDate").set("v.value", "");
                    component.set("v.BidExpirationDateError", true);
                    component.set("v.ExpirationDate", 'Choose any weekdays.');
                    errorFlag = 'true';
                }    
        //component.set("v.SubmitBid", true); 
        if(buyerCounterPrice11 != "" && buyerServicePrice12 != "" && errorFlag == 'false'){
            component.set("v.SubmitBid", true); 
        } 
    },      
    BidExpirationDateErrorcancel: function(component, event, helper){
        component.set("v.BidExpirationDateError", false);
    },
    submitBidCheck: function(component, event, helper) {
        if(component.find("chkSubmitBid").get('v.value')==true){
            component.find("submitBidbtn1").set("v.disabled", false);
        }
        else{
            component.find("submitBidbtn1").set("v.disabled", true);
        }
    },
    btnsubmitBid1: function(component, event, helper) {
        debugger;
        component.set("v.SubmitBid", false);
        var syndication = {
            'sobjectType': 'Quotes__c',
            'Bid_Amount__c': component.get("v.stringamount"),
            'Bid_Expiration_Date__c': component.get("v.stringExpirationDate"),
            'Bid_Service_Fee_bps__c': component.get("v.stringbidfee"),
            'Note__c':component.get("v.stringNotes"),
            'Cost_of_Funds__c' : component.find("auracostfundvalue").get("v.value")}; //
        var offerId = component.get("v.transactionId");
        var username = localStorage.getItem('UserSession');
        var action = component.get('c.SubmitBidTransaction');
        action.setParams({
            "syndication" : syndication,
            "offerId" : offerId ,
            "userid" : username,
            "Title" : localStorage.getItem("LoggeduserName"),
            "body" : component.find("body2").get("v.value")           
        });
        action.setCallback(this, function(actionResult) { 
            debugger;
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();            
            if(state === "SUCCESS"){
                if(localStorage.getItem("FromRaidCHK") && localStorage.getItem("FromRaidUserPWD") =='undefined')
                {
                    component.set("v.FromRaidCHK",true);
                }else{
                    component.set("v.FromRaidCHK",false);
                }
                component.set("v.offer",result);
                component.set("v.createBid",false);
                component.set("v.createcf",false);
                component.set("v.syndId", result.syndId); 
                
            }
        });
        $A.enqueueAction(action);
        debugger;
    },
    
    btnsubmitBid: function(component, event, helper) {
        debugger;
        component.set("v.SubmitBid", false);
        var action = component.get('c.SaveBid');
        var offer = component.get("v.offer"); 
        var syndication = offer.Syndication;
        debugger;
        syndication = component.get("v.syndication"); 
        var self = this; 
        action.setParams({
            "syndication" : syndication,
            "body" :  component.find("body2").get("v.value"),
            "CUID" : component.get("v.LoggeduserCunion"),
            "Pname" : component.get("v.Pname"),
            "cpname" : component.get("v.cpname") 
        });
        action.setCallback(this, function(actionResult) { 
            debugger;
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            
            if(state === "SUCCESS"){
                component.set("v.offer", result);
                component.set("v.offerId", result.offerId);   
                component.set("v.syndId", result.syndId); 
                component.set("v.parentId", result.syndId);  
                component.set("v.CUId", result.OfferCreditUnion.Id);
                component.set("v.offerId1", result.offerId);
                var syndication = {
                    'sobjectType': 'Syndications__c',
                    'Id': result.syndId,
                    'Name': result.Syndication.Name,
                    'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                    'Bid_Status__c': result.Syndication.Bid_Status__c,
                    'Buyer_Counter_Price__c': result.Syndication.Buyer_Counter_Price__c,
                    'Buyer_Service_Percentage__c': result.Syndication.Buyer_Service_Percentage__c,
                    'Note__c': result.Syndication.Note__c,
                    'Offer__c': result.Syndication.Offer__c,
                    'Participation__c': result.Syndication.Participation__c,
                    'Portfolio_Price__c': result.Syndication.Portfolio_Price__c,
                    'Seller_Service_Percentage__c': result.Syndication.Seller_Service_Percentage__c,
                    'submitted_Date__c': result.Syndication.submitted_Date__c,
                    'Buyer_Name__c': result.Syndication.Buyer_Name__c,
                    'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                };
                component.set("v.syndication", syndication);
                component.set("v.syndId", result.syndId); 
                
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    submitBidcancel: function(component, event) {
        component.set("v.SubmitBid", false);
    },
    BidDue : function(component, event) {  
        component.set("v.BidDue", true); 
    },
    BidDueCheck : function(component, event, helper) {
        if(component.find("chkBidDue").get('v.value') == true) {
            component.find("BidDuebtn1").set("v.disabled", false);
        }
        else{
            component.find("BidDuebtn1").set("v.disabled", true);
        }
    },
    btnBidDue: function(component, event, helper) {
        var action = component.get('c.BidDue1'); 
        action.setParams({
            "syndId" : component.get("v.syndId"),
            "body3" : component.find("body3").get("v.value")
        });
        action.setCallback(self, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.BidDue", false);
            component.set("v.SecondBidDue", true);
            if(state === "SUCCESS"){
                component.set("v.offer", result);
                component.set("v.offerId", result.offerId);   
                component.set("v.syndId", result.syndId); 
                component.set("v.parentId", result.syndId);
                var syndication = {
                    'sobjectType': 'Syndications__c',
                    'Id': result.syndId,
                    'Name': result.Syndication.Name,
                    'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                    'Bid_Status__c':result.Syndication.Bid_Status__c,
                    'Buyer_Counter_Price__c':result.Syndication.Buyer_Counter_Price__c,
                    'Buyer_Service_Percentage__c':result.Syndication.Buyer_Service_Percentage__c,
                    'Note__c':result.Syndication.Note__c,
                    'Offer__c':result.Syndication.Offer__c,
                    'Participation__c':result.Syndication.Participation__c,
                    'Portfolio_Price__c':result.Syndication.Portfolio_Price__c,
                    'Seller_Service_Percentage__c':result.Syndication.Seller_Service_Percentage__c,
                    'submitted_Date__c':result.Syndication.submitted_Date__c,
                    'Buyer_Name__c':result.Syndication.Buyer_Name__c,
                    'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                };
                component.set("v.syndication", syndication);  
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    BidDuecancel : function(component, event) {
        component.set("v.BidDue", false);
    },
    SecondBidDuecancel : function(component, event) {
        component.set("v.SecondBidDue", false);
    },
    AgreeWithdraw : function(component, event) { 
        component.set("v.AgreeWithdraw", true);
    },
    AgreeWithdrawCheck : function(component, event, helper) {
        if(component.find("chkAgreeWithdraw").get('v.value') == true) {
            component.find("AgreeWithdrawbtn1").set("v.disabled", false);
        }
        else{
            component.find("AgreeWithdrawbtn1").set("v.disabled", true);
        } 
    },
    btnAgreeWithdraw: function(component, event, helper) {
        var action = component.get('c.postWithdrawn'); 
        var self = this; 
        var username = localStorage.getItem('UserSession');
        action.setParams({
            "syndId" : component.get("v.syndId"),
            "Title" : localStorage.getItem("LoggeduserName"),
            "body2" : component.find("body2").get("v.value"),
            "userid" : username
        });
        action.setCallback(self, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.AgreeWithdraw", false);
            component.set("v.SecondPreWithdraw", true);
            if(state === "SUCCESS"){
                component.set("v.offer", result);
                component.set("v.offerId", result.offerId);   
                component.set("v.syndId", result.syndId); 
                component.set("v.parentId", result.syndId);
                var syndication = {
                    'sobjectType': 'Syndications__c',
                    'Id': result.syndId,
                    'Name': result.Syndication.Name,
                    'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                    'Bid_Status__c':result.Syndication.Bid_Status__c,
                    'Buyer_Counter_Price__c':result.Syndication.Buyer_Counter_Price__c,
                    'Buyer_Service_Percentage__c':result.Syndication.Buyer_Service_Percentage__c,
                    'Note__c':result.Syndication.Note__c,
                    'Offer__c':result.Syndication.Offer__c,
                    'Participation__c':result.Syndication.Participation__c,
                    'Portfolio_Price__c':result.Syndication.Portfolio_Price__c,
                    'Seller_Service_Percentage__c':result.Syndication.Seller_Service_Percentage__c,
                    'submitted_Date__c':result.Syndication.submitted_Date__c,
                    'Buyer_Name__c':result.Syndication.Buyer_Name__c,
                    'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                };
                component.set("v.syndication", syndication);  
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    AgreeWithdrawcancel : function(component, event) {
        component.set("v.AgreeWithdraw", false);
    },
    SecondPreWithdrawcancel: function(component, event) {
        component.set("v.SecondPreWithdraw", false);
    }, 
    BidWithdrawn : function(component, event) { 
        // alert(event.target.id);
        component.set("v.syndId", event.target.id);
        component.set("v.BidWithdrawn", true);       
    },     
    BidWidrawnCheck: function(component, event, helper) {
        if(component.find("chkBidWidrawn").get('v.value') == true){
            component.find("BidWidrawnbtn1").set("v.disabled", false);
        }
        else{
            component.find("BidWidrawnbtn1").set("v.disabled", true);
        }
    }, 
    btnBidWidrawn: function(component, event, helper) {
        debugger;
        var action = component.get('c.Withdrawcall'); 
        var synid = component.get("v.syndId");
        var body = component.find("body1").get("v.value");
        var username = localStorage.getItem('UserSession');
        var self = this;
        action.setParams({
            "syndId" : synid,
            "Title" : localStorage.getItem("LoggeduserName"),
            "body1" : body,
            "userid" : username
        });
        action.setCallback(self, function(actionResult) { 
            debugger;
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.BidWithdrawn", false);
            component.set("v.SecondBidWithdrawn", true);
            if(state === "SUCCESS"){
                component.set("v.offer", result);
                component.set("v.offerId", result.offerId);   
                component.set("v.syndId", result.syndId); 
                component.set("v.parentId", result.syndId);
                var syndication = {
                    'sobjectType': 'Syndications__c',
                    'Id': result.syndId,
                    'Name': result.Syndication.Name,
                    'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                    'Bid_Status__c':result.Syndication.Bid_Status__c,
                    'Buyer_Counter_Price__c':result.Syndication.Buyer_Counter_Price__c,
                    'Buyer_Service_Percentage__c':result.Syndication.Buyer_Service_Percentage__c,
                    'Note__c':result.Syndication.Note__c,
                    'Offer__c':result.Syndication.Offer__c,
                    'Participation__c':result.Syndication.Participation__c,
                    'Portfolio_Price__c':result.Syndication.Portfolio_Price__c,
                    'Seller_Service_Percentage__c':result.Syndication.Seller_Service_Percentage__c,
                    'submitted_Date__c':result.Syndication.submitted_Date__c,
                    'Buyer_Name__c':result.Syndication.Buyer_Name__c,
                    'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                };
                component.set("v.syndication", syndication);  
            }
            
        });
        $A.enqueueAction(action);
    }, 
    BidWidrawncancel: function(component, event) {
        component.set("v.BidWithdrawn", false);
    },
    SecondBidWithdrawncancel: function(component, event) {
        component.set("v.SecondBidWithdrawn", false);
    },
    popNotes: function(component, event) {
        component.set("v.Notes", true);
    },
    NotesCancel: function(component, event) {
        component.set("v.Notes", false);
        component.set("v.saveNote", ''); 
    },
    NotesSave: function(component, event, helper) {
        debugger;
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || savNote == null || savNote == undefined || savNote.trim().length*1 == 0 ) {            
            component.set("v.saveNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        }
        else{
            component.set("v.saveNote", ''); 
            var username = localStorage.getItem('UserSession');
            var action = component.get('c.saveNotes');
            var self = this; 
            action.setParams({
                "syndId" : component.get("v.syndId"),
                "Title" : localStorage.getItem("LoggeduserName"),
                "body" : component.find("body").get("v.value"),               
                "userid" : username,
                "Quotestatus" : component.get("v.closedbid")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.Notes", false);
                if(state === "SUCCESS"){                   
                    component.set("v.offer.Internalmemos", result.Internalmemos); 
                    /* component.set("v.syndId", result.syndId);
                    component.set("v.offer", result);
                    component.set("v.offerId", result.offerId);
                    component.set("v.parentId", result.syndId);
                    var syndication = {
                        'sobjectType': 'Syndications__c',
                        'Id': result.syndId,
                        'Name': result.Syndication.Name,
                        'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                        'Bid_Status__c':result.Syndication.Bid_Status__c,
                        'Buyer_Counter_Price__c':result.Syndication.Buyer_Counter_Price__c,
                        'Buyer_Service_Percentage__c':result.Syndication.Buyer_Service_Percentage__c,
                        'Note__c':result.Syndication.Note__c,
                        'Offer__c':result.Syndication.Offer__c,
                        'Participation__c':result.Syndication.Participation__c,
                        'Portfolio_Price__c':result.Syndication.Portfolio_Price__c,
                        'Seller_Service_Percentage__c':result.Syndication.Seller_Service_Percentage__c,
                        'submitted_Date__c':result.Syndication.submitted_Date__c,
                        'Buyer_Name__c':result.Syndication.Buyer_Name__c,
                        'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                    };
                    component.set("v.syndication", syndication); */
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    }, 
    delete1 : function(component, event, helper) {
        debugger;
        var action = component.get('c.DeleteFile');
        var username = localStorage.getItem('UserSession');
        action.setParams({
            "attId" : event.target.id ,
            "" : username
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.offer", result);
                component.set('v.DeleteFile', true);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.offer", emptyTask);
            }
        });
        $A.enqueueAction(action); 
    },
    delAttach : function(component, event, helper) {
        debugger;
        var action = component.get('c.DeleteAttach'); 
        var username = localStorage.getItem('UserSession');
        action.setParams({
            "attId" : event.target.id ,
            "userid" : username
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.offer", result);
                component.set('v.DeleteFile', true);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.offer", emptyTask);
            }
        });
        $A.enqueueAction(action); 
    },
    DeleteFilecancel: function(component, event, helper) {
        component.set('v.DeleteFile', false);
    },
    
    hoverCreditUnion: function(component, event) { 
        debugger;
        var action = component.get('c.CUDetails'); 
        action.setParams({
            "CUId" : component.get("v.CUId")
        });       
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.CUUser", result); 
                component.set("v.popoverCreditUnion", true);
            } 
        });
        $A.enqueueAction(action);
    },
    hoverCreditUnionhide: function(component, event) { 
        component.set("v.popoverCreditUnion", false); 
    },
    hoverOfferDetails: function(component, event) {  
        var action = component.get('c.OfferDetails'); 
        action.setParams({
            "offerId1" : component.get("v.offerId1")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){ 
                var popover = component.find("popoverOfferDetails"); 
                component.set("v.offr", result);
                $A.util.removeClass(popover,'slds-hide');
                $A.util.addClass(popover,'slds-show');        		 
                event.preventDefault();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    hoverofferhide : function(component, event) { 
        var popover = component.find("popoverOfferDetails"); 
        $A.util.addClass(popover,'slds-hide'); 
        $A.util.removeClass(popover,'slds-show');
        event.preventDefault();
    },
    
    buyerCount : function(component, event, helper) {
        var inp = component.find("buyerCounterPrice1").get("v.value"); 
        if(inp == 0)
            component.set("v.syndication.Buyer_Counter_Price__c","");
    },
    buyerSer : function(component, event, helper) {
        var inp = component.find("buyerServicePrice1").get("v.value"); 
        if(inp == 0)
            component.set("v.syndication.Buyer_Service_Percentage__c","");
    },
    changePortfolioPrice: function(component, event, helper) { 
        var portfolioPrice = component.find("buyerCounterPrice1").get("v.value"); 
        if(portfolioPrice == '')
            component.set("v.syndication.Buyer_Counter_Price__c","0"); 
    },
    changeServicingFee: function(component, event, helper) { 
        var principal = component.find("buyerServicePrice1").get("v.value"); 
        if(principal == '')
            component.set("v.syndication.Buyer_Service_Percentage__c","0"); 
    },
    AgreeBid: function(component, event) { 
        debugger;
        //component.set("v.syndId", event.target.id); 
        component.set("v.AgreetoTerms", true);
        
    }, 
    AgreeBidcheck: function(component, event, helper) {
        debugger;
        var ACDate = component.find("AntDate").get("v.value");
        if(component.find("chkAgreeBid").get('v.value') == true) { // && ACDate != "" && ACDate != undefined){
            component.find("AgreeBidbtn1").set("v.disabled", false);
        } else {
            component.find("AgreeBidbtn1").set("v.disabled", true);
        }
    }, 
    btnAgreeBid: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        if(errorFlag == 'false')
        { 
            var username = localStorage.getItem('UserSession');
            var action = component.get('c.AgreeBid1'); 
            action.setParams({
                "syndId" : component.get("v.syndId"),
                "Title" : localStorage.getItem("LoggeduserName"),
                "body4" : component.find("body4").get("v.value"),
                "userid" : username
                
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.AgreetoTerms", false);
                component.set("v.SecondAgree", true);
                if(state === "SUCCESS"){
                    component.set("v.offer", result);
                    component.set("v.offerId", result.offerId);   
                    component.set("v.syndId", result.Syndication.Id); 
=======
                    component.set("v.stringamount",result.TAmount); 
                    component.set("v.TransAMt", result.TAmount);
                }
                if(offerId != '')
                {
                    function addDays(days){
                        var currentDate = new Date();
                        currentDate.setDate(currentDate.getDate() + days); 
                        var date =currentDate.getFullYear()+ "-" +(((currentDate.getMonth() + 1) < 10 ) ? '0' + (currentDate.getMonth() + 1) : (currentDate.getMonth() + 1)) + "-" + ((currentDate.getDate() < 10) ?  '0' + currentDate.getDate() : currentDate.getDate());                return date; 
                    }
                    
                    var currentTime = new Date();
                    var day = currentTime.getDay(); 
                    
                    var addday = 2;
                    if(day == 4 || day == 5){
                        addday = addday + 2; 
                    }
                    
                    var date4 = addDays(addday); 
                    component.find("BidExpirationDate").set("v.value",date4);
                } 
            }
        });
        $A.enqueueAction(action); 
    },
    getBidDetails: function(component, event, helper) {
        debugger;
        var action = component.get('c.BidDetails');
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();
        component.set("v.currentDate",yyyy+'-'+mm+'-'+dd);
        var offerId = getUrlParameter('offerId');
        var syndId = getUrlParameter('syndId');
        component.set("v.Id",syndId);
        if(getUrlParameter('syndId') == undefined) 
            syndId = ""; 
        
        var self = this; 
        action.setParams({
            "offerId" : offerId,
            "syndId" : syndId
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                component.set("v.offer", result);
                component.set("v.offerId", result.Syndication.Offer__c);
                component.set("v.syndId", result.syndId); 
                component.set("v.parentId", result.syndId);  
                component.set("v.LoggeduserCunion", result.CreditUnion.Id);
                component.set("v.CUId", result.OfferCreditUnion.Id);
                component.set("v.Pname", result.OfferCreditUnion.CU_Name__c);
                component.set("v.cpname", result.CreditUnion.CU_Name__c);
                component.set("v.offerId1", result.offerId);
                component.set("v.isSuperUser", result.isSuperUser);
                component.set("v.BuyerUserId", result.Syndication.CreatedById);
                component.set("v.TransAMt", result.TAmount);
                if(result.Syndication.Name == undefined)
                {
                    var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
                    document.title =sitename +' : Quote Detail';
                    
                }else{
                    document.title = result.Syndication.Name;
                }
                var syndication = {
                    'sobjectType': 'Syndications__c',
                    'Id': result.syndId,
                    'Name': result.Syndication.Name == undefined ? 'Create Bid' : result.Syndication.Name,
                    'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                    'Bid_Status__c': result.Syndication.Bid_Status__c,
                    'Buyer_Counter_Price__c': result.Syndication.Buyer_Counter_Price__c,
                    'Buyer_Service_Percentage__c': result.Syndication.Buyer_Service_Percentage__c,
                    'Note__c': result.Syndication.Note__c,
                    'Offer__c': result.Syndication.Offer__c,
                    'Participation__c': result.Syndication.Participation__c,
                    'Portfolio_Price__c': result.Syndication.Portfolio_Price__c,
                    'Seller_Service_Percentage__c': result.Syndication.Seller_Service_Percentage__c,
                    'submitted_Date__c': result.Syndication.submitted_Date__c,
                    'Buyer_Name__c': result.Syndication.Buyer_Name__c,
                    'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                };
                component.set("v.syndication", syndication);  
                
                if(offerId != '')
                {
                    function addDays(days){
                        var currentDate = new Date();
                        currentDate.setDate(currentDate.getDate() + days); 
                        var date =currentDate.getFullYear()+ "-" +(((currentDate.getMonth() + 1) < 10 ) ? '0' + (currentDate.getMonth() + 1) : (currentDate.getMonth() + 1)) + "-" + ((currentDate.getDate() < 10) ?  '0' + currentDate.getDate() : currentDate.getDate());                return date; 
                    }
                    
                    var currentTime = new Date();
                    var day = currentTime.getDay(); 
                    
                    var addday = 2;
                    if(day == 4 || day == 5){
                        addday = addday + 2; 
                    }
                    
                    var date4 = addDays(addday); 
                    component.find("BidExpirationDate").set("v.value",date4);
                    component.set("v.syndication.Buyer_Counter_Price__c",result.TAmount);
                } 
                var nextAction = component.get("c.secondarymarketingflag"); 
                debugger;
                nextAction.setCallback(this, function(actionResult) {
                    var results = actionResult.getReturnValue();
                    if(state === "SUCCESS"){
                        debugger;
                        component.set("v.issecondaryMarket",results)
                    }
                });
                $A.enqueueAction(nextAction);
                
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.offer", emptyTask);
            }
            var menu = document.getElementById('Quotes');
            var classArr = menu.className.split(" ");
            if (classArr.indexOf("current") == -1) {
                menu.className += " " + "current";
            }
        });
        $A.enqueueAction(action);
    },      
    
    registeruser: function(component, event) { 
        component.set("v.Customer", true);
        component.set("v.benefits",true);
        component.set("v.clrbenefits",'active');
        component.set("v.userinfo",false);
        component.set("v.setpwd",false);
        component.set("v.finish",false);
    },
    bennext: function(component, event) { 
        
        
        var action = component.get('c.GetLoggedUserInfo');  
        action.setParams({
            "LoggedUserID" : localStorage.getItem('UserSession')
        });
        debugger;
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.UM",result);
                component.set("v.benefits",false);
                component.set("v.userinfo",true);
                component.set("v.clrbenefits",'completed');
                component.set("v.clrud",'active');
            }
        });
        debugger;
        $A.enqueueAction(action);
        
    },
    backtoben: function(component, event) { 
        component.set("v.benefits",true);
        component.set("v.userinfo",false);
        component.set("v.clrbenefits",'active');
        component.set("v.clrud",'');
    },
    nexttosetpwd: function(component, event) { 
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        
        //var FirstName = component.find("FirstName").get("v.value");
        //var sitename = component.find("sitename").get("v.value");
       // var email = component.find("email").get("v.value");
        //var lastname = component.find("lastname").get("v.value");
        //var swiftcode = component.find("swiftcode").get("v.value");
        //var location = component.find("location").get("v.value");
        var phone = component.find("phone").get("v.value");   
        var Mobile = component.find("Mobile").get("v.value");  
        var countrycode = component.find("auracountrycode").get("v.value");
        var mobilecountrycode = component.find("auramobilecountrycode").get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        
        
        /*if(FirstName == '' || FirstName == 'undefined' || FirstName == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.FirstNameError", 'Input field required'); 
        } else { 
            component.set("v.FirstNameError", ''); 
        } 
        
        if(sitename == '' || sitename == 'undefined' || sitename == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LegalNameError", 'Input field required'); 
        } else { 
            component.set("v.LegalNameError", ''); 
        } 
        
        if(lastname == '' || lastname == 'undefined' || lastname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LastNameError", 'Input field required'); 
        } else { 
            component.set("v.LastNameError", ''); 
        } 
        if(swiftcode == '' || swiftcode == 'undefined' || swiftcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.SwiftCodeError", 'Input field required'); 
        } else { 
            component.set("v.SwiftCodeError", ''); 
        } */
        if(!$A.util.isEmpty(phone))
        {   
            if(countrycode == '' || countrycode =='--Select--' || countrycode == 'undefined' || countrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.countrycodeError", 'Input field required'); 
            } else { 
                component.set("v.countrycodeError", ''); 
            } 
            if(!phone.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please enter valid phone number'); 
            } 
            else if(phone.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.PhoneError", ''); 
                }
        }
        if(!$A.util.isEmpty(Mobile))
        {   
            if(mobilecountrycode == '' || mobilecountrycode =='--Select--' || mobilecountrycode == 'undefined' || mobilecountrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.mobilecountrycodeError", 'Input field required'); 
            } else { 
                component.set("v.mobilecountrycodeError", ''); 
            }
            
            if(!Mobile.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please enter valid Mobile number'); 
            } 
            else if(Mobile.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.MobileError", ''); 
                }
        }
        if(errorFlag == 'false') {
            
            
            
            var action = component.get('c.UpdateRegForm');  
            action.setParams({
                "LoggedUserID" : localStorage.getItem('UserSession'),            
                "UM" : component.get("v.UM")
            });
            debugger;
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.newcounterPopup", false);
                    component.set("v.onboard", true);
                    
                    component.set("v.userinfo",false);
                    component.set("v.setpwd",true);
                    component.set("v.clrbenefits",'completed');
                    component.set("v.clrud",'completed');
                    component.set("v.clrspwd",'active');
                    
                }
            });
            debugger;
            $A.enqueueAction(action);
        }
    },
    backtouser: function(component, event) { 
        debugger;
        component.set("v.userinfo",true);
        component.set("v.setpwd",false);
        component.set("v.clrbenefits",'completed');
        component.set("v.clrud",'active');
        component.set("v.clrspwd",'');
    },
    nexttofinish: function(component, event) { 
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var newPassword = component.find("newPassword").get("v.value");
        var confirmPassword = component.find("confirmPassword").get("v.value"); 
        var re = new RegExp("^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$");
        
        if(newPassword == undefined || newPassword == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.newpassworderror",'Input field required');
        }else{
            component.set("v.newpassworderror",'');
        }
        if(confirmPassword == undefined || confirmPassword == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.confirmpassworderror",'Input field required');
        }
        else if(newPassword != confirmPassword){  
            errorFlag = 'true';
            component.set("v.confirmpassworderror",'');
            component.set("v.changepasswordvalidation",true);
            component.set("v.validation", false);
            component.set("v.combinationvalidation",false);
        }
            else if (!re.test(confirmPassword)) {
                errorFlag = 'true';
                component.set("v.combinationvalidation",true);
                component.set("v.changepasswordvalidation",false);
                component.set("v.validation",false);
            }
        
        
        if(errorFlag == 'false') {
            var action = component.get('c.UpdateLBFROMMain');
            var self = this; 
            action.setParams({
                "ID" : localStorage.getItem("UID"),               
                "newPassword" : newPassword
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                debugger;
                if(state === "SUCCESS"){       
                    component.set("v.setpwd",false);
                    component.set("v.finish",true);
                    component.set("v.clrbenefits",'completed');
                    component.set("v.clrud",'completed');
                    component.set("v.clrspwd",'completed');
                    component.set("v.clrfinish",'active');
                }
            }); 
            $A.enqueueAction(action);    
        } 
    },
    finishben: function(component, event) { 
        debugger;
        component.set("v.clrbenefits",'');
        component.set("v.clrud",'');
        component.set("v.clrspwd",'');
        component.set("v.clrfinish",'');
        component.set("v.Customer",false);
        $A.get('e.force:refreshView').fire();
    },
    closeregistration: function(component, event) { 
      component.set("v.clrbenefits",'');
        component.set("v.clrud",'');
        component.set("v.clrspwd",'');
        component.set("v.clrfinish",'');
        component.set("v.Customer",false);
    },
   
    SubmitBid : function(component, event) { 
        debugger;
        var errorFlag = 'false';
        var buyerCounterPrice11 = component.get("v.stringamount");//component.find("buyerCounterPrice1").get("v.value");
        var buyerServicePrice12 = component.get("v.stringbidfee");//component.find("buyerServicePrice1").get("v.value");
        var Tamt = component.get("v.TransAMt");        
        var peramt = (Tamt/100)*10;
        var Disamt=Tamt-peramt;
        /*alert('#$#$'+peramt);
        alert('#$#$'+buyerCounterPrice11);
        alert('#$#$'+Disamt);
        alert(buyerCounterPrice11 > Tamt);
        alert(buyerCounterPrice11 < Disamt);*/
        if(buyerCounterPrice11 == 0)
        {
            errorFlag = 'true';
            component.set("v.Bid_Port_Price", 'Enter a valid number');
        }
        else if(buyerCounterPrice11 > Tamt || buyerCounterPrice11 < Disamt)
        {
            errorFlag = 'true';
            component.set("v.Bid_Port_Price", 'The quote amount should be greater than 90% OR equal to the transaction amount.');
        }
            else if(isNaN(buyerCounterPrice11) || buyerCounterPrice11 <= 0)
            {
                errorFlag = 'true';
                component.set("v.Bid_Port_Price", 'Enter a valid number');
            } else {
                component.set("v.Bid_Port_Price", '');
            }
        if(buyerServicePrice12 == 0)
        {
            errorFlag = 'true';
            component.set("v.Bid_Ser_Fee", 'Enter a valid number');
        }
        else if(isNaN(buyerServicePrice12) || buyerServicePrice12 <= 0)
        {
            errorFlag = 'true';
            component.set("v.Bid_Ser_Fee", 'Enter a valid number');
        } else {
            component.set("v.Bid_Ser_Fee", '');  
        }        
        function addDays(days){
            var currentDate = new Date();
            var date = currentDate.setDate(currentDate.getDate() + days); 
            var date = (((currentDate.getMonth() + 1) < 10 ) ? '0' + (currentDate.getMonth() + 1) : (currentDate.getMonth() + 1)) + "/" + ((currentDate.getDate() < 10) ?  '0' + currentDate.getDate() : currentDate.getDate()) + "/" + currentDate.getFullYear();
            return date; 
        } 
        
        var dateFormat = $A.get("$Locale.dateFormat");
        //alert('dateFormat-'+dateFormat);
        var date1 =component.find("BidExpirationDate").get("v.value"); 
        // alert('date1-'+date1);
        //var dateString = $A.localizationService.formatDateTime(date1, dateFormat);
        // alert('dateString-'+dateString);
        var selectDate = new Date(date1);
        //alert('selectDate-'+selectDate);
        var date2 = addDays(0);
        var date3 = addDays(30);
        
        var add2 = $A.localizationService.formatDateTime(new Date(), dateFormat);
        var add3 = $A.localizationService.formatDateTime(date3, dateFormat);
        var selectPassDate = new Date(date2);
        var select30Date = new Date(date3); 
        var day1 = selectDate.getDay();
        //alert(day1);
        debugger;
        if (component.find("BidExpirationDate").get("v.value") == "") { 
            component.set("v.BidExpirationDateError", true);
            component.set("v.ExpirationDate", 'Please select the Bid Expiration Date.');
            errorFlag = 'true';
        } 
        else if (selectDate < selectPassDate) {
            component.find("BidExpirationDate").set("v.value", "");
            component.set("v.BidExpirationDateError", true);
            component.set("v.ExpirationDate", 'Bid Expiration Date must be in the future.');
            errorFlag = 'true';
        } 
            else if (selectDate >= select30Date) {
                component.find("BidExpirationDate").set("v.value", "");
                component.set("v.BidExpirationDateError", true);
                component.set("v.ExpirationDate", 'Selected date should be less than 30 days.');
                errorFlag = 'true';
            }
                else if (day1 == 0 || day1 == 6) { 
                    component.find("BidExpirationDate").set("v.value", "");
                    component.set("v.BidExpirationDateError", true);
                    component.set("v.ExpirationDate", 'Choose any weekdays.');
                    errorFlag = 'true';
                }    
        //component.set("v.SubmitBid", true); 
        if(buyerCounterPrice11 != "" && buyerServicePrice12 != "" && errorFlag == 'false'){
            component.set("v.SubmitBid", true); 
        } 
    },      
    BidExpirationDateErrorcancel: function(component, event, helper){
        component.set("v.BidExpirationDateError", false);
    },
    submitBidCheck: function(component, event, helper) {
        if(component.find("chkSubmitBid").get('v.value')==true){
            component.find("submitBidbtn1").set("v.disabled", false);
        }
        else{
            component.find("submitBidbtn1").set("v.disabled", true);
        }
    },
    btnsubmitBid1: function(component, event, helper) {
        debugger;
        component.set("v.SubmitBid", false);
        var syndication = {
            'sobjectType': 'Quotes__c',
            'Bid_Amount__c': component.get("v.stringamount"),
            'Bid_Expiration_Date__c': component.get("v.stringExpirationDate"),
            'Bid_Service_Fee_bps__c': component.get("v.stringbidfee"),
            'Note__c':component.get("v.stringNotes")}; //
        var offerId = component.get("v.transactionId");
        var username = localStorage.getItem('UserSession');
        var action = component.get('c.SubmitBidTransaction');
        action.setParams({
            "syndication" : syndication,
            "offerId" : offerId ,
            "userid" : username,
            "body" : component.find("body2").get("v.value")           
        });
        action.setCallback(this, function(actionResult) { 
            debugger;
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();            
            if(state === "SUCCESS"){
                if(localStorage.getItem("FromRaidCHK") && localStorage.getItem("FromRaidUserPWD") =='undefined')
                {
                    component.set("v.FromRaidCHK",true);
                }else{
                    component.set("v.FromRaidCHK",false);
                }
                component.set("v.offer",result);
                component.set("v.createBid",false);
                component.set("v.syndId", result.syndId); 
                
            }
        });
        $A.enqueueAction(action);
        debugger;
    },
    
    btnsubmitBid: function(component, event, helper) {
        debugger;
        component.set("v.SubmitBid", false);
        var action = component.get('c.SaveBid');
        var offer = component.get("v.offer"); 
        var syndication = offer.Syndication;
        debugger;
        syndication = component.get("v.syndication"); 
        var self = this; 
        action.setParams({
            "syndication" : syndication,
            "body" :  component.find("body2").get("v.value"),
            "CUID" : component.get("v.LoggeduserCunion"),
            "Pname" : component.get("v.Pname"),
            "cpname" : component.get("v.cpname") 
        });
        action.setCallback(this, function(actionResult) { 
            debugger;
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            
            if(state === "SUCCESS"){
                component.set("v.offer", result);
                component.set("v.offerId", result.offerId);   
                component.set("v.syndId", result.syndId); 
                component.set("v.parentId", result.syndId);  
                component.set("v.CUId", result.OfferCreditUnion.Id);
                component.set("v.offerId1", result.offerId);
                var syndication = {
                    'sobjectType': 'Syndications__c',
                    'Id': result.syndId,
                    'Name': result.Syndication.Name,
                    'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                    'Bid_Status__c': result.Syndication.Bid_Status__c,
                    'Buyer_Counter_Price__c': result.Syndication.Buyer_Counter_Price__c,
                    'Buyer_Service_Percentage__c': result.Syndication.Buyer_Service_Percentage__c,
                    'Note__c': result.Syndication.Note__c,
                    'Offer__c': result.Syndication.Offer__c,
                    'Participation__c': result.Syndication.Participation__c,
                    'Portfolio_Price__c': result.Syndication.Portfolio_Price__c,
                    'Seller_Service_Percentage__c': result.Syndication.Seller_Service_Percentage__c,
                    'submitted_Date__c': result.Syndication.submitted_Date__c,
                    'Buyer_Name__c': result.Syndication.Buyer_Name__c,
                    'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                };
                component.set("v.syndication", syndication);
                component.set("v.syndId", result.syndId); 
                
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    submitBidcancel: function(component, event) {
        component.set("v.SubmitBid", false);
    },
    BidDue : function(component, event) {  
        component.set("v.BidDue", true); 
    },
    BidDueCheck : function(component, event, helper) {
        if(component.find("chkBidDue").get('v.value') == true) {
            component.find("BidDuebtn1").set("v.disabled", false);
        }
        else{
            component.find("BidDuebtn1").set("v.disabled", true);
        }
    },
    btnBidDue: function(component, event, helper) {
        var action = component.get('c.BidDue1'); 
        action.setParams({
            "syndId" : component.get("v.syndId"),
            "body3" : component.find("body3").get("v.value")
        });
        action.setCallback(self, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.BidDue", false);
            component.set("v.SecondBidDue", true);
            if(state === "SUCCESS"){
                component.set("v.offer", result);
                component.set("v.offerId", result.offerId);   
                component.set("v.syndId", result.syndId); 
                component.set("v.parentId", result.syndId);
                var syndication = {
                    'sobjectType': 'Syndications__c',
                    'Id': result.syndId,
                    'Name': result.Syndication.Name,
                    'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                    'Bid_Status__c':result.Syndication.Bid_Status__c,
                    'Buyer_Counter_Price__c':result.Syndication.Buyer_Counter_Price__c,
                    'Buyer_Service_Percentage__c':result.Syndication.Buyer_Service_Percentage__c,
                    'Note__c':result.Syndication.Note__c,
                    'Offer__c':result.Syndication.Offer__c,
                    'Participation__c':result.Syndication.Participation__c,
                    'Portfolio_Price__c':result.Syndication.Portfolio_Price__c,
                    'Seller_Service_Percentage__c':result.Syndication.Seller_Service_Percentage__c,
                    'submitted_Date__c':result.Syndication.submitted_Date__c,
                    'Buyer_Name__c':result.Syndication.Buyer_Name__c,
                    'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                };
                component.set("v.syndication", syndication);  
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    BidDuecancel : function(component, event) {
        component.set("v.BidDue", false);
    },
    SecondBidDuecancel : function(component, event) {
        component.set("v.SecondBidDue", false);
    },
    AgreeWithdraw : function(component, event) { 
        component.set("v.AgreeWithdraw", true);
    },
    AgreeWithdrawCheck : function(component, event, helper) {
        if(component.find("chkAgreeWithdraw").get('v.value') == true) {
            component.find("AgreeWithdrawbtn1").set("v.disabled", false);
        }
        else{
            component.find("AgreeWithdrawbtn1").set("v.disabled", true);
        } 
    },
    btnAgreeWithdraw: function(component, event, helper) {
        var action = component.get('c.postWithdrawn'); 
        var self = this; 
        var username = localStorage.getItem('UserSession');
        action.setParams({
            "syndId" : component.get("v.syndId"),
            "Title" : localStorage.getItem("LoggeduserName"),
            "body2" : component.find("body2").get("v.value"),
            "userid" : username
        });
        action.setCallback(self, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.AgreeWithdraw", false);
            component.set("v.SecondPreWithdraw", true);
            if(state === "SUCCESS"){
                component.set("v.offer", result);
                component.set("v.offerId", result.offerId);   
                component.set("v.syndId", result.syndId); 
                component.set("v.parentId", result.syndId);
                var syndication = {
                    'sobjectType': 'Syndications__c',
                    'Id': result.syndId,
                    'Name': result.Syndication.Name,
                    'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                    'Bid_Status__c':result.Syndication.Bid_Status__c,
                    'Buyer_Counter_Price__c':result.Syndication.Buyer_Counter_Price__c,
                    'Buyer_Service_Percentage__c':result.Syndication.Buyer_Service_Percentage__c,
                    'Note__c':result.Syndication.Note__c,
                    'Offer__c':result.Syndication.Offer__c,
                    'Participation__c':result.Syndication.Participation__c,
                    'Portfolio_Price__c':result.Syndication.Portfolio_Price__c,
                    'Seller_Service_Percentage__c':result.Syndication.Seller_Service_Percentage__c,
                    'submitted_Date__c':result.Syndication.submitted_Date__c,
                    'Buyer_Name__c':result.Syndication.Buyer_Name__c,
                    'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                };
                component.set("v.syndication", syndication);  
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    AgreeWithdrawcancel : function(component, event) {
        component.set("v.AgreeWithdraw", false);
    },
    SecondPreWithdrawcancel: function(component, event) {
        component.set("v.SecondPreWithdraw", false);
    }, 
    BidWithdrawn : function(component, event) { 
        // alert(event.target.id);
        component.set("v.syndId", event.target.id);
        component.set("v.BidWithdrawn", true);       
    },     
    BidWidrawnCheck: function(component, event, helper) {
        if(component.find("chkBidWidrawn").get('v.value') == true){
            component.find("BidWidrawnbtn1").set("v.disabled", false);
        }
        else{
            component.find("BidWidrawnbtn1").set("v.disabled", true);
        }
    }, 
    btnBidWidrawn: function(component, event, helper) {
        debugger;
        var action = component.get('c.Withdrawcall'); 
        var synid = component.get("v.syndId");
        //var body = component.find("body2").get("v.value");
        var username = localStorage.getItem('UserSession');
        var self = this;
        action.setParams({
            "syndId" : synid,
            "Title" : localStorage.getItem("LoggeduserName"),
            "body1" : 't',
            "userid" : username
        });
        action.setCallback(self, function(actionResult) { 
            debugger;
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.BidWithdrawn", false);
            component.set("v.SecondBidWithdrawn", true);
            if(state === "SUCCESS"){
                component.set("v.offer", result);
                component.set("v.offerId", result.offerId);   
                component.set("v.syndId", result.syndId); 
                component.set("v.parentId", result.syndId);
                var syndication = {
                    'sobjectType': 'Syndications__c',
                    'Id': result.syndId,
                    'Name': result.Syndication.Name,
                    'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                    'Bid_Status__c':result.Syndication.Bid_Status__c,
                    'Buyer_Counter_Price__c':result.Syndication.Buyer_Counter_Price__c,
                    'Buyer_Service_Percentage__c':result.Syndication.Buyer_Service_Percentage__c,
                    'Note__c':result.Syndication.Note__c,
                    'Offer__c':result.Syndication.Offer__c,
                    'Participation__c':result.Syndication.Participation__c,
                    'Portfolio_Price__c':result.Syndication.Portfolio_Price__c,
                    'Seller_Service_Percentage__c':result.Syndication.Seller_Service_Percentage__c,
                    'submitted_Date__c':result.Syndication.submitted_Date__c,
                    'Buyer_Name__c':result.Syndication.Buyer_Name__c,
                    'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                };
                component.set("v.syndication", syndication);  
            }
            
        });
        $A.enqueueAction(action);
    }, 
    BidWidrawncancel: function(component, event) {
        component.set("v.BidWithdrawn", false);
    },
    SecondBidWithdrawncancel: function(component, event) {
        component.set("v.SecondBidWithdrawn", false);
    },
    popNotes: function(component, event) {
        component.set("v.Notes", true);
    },
    NotesCancel: function(component, event) {
        component.set("v.Notes", false);
        component.set("v.saveNote", ''); 
    },
    NotesSave: function(component, event, helper) {
        debugger;
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || savNote == null || savNote == undefined || savNote.trim().length*1 == 0 ) {            
            component.set("v.saveNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        }
        else{
            component.set("v.saveNote", ''); 
            var username = localStorage.getItem('UserSession');
            var action = component.get('c.saveNotes');
            var self = this; 
            action.setParams({
                "syndId" : component.get("v.syndId"),
                "Title" : localStorage.getItem("LoggeduserName"),
                "body" : component.find("body").get("v.value"),               
                "userid" : username,
                "Quotestatus" : component.get("v.closedbid")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.Notes", false);
                if(state === "SUCCESS"){                   
                    component.set("v.offer.Internalmemos", result.Internalmemos); 
                    /* component.set("v.syndId", result.syndId);
                    component.set("v.offer", result);
                    component.set("v.offerId", result.offerId);
                    component.set("v.parentId", result.syndId);
                    var syndication = {
                        'sobjectType': 'Syndications__c',
                        'Id': result.syndId,
                        'Name': result.Syndication.Name,
                        'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                        'Bid_Status__c':result.Syndication.Bid_Status__c,
                        'Buyer_Counter_Price__c':result.Syndication.Buyer_Counter_Price__c,
                        'Buyer_Service_Percentage__c':result.Syndication.Buyer_Service_Percentage__c,
                        'Note__c':result.Syndication.Note__c,
                        'Offer__c':result.Syndication.Offer__c,
                        'Participation__c':result.Syndication.Participation__c,
                        'Portfolio_Price__c':result.Syndication.Portfolio_Price__c,
                        'Seller_Service_Percentage__c':result.Syndication.Seller_Service_Percentage__c,
                        'submitted_Date__c':result.Syndication.submitted_Date__c,
                        'Buyer_Name__c':result.Syndication.Buyer_Name__c,
                        'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                    };
                    component.set("v.syndication", syndication); */
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    }, 
    delete1 : function(component, event, helper) {
        debugger;
        var action = component.get('c.DeleteFile');
        var username = localStorage.getItem('UserSession');
        action.setParams({
            "attId" : event.target.id ,
            "" : username
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.offer", result);
                component.set('v.DeleteFile', true);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.offer", emptyTask);
            }
        });
        $A.enqueueAction(action); 
    },
    delAttach : function(component, event, helper) {
        debugger;
        var action = component.get('c.DeleteAttach'); 
        var username = localStorage.getItem('UserSession');
        action.setParams({
            "attId" : event.target.id ,
            "userid" : username
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.offer", result);
                component.set('v.DeleteFile', true);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.offer", emptyTask);
            }
        });
        $A.enqueueAction(action); 
    },
    DeleteFilecancel: function(component, event, helper) {
        component.set('v.DeleteFile', false);
    },
    
    hoverCreditUnion: function(component, event) { 
        debugger;
        var action = component.get('c.CUDetails'); 
        action.setParams({
            "CUId" : component.get("v.CUId")
        });       
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.CUUser", result); 
                component.set("v.popoverCreditUnion", true);
            } 
        });
        $A.enqueueAction(action);
    },
    hoverCreditUnionhide: function(component, event) { 
        component.set("v.popoverCreditUnion", false); 
    },
    hoverOfferDetails: function(component, event) {  
        var action = component.get('c.OfferDetails'); 
        action.setParams({
            "offerId1" : component.get("v.offerId1")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){ 
                var popover = component.find("popoverOfferDetails"); 
                component.set("v.offr", result);
                $A.util.removeClass(popover,'slds-hide');
                $A.util.addClass(popover,'slds-show');        		 
                event.preventDefault();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    hoverofferhide : function(component, event) { 
        var popover = component.find("popoverOfferDetails"); 
        $A.util.addClass(popover,'slds-hide'); 
        $A.util.removeClass(popover,'slds-show');
        event.preventDefault();
    },
    
    buyerCount : function(component, event, helper) {
        var inp = component.find("buyerCounterPrice1").get("v.value"); 
        if(inp == 0)
            component.set("v.syndication.Buyer_Counter_Price__c","");
    },
    buyerSer : function(component, event, helper) {
        var inp = component.find("buyerServicePrice1").get("v.value"); 
        if(inp == 0)
            component.set("v.syndication.Buyer_Service_Percentage__c","");
    },
    changePortfolioPrice: function(component, event, helper) { 
        var portfolioPrice = component.find("buyerCounterPrice1").get("v.value"); 
        if(portfolioPrice == '')
            component.set("v.syndication.Buyer_Counter_Price__c","0"); 
    },
    changeServicingFee: function(component, event, helper) { 
        var principal = component.find("buyerServicePrice1").get("v.value"); 
        if(principal == '')
            component.set("v.syndication.Buyer_Service_Percentage__c","0"); 
    },
    AgreeBid: function(component, event) { 
        debugger;
        //component.set("v.syndId", event.target.id); 
        component.set("v.AgreetoTerms", true);
        
    }, 
    AgreeBidcheck: function(component, event, helper) {
        debugger;
        var ACDate = component.find("AntDate").get("v.value");
        if(component.find("chkAgreeBid").get('v.value') == true) { // && ACDate != "" && ACDate != undefined){
            component.find("AgreeBidbtn1").set("v.disabled", false);
        } else {
            component.find("AgreeBidbtn1").set("v.disabled", true);
        }
    }, 
    btnAgreeBid: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        if(errorFlag == 'false')
        { 
            var username = localStorage.getItem('UserSession');
            var action = component.get('c.AgreeBid1'); 
            action.setParams({
                "syndId" : component.get("v.syndId"),
                "Title" : localStorage.getItem("LoggeduserName"),
                "body4" : component.find("body4").get("v.value"),
                "userid" : username
                
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.AgreetoTerms", false);
                component.set("v.SecondAgree", true);
                if(state === "SUCCESS"){
                    component.set("v.offer", result);
                    component.set("v.offerId", result.offerId);   
                    component.set("v.syndId", result.syndId); 
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-clientdemo.git
                    component.set("v.parentId", result.syndId);
                    var syndication = {
                        'sobjectType': 'Syndications__c',
                        'Id': result.syndId,
                        'Name': result.Syndication.Name,
                        'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                        'Bid_Status__c':result.Syndication.Bid_Status__c,
                        'Buyer_Counter_Price__c':result.Syndication.Buyer_Counter_Price__c,
                        'Buyer_Service_Percentage__c':result.Syndication.Buyer_Service_Percentage__c,
                        'Note__c':result.Syndication.Note__c,
                        'Offer__c':result.Syndication.Offer__c,
                        'Participation__c':result.Syndication.Participation__c,
                        'Portfolio_Price__c':result.Syndication.Portfolio_Price__c,
                        'Seller_Service_Percentage__c':result.Syndication.Seller_Service_Percentage__c,
                        'submitted_Date__c':result.Syndication.submitted_Date__c,
                        'Buyer_Name__c':result.Syndication.Buyer_Name__c,
                        'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                    };
                    component.set("v.syndication", syndication);
                }
                else if(state === "ERROR"){
                    var emptyTask = component.get("v.offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.offer", emptyTask);
                }
                
            });
            $A.enqueueAction(action);
        }
    },
    AgreeBidcancel: function(component, event) {
        component.set("v.AgreetoTerms", false);
    },
    SecondAgreecancel: function(component, event) {
        component.set("v.SecondAgree", false);
    },
    BidReject: function(component, event) { 
        component.set("v.syndId", event.target.id); 
        component.set("v.BidReject", true);       
    },    
    BidRejectcheck: function(component, event, helper) {
        if(component.find("chkBidReject").get('v.value') == true){
            component.find("BidRejectbtn1").set("v.disabled", false);
        }
        else{
            component.find("BidRejectbtn1").set("v.disabled", true);
        }
    },
    btnBidReject: function(component, event, helper) {
        var username = localStorage.getItem('UserSession');
        var action = component.get('c.BidReject1'); 
        var self = this; 
        action.setParams({
            "syndId" : component.get("v.syndId"),
            "Title" : localStorage.getItem("LoggeduserName"),
            "body3" : component.find("body3").get("v.value"),
            "userid" : username
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.BidReject", false);
            component.set("v.SecondBidReject", true);
            if(state === "SUCCESS"){
                component.set("v.offer", result);
                component.set("v.offerId", result.offerId);   
                component.set("v.syndId", result.syndId); 
                component.set("v.parentId", result.syndId);
                var syndication = {
                    'sobjectType': 'Syndications__c',
                    'Id': result.syndId,
                    'Name': result.Syndication.Name,
                    'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                    'Bid_Status__c':result.Syndication.Bid_Status__c,
                    'Buyer_Counter_Price__c':result.Syndication.Buyer_Counter_Price__c,
                    'Buyer_Service_Percentage__c':result.Syndication.Buyer_Service_Percentage__c,
                    'Note__c':result.Syndication.Note__c,
                    'Offer__c':result.Syndication.Offer__c,
                    'Participation__c':result.Syndication.Participation__c,
                    'Portfolio_Price__c':result.Syndication.Portfolio_Price__c,
                    'Seller_Service_Percentage__c':result.Syndication.Seller_Service_Percentage__c,
                    'submitted_Date__c':result.Syndication.submitted_Date__c,
                    'Buyer_Name__c':result.Syndication.Buyer_Name__c,
                    'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                };
                component.set("v.syndication", syndication);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    BidRejectcancel: function(component, event) {
        component.set("v.BidReject", false);
    },
    SecondBidRejectcancel: function(component, event) {
        component.set("v.SecondBidReject", false);
        component.set("v.updateAnticipation", false);
    },
    refreshModel: function (component, event, helpler) {
        debugger;
        var img = component.find("imgloading");
        $A.util.addClass(img,'slds-hide'); 
        $A.util.removeClass(img,'slds-show');
        component.set("v.opendocvault", false); 
        component.set("v.FileUploadMsg", 'File(s) have been uploaded successfully');
        component.set("v.FileUploadMsgSorE", 'Success');
        component.set("v.FileUpload", true);
        
        var action = component.get('c.getContentslist');
        
        action.setParams({
            "parentId" : component.get("v.syndId") 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.offer.ContentVer", result.ContentVer); 
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    }, 
    ChooseFile : function(component, event) {  
        debugger;
        component.set("v.FileUploadMsg", 'Please choose the file(s)');
        component.set("v.FileUploadMsgSorE", 'Error');
        component.set("v.FileUpload", true); 
    },
    ChooseFailure : function(component, event) {        
        component.set("v.FileUploadMsg", 'An error has occurred. Please contact us at\ntradexbeta@tradex.com.');
        component.set("v.FileUploadMsgSorE", 'Error');        
        component.set("v.FileUpload", true); 
    },
    fileExtension : function(component, event) {        
        component.set("v.FileUploadMsg", 'This file format not allowed to upload');
        component.set("v.FileUploadMsgSorE", 'Error');        
        component.set("v.FileUpload", true); 
    },
    LoaderDisplay : function(component, event) {
        var img = component.find("imgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    }, 
    FileUploadcancel : function(component, event) {
        var img = component.find("imgloading");
        $A.util.addClass(img,'slds-hide'); 
        $A.util.removeClass(img,'slds-show'); 
        component.set("v.FileUpload", false); 
    },
    updateAnticipate: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        var dateFormat = $A.get("$Locale.dateFormat");
        
        var ACDate = component.find("AnticipatedDate").get("v.value");
        
        var dateString = $A.localizationService.formatDateTime(ACDate, dateFormat);
        var date = new Date(dateString);
        
        dateString = $A.localizationService.formatDateTime(new Date(), dateFormat);
        var today = new Date(dateString); 
        
        var day1 = date.getDay();
        
        if(ACDate == undefined || ACDate == "")
        {
            errorFlag = 'true';
            component.set("v.acDate", 'Input field required');
        }
        else
        {
            if(helper.checkDate(ACDate))
            {
                errorFlag = 'true';
                component.set("v.acDate", 'Please enter valid date'); 
            } 
            else if(today > date)
            {
                errorFlag = 'true';
                component.set("v.acDate", 'Please enter a date in the future');
            }
                else if(day1 == 0 || day1 == 6)
                {
                    errorFlag = 'true';
                    component.set("v.acDate", 'Choose any weekdays'); 
                }
                    else
                    { 
                        component.set("v.acDate", ''); 
                    } 
        }
        
        if(errorFlag == 'false')
        { 
            var action = component.get('c.UpdateAnticipate'); 
            action.setParams({
                "syndId" : component.get("v.syndId"),
                "ACDate": component.find("AnticipatedDate").get("v.value")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                //component.set("v.BidReject", false);
                component.set("v.updateAnticipation", true);
                if(state === "SUCCESS"){   
                    component.set("v.syndId", result.syndId); 
                    var syndication = {
                        'sobjectType': 'Syndications__c',
                        'Id': result.syndId,
                        'Name': result.Syndication.Name,
                        'Bid_Expiration_Date__c': result.Syndication.Bid_Expiration_Date__c,
                        'Bid_Status__c':result.Syndication.Bid_Status__c,
                        'Buyer_Counter_Price__c':result.Syndication.Buyer_Counter_Price__c,
                        'Buyer_Service_Percentage__c':result.Syndication.Buyer_Service_Percentage__c,
                        'Note__c':result.Syndication.Note__c,
                        'Offer__c':result.Syndication.Offer__c,
                        'Participation__c':result.Syndication.Participation__c,
                        'Portfolio_Price__c':result.Syndication.Portfolio_Price__c,
                        'Seller_Service_Percentage__c':result.Syndication.Seller_Service_Percentage__c,
                        'submitted_Date__c':result.Syndication.submitted_Date__c,
                        'Buyer_Name__c':result.Syndication.Buyer_Name__c,
                        'Anticipated_close_date__c':result.Syndication.Anticipated_close_date__c
                    };
                    component.set("v.syndication", syndication);
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    btnCmpTrans: function(component, event, helper) {
        debugger;
        var img = component.find("pdfImgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');
        // component.find(event.target.id).set("v.disabled", true);
        var action = component.get('c.createFinalized'); 
        action.setParams({
            "offerId" : component.get("v.TransId"),
            "syndId" : component.get("v.syndId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Transaction Completed Sucessfully',
                        'type': 'Success'
                    }
                );
                showToast.fire();
                $A.get('e.force:refreshView').fire();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        
        $A.enqueueAction(action);
    },
    uploadfilepopup: function(component, event) {     
        debugger;
        component.set("v.opendocvault", true); 
    },
    btncancelfor: function(component, event) {     
        debugger;
        component.set("v.opendocvault", false); 
    },
    cancelRegister : function(component, event) {     
        debugger;
        
        /* var action = component.get('c.LaterInviteEmail');
        var self = this; 
        action.setParams({
            "ID" : localStorage.getItem("UID")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            debugger;
            if(state === "SUCCESS"){                    
                component.set("v.Customer", false); 
            }
        }); 
        $A.enqueueAction(action);   */
        component.set("v.Customer", false); 
    },
    
    afterregister : function(component, event, helper) {
        debugger;
        component.set("v.Refercolleaguepop", true);
        //window.location.replace("https://tradeaix99-cmfg.cs95.force.com/demo/secur/logout.jsp?retUrl=https%3A%2F%2Ftradeaix99-cmfg.cs95.force.com%2Fdemo%2Fs%2Fadmin");
        
    },
    cancelRefercolleague: function(component, event, helper) {
        debugger;
        component.set("v.customerregisterconfirmation", true);
        //window.location.replace("https://tradeaix99-cmfg.cs95.force.com/demo/secur/logout.jsp?retUrl=https%3A%2F%2Ftradeaix99-cmfg.cs95.force.com%2Fdemo%2Fs%2Fadmin");
    },
    Refercolleague: function(component, event, helper) {
        debugger;
        var TId = component.get("v.transactionId");
        window.location = '/demo/s/counterpartynew?transactionId='+TId+'&flag=IC';
    },
    closeuserregister: function(component, event, helper) {
        debugger;        
        window.location.replace("https://tradeaix99-cmfg.cs95.force.com/demo/secur/logout.jsp?retUrl=https%3A%2F%2Ftradeaix99-cmfg.cs95.force.com%2Fdemo%2Fs%2Fadmin");
    },
    
})
({
	userload : function(component, event, helper) {
        debugger;
		var Action = component.get("c.getUserList");         
        var sessionid = localStorage.getItem("UserSession");
        Action.setParams({
            "username" : sessionid                
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Result",results);
            }
        });
        $A.enqueueAction(Action); 
	}
})
({
        getBanks: function(component, event, helper) { 
            var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
            document.title =sitename +' : Party Banks';
            if(localStorage.getItem('LoggeduserProfile') == $A.get("{!$Label.c.ACP_ProfileID}"))
{
	component.set("v.Tenantcheck",true);
var Action1 = component.get("c.loadorgs");   
Action1.setParams({
	"username" : localStorage.getItem('UserSession')                
});        
Action1.setCallback(this, function(actionResult1) {
	debugger;
	var state = actionResult1.getState(); 
	var results = actionResult1.getReturnValue();
	if(state === "SUCCESS"){
		component.set("v.OrgFilter",results);
	}
	
}); 
$A.enqueueAction(Action1);
}
else{
	component.set("v.Tenantcheck",false);                        
}
            var action = component.get('c.getBankList');
            var self = this; 
            component.set("v.Tenantid",localStorage.getItem('LoggeduserTenantID'));
            action.setParams({
                "counter" : (component.get("v.counter")).toString(),
                "sortbyField" : component.get("v.sortbyField"),
                "sortDirection" : component.get("v.sortDirection"),
                "selected" : component.get("v.selectedItem"),
                "Filtervalue" : component.get("v.selectedFilterItem")
            }); 
            action.setCallback(this, function(actionResult) {  
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){  
                    helper.setGridOption(component, actionResult.getReturnValue());
                }
            }); 
            $A.enqueueAction(action); 
        },
        editBank: function(component, event, helper) {   
            component.set("v.showEdit", true);     
        }, inactiveCheck: function(component, event, helper) {
            if(component.find("chkInactive").get('v.value')==true)
            {
                debugger;
                component.find("btnEditBank").set("v.disabled", "false");
            }
            else{
                debugger;
                component.find("btnEditBank").set("v.disabled", "true");
            }
        },
        showcancel: function(component, event, helper) {
            component.set("v.showEdit", false);
        },
        View: function(component, event, helper) {
            debugger;
            var action = component.get('c.Views');         
            var selected = event.getSource().get("v.text");        
            component.set("v.selectedItem", selected)
            var self = this; 
            action.setParams({
                "counter" : (component.get("v.counter")).toString(),
                "sortbyField" : component.get("v.sortbyField"),
                "sortDirection" : component.get("v.sortDirection"),
                "selected" : component.get("v.selectedItem"),
                "Filtervalue" : component.get("v.selectedFilterItem")
            });
            action.setCallback(this, function(actionResult) {  
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){  
                    helper.setGridOption(component, actionResult.getReturnValue());
                }
            }); 
            $A.enqueueAction(action); 
        },  
    onSingleSelectChange: function(component, event, helper) {
        debugger;
        var selectCmp = component.find("InputSelectSingle").get("v.value");
        //alert(selectCmp);
        	component.set("v.selectedFilterItem",selectCmp);
            var action = component.get('c.Filter');         
            var selected = event.getSource().get("v.text");        
            component.set("v.selectedItem", selected)
            var self = this; 
            action.setParams({
                "counter" : (component.get("v.counter")).toString(),
                "sortbyField" : component.get("v.sortbyField"),
                "sortDirection" : component.get("v.sortDirection"),
                "selected" : component.get("v.selectedItem"),
                "Filtervalue" : selectCmp                
            });
            action.setCallback(this, function(actionResult) {  
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){  
                    helper.setGridOption(component, actionResult.getReturnValue());
                }
            }); 
            $A.enqueueAction(action); 
        },   
        Beginning: function(component, event, helper) {
            var action = component.get('c.getBeginning'); 
            action.setParams({
                "counter" : '0',
                "sortbyField" : component.get("v.sortbyField"),
                "sortDirection" : component.get("v.sortDirection"),
                "selected" : component.get("v.selectedItem"),
                "Filtervalue" : component.get("v.selectedFilterItem")
            });
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){  
                    helper.setGridOption(component, actionResult.getReturnValue());
                }
            });
            $A.enqueueAction(action);
        },
        Previous: function(component, event, helper) {
            debugger;        
            var action = component.get('c.getPrevious'); 
            var list_size = component.get('v.list_size');        
            var counter = component.get('v.counter');
            var res = counter - list_size;  
            if(res < 0) { res = 0; }
            action.setParams({ 
                "selected" : component.get("v.selectedItem"), 
                "counter" : res.toString(),
                "sortbyField" : component.get("v.sortbyField"),
                "sortDirection" : component.get("v.sortDirection"),
                "Filtervalue" : component.get("v.selectedFilterItem")
            });       
            action.setCallback(this, function(actionResult) {           
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){ 
                    helper.setGridOption(component, actionResult.getReturnValue());
                }
            });
            $A.enqueueAction(action);
        },
        editBankDetail: function(component, event, helper) {
            debugger;
            var errorFlag = 'false';
            component.set("v.validation", false); 
            //var bankd = component.get("v.bank");
            alert(event.target.id);
            if(errorFlag == 'false') {
                component.find("btnEditBank").set("v.disabled", true);                    
                var action1 = component.get("c.EditBank");
                action1.setParams({
                    "ccid" : event.target.id
                }); 
                action1.setCallback(this, function(response1) {
                    var state1 = response1.getState();
                    var bankId = response1.getReturnValue(); 
                    debugger;
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'CounterParty Bank has been activated.',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                     $A.get('e.force:refreshView').fire();
                    component.set("v.showEdit", false);
                });
                $A.enqueueAction(action1);
            }
        },
        Next: function(component, event, helper) {
            var action = component.get('c.getNext'); 
            var list_size = component.get('v.list_size');
            var counter = component.get('v.counter');
            var res = list_size + counter;
            action.setParams({ 
                "counter" : res.toString(),
                "sortbyField" : component.get("v.sortbyField"),
                "sortDirection" : component.get("v.sortDirection"),
                "selected" : component.get("v.selectedItem"),
                "Filtervalue" : component.get("v.selectedFilterItem")
            });
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){  
                    helper.setGridOption(component, actionResult.getReturnValue());
                }
            });
            $A.enqueueAction(action);
        },
        End: function(component, event, helper) {
            var action = component.get('c.getEnd');  
            var list_size = component.get('v.list_size');
            var counter = component.get('v.counter');
            var total_size = component.get('v.total_size');
            var res = total_size - (total_size % list_size); 
            if(res == total_size) { res -= list_size; }
            action.setParams({ 
                "counter" : res.toString(),
                "sortbyField" : component.get("v.sortbyField"),
                "sortDirection" : component.get("v.sortDirection"),
                "selected" : component.get("v.selectedItem"),
                "Filtervalue" : component.get("v.selectedFilterItem")
            });
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){  
                    helper.setGridOption(component, actionResult.getReturnValue());
                }
            });
            $A.enqueueAction(action);
        }, 
        sort: function(component, event, helper) {
            var action = component.get('c.SortTable'); 
            var sortfield = event.currentTarget.getAttribute("data-recId");  
            action.setParams({
                "counter" : (component.get("v.counter")).toString(),
                "sortbyField" : sortfield,
                "sortDirection" : component.get("v.sortDirection"),
                "selected" : component.get("v.selectedItem"),
                "Filtervalue" : component.get("v.selectedFilterItem")
            });
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){  
                    helper.setGridOption(component, actionResult.getReturnValue());
                }
            });
            $A.enqueueAction(action);
        }, 
        changePageSizes: function(component, event, helper) {
            var action = component.get('c.changelist_size');  
            var list_size = component.find("recordSize").get("v.value"); 
            action.setParams({
                "counter" : '0',
                "sortbyField" : component.get("v.sortbyField"),
                "sortDirection" : component.get("v.sortDirection"),
                "selected" : component.get("v.selectedItem"), 
                "list_size" : list_size,
                "Filtervalue" : component.get("v.selectedFilterItem")
            });
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){  
                    helper.setGridOption(component, actionResult.getReturnValue());
                }
            });
            $A.enqueueAction(action);
        }, 
        changePageOptions: function(component, event, helper) {
            var action = component.get('c.changePage'); 
            var showpage = component.find("pageOptions").get("v.value"); 
            var list_size = component.find("recordSize").get("v.value"); 
            var counter = (parseInt(showpage)-1)*list_size;
            action.setParams({
                "counter" : counter.toString(),
                "sortbyField" : component.get("v.sortbyField"),
                "sortDirection" : component.get("v.sortDirection"),
                "selected" : component.get("v.selectedItem"),
                "Filtervalue" : component.get("v.selectedFilterItem")
            });
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){  
                    helper.setGridOption(component, actionResult.getReturnValue());
                }
            });
            $A.enqueueAction(action);
        },     
        createBank: function(component, event, helper) { 
            debugger;
            component.set("v.showCreate", true);
            component.set("v.bank.IsActive__c", true);
            component.set("v.bank.IsParty__c", true);
        },
        cancel: function(component, event, helper) {
            component.set("v.showCreate", false);
            component.set("v.validation", false); 
        }, 
        saveBank: function(component, event, helper) {
            debugger;
            var errorFlag = 'false';
            component.set("v.validation", false); 
            var Bankname = component.find("txtName").get("v.value");
            
            if(Bankname == '' || Bankname == undefined) {
                errorFlag = 'true';
                component.set("v.validation", true); 
                component.set("v.NameError", 'Please enter a Bank Name.');
                
            }
            else { 
                component.set("v.NameError", ''); 
            }
            var bankd = component.get("v.bank");
            var action = component.get("c.CheckBankName");
            action.setParams({ "bankName": bankd.CU_Name__c });
            action.setCallback(this, function(response) {
                var state = response.getState();
                var bank = response.getReturnValue(); 
                if(bank != '' && bank != undefined) {
                    if(bank == 'Already Used') { 
                        errorFlag = 'true';
                        component.set("v.validation", true); 
                        component.set("v.NameError", 'This Bank Name has already been used. Please enter a new Bank Name.');
                    } else {
                        component.set("v.NameError", ''); 
                    }
                } else { 
                    component.set("v.NameError", ''); 
                }
                if(errorFlag == 'false') {
                    component.find("btnSaveBank").set("v.disabled", true);                    
                    var action1 = component.get("c.SaveBank");
                    action1.setParams({
                        "creditUnion" : bankd
                    }); 
                    action1.setCallback(this, function(response1) {
                        var state1 = response1.getState();
                        var bankId = response1.getReturnValue(); 
                        debugger;
                        component.find("btnSaveBank").set("v.disabled", false);
                        if(bankId != '' && bankId != undefined) {
                            if(bankId == 'Already Used'){ 
                                component.set("v.validation", true);                     
                                component.set("v.NameError", 'This Bank Name has already been used. Please enter a new Bank Name.');
                            } else {
                                var site = $A.get("{!$Label.c.Org_URL}");
                                window.location = '/'+site+'/s/bank-detail?bankId='+ bankId;
                            }
                        }
                        else{ 
                            component.set("v.validation", false); 
                            component.set("v.showCreate", false); 
                        }
                        
                    });
                    $A.enqueueAction(action1);
                }
            });
            $A.enqueueAction(action);        
            /*var txtAttributeName = component.find("txtAttributeName").get("v.value");
            var dropAttributeType = component.find("dropAttributeType").get("v.value");
            var dropProductType = component.find("dropProductType").get("v.value");
            var txtAttributeSize = component.find("txtAttributeSize").get("v.value");
            var txtActive = component.find("txtActive").get("v.value");
            var txtPrimary = component.find("txtPrimary").get("v.value");
            var txtSecondary = component.find("txtSecondary").get("v.value"); 
            
            if(txtAttributeName == undefined || txtAttributeName == "" || txtAttributeName == null)
            {
                errorFlag = 'true';
                component.set("v.validation", true); 
                component.set("v.AttributeNameError", 'Input field required'); 
            }
            
            if(txtAttributeSize != undefined && txtAttributeSize != "" && txtAttributeSize != null)
            {
                if(helper.checkCharecter(txtAttributeSize))
                {
                    errorFlag = 'true';
                    component.set("v.validation", true);
                    component.set("v.AttributeSizeError", 'Please enter a numeric value.'); 
                } 
                else
                    component.set("v.AttributeSizeError", '');
            }
            else if(dropAttributeType != "DateTime")
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.AttributeSizeError", 'Input field required');
            }
            else 
                txtAttributeSize = '';
            
            if(txtAttributeName != undefined && txtAttributeName != "" && txtAttributeName != null)
            {
                var action = component.get("c.CheckTemplateAttributes");
                action.setParams({ "attributeName": txtAttributeName });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    var attribute = response.getReturnValue(); 
                    if(attribute != '' && attribute != undefined) {
                        if(attribute == 'Already Used') { 
                            errorFlag = 'true';
                            component.set("v.validation", true); 
                            component.set("v.AttributeNameError", 'This Attribute Name has already been used. Please enter a new Attribute Name.');
                        } else {
                            component.set("v.AttributeNameError", ''); 
                        }
                    } else { 
                        component.set("v.AttributeNameError", ''); 
                    }
                    if(errorFlag == 'false') {
                        component.find("btnSaveAttribute").set("v.disabled", true);                    
                        var action1 = component.get("c.SaveAttribute");
                        action1.setParams({
                            "Name" : txtAttributeName,
                            "AttributeType" : dropAttributeType,
                            "ProductType" : dropProductType,
                            "AttributeSize" : txtAttributeSize,
                            "IsActive" : txtActive,
                            "IsPrimary" : txtPrimary,
                            "IsSecondary" : txtSecondary
                        }); 
                        action1.setCallback(this, function(response1) {
                            var state1 = response1.getState();
                            var attributeId = response1.getReturnValue(); 
                            debugger;
                            component.find("btnSaveAttribute").set("v.disabled", false);
                            if(attributeId != '' && attributeId != undefined) {
                                if(attributeId == 'Already Used'){ 
                                    component.set("v.validation", true);                     
                                    component.set("v.AttributeNameError", 'This Attribute Name has already been used. Please enter a new Attribute Name.');
                                } else {
                                    window.location = '/demo/s/template-attribute-detail?attributeId='+ attributeId;
                                }
                            }
                            else{ 
                                component.set("v.validation", false); 
                                component.set("v.showCreate", false);
                                component.set("v.AttributeSizeError", '');
                            }
                            
                        });
                        $A.enqueueAction(action1);
                    }
                });
                $A.enqueueAction(action);
            }
            */
        },  
        addCounterPartiespop : function(component, event) { 
            debugger;
            component.set("v.newcounterPopup", true);       
        },
        
        popcounterPopupCancel : function(component, event) { 
            debugger;
            component.set("v.newcounterPopup", false);       
        },
        addCounterParties: function(component, event) {
            debugger;
            var errorFlag = 'false';
            component.set("v.validation", false); 
            var Bankname = component.find("Banknameval").get("v.value");
            var emval = component.find("emailval").get("v.value");
            var mval = component.find("mobileval").get("v.value");
            var vcode =  component.find("code").get("v.value");
            var vcity =component.find("city").get("v.value");
            var vpcontact =component.find("pcontact").get("v.value");
            var vwebsite =component.find("website").get("v.value");
            var vstate =component.find("state").get("v.value");
            
            var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var regPhoneNo = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
            if(mval == '' || mval == 'undefined')
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.mobileError", 'Input field required'); 
            }
            else if(!$A.util.isEmpty(mval))
            {   
                if(!mval.match(regPhoneNo))
                {
                    errorFlag = 'true';
                    component.set("v.validation", true);
                    component.set("v.mobileError", 'Please enter valid phone number.'); 
                } 
                else{
                    component.set("v.mobileError", ''); 
                }
            }
            if(emval == '' || emval == 'undefined')
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.emailError", 'Input field required'); 
            }
            else if(!$A.util.isEmpty(emval))
            {   
                if(!emval.match(regExpEmailformat))
                {
                    errorFlag = 'true';
                    component.set("v.validation", true);
                    component.set("v.emailError", 'Please Enter a Valid Email Address'); 
                }
                else
                {
                    component.set("v.emailError", ''); 
                }
            }
            
            if(Bankname == '' || Bankname == 'undefined' || Bankname == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.cpError", 'Input field required'); 
            } else { 
                component.set("v.cpError", ''); 
            } 
            if(vcode == '' || vcode == 'undefined' || vcode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.codeerror", 'Input field required'); 
            } else { 
                component.set("v.codeerror", ''); 
            } 
            if(vcity == '' || vcity == 'undefined'|| vcity == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.cityerror", 'Input field required'); 
            } else { 
                component.set("v.cityerror", ''); 
            } 
            if(vpcontact == '' || vpcontact == 'undefined' || vpcontact == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.Pcontacterror", 'Input field required'); 
            } else { 
                component.set("v.Pcontacterror", ''); 
            } 
            if(vwebsite == '' || vwebsite == 'undefined' || vwebsite == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.weberror", 'Input field required'); 
            } else { 
                component.set("v.weberror", ''); 
            } 
            if(vstate == '' || vstate == 'undefined' || vstate == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.Stateerror", 'Input field required'); 
            } else { 
                component.set("v.Stateerror", ''); 
            } 
            if(errorFlag == 'false') {
                console.log('@@'+component.get("v.counterpartyname"));
                var action = component.get('c.addCounterPartiesname');  
                action.setParams({
                    "saveCreditunion" : component.get("v.bank"),
                    "PName" : component.get("v.Partname"),
                    "PID" : component.get("v.PartyID")
                });
                debugger;
                action.setCallback(this, function(actionResult) { 
                    var state = actionResult.getState(); 
                    var result = actionResult.getReturnValue();
                    if(state === "SUCCESS"){
                        component.set("v.newcounterPopup", false);
                        component.set("v.onboard", true);
                    }
                });
                debugger;
                $A.enqueueAction(action);
            }
        },
        onboard: function(component, event) {
            component.set("v.onboard", false);
        },
    })
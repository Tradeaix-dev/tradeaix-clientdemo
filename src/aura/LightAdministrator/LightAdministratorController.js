({
    getList : function(component, event, helper) {
        var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
        document.title =sitename +' : Administration';
        
       component.set("v.OTPBCP",$A.get("{!$Label.c.OBCP_ProfileID}"));
        if(localStorage.getItem('LoggeduserProfile') == $A.get("{!$Label.c.ACP_ProfileID}"))
        {
            component.set("v.Tenantcheck",true);
            var Action1 = component.get("c.loadorgs");   
            Action1.setParams({
                "username" : localStorage.getItem('UserSession')                
            });        
            Action1.setCallback(this, function(actionResult1) {
                debugger;
                var state = actionResult1.getState(); 
                var results = actionResult1.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.OrgFilter",results);
                }
                
            }); 
            $A.enqueueAction(Action1);
        }
        else{
            component.set("v.Tenantcheck",false);                        
        }
        
        
        
        debugger;
        var Action = component.get("c.getallLists");
        component.set("v.userid",localStorage.getItem("UserSession"));
        var sessionname = localStorage.getItem("UserSession");  
         Action.setParams({
            "OrgId" : localStorage.getItem("LoggeduserOrgID"),
             "TId" : localStorage.getItem("LoggeduserTenantID"),
             "userId" : localStorage.getItem("IDTenant")
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Organizations",results.orgLists);
                component.set("v.Tenants",results.tenantLists);
                component.set("v.Users",results.userLists);              
            }
        });
        $A.enqueueAction(Action);
    },
    reqCheck: function(component, event, helper) {
        if(component.find("ChkreqAccess").get('v.value')==true)
        {
            component.find("Accessbtn1").set("v.disabled", false);
        }
        else{
            component.find("Accessbtn1").set("v.disabled", true);
        }
    },
    activeRuleEngine : function(component, event) {
        debugger;
        var Action = component.get("c.Activechk");
         Action.setParams({           
             "Id" : event.target.id
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                if(results=='Active')
                {
                    var site = $A.get("{!$Label.c.Org_URL}");
                    window.location = '/'+site+'/s/ruleengine?bankId='+event.target.id;
                }else{
                    component.set("v.alertpopup",true);
                }
            }
            });
        $A.enqueueAction(Action);
          
    },
    cancelalertpop: function(component, event) {
        component.set("v.alertpopup",false); 
    },
    reqAccesscancel: function(component, event) {
        component.set("v.activetenant",false); 
    },
    btnreqAccess: function(component, event, helper) {
        var action = component.get('c.updateTenantactive'); 
        var self = this; 
        action.setParams({
            "TenantId" : component.get("v.TenantId"),
            "LoggedUserID" : localStorage.getItem("UserSession")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.activetenant", false);
            
            if(state === "SUCCESS"){
                 $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },  
    deactivecancel: function(component, event) {
        component.set("v.deactivetenant",false); 
    },
    deactivebtncall: function(component, event, helper) {
        var action = component.get('c.Tenantdeactivecall'); 
        var self = this; 
        action.setParams({
            "TenantId" : component.get("v.TenantId"),
            "LoggedUserID" : localStorage.getItem("UserSession")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.deactivetenant", false);
            
            if(state === "SUCCESS"){
                 $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },  
    addOrganization : function(component, event, helper) {
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/organizationcreate';
    },
    addTenant : function(component, event, helper) {
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/tenantcreate';
    },
    addTenantUser : function(component, event, helper) {
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/user-create?F=A';
    },
    searchUser : function(component, event, helper) {
        debugger;
        var searchtext = component.find("searchtxt").get("v.value");   
        var errorFlag = 'false';
        if(searchtext == '' || searchtext == null || searchtext == undefined || searchtext.trim().length*1 == 0) 
        {  
            errorFlag = 'true';
            component.set("v.txttenantusersearch", 'Input field required'); 
        }
        else{  
            component.set("v.txttenantusersearch", ''); 
        } 
        if(errorFlag == 'false')
        { 
        var Action = component.get("c.getUserSearch");        
        var sessionname = localStorage.getItem("UserSession");
        Action.setParams({             
            "seachtext" : searchtext
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){               
                component.set("v.Users",results.userLists);                
            }
        });
        $A.enqueueAction(Action);
        }
    },
    activetenant : function(component, event, helper) {
        debugger;
        component.set("v.activetenant",true);        
        component.set("v.TenantId",event.target.id);
    },
    activetenantalert : function(component, event, helper) {
        debugger;
        component.set("v.activetenantalert",true);   
    },
    alertOK : function(component, event, helper) {
        debugger;
        component.set("v.activetenantalert",false);   
    },
    
    deactivetenant : function(component, event, helper) {
        debugger;
        component.set("v.deactivetenant",true);        
        component.set("v.TenantId",event.target.id);
    },
    searchTenant :  function(component, event, helper) {
        debugger;
        var searchtext = component.find("searchtxttenant").get("v.value");       
       
        var errorFlag = 'false';
        if(searchtext == '' || searchtext == null || searchtext == undefined || searchtext.trim().length*1 == 0) 
        {  
            errorFlag = 'true';
            component.set("v.txttenantsearch", 'Input field required'); 
        }
        else{  
            component.set("v.txttenantsearch", ''); 
        } 
        if(errorFlag == 'false')
        { 
        var Action = component.get("c.getTenantSearch");        
        var sessionname = localStorage.getItem("UserSession");
        Action.setParams({             
            "seachtext" : searchtext
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){               
                component.set("v.Tenants",results.tenantLists);                
            }
        });
        $A.enqueueAction(Action);
        }
    },
    searchOrganization :  function(component, event, helper) {
        debugger;
        var searchtext = component.find("searchtxtorg").get("v.value");
        var errorFlag = 'false';
        if(searchtext == '' || searchtext == null || searchtext == undefined || searchtext.trim().length*1 == 0) 
        {  
            errorFlag = 'true';
            component.set("v.txtorgsearch", 'Input field required'); 
        }
        else{  
            component.set("v.txtorgsearch", ''); 
        } 
        if(errorFlag == 'false')
        { 
            var Action = component.get("c.getOrgSearch");        
            var sessionname = localStorage.getItem("UserSession");
            Action.setParams({             
                "seachtext" : searchtext
            });
            Action.setCallback(this, function(actionResult) {
                debugger;
                var state = actionResult.getState(); 
                var results = actionResult.getReturnValue();
                if(state === "SUCCESS"){               
                    component.set("v.Organizations",results.orgLists);                
                }
            });
            $A.enqueueAction(Action);
        }
    },
    onSingleSelectChange : function(component, event, helper){
        debugger;
        var selectCmp = component.find("InputSelectSingle").get("v.value"); 
        component.set("v.userid",localStorage.getItem("UserSession"));
        var sessionname = localStorage.getItem("UserSession");          
        if(selectCmp == 'All Realms'){
            var Action = component.get("c.getallLists");       
            Action.setParams({
                "OrgId" : localStorage.getItem("LoggeduserOrgID"),
                "TId" : localStorage.getItem("LoggeduserTenantID"),
                "userId" : localStorage.getItem("IDTenant")
            });
        }else{
            var Action = component.get("c.getfilterLists"); 
            Action.setParams({           
                "tenantId" : selectCmp
            });
        }
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Organizations",results.orgLists);
                component.set("v.Tenants",results.tenantLists);
                component.set("v.Users",results.userLists);              
            }
        });
        $A.enqueueAction(Action);
<<<<<<< HEAD
    },
    //Steps page redirect
    brandingclick : function(component, event, helper){
        debugger;
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/adminconsole';
        //component.set("v.Brandingpopupwithtransactions", true);
       /* var Action = component.get("c.getAlltransactions");       
        Action.setParams({           
            "TId" : localStorage.getItem("LoggeduserTenantID"),
            "userId" : localStorage.getItem("IDTenant")
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Brandingpopupwithtransactions", true);
                component.set("v.transactioncount",results.TransactionsCount);
                component.set("v.quotecount",results.QuotesCount);                             
            }
        });
        $A.enqueueAction(Action); */
    },
    cancelbranding:function(component, event, helper){
        debugger;
        component.set("v.Brandingpopupwithtransactions", false);
    },
    deletealltransactions : function(component, event, helper){
        debugger;
        component.set("v.Brandingpopupwithtransactions", false);
        component.set("v.Brandingsteponedetails", false); 
        component.set("v.Deleteconfirmation", true);        
    },
    canceldelete: function(component, event, helper){
        debugger;
        component.set("v.Deleteconfirmation", false);        
    },
    deleteconfirm : function(component, event, helper){
        debugger;
        var Action = component.get("c.deletealltransactions");
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                //if(results == 'Success'){
                    component.set("v.Deleteconfirmation", false); 
                    component.set("v.Brandcreation", true);                     
                //}                                            
            }
        });
        $A.enqueueAction(Action);
    },
    steponeclick: function(component, event, helper){
        debugger;
        var Action = component.get("c.getAlltransactions");       
        Action.setParams({           
            "TId" : localStorage.getItem("LoggeduserTenantID"),
            "userId" : localStorage.getItem("IDTenant")
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                //component.set("v.Brandingpopupwithtransactions", true);
                component.set("v.Brandingpopupwithtransactions", false); 
                component.set("v.Brandingsteponedetails", true); 
                component.set("v.transactioncount",results.TransactionsCount);
                component.set("v.quotecount",results.QuotesCount);  
                component.set("v.notificationcount",results.NotificationCount);  
                component.set("v.activitylogcount",results.ActivitylogsCount);
                component.set("v.documentcount",results.documentcount);
                
            }
        });
        $A.enqueueAction(Action);
        
    },
    cancelstepone: function(component, event, helper){
        debugger;
        component.set("v.Brandingsteponedetails", false); 
    },
    cancelbrand: function(component, event, helper){
        debugger;
        component.set("v.Brandcreation", false);
    },
    createbrand: function(component, event, helper){
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/brandcreation';
    },
    steptwoclick: function(component, event, helper){
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/brandcreation';
=======
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-clientdemo.git
    }
    
})
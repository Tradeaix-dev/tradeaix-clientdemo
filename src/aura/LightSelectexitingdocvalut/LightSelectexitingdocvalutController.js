({
	 getvalutDetails: function(component, event, helper) { 
        var action = component.get('c.ValutDetails'); 
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            debugger;
            if(state === "SUCCESS"){
                component.set("v.bank", result);
                
                 var container = component.find("container");
                $A.util.removeClass(container, "slds-hide");
                $A.util.addClass(container, "slds-show");
                debugger;
            }
        }); 
        $A.enqueueAction(action);
    },  
    refreshModel: function (component, event, helpler) {
        debugger;
        var img = component.find("imgloading");
        $A.util.addClass(img,'slds-hide'); 
        $A.util.removeClass(img,'slds-show');
        component.set("v.FileUploadMsg", 'File(s) have been uploaded successfully');
        component.set("v.FileUploadMsgSorE", 'Success');
        component.set("v.FileUpload", true);
        var action = component.get('c.ValutDetails'); 
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.bank", result);
                $A.get('e.force:refreshView').fire();
                //component.set("v.Offer.attachment", result); 
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
	
	ChooseFile : function(component, event) {     
        debugger;
        component.set("v.FileUploadMsg", 'Please choose the file(s)');
        component.set("v.FileUploadMsgSorE", 'Error');
        component.set("v.FileUpload", true); 
    },
    ChooseFailure : function(component, event) {        
        component.set("v.FileUploadMsg", 'An error has occurred. Please contact us at\nLoanParticipationBeta@cunamutual.com.');
        component.set("v.FileUploadMsgSorE", 'Error');        
        component.set("v.FileUpload", true); 
    },
    fileExtension : function(component, event) {        
        component.set("v.FileUploadMsg", 'This file format not allowed to upload');
        component.set("v.FileUploadMsgSorE", 'Error');        
        component.set("v.FileUpload", true); 
    },
    LoaderDisplay : function(component, event) {
        var img = component.find("imgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
     FileUploadcancel : function(component, event) {
        var img = component.find("imgloading");
        $A.util.addClass(img,'slds-hide'); 
        $A.util.removeClass(img,'slds-show');
        var img1 = component.find("imgloadingDue");
        $A.util.addClass(img1,'slds-hide'); 
        $A.util.removeClass(img1,'slds-show');
        component.set("v.FileUpload", false); 
    },  
    delete1: function(component, event, helper) {
        debugger;
        var action = component.get('c.DeleteFile'); 
         var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        };
        
        var offerId = getUrlParameter('transactionId');
        if(offerId==undefined)
        {
            var offerId = getUrlParameter('syndId');
        }
        debugger;
        console.log('offerId'+offerId);
        action.setParams({
            "attId" : event.target.id,
            "offerId" : offerId
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                debugger;
                component.set("v.bank", result);
                component.set("v.DeleteFile", true);
                //$A.get('e.force:refreshView').fire();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.bank");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.bank", emptyTask);
                component.set("v.hideDiv", false);
            }
            
            
        });
        $A.enqueueAction(action); 
    },
    DeleteFilecancel: function(component, event, helper) {
        component.set('v.DeleteFile', false);
        $A.get('e.force:refreshView').fire();
    }, 
})
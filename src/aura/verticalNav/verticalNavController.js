({
    isUserchk : function(component, event, helper) {
        debugger; 
        var Action1 = component.get("c.getMenulist");
        var sessionname1 = localStorage.getItem("UserSession");
        Action1.setParams({
            "username" : sessionname1                
        });
        Action1.setCallback(this, function(actionResult1) {
            debugger;
            var state1 = actionResult1.getState(); 
            var results1 = actionResult1.getReturnValue();
            if(state1=== "SUCCESS"){
                component.set("v.menulist",results1);
            }
        });
        $A.enqueueAction(Action1);
        
    },
    
    onClick : function(component, event, helper) {
        debugger;
        var id = event.target.dataset.menuItemId;
        if (id) {
            component.getSuper().navigate(id);
        }
    }
})
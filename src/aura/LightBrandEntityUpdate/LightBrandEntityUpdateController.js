({
    getentitydetails : function(component, event, helper) {
        debugger;
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        };
        var flagchk = getUrlParameter('flag');
        component.set("v.flagchk", flagchk); 
        var TID = getUrlParameter('Id');
        component.set("v.entityId", TID); 
        var Action1 = component.get("c.getentity");   
        Action1.setParams({
            "entity" : TID                
        });        
        Action1.setCallback(this, function(actionResult1) {
            debugger;
            var state = actionResult1.getState(); 
            var results = actionResult1.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Banks",results);
                if(results.ContentBanner.length > 0){
                    component.set("v.bannerattachment", true);
                }
                if(results.ContentLogo.length > 0){
                    component.set("v.logoattachment", true);
                }
            }
            
        }); 
        $A.enqueueAction(Action1);
    },
    updatecounterparties: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var salutation = component.find("Tenanttitle").get("v.value");
        var lastname = component.find("lastname").get("v.value");
        var sitename = component.find("sitename").get("v.value");
        //var bannerurl = component.find("bannerurl").get("v.value");
        //var logourl = component.find("logourl").get("v.value");
        var firstname = component.find("FirstName").get("v.value");
        var email = component.find("email").get("v.value");
        var username = component.find("username").get("v.value");
        var swiftcode = component.find("swiftcode").get("v.value");
        var landingmsg = component.find("landingmessage").get("v.value");
        var tenantwebsite = component.find("website").get("v.value");
        var footermessage = component.find("footermsg").get("v.value");
        var phone = component.find("phone").get("v.value");
        var street = component.find("street").get("v.value");
        var state = component.find("state").get("v.value");
        var country = component.find("country").get("v.value");
        var city = component.find("city").get("v.value");
        var zipcode = component.find("zipcode").get("v.value");
        var Action = component.get("c.updateentitydetails"); 
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        if(salutation == '' || salutation =='--Select--' || salutation == 'undefined' || salutation == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.SalutationError", 'Input field required'); 
        } else { 
            component.set("v.SalutationError", ''); 
        }
        if(firstname == '' || firstname == 'undefined' || firstname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.FirstNameError", 'Input field required'); 
        } else { 
            component.set("v.FirstNameError", ''); 
        } 
        if(lastname == '' || lastname == 'undefined' || lastname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LastNameError", 'Input field required'); 
        } else { 
            component.set("v.LastNameError", ''); 
        }
        if(email == '' || email == 'undefined' || email == null)
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.EmailError", 'Input field required'); 
        }
        else if(!$A.util.isEmpty(email))
        {   
            if(!email.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.EmailError", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.EmailError", ''); 
            }
        }
        if(sitename == '' || sitename == 'undefined' || sitename == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LegalNameError", 'Input field required'); 
        } else { 
            component.set("v.LegalNameError", ''); 
        }
        if(username == '' || username == 'undefined' || username == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.UsernameError", 'Input field required'); 
        } else { 
            component.set("v.UsernameError", ''); 
        }
        if(swiftcode == '' || swiftcode == 'undefined' || swiftcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.SwiftCodeError", 'Input field required'); 
        } else { 
            component.set("v.SwiftCodeError", ''); 
        }
        if(tenantwebsite == '' || tenantwebsite == 'undefined' || tenantwebsite == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.WebsiteError", 'Input field required'); 
        } else { 
            component.set("v.WebsiteError", ''); 
        }
        if(tenantwebsite == '' || tenantwebsite == 'undefined' || tenantwebsite == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.WebsiteError", 'Input field required'); 
        } else { 
            component.set("v.WebsiteError", ''); 
        }
        if(landingmsg == '' || landingmsg == 'undefined' || landingmsg == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LandingmessageError", 'Input field required'); 
        } else { 
            component.set("v.LandingmessageError", ''); 
        }
        if(footermessage == '' || footermessage == 'undefined' || footermessage == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.FootermessageError", 'Input field required'); 
        } else { 
            component.set("v.FootermessageError", ''); 
        }
        if(footermessage == '' || footermessage == 'undefined' || footermessage == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.FootermessageError", 'Input field required'); 
        } else { 
            component.set("v.FootermessageError", ''); 
        }
        if(!$A.util.isEmpty(phone))
        {   
            if(phone == '' || phone == 'undefined' || phone == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Input field required'); 
            } else { 
                component.set("v.PhoneError", ''); 
            } 
            
            if(!phone.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please enter valid phone number'); 
            } 
            else if(phone.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.PhoneError", ''); 
                }
        }
        if(errorFlag == 'false') {
            Action.setParams({
                "entity" : component.get("v.entityId"),
                "salutation" : salutation,
                "lastname" : lastname,
                "sitename" : sitename,
                "footermsg" : footermessage,
                "firstname" : firstname,
                "email" : email,
                "username" : username,
                "swiftcode" : swiftcode, 
                "landingmsg" : landingmsg,
                "tenantwebsite" : tenantwebsite,
                "phone" : phone,
                "street" : street,
                "state" :state,
                "country" : country,
                "city" : city,
                "zipcode" : zipcode
                
            });        
            Action.setCallback(this, function(actionResult1) {
                debugger;
                var state = actionResult1.getState(); 
                var results = actionResult1.getReturnValue();
                if(state === "SUCCESS"){
                    var msg;
                    if(component.get("v.flagchk") == 'P'){
                        msg = 'Party Details Updated Successfully'
                    }
                    if(component.get("v.flagchk") == 'CP1'){
                        msg = 'Counterparty1 Details Updated Successfully'
                    }
                    if(component.get("v.flagchk") == 'CP2'){
                        msg = 'Counterparty2 Details Updated Successfully'
                    }
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': msg,
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                    var site = $A.get("{!$Label.c.Org_URL}");
                    window.location = '/'+site+'/s/brandcreation';
                }
                
            }); 
            $A.enqueueAction(Action);
        }
    },
    cancelupdatecounterparties: function(component, event, helper) {
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/brandcreation';
        
    },
    refreshModel1: function (component, event, helpler) {
        debugger;
        var action = component.get('c.getbannerattachment');  
        action.setParams({
            "parentId" : component.get("v.entityId")            
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                debugger;
                component.set("v.bannerattachment", true);
                component.set("v.Banks.ContentBanner", result.ContentBanner);   
                component.set("v.Banks.ContentLogo", result.ContentLogo);    
                if(result.ContentLogo.length > 0){
                    component.set("v.logoattachment", true);
                }
                var img = component.find("imgloading");
                $A.util.addClass(img,'slds-hide'); 
                $A.util.removeClass(img,'slds-show');                
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
    ChooseFile1 : function(component, event) {        
        component.set("v.FileUploadMsg", 'Please choose the file(s)');
        component.set("v.FileUploadMsgSorE", 'Error');
        component.set("v.FileUpload", true); 
    },
    LoaderDisplay1 : function(component, event) {
        var img = component.find("imgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    refreshModel2: function (component, event, helpler) {
        debugger;
        var action = component.get('c.getlogoattachment');  
        action.setParams({
            "parentId" : component.get("v.entityId")            
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                debugger;
                component.set("v.logoattachment", true);
                component.set("v.Banks.ContentLogo", result.ContentLogo);                
                var img = component.find("imgloading");
                $A.util.addClass(img,'slds-hide'); 
                $A.util.removeClass(img,'slds-show');                
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
    ChooseFile2 : function(component, event) {        
        component.set("v.FileUploadMsg", 'Please choose the file(s)');
        component.set("v.FileUploadMsgSorE", 'Error');
        component.set("v.FileUpload", true); 
    },
    LoaderDisplay2 : function(component, event) {
        var img = component.find("imgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    deletebanner: function(component, event, helper) {
        debugger;
        var action = component.get('c.DeleteFile');  
        action.setParams({
            "attId" : event.target.id,
            "parentId" : component.get("v.entityId") 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.Banks.ContentBanner",result.ContentBanner);
                if(result.ContentBanner.length == 0){
                    component.set("v.bannerattachment", false);
                }
                
            }
        });
        $A.enqueueAction(action);
    },
    deletelogo: function(component, event, helper) {
        debugger;
        var action = component.get('c.DeletelogoFile');  
        action.setParams({
            "attId" : event.target.id,
            "parentId" : component.get("v.entityId") 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.Banks.ContentLogo",result.ContentLogo);
                if(result.ContentLogo.length == 0){
                    component.set("v.logoattachment", false);
                }
            }
        });
        $A.enqueueAction(action);
    },
})
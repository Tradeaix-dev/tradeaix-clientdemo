({
	getentitydetails : function(component, event, helper) {
        debugger;
		var Action = component.get("c.Branddetails"); 
        Action.setCallback(this, function(actionResult1) {
            debugger;
            var state = actionResult1.getState(); 
            var results = actionResult1.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Banks",results);
            }
            
        }); 
        $A.enqueueAction(Action);
	},
    brandreviewcancel: function(component, event, helper) {
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/adminconsole';
    },
    brandcomplete : function(component, event, helper) {
        debugger;        
        var Action = component.get("c.sendemailtoadmin");        
        Action.setCallback(this, function(actionResult1) {
            debugger;
            var state = actionResult1.getState(); 
            var results = actionResult1.getReturnValue();
            if(state === "SUCCESS"){
                var site = $A.get("{!$Label.c.Org_URL}");
                window.location = '/'+site+'/s/adminconsole';
            }
            
        }); 
        $A.enqueueAction(Action);
    }
})
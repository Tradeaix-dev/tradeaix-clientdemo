({  
    getTransactionDetails: function(component, event, helper) { 
        
        
         debugger;
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        };
       // alert(localStorage.getItem("IDTenant"));
       // alert(localStorage.getItem("LoggeduserTenantID"));
        //alert(localStorage.getItem("FromRaidCHK"));
       // alert(localStorage.getItem("FromRaidUserPWD"));
//        alert(localStorage.getItem("LoggeduserProfile"));
        var ACP = $A.get("{!$Label.c.ACP_ProfileID}");
        var SACP = $A.get("{!$Label.c.SACP_ProfileID}");
        var SMCP = $A.get("{!$Label.c.SMCP_ProfileId}");
        var BMCP = $A.get("{!$Label.c.BMCP_ProfileID}");
        var OBCP = $A.get("{!$Label.c.OBCP_ProfileID}");
        var AMCP = $A.get("{!$Label.c.AMCP}");
        component.set("v.Loggeduserid",localStorage.getItem("IDTenant"));
        if(localStorage.getItem("LoggeduserProfile") == ACP || localStorage.getItem("LoggeduserProfile") == SMCP || localStorage.getItem("LoggeduserProfile") == SACP || localStorage.getItem("LoggeduserProfile") == AMCP){
            component.set("v.IsSellerManager",true);
            component.set("v.IsBuyerManager",false);
        }
        else if(localStorage.getItem("LoggeduserProfile") == BMCP || localStorage.getItem("LoggeduserProfile") == OBCP){
            component.set("v.IsBuyerManager",true);
            component.set("v.IsSellerManager",false);
        }
         var offerId = getUrlParameter('transactionId');        
        if(component.get("v.IsSellerManager")==undefined)
        {
           // alert('Test');
            //var site = $A.get("{!$Label.c.Org_URL}");
            //window.location.reload();
             window.location = '/'+site+'/s/transactionviewdetails?transactionId='+offerId;
        }
        
        if(localStorage.getItem("LoggeduserProfile") == BMCP && localStorage.getItem("FromRaidCHK")=='true')
        {
            component.set("v.FromRaidCHK",true);
        }else{
            component.set("v.FromRaidCHK",false);
        }
        
        if(localStorage.getItem("FromRaidCHK") && localStorage.getItem("FromRaidUserPWD") =='undefined')
        {
            component.set("v.FromRaidCHKForRUser",true);
        }else{
            component.set("v.FromRaidCHKForRUser",false);
        }
        debugger;
        if(localStorage.getItem("TaskAssigneeloggeduser") == "true"){
            if(localStorage.getItem("TaskviewAssigneeloggeduser") == "true"){
                component.set("v.taskassigneeuser", false);
            }else{
                component.set("v.taskassigneeuser", true);
            }
        }        
       
        var managetassk = getUrlParameter('manage');
        component.set("v.TransId",offerId);
        var action = component.get('c.TransactionDetails');
        var self = this; 
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "offerId" : offerId
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            debugger;
            if(state === "SUCCESS"){
                
                component.set("v.Transactions",result);
<<<<<<< HEAD
                //alert(result.Trans.recall__c);
                component.set("v.recallflag",result.Trans.recall__c);
                component.set("v.Created",result.Trans.CreatedDate);
                component.set("v.approveddate",result.Trans.CreatedDate);
                if(result.CounterParties.length > 0){  
                    component.set("v.Declinedflag",result.CounterParties[0].Declined__c);
                }
                else{
                    component.set("v.Declinedflag",false);
                }
                if(result.Tasks.length > 0){
                    component.set("v.Istask", true);
                }else{
                    component.set("v.Istask", false);
                }
                component.set("v.isAmendoffer", result.isAmend);
                component.set("v.isAmendAcceptoffer",result.isAmendAccept);
                if(localStorage.getItem("TaskAssigneeloggeduser") == "true"){
                    component.set("v.taskassigneeuserregister", true);
                    //component.set("v.managetransaction", true);
                    component.set("v.Taskcreated", true);
                    component.set("v.noTaskcreated", false);    
                    component.set("v.Taskprogress", "process");
                    component.set("v.Taskbox", "p-card progress-border");
                    component.set("v.hTaskstatus","spinneryes");
                }
                document.title = result.Trans.TransactionRefNumber__c;
                var Transstatus=result.Trans.Status__c;
                component.set("v.Tstatus",Transstatus);
                var Bidstatus='';
                var Bidstatus1='';
                component.set("v.MorethanoneQuotecountCHK",false);
                if(result.Quotes.length > 0)  
                {
                    component.set("v.MorethanoneQuotecountCHK",true);
                    if(result.Quotes[0].Bid_Status__c=='Quote Closed'){
                        Bidstatus ='Quote Closed';
                    }
                    if(result.Quotes[0].Bid_Status__c=='Quote Accepted'){
                        Bidstatus1 ='Quote Accepted';
                    }
                    
                }
                
                if(Transstatus==='Transaction Created' || Transstatus==='Pending Validation' || Transstatus==='Level 1 - Pending Approval' 
                   || Transstatus==='Pending Approval' || Transstatus==='Validation Rejected' || Transstatus==='Transaction Rejected')
                {
                    component.set("v.Tprogress", "process");
                    component.set("v.Tbox", "p-card progress-border");
                    component.set("v.hTstatus","spinneryes");
                } 
                else if(Transstatus == 'Inactive'){
                    component.set("v.Tprogress", "red");
                    component.set("v.Tbox", "p-card red-border");
                }
                    else if(Transstatus==='Transaction Approved' || Transstatus==='Requested For Info')
                    {   
                        if(Transstatus==='Transaction Approved'){
                            component.set("v.RFI","Requested For Info / Quote");
                        }else{
                            component.set("v.Requesteddate",result.Trans.Published_Date__c);
                            component.set("v.RequestedBy",result.Trans.Tenant__r.Tenant_Site_Name__c);
                            component.set("v.RFI","Requested For Info");
                        }
                        component.set("v.Tprogress", "active");
                        component.set("v.Tbox", "p-card active-border");
                        component.set("v.RFIprogress", "process");
                        component.set("v.RFIbox", "p-card progress-border");
                        component.set("v.hTstatus","spinnerno");
                        component.set("v.hRFQstatus","spinneryes");
                        component.set("v.FullRFIstatus",true);
                        component.set("v.Fullcomstatus1",true);
                        component.set("v.combox1", "p-card");
                        component.set("v.Fullcomstatus2",true);
                        component.set("v.combox2", "p-card");
                        
                    }      
                        else if(Transstatus==='Requested For Quote' || Transstatus==='Quote Withdrawn' || Transstatus==='Quote Submitted' || Transstatus==='Quote Rejected')
                        {
                            debugger;
                            if(localStorage.getItem("TaskAssigneeloggeduser") == "true"){
                                if(localStorage.getItem("TaskviewAssigneeloggeduser") == "true"){
                                    component.set("v.taskassigneeuser", false);
                                }else{
                                    component.set("v.taskassigneeuser", true);
                                }
                            }   
                            component.set("v.Requesteddate",result.Trans.Published_Date__c);                            
                            component.set("v.RequestedBy",result.Trans.Tenant__r.Tenant_Site_Name__c);
                            component.set("v.Tprogress", "active");
                            component.set("v.RFIprogress", "active");
                            component.set("v.RFIbox", "p-card active-border");
                            component.set("v.hTstatus","spinnerno");
                            component.set("v.hRFQstatus","spinnerno");
                            component.set("v.Tbox", "p-card active-border");
                            component.set("v.Bidprogress", "process");
                            component.set("v.Bidbox", "p-card progress-border");
                            component.set("v.hTstatus","spinnerno");
                            component.set("v.RFI","Requested For Quote");
                            component.set("v.hRFQstatus","spinnerno");
                            component.set("v.hbidstatus","spinneryes");
                            component.set("v.FullRFIstatus",true);
                            component.set("v.FullBidstatus",true); 
                            component.set("v.Fullcomstatus1",true);
                            component.set("v.combox1", "p-card");
                            /*if(result.OfferExtended.Syndications.length > 0 && result.LogeduserCBId !=''){
                                component.set("v.Tstatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                            }*/
                            if(Transstatus==='Quote Submitted')
                            {
                                if(result.Quotes.length > 0){
                                    component.set("v.BidStatus",result.Quotes[0].Bid_Status__c);
                                    component.set("v.LastBidDate",result.Quotes[0].CreatedDate);
                                    component.set("v.HighestBidder",result.Quotes[0].CreatedBy__r.First_Name__c+' '+result.Quotes[0].CreatedBy__r.Last_Name__c);
                                    component.set("v.progressBidamount",result.Quotes[0].Bid_Amount__c);
                                }
                            }
                        }
                            else if(Transstatus==='Quote Accepted')
                            {
                                
                                //if(result.Quotes.length > 0 && result.LogeduserCBId !=''){
                                //  component.set("v.Tstatus",result.Quotes[0].Bid_Status__c);
                                //}
                                //if(Bidstatus1 =='Bid Accepted' && result.LogeduserCBId !='' || result.LogeduserCBId==''){
                                if(Transstatus =='Quote Accepted'){
                                    if(result.Quotes.length > 0){
                                        
                                        component.set("v.BidStatus",result.Quotes[0].Bid_Status__c);
                                        component.set("v.LastBidDate",result.Quotes[0].CreatedDate);
                                        component.set("v.HighestBidder",result.Quotes[0].CreatedBy__r.First_Name__c+' '+result.Quotes[0].CreatedBy__r.Last_Name__c);
                                        component.set("v.progressBidamount",result.Quotes[0].Bid_Amount__c);
                                    }
                                    component.set("v.Requesteddate",result.Trans.Published_Date__c);                                
                                    component.set("v.RequestedBy",result.Trans.Tenant__r.Tenant_Site_Name__c);
                                    component.set("v.Tprogress", "active");
                                    component.set("v.RFIprogress", "active");
                                    component.set("v.RFIbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.hRFQstatus","spinnerno");
                                    component.set("v.Tbox", "p-card active-border");
                                    component.set("v.Bidprogress", "active");
                                    component.set("v.Bidbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.RFI","Requested For Quote");
                                    component.set("v.comprogress", "process");
                                    component.set("v.combox", "p-card progress-border");
                                    component.set("v.hcomstatus","spinneryes");
                                    component.set("v.hbidstatus","spinnerno");
                                    component.set("v.FullRFIstatus",true);
                                    component.set("v.FullBidstatus",true); 
                                    component.set("v.Fullcomstatus",true);
                                }else{
                                    //component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                                    //component.set("v.RequestedBy",result.OfferExtended.CreditUnion.CU_Name__c);
                                    component.set("v.Tprogress", "active");
                                    component.set("v.RFIprogress", "active");
                                    component.set("v.RFIbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.hRFQstatus","spinnerno");
                                    component.set("v.Tbox", "p-card active-border");
                                    component.set("v.Bidprogress", "active");
                                    component.set("v.Bidbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.RFI","Requested For Quote");
                                    component.set("v.comprogress", "process");
                                    component.set("v.combox", "p-card progress-border");
                                    component.set("v.hcomstatus","spinneryes");
                                    component.set("v.hbidstatus","spinnerno");
                                    component.set("v.FullRFIstatus",true);
                                    component.set("v.FullBidstatus",true); 
                                    component.set("v.Fullcomstatus",false);
                                }
                            }
                                else if(Transstatus==='Completed Transaction')
                                {
                                    
                                    //if(result.OfferExtended.Syndications.length > 0 && result.LogeduserCBId !=''){
                                    //    component.set("v.Tstatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                    //}
                                    if(Bidstatus =='Quote Closed' && result.LogeduserCBId !='' || result.LogeduserCBId==''){                                
                                        component.set("v.FullRFIstatus",true);
                                        component.set("v.FullBidstatus",true); 
                                        component.set("v.Fullcomstatus",true);
                                        debugger;
                                        component.set("v.Completed",result.Trans.Published_Date__c);
                                        component.set("v.Requesteddate",result.Trans.Published_Date__c);
                                        component.set("v.RequestedBy",result.Trans.Tenant__r.Tenant_Site_Name__c);
                                        component.set("v.Tprogress", "active");
                                        component.set("v.RFIprogress", "active");
                                        component.set("v.RFIbox", "p-card active-border");
                                        component.set("v.hTstatus","spinnerno");
                                        component.set("v.hRFQstatus","spinnerno");
                                        component.set("v.Tbox", "p-card active-border");
                                        component.set("v.Bidprogress", "active");
                                        component.set("v.Bidbox", "p-card active-border");
                                        component.set("v.hTstatus","spinnerno");
                                        component.set("v.RFI","Requested For Quote");
                                        component.set("v.hcomstatus","spinnerno");
                                        component.set("v.comprogress", "active");
                                        component.set("v.combox", "p-card active-border");
                                        component.set("v.hbidstatus","spinnerno");
                                        if(result.Quotes.length > 0){
                                            {
                                                //if(result.LogeduserCBId !=''){
                                                //    component.set("v.Tstatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                                //}
                                                component.set("v.BidStatus",result.Quotes[0].Bid_Status__c);
                                                component.set("v.LastBidDate",result.Quotes[0].CreatedDate);
                                                component.set("v.HighestBidder",result.Quotes[0].CreatedBy__r.First_Name__c+' '+result.Quotes[0].CreatedBy__r.Last_Name__c);
                                                component.set("v.progressBidamount",result.Quotes[0].Bid_Amount__c);
                                            }
                                        }
                                    }else{
                                        component.set("v.FullRFIstatus",true);
                                        component.set("v.FullBidstatus",true); 
                                        component.set("v.Fullcomstatus",false);
                                        debugger;
                                        component.set("v.Completed",result.Trans.Published_Date__c);
                                        component.set("v.Requesteddate",result.Trans.Published_Date__c);                        
                                        component.set("v.RequestedBy",result.Trans.Tenant__r.Tenant_Site_Name__c);
                                        component.set("v.Tprogress", "active");
                                        component.set("v.RFIprogress", "active");
                                        component.set("v.RFIbox", "p-card active-border");
                                        component.set("v.hTstatus","spinnerno");
                                        component.set("v.hRFQstatus","spinnerno");
                                        component.set("v.Tbox", "p-card active-border");
                                        component.set("v.Bidprogress", "active");
                                        component.set("v.Bidbox", "p-card active-border");
                                        component.set("v.hTstatus","spinnerno");
                                        component.set("v.RFI","Requested For Quote");
                                        component.set("v.comprogress", "process");
                                        component.set("v.combox", "p-card progress-border");
                                        component.set("v.hcomstatus","spinneryes");
                                        component.set("v.hbidstatus","spinnerno");
                                    }                                    
                                 
                                }
                if(component.get("v.IsBuyerManager") && !result.Trans.RFQChk__c)
                {
                    var json = JSON.parse(result.Trans.Request_For_Information__c);
                    for(var i = 0; i < json.length; i++) {
                        var obj = json[i];
                        if(obj.Flag=='TRUE'){
                            debugger; 
                            // alert(obj.ID);
                            //alert(document.getElementById(obj.ID).value);
                            var hidediv = component.find("Amount").get("v.value");
                            
                            if(hidediv){
                                $A.util.addClass(hidediv, 'hide');
                            }
                        }
                        
                    }
                }
                
            
            component.set("v.managevalue",false);
              if(managetassk == "y"){
                  debugger;
                  
            component.set("v.managevalue",true);
            var checkbox = event.getSource();
            var checked = checkbox.get("v.value");            
                
                var CP = component.get("c.getCPusers");
                CP.setParams({ 
                    "tenantId" :  localStorage.getItem("LoggeduserTenantID"),
                    "transId" : component.get("v.Transactions.Trans.Id") 
                }); 
                CP.setCallback(this, function(response) {
                    var state = response.getState();
                    if (component.isValid() && state == 'SUCCESS') {   
                        var CP = response.getReturnValue();
                        var options = [];
                        CP.forEach(function(CPdata)  { 
                            //alert(CPdata.Id);
                            options.push({ value: CPdata.Id, label: CPdata.First_Name__c});
                            
                        });
                        component.set("v.listOptions", options);
                    } else {
                        console.log('Failed with state: ' + state);
                    }
                });
                $A.enqueueAction(CP); 
                
                var action = component.get("c.getTransactiontasks");
                action.setParams({ 
                    "transId" : component.get("v.Transactions.Trans.Id")             
                }); 
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    var result = response.getReturnValue();
                    if (state == 'SUCCESS') {   
                        component.set("v.Transactionstask",result);
                        component.set("v.managetransaction", true);
                        component.set("v.AmendBillingstatus", true);
                        component.set("v.combox1", "p-card");
                        component.set("v.combox2", "p-card");
                        if(result.Tasks.length > 0){
                            component.set("v.Taskcreated", true);
                            component.set("v.noTaskcreated", false);    
                            component.set("v.Taskprogress", "process");
                            component.set("v.Taskbox", "p-card progress-border");
                            component.set("v.hTaskstatus","spinneryes");
                            
                        }else{
                            component.set("v.Taskcreated", false);
                            component.set("v.noTaskcreated", true); 
                        }
                        if(result.TransAttAmendgroup.length > 0){
                            component.set("v.Amendcreated", true);
                            component.set("v.AmendBillingstatus", false);
                            component.set("v.Amendbox", "p-card progress-border");
                        }else{
                            component.set("v.Amendcreated", false);
                            component.set("v.AmendBillingstatus", true);
                        }
                        if(result.isTaskclosedstatus == true){
                            component.set("v.Taskbox", "p-card active-border");
                            component.set("v.isTaskclosedstatus", true);
                        }else{
                            component.set("v.isTaskclosedstatus", false);
                        }
                    } else {
                        console.log('Failed with state: ' + state);
                    }
                });
                $A.enqueueAction(action); 
            
        }
            }
        }); 
        $A.enqueueAction(action);
        debugger;
        //Add for Direct Manage Task        
      
        
    },
     doneRendering: function(component, event, helper) {
         debugger;
        var action = component.get('c.Chkunregisteruser');  
        action.setParams({ 
            "creditUnionId" :  localStorage.getItem("LoggeduserTenantID"),
            "GID" : component.get("v.TransId")
        }); 
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState();  
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                if(result.CPSelection.length>0){
                    for (var k = 0; k < result.CPSelection.length; k++) {
                        document.getElementById(result.CPSelection[k].Tenant_User__c).checked = true;                  
                    }
                }
                
            }
        }) 
        $A.enqueueAction(action);
         
        /* 
        var action = component.get('c.TransactionDetails');
        var self = this; 
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "offerId" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            debugger;
            if(state === "SUCCESS"){
                component.set("v.Transactions",result);
          if(component.get("v.IsBuyerManager"))// && !result.Trans.RFQChk__c)
                {
                    var json = JSON.parse(result.Trans.Request_For_Information__c);
                    for(var i = 0; i < json.length; i++) {
                        var obj = json[i];
                        if(obj.Flag=='TRUE'){
                            debugger; 
                            // alert(obj.ID);
                            document.getElementById(obj.ID).style.display = "none";
                            //var hidediv = document.getElementById(obj.ID).value; 
                            //var hidediv = component.find(obj.ID);  
                            
                            //if(hidediv){
                             //   $A.util.addClass(hidediv, 'hide');
                           // }
                        }
                        
                    }
                }
            }  }) 
        $A.enqueueAction(action);*/
     },
    onchangechkbox : function(component, event, helper){
        debugger;
        var lfckv = document.getElementById(event.target.id).checked;
        
        var action = component.get('c.addremoveunregisteredCP');  
        action.setParams({
            "UserID" : event.target.id,
            "GId" : component.get("v.TransId"),
            "Flag" : lfckv
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
            }
        });
        $A.enqueueAction(action);
        
    },
    registeruser: function(component, event) { 
        component.set("v.Customer", true);
        component.set("v.benefits",true);
        component.set("v.clrbenefits",'active1');
        component.set("v.userinfo",false);
        component.set("v.setpwd",false);
        component.set("v.finish",false);
    },
	registeruser: function(component, event) { 
        component.set("v.Customer", true);
        component.set("v.benefits",true);
        component.set("v.clrbenefits",'active1');
        component.set("v.userinfo",false);
        component.set("v.setpwd",false);
        component.set("v.finish",false);
    },
    bennext: function(component, event) { 
        
        
        var action = component.get('c.GetLoggedUserInfo');  
        action.setParams({
            "LoggedUserID" : localStorage.getItem('UserSession')
        });
        debugger;
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.UM",result);
                component.set("v.benefits",false);
                component.set("v.userinfo",true);
                component.set("v.clrbenefits",'completed');
                component.set("v.clrud",'active1');
            }
        });
        debugger;
        $A.enqueueAction(action);
        
    },
    bennext1 : function(component, event) {
        debugger;
        var action = component.get('c.viewRequestUsers1');  
        action.setParams({
            "GId" : component.get("v.TransId")                               
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){                
                component.set("v.Transactions", result.CounterParties);
               
                if(result.CounterParties.length>0){
                    component.set("v.SelectedCPlength",true);
                }else{
                    component.set("v.SelectedCPlength",false);
                }
                component.set("v.UnRegisterviewList",result.CPSelection); 
                if(result.CPSelection.length>0){
                    component.set("v.UnRegisterviewListlength",true);
                }else{
                    component.set("v.UnRegisterviewListlength",false);
                }
                
                if(!component.get("v.UnRegisterviewListlength") && !component.get("v.SelectedCPlength"))
                {
                   
                   component.set("v.errorchk", true);
                }else{
                    component.set("v.errorchk", false);
                    component.set("v.benefits1",false);
                    component.set("v.userinfo1",true);
                    component.set("v.clrbenefits1",'completed');
                    component.set("v.clrud1",'active1');
                }
                
            }
        });
        $A.enqueueAction(action); 
        
    },
    
    
    backtoben: function(component, event) { 
        component.set("v.benefits",true);
        component.set("v.userinfo",false);
        component.set("v.clrbenefits",'active1');
        component.set("v.clrud",'');
    }, backtoben1: function(component, event) { 
        component.set("v.benefits1",true);
        component.set("v.userinfo1",false);
        component.set("v.clrbenefits1",'active1');
        component.set("v.clrud",'');
    },
    nexttosetpwd: function(component, event) { 
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        
        //var FirstName = component.find("FirstName").get("v.value");
        //var sitename = component.find("sitename").get("v.value");
       // var email = component.find("email").get("v.value");
        //var lastname = component.find("lastname").get("v.value");
        //var swiftcode = component.find("swiftcode").get("v.value");
        //var location = component.find("location").get("v.value");
        var phone = component.find("phone").get("v.value");   
        var Mobile = component.find("Mobile").get("v.value");  
        var countrycode = component.find("auracountrycode").get("v.value");
        var mobilecountrycode = component.find("auramobilecountrycode").get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        
        
        /*if(FirstName == '' || FirstName == 'undefined' || FirstName == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.FirstNameError", 'Input field required'); 
        } else { 
            component.set("v.FirstNameError", ''); 
        } 
        
        if(sitename == '' || sitename == 'undefined' || sitename == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LegalNameError", 'Input field required'); 
        } else { 
            component.set("v.LegalNameError", ''); 
        } 
        
        if(lastname == '' || lastname == 'undefined' || lastname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LastNameError", 'Input field required'); 
        } else { 
            component.set("v.LastNameError", ''); 
        } 
        if(swiftcode == '' || swiftcode == 'undefined' || swiftcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.SwiftCodeError", 'Input field required'); 
        } else { 
            component.set("v.SwiftCodeError", ''); 
        } */
        if(!$A.util.isEmpty(phone))
        {   
            if(countrycode == '' || countrycode =='--Select--' || countrycode == 'undefined' || countrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.countrycodeError", 'Input field required'); 
            } else { 
                component.set("v.countrycodeError", ''); 
            }
            
            if(!phone.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please enter valid phone number'); 
            } 
            else if(phone.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.PhoneError", ''); 
                }
        }
        if(!$A.util.isEmpty(Mobile))
        {   
            if(mobilecountrycode == '' || mobilecountrycode =='--Select--' || mobilecountrycode == 'undefined' || mobilecountrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.mobilecountrycodeError", 'Input field required'); 
            } else { 
                component.set("v.mobilecountrycodeError", ''); 
            }            
            if(!Mobile.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please enter valid Mobile number'); 
            } 
            else if(Mobile.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.MobileError", ''); 
                }
        }
        if(errorFlag == 'false') {
            
            
            
            var action = component.get('c.UpdateRegForm');  
            action.setParams({
                "LoggedUserID" : localStorage.getItem('UserSession'),            
                "UM" : component.get("v.UM")
            });
            debugger;
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.newcounterPopup", false);
                    component.set("v.onboard", true);
                    
                    component.set("v.userinfo",false);
                    component.set("v.setpwd",true);
                    component.set("v.clrbenefits",'completed');
                    component.set("v.clrud",'completed');
                    component.set("v.clrspwd",'active1');
                    
                }
            });
            debugger;
            $A.enqueueAction(action);
        }
    },
    declinequote: function(component, event) { 
        
         component.set("v.rejectpopup",true);
    },
    rerequestquote: function(component, event) { 
        component.set("v.rereqRFQ",event.target.id);
        component.set("v.reRFQ",true);
    },
    RFQrecancel: function(component, event) { 
        
        component.set("v.reRFQ",false);
    },
    RFQresave: function(component, event) { 
        
         var action = component.get('c.RerequstforRFQ');
            var self = this; 
            action.setParams({
                "offerId" : component.get("v.TransId"),
                "LoggedUserId" : localStorage.getItem('UserSession'),
                "RreqRFQID" :component.get("v.rereqRFQ")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                debugger;
                if(state === "SUCCESS"){  
                    component.set("v.reRFQ",true);
                    $A.get('e.force:refreshView').fire();
                }
            }); 
            $A.enqueueAction(action);   
        
    },
    
    rejectsuccess: function(component, event) { 
        var errorFlag = 'false';
        var deccmt = component.find("declinecmt").get("v.value");
        
        
        if(deccmt == undefined || deccmt == ""){
            errorFlag = 'true';
            component.set("v.saverejectnotes",'Input field required');
        }else{
            component.set("v.saverejectnotes",'');
        }
        if(errorFlag == 'false') {
            debugger;
            var action = component.get('c.declinequotemethod');
            var self = this; 
            action.setParams({
                "offerId" : component.get("v.TransId"),
                "LoggedUserId" : localStorage.getItem('UserSession'),
                "comments" :deccmt
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                debugger;
                if(state === "SUCCESS"){  
                    component.set("v.rejectpopup",false);
                    $A.get('e.force:refreshView').fire();
                }
            }); 
            $A.enqueueAction(action);   
        }
    },
    
    rejectcancel: function(component, event) { 
        
         component.set("v.rejectpopup",false);
    },
    backtouser: function(component, event) { 
        debugger;
        component.set("v.userinfo",true);
        component.set("v.setpwd",false);
        component.set("v.clrbenefits",'completed');
        component.set("v.clrud",'active1');
        component.set("v.clrspwd",'');
    },
    nexttofinish: function(component, event) { 
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var newPassword = component.find("newPassword").get("v.value");
        var confirmPassword = component.find("confirmPassword").get("v.value"); 
        var re = new RegExp("^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$");
        
        if(newPassword == undefined || newPassword == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.newpassworderror",'Input field required');
        }else{
            component.set("v.newpassworderror",'');
        }
        if(confirmPassword == undefined || confirmPassword == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.confirmpassworderror",'Input field required');
        }
        else if(newPassword != confirmPassword){  
            errorFlag = 'true';
            component.set("v.confirmpassworderror",'');
            component.set("v.changepasswordvalidation",true);
            component.set("v.validation", false);
            component.set("v.combinationvalidation",false);
        }
            else if (!re.test(confirmPassword)) {
                errorFlag = 'true';
                component.set("v.combinationvalidation",true);
                component.set("v.changepasswordvalidation",false);
                component.set("v.validation",false);
            }
        
        
        if(errorFlag == 'false') {
            var action = component.get('c.UpdateLBFROMMain');
            var self = this; 
            action.setParams({
                "ID" : localStorage.getItem("UID"),               
                "newPassword" : newPassword
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                debugger;
                if(state === "SUCCESS"){       
                    component.set("v.setpwd",false);
                    component.set("v.finish",true);
                    component.set("v.clrbenefits",'completed');
                    component.set("v.clrud",'completed');
                    component.set("v.clrspwd",'completed');
                    component.set("v.clrfinish",'active1');
                }
            }); 
            $A.enqueueAction(action);    
        } 
    },
    finishben: function(component, event) { 
        debugger;
        component.set("v.clrbenefits",'');
        component.set("v.clrud",'');
        component.set("v.clrspwd",'');
        component.set("v.clrfinish",'');
        component.set("v.Customer",false);
        $A.get('e.force:refreshView').fire();
    },
    closeregistration: function(component, event) { 
        component.set("v.clrbenefits",'');
        component.set("v.clrud",'');
        component.set("v.clrspwd",'');
        component.set("v.clrfinish",'');
        component.set("v.Customer",false);
    },
    getOfferDetails: function(component, event, helper) { 
        debugger;
        // component.set("v.bindtabnamesforoff",'Offers');
        var action = component.get('c.OfferDetails');
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        };
        
        var offerId = getUrlParameter('transactionId');
        
        var status = getUrlParameter('Status');
        
        var self = this; 
        action.setParams({
            "offerId" : offerId
        });
        
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            debugger;
            if(state === "SUCCESS"){
                
                var nextAction = component.get("c.secondarymarketingflag"); 
                debugger;
                nextAction.setCallback(this, function(actionResult1) {
                    var state1 = actionResult1.getState(); 
                    var results = actionResult1.getReturnValue();
                    if(state1 === "SUCCESS"){
                        debugger;
                        if(results)
                        {
                            component.set("v.bindtabnamesforoff",'Quotes');
                            component.set("v.bindtabnamesfornotes",'Internal Memos');
                            component.set("v.bindtabnamesforPart",'Participant');
                            
                        }else{
                            component.set("v.bindtabnamesforoff",'Quotes');
                            component.set("v.bindtabnamesfornotes",'Internal Memos');
                            component.set("v.bindtabnamesforPart",'Counterparties');
                        }
                        component.set("v.issecondaryMarket",results);
                        component.set("v.issecondaryMarketflag",true);
                    }
                });
                $A.enqueueAction(nextAction);
                //alert(result.OfferExtended.Offer.Offer_Status__c);
                var Transstatus=result.OfferExtended.Offer.Offer_Status__c;
                component.set("v.Created",result.OfferExtended.Offer.CreatedDate);
                
                var appdate = result.OfferExtended.Offer.Approvedate__c;
                //alert(appdate);
                if(appdate!='undefined' || appdate!='' )
                {
                    component.set("v.approveddate",appdate);
                }
                
                
                var valdate = result.OfferExtended.Offer.validateddate__c;
                if(valdate=='undefined' || valdate!='' )
                {  
                    component.set("v.Validateddate",valdate);
                }
                //alert(Transstatus);
                component.set("v.Tstatus",Transstatus);
                
                debugger;
                var Bidstatus='';
                var Bidstatus1='';
                if(result.OfferExtended.Syndications.length > 0)
                {
                    if(result.OfferExtended.Syndications[0].Syndication.Bid_Status__c=='Bid Closed'){
                        Bidstatus ='Bid Closed';
                    }
                    if(result.OfferExtended.Syndications[0].Syndication.Bid_Status__c=='Bid Accepted'){
                        Bidstatus1 ='Bid Accepted';
                    }
                }
                if(Transstatus==='Transaction Created' || Transstatus==='Pending Validation' || Transstatus==='Level 1 - Pending Approval' 
                   || Transstatus==='Pending Approval' || Transstatus==='Validation Rejected' || Transstatus==='Transaction Rejected')
                {
                    component.set("v.Tprogress", "process");
                    component.set("v.Tbox", "p-card progress-border");
                    component.set("v.hTstatus","spinneryes");
                } else if(Transstatus == 'Inactive'){
                    component.set("v.Tprogress", "red");
                    component.set("v.Tbox", "p-card red-border");
                    //component.set("v.hTstatus","spinneryes"); 
                    
                }
                
                    else if(Transstatus==='Transaction Approved' || Transstatus==='Requested For Info')
                    {   
                        if(Transstatus==='Transaction Approved'){
                            component.set("v.RFI","Requested For Info / Quote");
                        }else{
                            component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                            component.set("v.RequestedBy",result.OfferExtended.CreditUnion.CU_Name__c);
                            
                            component.set("v.RFI","Requested For Info");
                        }
                        component.set("v.Tprogress", "active");
                        component.set("v.Tbox", "p-card active-border");
                        component.set("v.RFIprogress", "process");
                        component.set("v.RFIbox", "p-card progress-border");
                        component.set("v.hTstatus","spinnerno");
                        component.set("v.hRFQstatus","spinneryes");
                        component.set("v.FullRFIstatus",true);
                    }      
                        else if(Transstatus==='Requested For Quote' || Transstatus==='Bid Submitted')
                        {
                            
                            component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                            component.set("v.RequestedBy",result.OfferExtended.CreditUnion.CU_Name__c);
                            component.set("v.Tprogress", "active");
                            component.set("v.RFIprogress", "active");
                            component.set("v.RFIbox", "p-card active-border");
                            component.set("v.hTstatus","spinnerno");
                            component.set("v.hRFQstatus","spinnerno");
                            component.set("v.Tbox", "p-card active-border");
                            component.set("v.Bidprogress", "process");
                            component.set("v.Bidbox", "p-card progress-border");
                            component.set("v.hTstatus","spinnerno");
                            component.set("v.RFI","Requested For Quote");
                            component.set("v.hRFQstatus","spinnerno");
                            component.set("v.hbidstatus","spinneryes");
                            component.set("v.FullRFIstatus",true);
                            component.set("v.FullBidstatus",true); 
                            if(result.OfferExtended.Syndications.length > 0 && result.LogeduserCBId !=''){
                                component.set("v.Tstatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                            }
                            if(Transstatus==='Bid Submitted')
                            {
                                if(result.OfferExtended.Syndications.length > 0){
                                    component.set("v.BidStatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                    component.set("v.LastBidDate", result.OfferExtended.Syndications[0].Syndication.submitted_Date__c);
                                    component.set("v.HighestBidder",result.OfferExtended.Syndications[0].Syndication.Buyer_Name__c);
                                    component.set("v.progressBidamount",result.OfferExtended.Syndications[0].Syndication.Buyer_Counter_Price__c);
                                }
                            }
                            
                            
                        }
                            else if(Transstatus==='Bid Accepted')
                            {
                                
                                if(result.OfferExtended.Syndications.length > 0 && result.LogeduserCBId !=''){
                                    component.set("v.Tstatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                }
                                if(Bidstatus1 =='Bid Accepted' && result.LogeduserCBId !='' || result.LogeduserCBId==''){
                                    if(result.OfferExtended.Syndications.length > 0){
                                        
                                        component.set("v.BidStatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                        component.set("v.LastBidDate", result.OfferExtended.Syndications[0].Syndication.submitted_Date__c);
                                        component.set("v.HighestBidder",result.OfferExtended.Syndications[0].Syndication.Buyer_Name__c);
                                        component.set("v.progressBidamount",result.OfferExtended.Syndications[0].Syndication.Buyer_Counter_Price__c);
                                        
                                    }
                                    component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                                    component.set("v.RequestedBy",result.OfferExtended.CreditUnion.CU_Name__c);
                                    component.set("v.Tprogress", "active");
                                    component.set("v.RFIprogress", "active");
                                    component.set("v.RFIbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.hRFQstatus","spinnerno");
                                    component.set("v.Tbox", "p-card active-border");
                                    component.set("v.Bidprogress", "active");
                                    component.set("v.Bidbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.RFI","Requested For Quote");
                                    component.set("v.comprogress", "process");
                                    component.set("v.combox", "p-card progress-border");
                                    component.set("v.hcomstatus","spinneryes");
                                    component.set("v.hbidstatus","spinnerno");
                                    component.set("v.FullRFIstatus",true);
                                    component.set("v.FullBidstatus",true); 
                                    component.set("v.Fullcomstatus",true);
                                }else{
                                    //component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                                    //component.set("v.RequestedBy",result.OfferExtended.CreditUnion.CU_Name__c);
                                    component.set("v.Tprogress", "active");
                                    component.set("v.RFIprogress", "active");
                                    component.set("v.RFIbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.hRFQstatus","spinnerno");
                                    component.set("v.Tbox", "p-card active-border");
                                    component.set("v.Bidprogress", "active");
                                    component.set("v.Bidbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.RFI","Requested For Quote");
                                    component.set("v.comprogress", "process");
                                    component.set("v.combox", "p-card progress-border");
                                    component.set("v.hcomstatus","spinneryes");
                                    component.set("v.hbidstatus","spinnerno");
                                    component.set("v.FullRFIstatus",true);
                                    component.set("v.FullBidstatus",true); 
                                    component.set("v.Fullcomstatus",false);
                                }
                            }else if(Transstatus==='Completed Transaction')
                            {
                                
                                if(result.OfferExtended.Syndications.length > 0 && result.LogeduserCBId !=''){
                                    component.set("v.Tstatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                }
                                if(Bidstatus =='Bid Closed' && result.LogeduserCBId !='' || result.LogeduserCBId==''){                                
                                    component.set("v.FullRFIstatus",true);
                                    component.set("v.FullBidstatus",true); 
                                    component.set("v.Fullcomstatus",true);
                                    debugger;
                                    component.set("v.Completed",result.finalizedOfferdate);
                                    component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                                    component.set("v.RequestedBy",result.OfferExtended.CreditUnion.CU_Name__c);
                                    component.set("v.Tprogress", "active");
                                    component.set("v.RFIprogress", "active");
                                    component.set("v.RFIbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.hRFQstatus","spinnerno");
                                    component.set("v.Tbox", "p-card active-border");
                                    component.set("v.Bidprogress", "active");
                                    component.set("v.Bidbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.RFI","Requested For Quote");
                                    component.set("v.hcomstatus","spinnerno");
                                    component.set("v.comprogress", "active");
                                    component.set("v.combox", "p-card active-border");
                                    component.set("v.hbidstatus","spinnerno");
                                    if(result.OfferExtended.Syndications.length > 0)
                                    {
                                        if(result.LogeduserCBId !=''){
                                            component.set("v.Tstatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                        }
                                        component.set("v.BidStatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                        component.set("v.LastBidDate", result.OfferExtended.Syndications[0].Syndication.submitted_Date__c);
                                        component.set("v.HighestBidder",result.OfferExtended.Syndications[0].Syndication.Buyer_Name__c);
                                        component.set("v.progressBidamount",result.OfferExtended.Syndications[0].Syndication.Buyer_Counter_Price__c);
                                    }
                                }else{
                                    component.set("v.FullRFIstatus",true);
                                    component.set("v.FullBidstatus",true); 
                                    component.set("v.Fullcomstatus",false);
                                    debugger;
                                    component.set("v.Completed",result.finalizedOfferdate);
                                    component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                                    component.set("v.RequestedBy",result.OfferExtended.CreditUnion.CU_Name__c);
                                    component.set("v.Tprogress", "active");
                                    component.set("v.RFIprogress", "active");
                                    component.set("v.RFIbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.hRFQstatus","spinnerno");
                                    component.set("v.Tbox", "p-card active-border");
                                    component.set("v.Bidprogress", "active");
                                    component.set("v.Bidbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.RFI","Requested For Quote");
                                    component.set("v.comprogress", "process");
                                    component.set("v.combox", "p-card progress-border");
                                    component.set("v.hcomstatus","spinneryes");
                                    component.set("v.hbidstatus","spinnerno");
                                }
                            }
                
                debugger;
                debugger;
                var container = component.find("container");
                $A.util.removeClass(container, "slds-hide");
                $A.util.addClass(container, "slds-show");
                
                $A.createComponent("c:"+result.OfferExtended.Offer.Component_Name__c,
                                   {
                                       "aura:id": 'MyCustomComponent',
                                       "edit": 'false',
                                       "mode": 'VIEW',
                                       "objectApiName": result.OfferExtended.Offer.SObject_Name__c,
                                       "recordId": result.OfferExtended.Offer.Record_Id__c
                                   },
                                   function(newComponent, status, errorMessage){ 
                                       debugger;
                                       var targetCmp = component.find('placeHolder');
                                       
                                       targetCmp.set("v.body", []); 
                                       var body = targetCmp.get("v.body");
                                       console.log('@body'+body);          
                                       if((result.isBuyer || result.isBuyerValidator) && !result.OfferExtended.Offer.RFQChk__c && !(result.OfferExtended.Offer.Granted__c))
                                       {
                                           //if(result.OfferExtended.Offer.Request_For_Information__c!=NULL){
                                           debugger;
                                           var json = JSON.parse(result.OfferExtended.Offer.Request_For_Information__c);
                                           for(var i = 0; i < json.length; i++) {
                                               var obj = json[i];
                                               if(obj.Flag=='TRUE'){
                                                   var hidediv = newComponent.find('div'+obj.ID);
                                                   if(hidediv){
                                                       $A.util.addClass(hidediv, 'hide');
                                                   }
                                               }
                                               
                                           }
                                           //}
                                       }
                                       body.push(newComponent);
                                       targetCmp.set("v.body", body); 
                                       
                                       
                                   });   
                debugger;
                component.set("v.Offer", result);
                
                // if(result.isBuyerValidator || result.isBuyer){
                //     component.set("v.RNSize",result.requestNotes.length);
                // }
                
                if(result.OfferExtended.Offer.Seller_Notes__c !=''){
                    //component.set("v.corporateKeyword",result.Offers.BankMSg);
                    component.set("v.editcall",'no');
                }
                else
                {
                    component.set("v.editcall",'yes');
                }
                //alert(result.LogeduserID);
                component.set("v.Loggeduserid",result.LogeduserID);
                
                document.title = result.OfferExtended.Offer.TransactionRefNumber__c;
                component.set("v.shareSubject",'TradeAix : Transaction Details - '+result.OfferExtended.Offer.TransactionRefNumber__c);
                component.set("v.sharebodytxt",'Hello,  I am sharing this transaction from TradeAix platform and would like you to have a look at the attached PDF document and let me know your feedback.  Regards , '+result.OfferExtended.CreditUnion.CU_Name__c);
                
                component.set("v.offerId", result.offerId); 
                component.set("v.parentId", result.offerId);
                component.set("v.SellerUserId", result.OfferExtended.Offer.CreatedById);
                debugger;
                component.set("v.validation", false);
                component.set("v.loanError", '');
                component.set("v.templateError", '');
                component.set("v.nameError", '');    
                component.set("v.participationAmountError", '');
                component.set("v.participationPercentageError", '');
                component.set("v.LogeduserCUnionId", result.LogeduserCBId);
                component.set("v.credtiunionchk", result.credtiunionchk);
                component.set("v.PartyID", result.OfferExtended.CreditUnion.Id);
                component.set("v.Partname", result.OfferExtended.CreditUnion.CU_Name__c);
                if(result.isSeller) {
                    debugger;    
                    component.set("v.Quotelength", result.OfferExtended.Syndications.length);
                    component.set("v.attlen", result.OfferExtended.AttachmentRequests.length);
                    try{ component.set("v.selectedTabId", "0"); } catch(ex){}
                } else {
                    component.set("v.selectedTabId", "1");
                }
                //changeParticipationAmount(component, event, helper);
                //changeParticipationPercentage(component, event, helper);
            }
            var menu = document.getElementById('Transactions');
            var classArr = menu.className.split(" ");
            if (classArr.indexOf("current") == -1) {
                menu.className += " " + "current";
            }
            
        }); 
        $A.enqueueAction(action);
    },  
    savecorporatetxt : function(component, event,helper) { 
        debugger;
        var txtcorporate = component.find("cptext").get("v.value"); 
        // alert(txtcorporate);
        var errorFlag = 'false';
        if(txtcorporate == '' || txtcorporate == null || txtcorporate == undefined || txtcorporate.trim().length*1 == 0) 
        {  
            errorFlag = 'true';
            component.set("v.txtcor", 'Input field required'); 
        }
        else{  
            component.set("v.txtcor", ''); 
        }  
        if(errorFlag == 'false')
        { 
            var action = component.get('c.savesavecorporatetxt'); 
            action.setParams({ 
                "txtcor" : txtcorporate,
                "offerId" : component.get("v.offerId")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    debugger;
                    $A.get('e.force:refreshView').fire();
                    
                } 
            });
            $A.enqueueAction(action);
        }
    }, 
    handleResponse : function(component, event, helper) {
        var response = event.getParam("response");
    },
    uploadfilepopup: function(component, event) {     
        debugger;
        component.set("v.opendocvault", true); 
    },
    editclick: function(component){
        //component.set("v.BankMSg",'');
        component.set("v.editcall",'yes');
    },
    cancelcorporatetxt: function(component){
        debugger;
        //component.set("v.BankMSg",'');
        component.set("v.editcall",'no');
    },
    btncancelfor: function(component, event) {     
        debugger;
        component.set("v.opendocvault", false); 
    },
    CPrevokeCheck: function(component, event, helper) {
        /*  if(component.find("CPrevokeAccess").get('v.value')==true)
        {
            component.find("CPrevoke").set("v.disabled", false);
        }
        else{
            component.find("CPrevoke").set("v.disabled", true);
        }*/
    },
    CPConfirmcan: function(component, event) {
        component.set("v.CPConfirm", false);
    },
    downloadCP: function(component, event) {
        var action = component.get("c.downloadCP"); 
        action.setParams({
            "offerId" : event.target.id           
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){
            }
        });
    },
    CPConfirmpopup : function(component, event) { 
        //alert(event.target.id);
        component.set("v.counterPopup", false);
        component.set("v.CPConfirm", true);
        
    }, 
    
    CPrevokecancel: function(component, event) {
        component.set("v.CPrevokeCheckpop", false);
    },
    
    CPrevokepopup : function(component, event) { 
        //alert(event.target.id);
        debugger;
        component.set("v.CPID",event.target.id);
        component.set("v.CPrevokeCheckpop", true);       
    },  
    removeCP: function(component, event) {     
        debugger;       
        var action = component.get("c.removeCPMethod"); 
        action.setParams({
            "offerId" : component.get("v.TransId"),
            "counterids" :component.get("v.CPID"),
            "LoggedUserId" : localStorage.getItem('UserSession')
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){
                $A.get('e.force:refreshView').fire();
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Counterparties Sucessfully Revoked',
                        'type': 'Success'
                    }
                );
                showToast.fire();
            }});
        $A.enqueueAction(action);
        
    },
     recall : function(component, event) {
         
          component.set("v.recallpopup", true); 
     },cancelrecall : function(component, event) {
          component.set("v.recallpopup", false); 
     },recallsubmit : function(component, event) {
         
         debugger;
         var action = component.get('c.recallupdate');
         var self = this;
         action.setParams({
             "Transid" : component.get("v.TransId")          
         });
         action.setCallback(this, function(actionResult) { 
             var state = actionResult.getState(); 
             var result = actionResult.getReturnValue();
             if(result === "Success"){
                 component.set("v.recallpopup", false); 
                 $A.get('e.force:refreshView').fire();
             }
         });
         $A.enqueueAction(action);
     },
    requestForInfo : function(component, event) {
        
        debugger;
        console.log('@inside info@');
        if(component.get("v.recallflag")==true)
        {
            component.set("v.publishOffer", true); 
            component.set("v.POPrequestForInfo", false); 
        }else{
            component.set("v.POPrequestForInfo", true); 
         
        var contentVersion = component.get("v.Offer.content");
        var attachment = component.get("v.Offer.attachment");
        var errorFlag = 'false';
        var counterParties = component.get("v.Transactions.CounterParties");
        if(errorFlag == 'true') 
        {
            component.set("v.PubEditError", 'One or more fields below do not have appropriate data');
        }
        else{ 
            /*
            if((contentVersion.length > 0 || attachment.length > 0) && counterParties.length > 0 && errorFlag == 'false') 
            {
                component.set("v.publishOffer", true); 
            }         
            else
            {
                if(counterParties.length <= 0){
                    component.set("v.counterError", true);
                } else {
                    component.set("v.publishError", true);
                }
            }
            */
            
            if((counterParties.length > 0 || component.get("v.Transactions.CPSelection").length >0) && errorFlag == 'false') 
            {
                component.set("v.POPrequestForInfo", true); 
                //component.set("v.publishOffer", true); 
            }         
            else
            {
                if(counterParties.length <= 0 || component.get("v.Transactions.CPSelection").length <=0){
                    component.set("v.counterError", true);
                    component.find("tabs").set("v.Activetab",3);
                } 
            }
        }
        var action = component.get('c.getTransactionfields');
        //var Tid='a07m0000008DkAbAAK';
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        };
        var Tid=getUrlParameter('transactionId'); 
        console.log('@@'+Tid);
        debugger;
        var self = this;
        action.setParams({
            "Transid" : Tid          
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                console.log('@@'+result);
                //component.set("v.templateforRFI", result);
                var custs = [];
                for(var key in result){
                    debugger;
                    custs.push({value:result[key], key:key}); //Here we are creating the list to show on UI.
                }
                component.set("v.lensize",custs.length%2); 
                component.set("v.templateforRFI",custs); 
            }
            else if(state === "ERROR") {
                debugger;
            }
        });
        $A.enqueueAction(action);
        }
    },
    onCheck: function(component, event) { 
        debugger;
        var checkboxes = document.getElementsByClassName("chk");
        
        var chkTF = [];
        for(var i = 0; i < checkboxes.length; i++)
        {  
            if (checkboxes[i].checked) {
                
                console.log('##'+event.target.value);
                
                chkTF.push(event.target.value);
            }
            
            
        } 
        
        component.set("v.CheckedTfields", chkTF);
        var d=component.get("v.CheckedTfields");
        onsole.log('#&#'+d);
    },	
    saveTR: function(component, event) {
        debugger;
        var jsonObj='';
        var getAllId = component.find("boxPack");
        var len=getAllId.length-1;
        var j = 0;
        for (var i = 0; i < getAllId.length; i++) {        
            
            if (getAllId[i].get("v.value") == true) {
                jsonObj=jsonObj+getAllId[i].get("v.text")+',';
                j++;
            }
            
        }  
       
        component.set('v.maskcount',j);
        debugger;
        var action = component.get('c.SaveRFI'); 
        
        action.setParams({
            "RFI" : jsonObj,
            "Transid" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.POPrequestForInfo", false); 
                component.set("v.publishOffer", true); 
            }
        });
        $A.enqueueAction(action);
    },
    publishcancel : function(component, event) {
        component.set("v.publishOffer", false);
    },
    btnpublish: function(component, event, helper) {
        debugger;
        var action = component.get('c.publish');
        var self = this; 
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "offerId" : component.get("v.TransId"),
            "requestForInfo": component.get("v.requestForInfo")            
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.publishOffer", false);
            component.set("v.SecondpublishPopup", true);
            component.set("v.requestForInfo", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);
                component.set("v.offerId", result.offerId);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    publishingOffer : function(component, event) {
        debugger;
        component.set("v.requestForInfo", false); 
        var contentVersion = component.get("v.Offer.content");
        var attachment = component.get("v.Offer.attachment");
        var errorFlag = 'false';
        
        var counterParties = component.get("v.Transactions.CounterParties");
        
        if(errorFlag == 'true') 
        {
            component.set("v.PubEditError", 'One or more fields below do not have appropriate data');
        }
        else{ 
            /*
            if((contentVersion.length > 0 || attachment.length > 0) && counterParties.length > 0 && errorFlag == 'false') 
            {
                component.set("v.publishOffer", true); 
            }         
            else
            {
                if(counterParties.length <= 0){
                    component.set("v.counterError", true);
                } else {
                    component.set("v.publishError", true);
                }
            }
            */
            
            if((counterParties.length > 0 || component.get("v.Transactions.CPSelection").length >0) && errorFlag == 'false') 
            {
                //component.set("v.requestForInfo", true); 
                component.set("v.publishOfferRFQ", true); 
            }         
            else
            {
                if(counterParties.length <= 0 || component.get("v.Transactions.CPSelection").length <=0){
                    debugger;
                    component.set("v.counterError", true);
                    component.find("tabs").set("v.Activetab","3");
                    
                } 
            }
        }
    },
    counterErrorcancel : function(component, event) {
        debugger;
        component.set("v.counterError", false);
        component.find("tabs").set("v.Activetab","3");
    },
    SecondpublishPopupcancel : function(component, event) {
        component.set("v.SecondpublishPopup", false);
        $A.get('e.force:refreshView').fire();
    },
    completePopupcancel : function(component, event) {
        component.set("v.CompletePopup", false);
        $A.get('e.force:refreshView').fire();
    },
    publishErrorcancel : function(component, event) {
        component.set("v.publishError", false);
    }, 
    publishcancel : function(component, event) {
        component.set("v.publishOffer", false);
    }, 
    selectdocuvault : function(component, event) {
        component.set("v.popdocuvault", true);
    },
    canceldocuvault : function(component, event) {
        component.set("v.popdocuvault", false);
    },
    selectChangerecall : function(component, event, helper) {
        if(component.find("chk1recall").get('v.value')==true)
        {
            component.find("btn1recall").set("v.disabled", false);
        }
        else{
            component.find("btn1recall").set("v.disabled", true);
        }
    }, 
    selectChange : function(component, event, helper) {
        if(component.find("chk1").get('v.value')==true)
        {
            component.find("btn1").set("v.disabled", false);
        }
        else{
            component.find("btn1").set("v.disabled", true);
        }
    }, 
    
    btnpublishRFQ: function(component, event, helper) {
        debugger;
        component.find("btn1").set("v.disabled", true);
        var action = component.get('c.publishRFQ'); 
        var self = this; 
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "offerId" : component.get("v.TransId"),
            "requestForInfo": component.get("v.requestForInfo")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.publishOfferRFQ", false);
            component.set("v.SecondpublishPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);
                component.set("v.offerId", result.offerId);  
                component.set("v.parentId", result.offerId);
                $A.get('e.force:refreshView').fire();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    publishcancel : function(component, event) {
        component.set("v.publishOffer", false);
    }, 
    publishcancelRFQ : function(component, event) {
        component.set("v.publishOfferRFQ", false);
    }, 
    UnpublishOffer : function(component, event) { 
        component.set("v.Unpublish", true);
    }, 
    unpublishCheck: function(component, event, helper) {
        if(component.find("chkUnpublish").get('v.value')==true)
        {
            component.find("Unpublishbtn1").set("v.disabled", false);
        }
        else{
            component.find("Unpublishbtn1").set("v.disabled", true);
        }
    }, 
    SecondUnpublishPopupcancel : function(component, event) {
        component.set("v.SecondUnpublishPopup", false);
    },
    btnUnpublish: function(component, event, helper) {
        var action = component.get('c.Unpublish'); 
        var self = this; 
        action.setParams({
            "offerId" : component.get("v.offerId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.Unpublish", false);
            component.set("v.SecondUnpublishPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);
                component.set("v.offerId", result.offerId);  
                component.set("v.parentId", result.offerId);               
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    Unpublishcancel: function(component, event) {
        component.set("v.Unpublish", false);
    },
    InactiveOffer1 : function(component, event) { 
        component.set("v.Inactive1", true);       
    },    
    inactiveCheck: function(component, event, helper) {
        if(component.find("chkInactive").get('v.value')==true)
        {
            debugger;
            component.find("Inactivebtn1").set("v.disabled", "false");
        }
        else{
            debugger;
            component.find("Inactivebtn1").set("v.disabled", "true");
        }
    },
    btnInactive: function(component, event, helper) {
        var action = component.get('c.Inactive'); 
        var self = this; 
        action.setParams({
            "offerId" : component.get("v.offerId"),
            "body5" : component.find("body5").get("v.value")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.Inactive1", false);
            component.set("v.SecondInactive", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);
                component.set("v.offerId", result.offerId); 
                component.set("v.parentId", result.offerId);                
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    Inactivecancel: function(component, event) {
        component.set("v.Inactive1", false);
    },
    SecondInactivecancel: function(component, event) {
        component.set("v.SecondInactive", false);
    },
    reqAccess : function(component, event) { 
        component.set("v.reqAccess", true);       
    },    
    reqCheck: function(component, event, helper) {
        if(component.find("ChkreqAccess").get('v.value')==true)
        {
            component.find("Accessbtn1").set("v.disabled", false);
        }
        else{
            component.find("Accessbtn1").set("v.disabled", true);
        }
    },
    btnreqAccess: function(component, event, helper) {
        var action = component.get('c.RequestAccess'); 
        var self = this; 
        action.setParams({
            "offerId" : component.get("v.offerId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.reqAccess", false);
            component.set("v.SecondRequestPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);
                component.set("v.offerId", result.offerId);  
                component.set("v.parentId", result.offerId);               
            }
            else if(state === "ERROR") { 
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },  
    reqAccesscancel: function(component, event) {
        component.set("v.reqAccess", false);
    },
    SecondRequestPopupcancel: function(component, event) {
        component.set("v.SecondRequestPopup", false);
    },
    grantAccess : function(component, event) {
        component.set("v.docId", event.target.id); 
        component.set("v.grantAccess", true);       
    },     
    grantCheck: function(component, event, helper) {
        if(component.find("chkGrant").get('v.value')==true)
        {
            component.find("Grantbtn1").set("v.disabled", false);
        }
        else{
            component.find("Grantbtn1").set("v.disabled", true);
        }
    }, 
    btnRequestForQuote: function(component, event, helper) {
        debugger;
        component.set("v.docId", event.target.id); 
        var action = component.get('c.GrantAccessToRFQ');
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "offerId" : component.get("v.TransId"),
            "docId" : component.get("v.docId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.SecondGrantPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }             
        });
        
        $A.enqueueAction(action);
    }, 
    btngrantAccess: function(component, event, helper) {
        var action = component.get('c.GrantAccess'); 
        var self = this; 
        action.setParams({
            "docId" : component.get("v.docId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.grantAccess", false);
            component.set("v.SecondGrantPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }             
        });
        $A.enqueueAction(action);
    }, 
    grantAccesscancel: function(component, event) {
        component.set("v.grantAccess", false);
        $A.get('e.force:refreshView').fire();
    },
    SecondGrantPopupcancel: function(component, event) {
        component.set("v.SecondGrantPopup", false);
        $A.get('e.force:refreshView').fire();
    },
    RejectAccess : function(component, event) { 
        component.set("v.docId", event.target.id);
        component.set("v.RejectAccess", true);       
    },    
    rejectCheck: function(component, event, helper) {
        if(component.find("chkReject").get('v.value')==true)
        {
            component.find("Rejectbtn1").set("v.disabled", false);
        }
        else{
            component.find("Rejectbtn1").set("v.disabled", true);
        }
    },
    btnrejectAccess: function(component, event, helper) {
        var action = component.get('c.rejectAccess'); 
        var self = this; 
        action.setParams({
            "docId" : component.get("v.docId"),
            "body3" : component.find("body3").get("v.value")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue(); 
            component.set("v.RejectAccess", false);
            component.set("v.SecondRejectPopup", true);
            $A.log(actionResult);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    rejectAccesscancel: function(component, event) {
        component.set("v.RejectAccess", false);
    },
    POPrequestForInfocel: function(component, event) {
        component.set("v.POPrequestForInfo", false);
    },
    
    SecondRejectPopupcancel: function(component, event) {
        component.set("v.SecondRejectPopup", false);
    },
    RevokeAccess : function(component, event) { 
        component.set("v.docId", event.target.id);
        component.set("v.RevokeAccess", true);       
    },   
    revokeCheck: function(component, event, helper) {
        if(component.find("chkRevoke").get('v.value')==true)
        {
            component.find("Revokebtn1").set("v.disabled", false);
        }
        else{
            component.find("Revokebtn1").set("v.disabled", true);
        }
    }, 
    btnrevokeAccess: function(component, event, helper) {
        var action = component.get('c.revokeAccess'); 
        var self = this; 
        action.setParams({
            "docId" : component.get("v.docId"),
            "body4" : component.find("body4").get("v.value")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.RevokeAccess", false);
            component.set("v.SecondRevokePopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    revokeAccesscancel: function(component, event) {
        component.set("v.RevokeAccess", false);
    }, 
    SecondRevokePopupcancel: function(component, event) {
        component.set("v.SecondRevokePopup", false);
    },
    BidWithdrawn : function(component, event) { 
        component.set("v.syndId", event.target.id); 
        component.set("v.BidWithdrawn", true);       
    },     
    BidWidrawnCheck: function(component, event, helper) {
        if(component.find("chkBidWidrawn").get('v.value')==true)
        {
            component.find("BidWidrawnbtn1").set("v.disabled", false);
        }
        else{
            component.find("BidWidrawnbtn1").set("v.disabled", true);
        }
        
    }, 
    btnBidWidrawn: function(component, event, helper) {
        var action = component.get('c.postWithdrawn'); 
        var self = this; 
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "syndId" : component.get("v.syndId"),
            "Title" : localStorage.getItem("LoggeduserName"),
            "body1" : component.find("body1").get("v.value")
        });
        action.setCallback(self, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.BidWithdrawn", false);
            component.set("v.SecondBidWithdrawn", true);
            $A.log(actionResult);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
                $A.get('e.force:refreshView').fire();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    BidWidrawncancel: function(component, event) {
        component.set("v.BidWithdrawn", false);
        $A.get('e.force:refreshView').fire();
    },
    SecondBidWithdrawncancel: function(component, event) {
        component.set("v.SecondBidWithdrawn", false);
        $A.get('e.force:refreshView').fire();
    },
    PreWithdraw : function(component, event) { 
        component.set("v.syndId", event.target.id); 
        component.set("v.PreWithdrawn", true);       
    }, 
    PreWithdrawnCheck: function(component, event, helper) {
        if(component.find("chkPreWithdrawn").get('v.value')==true)
        {
            component.find("PreWithdrawnbtn1").set("v.disabled", false);
        }
        else{
            component.find("PreWithdrawnbtn1").set("v.disabled", true);
        }
    }, 
    btnPreWithdrawn: function(component, event, helper) {
        debugger;
        var action = component.get('c.PreWithdraw1'); 
        var self = this; 
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "syndId" : component.get("v.syndId"),
            "Title" : localStorage.getItem("LoggeduserName"),
            "body2" : component.find("body2").get("v.value")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.PreWithdrawn", false);
            component.set("v.SecondPreWithdraw", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result); 
                component.set("v.parentId", result.offerId); 
                $A.get('e.force:refreshView').fire();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        
        $A.enqueueAction(action);
    }, 
    PreWithdrawncancel: function(component, event) {
        component.set("v.PreWithdrawn", false);
        $A.get('e.force:refreshView').fire();
    },    
    SecondPreWithdrawcancel: function(component, event) {
        component.set("v.SecondPreWithdraw", false);
    }, 
    AgreeBid: function(component, event) { 
        
        component.set("v.syndId", event.target.id); 
        component.set("v.AgreetoTerms", true);       
    }, 
    AgreeBidcheck: function(component, event, helper) {
        debugger;
        //var ACDate = component.find("AntDate").get("v.value");
        if(component.find("chkAgreeBid").get('v.value') == true) {// && ACDate != "" && ACDate != undefined){
            component.find("AgreeBidbtn1").set("v.disabled", false);
        } else {
            component.find("AgreeBidbtn1").set("v.disabled", true);
        }
    }, 
    btnAgreeBid: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        if(errorFlag == 'false')
        { 
            var action = component.get('c.AgreeBid1'); 
            var self = this; 
            action.setParams({
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
                "IsManager" : component.get("v.IsSellerManager"),
                "syndId" : component.get("v.syndId"),
                "Title" : localStorage.getItem("LoggeduserName"),
                "body4" : component.find("body4").get("v.value")                
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.AgreetoTerms", false);
                component.set("v.SecondAgree", true);
                if(state === "SUCCESS"){
                    component.set("v.Offer", result);  
                    component.set("v.parentId", result.offerId);
                    $A.get('e.force:refreshView').fire();
                }
                else if(state === "ERROR"){
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    }, 
    AgreeBidcancel: function(component, event) {
        component.set("v.AgreetoTerms", false);
        // $A.get('e.force:refreshView').fire();
    },
    SecondAgreecancel: function(component, event) {
        component.set("v.SecondAgree", false);
        //$A.get('e.force:refreshView').fire();
    },
    BidReject: function(component, event) { 
        component.set("v.syndId", event.target.id); 
        component.set("v.BidReject", true);       
    },    
    BidRejectcheck: function(component, event, helper) {
        if(component.find("chkBidReject").get('v.value')==true)
        {
            component.find("BidRejectbtn1").set("v.disabled", false);
        }
        else{
            component.find("BidRejectbtn1").set("v.disabled", true);
        }
    },
    btnBidReject: function(component, event, helper) {
        var action = component.get('c.BidReject1'); 
        var self = this; 
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "syndId" : component.get("v.syndId"),
            "Title" : localStorage.getItem("LoggeduserName"),
            "body3" : component.find("body3").get("v.value")/*jaya*/
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.BidReject", false);
            component.set("v.SecondBidReject", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result); 
                component.set("v.parentId", result.offerId); 
                $A.get('e.force:refreshView').fire();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    BidRejectcancel: function(component, event) {
        component.set("v.BidReject", false);
        // $A.get('e.force:refreshView').fire();
    },
    SecondBidRejectcancel: function(component, event) {
        component.set("v.SecondBidReject", false);
    },
    CmpTrans: function(component, event) { 
        component.set("v.syndId", event.target.id);
        component.set("v.CmpTrans", true);       
    },    
    CmpTranscheck: function(component, event, helper) {
        if(component.find("chkCmpTrans").get('v.value')==true)
        {
            component.find("CmpTransbtn1").set("v.disabled", false);
        }
        else{
            component.find("CmpTransbtn1").set("v.disabled", true);
        }
    },
    btnCmpTrans: function(component, event, helper) {
        
        var img = component.find("pdfImgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');
        
        debugger;
        //component.find(event.target.id).set("v.disabled", true);
        var action = component.get('c.createFinalized'); 
        var self = this;
        action.setParams({
            "offerId" : component.get("v.TransId"),
            "syndId" : event.target.id
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                $A.get('e.force:refreshView').fire();
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Transaction Completed Successfully',
                        'type': 'Success'
                    }
                );
                showToast.fire();
                component.set("v.CompletePopup", true);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        
        $A.enqueueAction(action);
    }, 
    CmpTranscancel: function(component, event) {
        component.set("v.CmpTrans", false);
    },
    EditofferNotes : function(component, event, helper) { 
        component.set("v.EditofferNotes", true);       
    }, 
    EditcancelNotes : function(component, event) {
        component.set("v.EditofferNotes", false);
    },
    
    updateSellerNotes: function(component, event, helper) {
        
        var OfferObjectnote = component.get("v.Offer.OfferExtended.Offer");
        // Create the action
        var action = component.get("c.updateNotes");
        action.setParams({ "saveSellerOffersNotes": OfferObjectnote });
        
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Offer.OfferExtended.Offer", response.getReturnValue());
                component.set("v.showpopup", false);
                $A.get('e.force:refreshView').fire();
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Transaction notes Updated Sucessfully',
                        'type': 'Success'
                    }
                );
                showToast.fire();
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        
        // Send action off to be executed
        $A.enqueueAction(action);
    },
    Editoffer : function(component, event, helper) { 
        debugger;              
        var action = component.get('c.editviewTransaction'); 
        var self = this;
        action.setParams({
            "transid" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
               component.set("v.Transactions.TransAtt", result.TransAtt); 
               component.set("v.Editoffer", true); 
            }
            
        });        
        $A.enqueueAction(action);
    }, 
    Editcancel : function(component, event) {
        component.set("v.Editoffer", false);
        component.set("v.validation", false);
        component.set("v.loanError", '');
        component.set("v.templateError", '');
        component.set("v.nameError", '');
        component.set("v.participationAmountError", '');
        component.set("v.participationPercentageError", '');
        // $A.get('e.force:refreshView').fire();
    },
    changeParticipationAmount: function(component, event, helper) { 
        var ParticipationAmount = component.find("ParticipationAmount").get("v.value");        
        if(ParticipationAmount != undefined)
        { 
            var val = helper.numberWithoutCommas(ParticipationAmount); 
            var val1 = helper.numberWithCommas(val);
            //component.find("ParticipationAmount").set("v.value",val1);
            component.set("v.Offer.OfferExtended.Offer.ParticipationAmount__c", val1);
        }  
    },
    changeParticipationPercentage: function(component, event, helper) {  
        var ParticipationPercentage = component.find("ParticipationPercentage").get("v.value"); 
        if(ParticipationPercentage != undefined)
        { 
            var val = helper.numberWithoutCommas(ParticipationPercentage); 
            var val1 = helper.round(val, 3); 
            var val2 = helper.numberWithCommas(val1);
            //component.find("ParticipationPercentage").set("v.value", val2);
            component.set("v.Offer.OfferExtended.Offer.ParticipationPercentage__c", val2); 
        }         
    }, 
    ParticipationAmount: function(component, event, helper) {
        var inp = component.find("ParticipationAmount").get("v.value"); 
        if(inp == 0)
            //component.find("ParticipationAmount").set("v.value", "");
            component.set("v.Offer.OfferExtended.Offer.ParticipationAmount__c", null);
    },
    ParticipationPercentage: function(component, event, helper) {
        var inp = component.find("ParticipationPercentage").get("v.value"); 
        if(inp == 0.000)
            //component.find("ParticipationPercentage").set("v.value","");
            component.set("v.Offer.OfferExtended.Offer.ParticipationPercentage__c", null);
    },
    SaveEditTransaction:function(component, event, helper){
        debugger;   
        
        var SellNotesib = component.find("seller_notesib").get("v.value");
        var Attoptions = [];
        var tempatt = component.get("v.editattributesstring");
        for(var i = 0; i < tempatt.length; i++) {
            if(i !=''){
                Attoptions.push(tempatt[i]);
            }            
        }
        /*var TemObj = component.get("v.Transactions.TransAtt");
        for(var k = 0; k < TemObj.length; k++) {
            var I = TemObj[k].Id;
            var T = TemObj[k].Attribute_Name__c;
            var D = document.getElementById(T).value; 
            
            Attoptions.push(I+':'+T+':'+D);
        } */
        var idListJSON=JSON.stringify(Attoptions);
        var Action = component.get("c.updateTransaction");        
        Action.setParams({
            "transid" : component.get("v.TransId"),
            "TransAttribute" : idListJSON,
            "userid" : component.get("v.Loggeduserid"),
            "notes" : SellNotesib
        });        
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Attributes Updated Successfully',
                        'type': 'Success'
                    }
                );
                showToast.fire();
                $A.get('e.force:refreshView').fire();
                
            }
        });
        $A.enqueueAction(Action);
        
        
        
    },
    SaveEditOffer : function(component, event, helper) { 
        debugger;
        var errorFlag = 'false';
        //var offerName = component.find("offername").get("v.value");
        /*
         * commented by jaya since name field is not needed
         * 
        if(offerName == '' || offerName == null || offerName == undefined || offerName.trim().length*1 == 0) 
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.nameError", 'Input field required'); 
        }
        else
        { 
            if(helper.checkSpecialCharecter1(offerName))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.nameError", 'The product name has special characters. These are not allowed.'); 
            } 
            else{  
                component.set("v.nameError", ''); 
            }  
        }
        */
        
        /*
         * commented by jaya since participation amount is not needed
         * 
        if(participationAmount == undefined || participationAmount == ''){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.participationAmountError", 'Input field required'); 
        } else {   
            var numWithoutComma = helper.numberWithoutCommas(participationAmount);
            if(helper.checkSpecialCharecter(numWithoutComma) || helper.checkCharecter(numWithoutComma)) { 
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.participationAmountError", 'Please enter a numeric value.'); 
            } else {
                if(numWithoutComma <= 0)
                {
                    errorFlag = 'true';
                    component.set("v.validation", true);
                    component.set("v.participationAmountError", 'Enter a value greater than 0'); 
                } else {
                    component.set("v.participationAmountError", '');
                }
            }
        }
		*/
        
        /*
         * commented by jaya since participation percentage is not needed
         * 
        if(participationPercentage == undefined || participationPercentage == '') {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.participationPercentageError", 'Input field required'); 
        } else {
            var numWithoutComma = helper.numberWithoutCommas(participationPercentage);
            if(helper.checkSpecialCharecter(numWithoutComma) || helper.checkCharecter(numWithoutComma)) { 
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.participationPercentageError", 'Please enter a numeric value.'); 
            } else {
                if(isNaN(numWithoutComma) || numWithoutComma <= 0 || numWithoutComma > 99)
                {
                    errorFlag = 'true';
                    component.set("v.validation", true);
                    component.set("v.participationPercentageError", 'Enter a value between 1% - 99%'); 
                } else {
                    component.set("v.participationPercentageError", '');  
                }
            }
        }
        */
        
        if(errorFlag == 'false')
        { 
            var img = component.find("pdfImgloading");
            
            $A.util.removeClass(img,'slds-hide');
            $A.util.addClass(img,'slds-show');  
            
            component.find("btnSaveTrans").set("v.disabled", true); 
            debugger;            
            var customComponent = component.find("MyCustomComponentEdit");
            customComponent.find("recordCreator").saveRecord(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    customComponent.set("v.recordId", saveResult.recordId);
                    component.set("v.Offer.OfferExtended.Offer.Record_Id__c", customComponent.get("v.recordId")); 
                    var OfferObject = component.get("v.Offer.OfferExtended.Offer"); 
                    var action = component.get("c.editSellerOffersItems");
                    action.setParams({ "saveSellerOffers": OfferObject });
                    action.setCallback(this, function(response) {
                        debugger;
                        var offerId = response.getReturnValue();
                        if(offerId != '') {
                            if(offerId == 'Already Used'){
                                component.find("btnSaveTrans").set("v.disabled", true); 
                                errorFlag = 'true';
                                component.set("v.validation", true);
                                component.set("v.nameError", 'This Transaction Name has already been used. Please enter a new Transaction Name.');
                                
                                $A.util.addClass(img,'slds-hide'); 
                                $A.util.removeClass(img,'slds-show');
                                
                            } else { 
                                $A.util.addClass(img,'slds-hide'); 
                                $A.util.removeClass(img,'slds-show');
                                component.set("v.showpopup", false);
                                $A.get('e.force:refreshView').fire();
                                var showToast = $A.get('e.force:showToast');
                                showToast.setParams(
                                    {
                                        'title': 'Success: ',
                                        'message': 'Transaction Updated Sucessfully',
                                        'type': 'Success'
                                    }
                                );
                                showToast.fire();
                            }
                        } else { 
                            component.find("btnSaveTrans").set("v.disabled", false); 
                            component.set("v.validation", false);
                            component.set("v.Editoffer", false);
                            
                            $A.util.addClass(img,'slds-hide'); 
                            $A.util.removeClass(img,'slds-show');
                            
                            var showToast = $A.get('e.force:showToast');
                            showToast.setParams(
                                {
                                    'title': 'Failure : ',
                                    'message': 'Unable to Update Transaction',
                                    'type': 'Success'
                                }
                            );
                            showToast.fire();
                        } 
                    });
                    $A.enqueueAction(action);
                } else if (saveResult.state === "INCOMPLETE") {
                    component.find("btnSaveTrans").set("v.disabled", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    component.find("btnSaveTrans").set("v.disabled", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    console.log('Problem saving transaction, error: ' + JSON.stringify(saveResult.error));
                } else {
                    component.find("btnSaveTrans").set("v.disabled", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                }
            });
        }
    },
    partcipation: function(component, event, helper) {
        var selected = component.find("levels").get("v.value"); 
    }, 
    LoanType: function(component, event, helper) {
        var selected = component.find("loan").get("v.value"); 
    },
    delete1: function(component, event, helper) {
        var action = component.get('c.DeleteFile');  
        action.setParams({
            "attId" : event.target.id 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.Offer", result);
                component.set("v.DeleteFile", true);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action); 
    },
    delAttach: function(component, event, helper) {
        var action = component.get('c.DeleteAttach');  
        action.setParams({
            "attId" : event.target.id 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.Offer", result);
                component.set("v.DeleteFile", true);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action); 
    },
    DeleteFilecancel: function(component, event, helper) {
        component.set('v.DeleteFile', false);
        $A.get('e.force:refreshView').fire();
    }, 
    loadFirstTab: function(component, event, helper) {
        //component.set("v.hideDiv", true);
    },
    loadSecondTab: function(component, event, helper) {
        component.set("v.hideDiv", false);
    },
    
    hoverCreditUnion: function(component, event) { 
        var action = component.get('c.CUDetails'); 
        action.setParams({
            "CUId" : event.target.id
        });       
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.CUUser", result); 
                component.set("v.popoverCreditUnion", true);
            } 
        });
        $A.enqueueAction(action);
    },
    hoverCreditUnionhide: function(component, event) { 
        component.set("v.popoverCreditUnion", false);
    },  
    hoverBuyerUser: function(component, event) { 
        debugger;
        var action = component.get('c.UserDetails1'); 
        action.setParams({
            "attachId" : event.target.id
        });       
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.user", result);
                component.set("v.popoverBidBuyerName", true);
            } 
        });
        $A.enqueueAction(action);
    },
    hoverBidBuyerUser: function(component, event) { 
        var action = component.get('c.UserDetails2'); 
        action.setParams({
            "syndId1" : event.target.id
        });       
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.user", result); 
                component.set("v.popoverBidBuyerName", true);
            } 
        });
        $A.enqueueAction(action);
    },
    hoverBidbuyerhide: function(component, event) { 
        component.set("v.popoverBidBuyerName", false); 
    },
    showNotes:function(component, event, helper) {
        component.set("v.docId", event.target.id);
        component.set("v.ShowNotes", true);
        component.set("v.rerequestflag", false);
    },
    showrerequestNotes:function(component, event, helper) {
        component.set("v.docId", event.target.id);
        component.set("v.ShowNotes", true);
        component.set("v.rerequestflag", true);
    },
    showNotesCancel: function(component, event) {
        component.set("v.ShowNotes", false);
        component.set("v.showSaveNote", '');
    }, 
    Bidhideshow:function(component, event) {
        debugger;
        var alltrs = document.getElementsByClassName('hidden-tr');
        var alltrs1 = document.getElementsByClassName('fas fa-plus-circle');
        var alltrs2 = document.getElementsByClassName('fas fa-minus-circle');        
        for (var i = 0; i < alltrs.length; i++) {
            alltrs[i].style.display = 'none';
            alltrs1[i].style.display = 'block';
            alltrs2[i].style.display = 'none';
        }
        var elementId ='tr_'+event.target.id;
        document.getElementById(elementId).style.display = 'table-row';
        var MelementId =event.target.id;
        var Replaceval = MelementId.replace("P_", "M_");
        var n = MelementId.includes("P_");
        if(n){
            document.getElementById(MelementId).style.display = 'none';
            document.getElementById(Replaceval).style.display = 'block';
        }else{
            document.getElementById(MelementId).style.display = 'block';
            document.getElementById(Replaceval).style.display = 'none';  
        }
    },  
    Amendhideshow:function(component, event) {
        debugger;
        var alltrs = document.getElementsByClassName('hidden-tr');
        var alltrs1 = document.getElementsByClassName('fas fa-plus-circle');
        var alltrs2 = document.getElementsByClassName('fas fa-minus-circle');        
        for (var i = 0; i < alltrs.length; i++) {
            alltrs[i].style.display = 'none';
            alltrs1[i].style.display = 'block';
            alltrs2[i].style.display = 'none';
        }
        var elementId ='tr_'+event.target.id;
        document.getElementById(elementId).style.display = 'table-row';
        var MelementId =event.target.id;
        var Replaceval = MelementId.replace("PL_", "MI_");
        var n = MelementId.includes("PL_");
        if(n){
            document.getElementById(MelementId).style.display = 'none';
            document.getElementById(Replaceval).style.display = 'block';
        }else{
            document.getElementById(MelementId).style.display = 'block';
            document.getElementById(Replaceval).style.display = 'none';  
        }
    },  
    loadNotes:function(component, event) {
        debugger;
        component.set("v.docId", event.target.id); 
        component.set("v.showRequestNotes", []);
        var alltrs = document.getElementsByClassName('hidden-tr');
        var alltrs1 = document.getElementsByClassName('fas fa-plus-circle');
        var alltrs2 = document.getElementsByClassName('fas fa-minus-circle');    
        for (var i = 0; i < alltrs.length; i++) {
            alltrs[i].style.display = 'none';
            alltrs1[i].style.display = 'block';
            alltrs2[i].style.display = 'none';
        }
        var action = component.get('c.showLoadNotes');
        action.setParams({
            "offerId" : component.get("v.TransId"),
            "docId" : component.get("v.docId"),
        });
        action.setCallback(this, function(actionResult) { 
            debugger;
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.showRequestNotes", result);
                var showselleernotechk = component.get("v.showsellernotes");
                if(showselleernotechk){
                    component.set("v.showsellernotes", false);
                }else
                {
                    component.set("v.showsellernotes", true);
                }
                
                var elementId ='tr_'+event.target.id;
                document.getElementById(elementId).style.display = 'table-row';
                var MelementId =event.target.id;
                var Replaceval = MelementId.replace("RP_", "RM_");
                var n = MelementId.includes("RP_");
                if(n){
                    document.getElementById(MelementId).style.display = 'none';
                    document.getElementById(Replaceval).style.display = 'block';
                }else{
                    document.getElementById(MelementId).style.display = 'block';
                    document.getElementById(Replaceval).style.display = 'none';  
                }
            }
        });
        $A.enqueueAction(action);
    },
    showNotesSave:function(component, event) {
        // alert('inside Notes');
        debugger;
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || savNote == null || savNote == undefined || savNote.trim().length*1 ==0)
        {            
            component.set("v.showSaveNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        }
        else{
            
            component.set("v.showSaveNote", ''); 
            var action = component.get('c.showSaveNotes');
            action.setParams({
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
                "IsManager" : component.get("v.IsSellerManager"),
                "offerId" : component.get("v.TransId"),
                "docId" : component.get("v.docId"),
                "body" : component.find("body").get("v.value")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.ShowNotes", false);
                if(state === "SUCCESS"){
                    component.set("v.showRequestNotes", result);
                    //var elementId ='tr_'+event.target.id;
                    //document.getElementById(elementId).style.display = 'table-row';
                    $A.get('e.force:refreshView').fire();
                }
            });
            
            $A.enqueueAction(action);
        }
    },
    
    requestPopNotes: function(component, event) {
        component.set("v.RequestNotes", true);
        component.set("v.WtQyes", true);
        component.set("v.WtQno", false);
    },
    RequestNotesCancel: function(component, event) {
        component.set("v.RequestNotes", false);
        component.set("v.requestSaveNote", '');
    },
    handleno: function(component, event) {
        //var yescall = component.find("rbtnyes").get("v.value"); 
        var nocall = component.find("rbtnno").get("v.value"); 
        if(nocall)
        {
            component.set("v.WtQyes", false);
            component.set("v.WtQno", true);
        }
    },
    handleyes: function(component, event) {
        
        var yescall = component.find("rbtnyes").get("v.value"); 
        if(yescall)
        {
            component.set("v.WtQyes", true);
            component.set("v.WtQno", false);
        }
    }, 
    RequestNotesSave: function(component, event, helper) {
        debugger;
        var savNote = component.find("responsebody").get("v.value");
        if(savNote == '' || savNote == null || savNote == undefined || savNote.trim().length*1 ==0)
        {            
            component.set("v.requestSaveNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        }
        else{
            debugger; 
            component.set("v.requestSaveNote", ''); 
            var action = component.get('c.requestSaveNotes');
            var RFI = component.find("rbtnyes").get('v.value');
            action.setParams({
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
                "IsManager" : component.get("v.IsSellerManager"),
                "offerId" : component.get("v.TransId"),
                "docId" : component.get("v.docId"),
                "body" : component.find("responsebody").get("v.value"),
                "CRTQ" : RFI
            });
            
            action.setCallback(this, function(actionResult) { 
                debugger;
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.RequestNotes", false);
                if(state === "SUCCESS"){
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            
            $A.enqueueAction(action);
        }
    }, 
    popNotes: function(component, event) {
        component.set("v.Notes", true);
    },
    NotesCancel: function(component, event) {
        component.set("v.Notes", false);
        component.set("v.saveNote", '');
    },
    NotesSave: function(component, event, helper) {
        debugger;
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || savNote == null || savNote == undefined || savNote.trim().length*1 ==0)
        {            
            component.set("v.saveNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        }
        else{
            component.set("v.saveNote", ''); 
            var action = component.get('c.saveNotes');
            action.setParams({
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
                "IsManager" : component.get("v.IsSellerManager"),
                "offerId" : component.get("v.TransId"),
                "Title" : localStorage.getItem("LoggeduserName"),
                "body" : component.find("body").get("v.value")  
            });
            
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.Notes", false);
                $A.log(actionResult);
                if(state === "SUCCESS"){
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            
            $A.enqueueAction(action);
        }
    }, 
    OriginalPrincipal: function(component, event, helper) {
        var inp = component.find("oPrincipal").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.OriginalPrincipal__c","");
    },
    Wac: function(component, event, helper) {
        var inp = component.find("wac").get("v.value"); 
        if(inp == 0.000)
            component.set("v.Offer.OfferExtended.Offer.WAC__c","");
    },
    CurrentPrincipal: function(component, event, helper) {
        var inp = component.find("cPrincipal").get("v.value"); ;
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.OutstandingPrincipal__c","");
    },
    WAFICO: function(component, event, helper) {
        var inp = component.find("fico").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.FICO_WA__c","");
    },
    NumOfLoan: function(component, event, helper) {
        var inp = component.find("nof").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.NumberOfLoans__c","");
    },
    MinFICO: function(component, event, helper) {
        var inp = component.find("minfico").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.MinFICO__c","");
    },
    AvgLoanAmount: function(component, event, helper) {
        var inp = component.find("avgloan").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.AverageLoanAmount__c","");
    },
    WAOriginal: function(component, event, helper) {
        var inp = component.find("oTerm").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.Term_WA__c","");
    },
    WALTV: function(component, event, helper) {
        var inp = component.find("ltv").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.LoanToValue__c","");
    },
    WARemaining: function(component, event, helper) {
        var inp = component.find("Rmonth").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.RemainingTerm__c","");
    },
    WADTI: function(component, event, helper) {
        var inp = component.find("dti").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.WA_DTI__c","");
    },
    New: function(component, event, helper) {
        var inp = component.find("newper").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.NewPercentage__c","");
    },
    Indirect: function(component, event, helper) {
        var inp = component.find("indirect").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.IndirectPercentage__c","");
    },
    AvgVehAge: function(component, event, helper) {
        var inp = component.find("AvgVhclAge").get("v.value"); 
        if(inp == 0.0)
            component.set("v.Offer.OfferExtended.Offer.AverageVehicleAge__c","");
    },
    ChargeOff: function(component, event, helper) {
        var inp = component.find("ChrgOff").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.Charge_Off__c","");
    },
    WALoan: function(component, event, helper) {
        var inp = component.find("WaLifeLoan").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.Average_Life_of_Loan__c","");
    },
    PortPrice: function(component, event, helper) {
        var inp = component.find("PPrice").get("v.value"); 
        if(inp == 0.00)
            component.set("v.Offer.OfferExtended.Offer.PortfolioPrice__c","");
    },
    ServFee: function(component, event, helper) {
        var inp = component.find("SFee").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.ServicingFee__c","");
    },
    bidNameClick: function(component, event, helper) {
        debugger;
        var address = "/bid-detail?offerId=&syndId="+ event.target.id;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": address,
            "isredirect" :false
        });
        urlEvent.fire();
    },
    refreshModel: function (component, event, helpler) {
        debugger;
        var action = component.get('c.getContentslist');  
        action.setParams({
            "parentId" : component.get("v.TransId")
            //"description" : ''
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                debugger;
                component.set("v.Transactions.ContentVer", result.ContentVer);
                
                component.set("v.opendocvault", false);
                component.set("v.popdocuvault", false);
                var img = component.find("imgloading");
                $A.util.addClass(img,'slds-hide'); 
                $A.util.removeClass(img,'slds-show');
                //component.set("v.FileUploadMsg", 'File(s) have been uploaded successfully');
                //component.set("v.FileUploadMsgSorE", 'Success');
                //component.set("v.FileUpload", true);
                //component.set("v.Offer.attachment", result); 
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
    refreshModel1: function (component, event, helpler) {
        debugger;
        var action = component.get('c.getContentslisttask');  
        action.setParams({
            "parentId" : component.get("v.TransId")
            //"description" : ''
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                debugger;
                component.set("v.taskattachment", true);
                component.set("v.Transactionstask.ContentVer", result.ContentVer);                
                var img = component.find("imgloading");
                $A.util.addClass(img,'slds-hide'); 
                $A.util.removeClass(img,'slds-show');                
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
    refreshModel2: function (component, event, helpler) {
        debugger;
        var action = component.get('c.getContentslistcptask');  
        action.setParams({
            "parentId" : component.get("v.TransId"),
            "taskId" : component.get("v.taskId")
            //"description" : ''
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                debugger;
                component.set("v.taskcpattachment", true);
                component.set("v.Transactions.ContentVer", result.ContentVer);                
                var img = component.find("imgloading");
                $A.util.addClass(img,'slds-hide'); 
                $A.util.removeClass(img,'slds-show');                
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
    refreshModel3: function (component, event, helpler) {
        debugger;
        var action = component.get('c.getContentslistownertask');  
        action.setParams({
            "parentId" : component.get("v.TransId"),
            "taskId" : component.get("v.taskId")            
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                debugger;
                component.set("v.taskownerattachment", true);
                component.set("v.TransactionsTask.ContentVer", result.ContentVer);                
                var img = component.find("imgloading");
                $A.util.addClass(img,'slds-hide'); 
                $A.util.removeClass(img,'slds-show');                
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
    refreshModelDue: function (component, event, helpler) {
        var img = component.find("imgloadingDue");
        $A.util.addClass(img,'slds-hide'); 
        $A.util.removeClass(img,'slds-show');
        component.set("v.FileUploadMsg", 'File(s) have been uploaded successfully');
        component.set("v.FileUploadMsgSorE", 'Success');
        component.set("v.FileUpload", true);
        var action = component.get('c.getContentVersions');  
        action.setParams({
            "parentId" : component.get("v.parentId"),
            "description" : 'due'
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.Offer.duecontent", result);
                //component.set("v.Offer.dueAttachment", result); 
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
    ChooseFile : function(component, event) {        
        component.set("v.FileUploadMsg", 'Please choose the file(s)');
        component.set("v.FileUploadMsgSorE", 'Error');
        component.set("v.FileUpload", true); 
    },
    ChooseFailure : function(component, event) {        
        component.set("v.FileUploadMsg", 'An error has occurred. Please contact us at\nLoanParticipationBeta@cunamutual.com.');
        component.set("v.FileUploadMsgSorE", 'Error');        
        component.set("v.FileUpload", true); 
    },
    ChooseFile1 : function(component, event) {        
        component.set("v.FileUploadMsg", 'Please choose the file(s)');
        component.set("v.FileUploadMsgSorE", 'Error');
        component.set("v.FileUpload", true); 
    },
    ChooseFile2 : function(component, event) {        
        component.set("v.FileUploadMsg", 'Please choose the file(s)');
        component.set("v.FileUploadMsgSorE", 'Error');
        component.set("v.FileUpload", true); 
    },
    ChooseFile3 : function(component, event) {        
        component.set("v.FileUploadMsg", 'Please choose the file(s)');
        component.set("v.FileUploadMsgSorE", 'Error');
        component.set("v.FileUpload", true); 
    },
    fileExtension : function(component, event) {        
        component.set("v.FileUploadMsg", 'This file format not allowed to upload');
        component.set("v.FileUploadMsgSorE", 'Error');        
        component.set("v.FileUpload", true); 
    },
    LoaderDisplay : function(component, event) {
        var img = component.find("imgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    LoaderDisplay1 : function(component, event) {
        var img = component.find("imgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    LoaderDisplay2 : function(component, event) {
        var img = component.find("imgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    LoaderDisplay3 : function(component, event) {
        var img = component.find("imgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    LoaderDisplayDue : function(component, event) {
        var img = component.find("imgloadingDue");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    FileUploadcancel : function(component, event) {
        var img = component.find("imgloading");
        $A.util.addClass(img,'slds-hide'); 
        $A.util.removeClass(img,'slds-show');
        var img1 = component.find("imgloadingDue");
        $A.util.addClass(img1,'slds-hide'); 
        $A.util.removeClass(img1,'slds-show');
        component.set("v.FileUpload", false); 
        $A.get('e.force:refreshView').fire();
    },    
    BidDue : function(component, event) {
        component.set("v.syndId", event.target.id); 
        component.set("v.BidDue", true); 
    },
    BidDueCheck : function(component, event, helper) {
        if(component.find("chkBidDue").get('v.value') == true) {
            component.find("BidDuebtn1").set("v.disabled", false);
        }
        else{
            component.find("BidDuebtn1").set("v.disabled", true);
        }
    },
    btnBidDue: function(component, event, helper) {
        debugger;
        var action = component.get('c.BidDue1'); 
        action.setParams({
            "syndId" : component.get("v.syndId"),
            "body6" : component.find("body6").get("v.value")
        });
        action.setCallback(self, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.BidDue", false);
            component.set("v.SecondBidDue", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);  
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    BidDuecancel : function(component, event) {
        component.set("v.BidDue", false);
    },
    SecondBidDuecancel : function(component, event) {
        component.set("v.SecondBidDue", false);
    },
    ShareSave: function(component, event) {
        var errorFlag = 'false';
        var vsharerecip =component.find("sharerecip").get("v.value");
        var vsharesub =component.find("sharesub").get("v.value");
        var vsharebody =component.find("sharebody").get("v.value");
        // var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(vsharerecip == '' || vsharerecip == 'undefined' || vsharerecip == null)
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.sharerecipError", 'Input field required'); 
        }
        else
        {component.set("v.sharerecipError", ''); }
        /*else if(!$A.util.isEmpty(vsharerecip))
        {   
            if(!vsharerecip.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.sharerecipError", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.sharerecipError", ''); 
            }
        } */
        if(vsharesub == '' || vsharesub == 'undefined' || vsharesub == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.sharesubError", 'Input field required'); 
        } else { 
            component.set("v.sharesubError", ''); 
        } 
        if(vsharebody == '' || vsharebody == 'undefined' || vsharebody == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.sharebodyError", 'Input field required'); 
        } else { 
            component.set("v.cpError", ''); 
        }  
        if(errorFlag == 'false') {
            var action = component.get('c.SaveShareTransaction');  
            action.setParams({
                "offerId" : component.get("v.Offer.OfferExtended.Offer.Id"),
                "Recepit" : vsharerecip,
                "sub" : vsharesub,
                "body" : vsharebody
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                }
            });
            $A.enqueueAction(action);
            component.set("v.SharePopup", false);
            $A.get('e.force:refreshView').fire();
        }
        
    },
    counterPopupCancel1 : function(component, event) {
        $A.get('e.force:refreshView').fire();
        //component.set("v.counterPopup", false);
        //component.set("v.counterpartyselectionPopup", false);
        component.set("v.counterpartyselectionPopupnew", false);
    },
    counterPopupCancel : function(component, event) {
        var action = component.get('c.getCounterParties');  
        action.setParams({
            "offerId" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                 component.set("v.Offer.OfferExtended.CounterParties", result);
                //component.set("v.counterPopup", false);
                component.set("v.counterpartyselectionPopup", false);
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
        component.set("v.counterPopup", false);
    },
      /*showCounterParties: function(component, event) {
        debugger;
        var action = component.get('c.getApprovedCreditUnions');  
        action.setParams({
            "creditUnionId" : component.get("v.Transactions.Trans.Tenant__c"),
            "offerId" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                component.set("v.ApprovedCounterParties", result);
                //component.set("v.counterPopup", true);
                component.set("v.counterpartyselectionPopup", true);
            }
        });
        $A.enqueueAction(action);
    },*/
    showCounterParties: function(component, event) {
        debugger;
        var action = component.get('c.counterPartylist');  
        action.setParams({
            "creditUnionId" : component.get("v.Transactions.Trans.Tenant__c"),
            "offerId" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                component.set("v.Tusers",actionResult.getReturnValue());               
                //component.set("v.counterPopup", true);
                component.set("v.counterpartyselectionPopup", true);
            }
        });
        $A.enqueueAction(action);
    }, 
    showCounterPartiesnew: function(component, event) {
        debugger;
        var action = component.get('c.OrgcounterPartylist');  
        action.setParams({
            "creditUnionId" : component.get("v.Transactions.Trans.Tenant__c"),
            "offerId" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                component.set("v.Orgusers",result.Org);  
                component.set("v.UnRegisterList",result.userslist); 
                if(result.userslist.length>0){
                    component.set("v.UnRegisterListlength",true);
                }else{
                    component.set("v.UnRegisterListlength",false);
                }
                //component.set("v.counterPopup", true);
                component.set("v.counterpartyselectionPopupnew", true);
                component.set("v.benefits1", true);
                component.set("v.userinfo1", false);
                component.set("v.clrbenefits1",'active');
                component.set("v.clrud1",'');
                
            }
        });
        $A.enqueueAction(action);
    },
    sharepopupopen : function(component, event) { 
        debugger;
        component.set("v.SharePopup", true);       
    },
    sharecancel : function(component, event) { 
        debugger;
        component.set("v.SharePopup", false);       
    },
    addCounterPartiespop : function(component, event) { 
        debugger;
        component.set("v.newcounterPopup", true);       
    },
    popcounterPopupCancel : function(component, event) { 
        debugger;
        component.set("v.newcounterPopup", false);       
    },
    addCounterParties: function(component, event) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var Bankname = component.find("Banknameval").get("v.value");
        var emval = component.find("emailval").get("v.value");
        var mval = component.find("mobileval").get("v.value");
        var vcode =  component.find("code").get("v.value");
        var vcity =component.find("city").get("v.value");
        var vpcontact =component.find("pcontact").get("v.value");
        var vwebsite =component.find("website").get("v.value");
        var vstate =component.find("state").get("v.value");
        
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        if(mval == '' || mval == 'undefined')
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.mobileError", 'Input field required'); 
        }
        else if(!$A.util.isEmpty(mval))
        {   
            if(!mval.match(regPhoneNo))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.mobileError", 'Please enter valid phone number.'); 
            } 
            else{
                component.set("v.mobileError", ''); 
            }
        }
        if(emval == '' || emval == 'undefined')
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.emailError", 'Input field required'); 
        }
        else if(!$A.util.isEmpty(emval))
        {   
            if(!emval.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.emailError", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.emailError", ''); 
            }
        }
        
        if(Bankname == '' || Bankname == 'undefined' || Bankname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.cpError", 'Input field required'); 
        } else { 
            component.set("v.cpError", ''); 
        } 
        if(vcode == '' || vcode == 'undefined' || vcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.codeerror", 'Input field required'); 
        } else { 
            component.set("v.codeerror", ''); 
        } 
        if(vcity == '' || vcity == 'undefined'|| vcity == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.cityerror", 'Input field required'); 
        } else { 
            component.set("v.cityerror", ''); 
        } 
        if(vpcontact == '' || vpcontact == 'undefined' || vpcontact == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Pcontacterror", 'Input field required'); 
        } else { 
            component.set("v.Pcontacterror", ''); 
        } 
        if(vwebsite == '' || vwebsite == 'undefined' || vwebsite == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.weberror", 'Input field required'); 
        } else { 
            component.set("v.weberror", ''); 
        } 
        if(vstate == '' || vstate == 'undefined' || vstate == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Stateerror", 'Input field required'); 
        } else { 
            component.set("v.Stateerror", ''); 
        } 
        if(errorFlag == 'false') {
            console.log('@@'+component.get("v.counterpartyname"));
            var action = component.get('c.addCounterPartiesname');  
            action.setParams({
                "saveCreditunion" : component.get("v.Creditunion"),
                "PName" : component.get("v.Partname"),
                "PID" : component.get("v.PartyID")
            });
            debugger;
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.newcounterPopup", false);
                    component.set("v.onboard", true);
                }
            });
            debugger;
            $A.enqueueAction(action);
        }
    },
    onboard: function(component, event) {
        component.set("v.onboard", false);
    },
    selectCounterParties: function(component, event) {
        debugger;
        var checkbox = event.getSource();
        var checked = checkbox.get("v.value");
        if(checked) {
            component.find("btnSelect").set("v.disabled", false);
        }
        else{
            component.find("btnSelect").set("v.disabled", true);
        }
        var creditUnionId = checkbox.get("v.text");
        debugger;
        var action1 = component.get('c.Chkissuingbank');  
        action1.setParams({
            "creditUnionId" : creditUnionId,
            "offerId" : component.get("v.Transactions.Trans.Id")
        });
        action1.setCallback(this, function(actionResult1) { 
            var state1 = actionResult1.getState(); 
            var result1 = actionResult1.getReturnValue();
            if(state1 === "SUCCESS"){
                
                if(result1 =='MATCH')
                {
                    component.set("v.issuingpopup",true);
                }else{
                    var action = component.get('c.addRemoveCounterParty');  
                    action.setParams({
                        "creditUnionId" : creditUnionId,
                        "offerId" : component.get("v.Transactions.Trans.Id"),
                        "forAdd" : checked
                    });
                    action.setCallback(this, function(actionResult) { 
                        var state = actionResult.getState(); 
                        var result = actionResult.getReturnValue();
                        if(state === "SUCCESS"){
                        }
                    });
                    $A.enqueueAction(action);
                }
            }
        });
        $A.enqueueAction(action1);
        
        //alert(creditUnionId);
        
    },
    issuingcancel : function(component, event) { 
        component.set("v.issuingpopup",false);     
    },
    approveValidation : function(component, event) { 
        component.set("v.ApproveValidation", true);       
    },
    approveValidationCancel : function(component, event) { 
        component.set("v.ApproveValidation", false);       
    },
    Validationresubmit : function(component, event) { 
        component.set("v.resubmitValidationpop", true);       
    },
    Validationsubmit : function(component, event) { 
        component.set("v.submitValidationpop", true);       
    },
    resubmitValidationcancel : function(component, event) { 
        component.set("v.resubmitValidationpop", false);       
    },
    submitValidationcancel : function(component, event) { 
        component.set("v.submitValidationpop", false);       
    },
    approveValidationre : function(component, event) { 
        component.set("v.ApproveValidationre", true);       
    },
    approveValidationCancelre : function(component, event) { 
        component.set("v.ApproveValidationre", false);       
    },
    resubmitValidationCancel : function(component, event) { 
        component.set("v.ResubmitValidation", false);       
    },
    submitForApproval: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyValidation").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteValidation", 'Input field required'); 
            component.find("bodyValidation").set("v.value", '')
        } else {
            component.set("v.saveNoteValidation", ''); 
            var action = component.get('c.SubmitForApprove'); 
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" :component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.ApproveValidation", false); 
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction is validated Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    }, 
    sellerApproval1m : function(component, event) { 
        component.set("v.SellerApproval1m", true);       
    },
    sellerApproval1mCancel : function(component, event) { 
        component.set("v.SellerApproval1m", false);       
    },
    approve1m: function(component, event, helper) {
        debugger;        
        var savNote = component.find("bodyApproval1").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteApproval1m", 'Input field required'); 
            component.find("bodyApproval1m").set("v.value", '')
        } else {
            component.set("v.saveNoteApproval1m", ''); 
            var action = component.get('c.SellerApproval1m');            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" : component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.SellerApproval1m", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction is approved Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    resubmitForApproval: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyValidation").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteValidation", 'Input field required'); 
            component.find("bodyValidation").set("v.value", '')
        } else {
            component.set("v.saveNoteValidation", ''); 
            var action = component.get('c.reSubmitForApprove1'); 
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" :component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.ApproveValidation", false); 
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Re-Submitted Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    resubmitValidationsave: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyValidation").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteValidation", 'Input field required'); 
            component.find("bodyValidation").set("v.value", '')
        } else {
            component.set("v.saveNoteValidation", ''); 
            var action = component.get('c.reSubmitForvalidation'); 
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" :component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.ApproveValidation", false); 
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Re-Submitted Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    submitValidationsave: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyValidation").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteValidation", 'Input field required'); 
            component.find("bodyValidation").set("v.value", '')
        } else {
            component.set("v.saveNoteValidation", ''); 
            var action = component.get('c.SubmitForvalidation'); 
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" :component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.ApproveValidation", false); 
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Submitted Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    rejectValidation : function(component, event) { 
        component.set("v.RejectValidation", true);       
    },
    rejectValidationCancel : function(component, event) { 
        component.set("v.RejectValidation", false);       
    },
    rejectForApproval: function(component, event, helper) {
        debugger;        
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        } else {
            component.set("v.saveNote", ''); 
            var action = component.get('c.RejectValidation');
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" :component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.RejectValidation", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Rejected: ',
                            'message': 'Transaction is rejected Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            
            $A.enqueueAction(action);
        }        
    }, 
    Resubmitforvalidate : function(component, event) { 
        debugger;
        component.set("v.ResubmitValidation", true);
        component.set("v.setTargetids",event.target.id)
    },
    Resubmitforvalidatesave : function(component, event) { 
        debugger;
        
        var action = component.get('c.Resubmitforvalidatecall'); 
        action.setParams({
            "PCPids" : component.get("v.setTargetids")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){ 
                var savNote = component.find("bodyValidation").get("v.value");
                if(savNote == '' || 
                   savNote == null || 
                   savNote == undefined || 
                   savNote.trim().length*1 == 0) {            
                    component.set("v.saveNoteValidation", 'Input field required'); 
                    component.find("bodyValidation").set("v.value", '')
                } else {
                    component.set("v.saveNoteValidation", ''); 
                    var action = component.get('c.reSubmitForApprove'); 
                    
                    action.setParams({
                        "offerId" : component.get("v.offerId"),
                        "isSellerOrBuyer" : true,
                        "body" : savNote,
                        "counterids" :component.get("v.setTargetids")
                    });
                    action.setCallback(this, function(actionResult) { 
                        var state = actionResult.getState(); 
                        if(state === "SUCCESS"){
                            var result = actionResult.getReturnValue();
                            component.set("v.ResubmitValidation", false); 
                            component.set("v.Offer", result);
                            component.set("v.offerId", result.offerId);  
                            component.set("v.parentId", result.offerId);  
                            
                            $A.get('e.force:refreshView').fire();
                            var showToast = $A.get('e.force:showToast');
                            showToast.setParams(
                                {
                                    'title': 'Success: ',
                                    'message': 'Transaction Updated Sucessfully',
                                    'type': 'Success'
                                }
                            );
                            showToast.fire();
                        }
                        else if(state === "ERROR") {
                            var emptyTask = component.get("v.Offer");
                            emptyTask.Subject = "";
                            emptyTask.Description = "";  
                            component.set("v.Offer", emptyTask);
                        }
                    });
                    $A.enqueueAction(action);
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    //jaya
    approveCounterValidation : function(component, event) { 
        component.set("v.ApproveCounterValidation", true);       
    },
    approveCounterValidationCancel : function(component, event) { 
        component.set("v.ApproveCounterValidation", false);       
    },
    submitForCounterApproval: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyCounterValidation").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteCounterValidation", 'Input field required'); 
            component.find("bodyCounterValidation").set("v.value", '')
        } else {
            component.set("v.saveNoteCounterValidation", ''); 
            var action = component.get('c.SubmitForCounterApprove'); 
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : false,
                "body" : savNote,
                "LogedCUnionId" : component.get("v.LogeduserCUnionId")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.ApproveCounterValidation", false); 
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction is validated successfully.',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    rejectCounterValidation : function(component, event) { 
        component.set("v.RejectCounterValidation", true);       
    },
    rejectCounterValidationCancel : function(component, event) { 
        component.set("v.RejectCounterValidation", false);       
    },
    rejectForCounterApproval: function(component, event, helper) {
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveCounterNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        } else {
            component.set("v.saveCounterNote", ''); 
            var action = component.get('c.RejectCounterValidation');
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : false,
                "body" : savNote,
                "LogedCUnionId" : component.get("v.LogeduserCUnionId")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.RejectCounterValidation", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Reject: ',
                            'message': 'Transaction is rejected successfully.',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            
            $A.enqueueAction(action);
        }        
    }, 
    //jaya
    
    sellerApproval1 : function(component, event) { 
        component.set("v.SellerApproval1", true);       
    },
    sellerApproval1Cancel : function(component, event) { 
        component.set("v.SellerApproval1", false);       
    },
    approve1: function(component, event, helper) {
        debugger;        
        var savNote = component.find("bodyApproval1").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteApproval1", 'Input field required'); 
            component.find("bodyApproval1").set("v.value", '')
        } else {
            component.set("v.saveNoteApproval1", ''); 
            var action = component.get('c.SellerApproval1');            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" :component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.SellerApproval1", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction is approved Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });            
            $A.enqueueAction(action);
        }        
    },
    
    sellerReject1 : function(component, event) { 
        component.set("v.SellerReject1", true);       
    },
    sellerReject1Cancel : function(component, event) { 
        component.set("v.SellerReject1", false);       
    },
    reject1: function(component, event, helper) {
        debugger;        
        var savNote = component.find("bodyReject1").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteReject1", 'Input field required'); 
            component.find("bodyReject1").set("v.value", '')
        } else {
            component.set("v.saveNoteReject1", ''); 
            var action = component.get('c.SellerReject1');
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" :component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.SellerReject1", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Reject: ',
                            'message': 'Transaction is rejected Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });            
            $A.enqueueAction(action);
        }        
    },  
    
    sellerApproval2 : function(component, event) { 
        component.set("v.SellerApproval2", true);       
    },
    sellerApproval2Cancel : function(component, event) { 
        component.set("v.SellerApproval2", false);       
    },
    approve2: function(component, event, helper) {
        debugger;        
        var savNote = component.find("bodyApproval2").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteApproval2", 'Input field required'); 
            component.find("bodyApproval2").set("v.value", '')
        } else {
            component.set("v.saveNoteApproval2", ''); 
            var action = component.get('c.SellerApproval2');
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote  
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.SellerApproval2", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });            
            $A.enqueueAction(action);
        }        
    },
    
    sellerReject2 : function(component, event) { 
        component.set("v.SellerReject2", true);       
    },
    sellerReject2Cancel : function(component, event) { 
        component.set("v.SellerReject2", false);       
    },
    reject2: function(component, event, helper) {
        debugger;        
        var savNote = component.find("bodyReject2").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteReject2", 'Input field required'); 
            component.find("bodyReject2").set("v.value", '')
        } else {
            component.set("v.saveNoteReject2", ''); 
            var action = component.get('c.SellerReject2');
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.SellerReject2", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });            
            $A.enqueueAction(action);
        }        
    },
    
    sellerApproval3 : function(component, event) { 
        component.set("v.SellerApproval3", true);       
    },
    sellerApproval3Cancel : function(component, event) { 
        component.set("v.SellerApproval3", false);       
    },
    approve3: function(component, event, helper) { 
        debugger;        
        var savNote = component.find("bodyApproval3").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteApproval3", 'Input field required'); 
            component.find("bodyApproval3").set("v.value", '')
        } else {
            component.set("v.saveNoteApproval3", ''); 
            var action = component.get('c.Approve'); 
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.SellerApproval3", false);
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Updated Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    }, 
    selectAll: function(component, event, helper) {
        var getAllId = component.find("boxPack");
        for (var i = 0; i < getAllId.length; i++) {
            component.find("boxPack")[i].set("v.value", true);
            
        }
    },
    deselectAll: function(component, event, helper) {
        var getAllId = component.find("boxPack");
        for (var i = 0; i < getAllId.length; i++) {
            component.find("boxPack")[i].set("v.value", false);
            
        }
    },
    sellerReject3 : function(component, event) { 
        component.set("v.SellerReject3", true);       
    },
    sellerReject3Cancel : function(component, event) { 
        component.set("v.SellerReject3", false);       
    },
    reject3: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyReject3").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteReject3", 'Input field required'); 
            component.find("bodyReject3").set("v.value", '')
        } else {
            component.set("v.saveNoteReject3", ''); 
            var action = component.get('c.Reject'); 
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.SellerReject3", false);
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Updated Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    textChange: function(component, event,helper) {  
        debugger;
        if(event.getSource){
            var target = event.getSource();  
            var txtVal = target.get("v.value") ;
            component.set("v.selectedItem",txtVal);   
        }else{
            var target = event.target;  
            var dataEle = target.getAttribute("data-selected-Index"); 
            var Attoptions = [];
            //component.set("v.selectedItem", "Component at index "+dataEle+" has value "+target.value); 
            if(component.get("v.editattributesstring").length > 0){                
                var TemObj = component.get("v.Transactions.TransAtt"); 
                var tempatt = component.get("v.editattributesstring");
                for(var i = 0; i < tempatt.length; i++) {
                    Attoptions.push(tempatt[i]);
                }
                for(var k = 0; k < TemObj.length; k++) {
                    if(dataEle == k){
                        var I = TemObj[k].Id;
                        var T = TemObj[k].Attribute_Name__c;
                        var D = target.value;
                        Attoptions.push(I+':'+T+':'+D);
                    }
                    component.set("v.editattributesstring",Attoptions) ;
                }                
            }else{                
                var TemObj = component.get("v.Transactions.TransAtt");
                Attoptions.push(component.get("v.editattributesstring"));
                for(var k = 0; k < TemObj.length; k++) {
                    if(dataEle == k){
                        var I = TemObj[k].Id;
                        var T = TemObj[k].Attribute_Name__c;
                        var D = target.value;
                        Attoptions.push(I+':'+T+':'+D);
                    }
                    component.set("v.editattributesstring",Attoptions) ;
                }
            }           
            
            
        }
        
    },
     toggleSection : function(component, event, helper) {
        debugger;
        // dynamically get aura:id name from 'data-auraId' attribute
        var sectionAuraId = event.target.getAttribute("data-auraId");
        // get section Div element using aura:id
        var sectionDiv = component.find(sectionAuraId).getElement();
        /* The search() method searches for 'slds-is-open' class, and returns the position of the match.
         * This method returns -1 if no match is found.
        */
        var sectionState = sectionDiv.getAttribute('class').search('slds-is-open'); 
        
        // -1 if 'slds-is-open' class is missing...then set 'slds-is-open' class else set slds-is-close class to element
        if(sectionState == -1){
            sectionDiv.setAttribute('class' , 'slds-section slds-is-open');
        }else{
            sectionDiv.setAttribute('class' , 'slds-section slds-is-close');
        }
    },
    CPhideshow:function(component, event) {
        debugger;
        var alltrs = document.getElementsByClassName('hidden-tr');
        var alltrs1 = document.getElementsByClassName('fas fa-plus-circle');
        var alltrs2 = document.getElementsByClassName('fas fa-minus-circle');        
        for (var i = 0; i < alltrs.length; i++) {
            //alltrs[i].style.display = 'none';
            //alltrs1[i].style.display = 'block';
            //alltrs2[i].style.display = 'none';
            //alert(event.target.id.replace("P_", ""));
        var action = component.get('c.viewTenantUsersList');  
        action.setParams({
            "tenantId" : event.target.id.replace("P_", "")
            
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                    component.set("v.Transactions",result);
                  }
        });
        $A.enqueueAction(action);    
        }
        
        
        var elementId ='tr_'+event.target.id;
        document.getElementById(elementId).style.display = 'table-row';
        var MelementId =event.target.id;
        var Replaceval = MelementId.replace("P_", "M_");
        var n = MelementId.includes("P_");
        if(n){
            document.getElementById(MelementId).style.display = 'none';
            document.getElementById(Replaceval).style.display = 'block';
        }else{
            document.getElementById(MelementId).style.display = 'block';
            document.getElementById(Replaceval).style.display = 'none';  
        }
    }, 
    
    Orghideshow:function(component, event) {
        debugger;
        var alltrs = document.getElementsByClassName('hidden-tr'+event.target.id);
        var alltrs1 = document.getElementsByClassName('fas fa-plus-circle');
        var alltrs2 = document.getElementsByClassName('fas fa-minus-circle'); 
        for (var i = 0; i < alltrs.length; i++) {
            alltrs[i].style.display = 'table-row';
            alltrs1[i].style.display = 'block';
            alltrs2[i].style.display = 'none';
        }
       
        var MelementId =event.target.id;
       
        
        var n = MelementId.includes("PO_");
        if(n){
            var Replaceval = MelementId.replace("PO_", "MO_");
            document.getElementById(MelementId).style.display = 'none';
            document.getElementById(Replaceval).style.display = 'block';
        }else{
            var Replaceval = MelementId.replace("MO_", "PO_");
            document.getElementById(MelementId).style.display = 'none';
            document.getElementById(Replaceval).style.display = 'block'; 
            
        }
    }, 
    
    selectAll1: function(component, event, helper) {
        debugger;
        //get the header checkbox value  
        var selectedHeaderCheck = event.getSource().get("v.value");       
        var getAllId = component.find("checkdoc");        
        if(! Array.isArray(getAllId)){
            if(selectedHeaderCheck == true){ 
                component.find("checkdoc").set("v.value", true);                
            }else{
                component.find("checkdoc").set("v.value", false);               
            }
        }else{           
            if (selectedHeaderCheck == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("checkdoc")[i].set("v.value", true);                    
                }
            } else {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("checkdoc")[i].set("v.value", false);                    
                }
            } 
        }
        
    },
    onchangecheckbox :  function(component, event, helper) {
        debugger;
        var selectedContacts = [];
        var checkvalue = component.find("checkdoc");
        
        if(!Array.isArray(checkvalue)){
            if (checkvalue.get("v.value") == true) {
                selectedContacts.push(checkvalue.get("v.text"));
            }
        }
    },
    CPConfirmationpopup : function(component, event, helper) {
        debugger;
        /*var selectedUser = [];
        var checkvalue = component.find("checkdoc");
        
        if(!Array.isArray(checkvalue)){
            if (checkvalue.get("v.value") == true) {
                selectedUser.push(checkvalue.get("v.text"));
            }
        }else{
            for (var i = 0; i < checkvalue.length; i++) {
                if (checkvalue[i].get("v.value") == true) {
                    selectedUser.push(checkvalue[i].get("v.text"));
                }
            }
        }        
        console.log('selectedUser-' + selectedUser);
        component.set("v.publishedTenantuser",selectedUser);
        component.set("v.counterpartyselectionPopup",false);
        component.set("v.CPConfirm",true);*/
        component.set("v.counterpartyselectionPopupnew", false);
         $A.get('e.force:refreshView').fire();
        
    },
    
    AddcounterPartyusers :  function(component, event, helper) {
        debugger;   
        var tenantUser = component.get("v.publishedTenantuser");
        var action = component.get('c.addCPTenantUsers');  
        action.setParams({
            "tenantusers" : tenantUser,
            "offerId" : component.get("v.Transactions.Trans.Id")                        
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                if(result == 'Success'){
                    component.set("v.CPConfirm",false);
                    $A.get('e.force:refreshView').fire();
                }
            }
        });
        $A.enqueueAction(action);       
        
    },
    viewRequestedCounterParties  :  function(component, event, helper) {
        debugger; 
        var idx = event.target.id;
        var action = component.get('c.viewRequestUsers');  
        action.setParams({
            "publishtenantid" : idx                               
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){                
                component.set("v.Transactions", result.CounterPartiesUsers);
                component.set("v.counterpartyrequestviewPopup", true);               
            }
        });
        $A.enqueueAction(action);   
    },
    closecp: function(component, event, helper) {
        component.set("v.benefits1", true);
        component.set("v.userinfo1", false);
        component.set("v.counterpartyselectionPopupnew", false); 
        debugger;
        $A.get('e.force:refreshView').fire();
    },
    viewrequestuserclose: function(component, event, helper) {
        debugger;
        component.set("v.counterpartyrequestviewPopup", false);
        $A.get('e.force:refreshView').fire();
    },
    selectManagetransaction:function(component, event, helper) {
        debugger;
        var checkbox = event.getSource();
        var checked = checkbox.get("v.value");
        if(checked){
            
            var CP = component.get("c.getCPusers");
            CP.setParams({ 
                "tenantId" :  localStorage.getItem("LoggeduserTenantID"),
                "transId" : component.get("v.Transactions.Trans.Id") 
            }); 
            CP.setCallback(this, function(response) {
                var state = response.getState();
                if (component.isValid() && state == 'SUCCESS') {   
                    var CP = response.getReturnValue();
                    var options = [];
                    CP.forEach(function(CPdata)  { 
                        //alert(CPdata.Id);
                        options.push({ value: CPdata.Id, label: CPdata.First_Name__c});
                        
                    });
                    component.set("v.listOptions", options);
                } else {
                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(CP); 
            
            var action = component.get("c.getTransactiontasks");
            action.setParams({ 
                "transId" : component.get("v.Transactions.Trans.Id")             
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') {   
                    component.set("v.Transactionstask",result);
                    component.set("v.managetransaction", true);
                    component.set("v.AmendBillingstatus", true);
                    component.set("v.combox1", "p-card");
                    component.set("v.combox2", "p-card");
                    if(result.Tasks.length > 0){
                        component.set("v.Taskcreated", true);
                        component.set("v.noTaskcreated", false);    
                        component.set("v.Taskprogress", "process");
                        component.set("v.Taskbox", "p-card progress-border");
                        component.set("v.hTaskstatus","spinneryes");
                        component.set("v.isTaskclosedstatus", false);
                        
                    }else{
                        component.set("v.Taskcreated", false);
                        component.set("v.noTaskcreated", true); 
                    }
                    if(result.isTaskclosedstatus == true){
                        component.set("v.Taskbox", "p-card active-border");
                        component.set("v.isTaskclosedstatus", true);
                    }else{
                        component.set("v.isTaskclosedstatus", false);
                    }
                    if(result.TransAttAmendgroup.length > 0){
                        component.set("v.Amendcreated", true);
                        component.set("v.AmendBillingstatus", false);
                        component.set("v.Amendbox", "p-card progress-border");
                    }else{
                        component.set("v.Amendcreated", false);
                        component.set("v.AmendBillingstatus", true);
                    }
                } else {
                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(action); 
        }else{
            component.set("v.managetransaction", false);
        }
    },
    createnewtask:function(component, event, helper) {
        debugger;
        component.set("v.taskCreate", true);
    },
    taskcancel:function(component, event, helper) {
        debugger;
        component.set("v.taskCreate", false);
    },
    saveTask:function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        var taskType = component.find("taskType").get("v.value"); 
        var Userlist = component.get("v.selectedOptions");
        var subject = component.find("txtsubject").get("v.value"); 
        var duedate = component.find("duedate").get("v.value");
        var comments = component.find("txtcomments").get("v.value");
        var priority = component.find("priorityTypes").get("v.value");
        var status = component.find("statusTypes").get("v.value");
        var reminder = component.find("reminderdate").get("v.value");
        var remindertime = component.find("auratimevalue").get("v.value");
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        
        if(subject == '' || subject == null || subject == undefined || subject.trim().length*1 == 0) {
            errorFlag = 'true';           
            component.set("v.txtsubjectError", 'Input field required'); 
        } else { 
            component.set("v.txtsubjectError", ''); 
        }
        if(duedate == '' || duedate == null || duedate == undefined ) {
            errorFlag = 'true';           
            component.set("v.duedateError", 'Input field required'); 
        } else { 
            component.set("v.duedateError", ''); 
        }
        if(comments == '' || comments == null || comments == undefined || comments.trim().length*1 == 0) {
            errorFlag = 'true';           
            component.set("v.commentsError", 'Input field required'); 
        } else { 
            component.set("v.commentsError", ''); 
        }
        if(Userlist.length ==0){
            if(Userlist == '' || Userlist == null || Userlist == undefined) {
                errorFlag = 'true';                   
                component.set("v.AssigneeError", 'Input field required'); 
            }
        } else { 
            component.set("v.AssigneeError", ''); 
        }
        if(today > duedate){
            errorFlag = 'true';  
            component.set("v.duedateError", 'Due date should not less than today date');
        }else{
            component.set("v.duedateError", '');
        }
        if(reminder == ''){
            reminder = '2019-10-10';
        }else{
            if(duedate < reminder){
                errorFlag = 'true';                   
                component.set("v.reminderdateError", 'Reminder date should not greater than due date'); 
            }else{
                component.set("v.reminderdateError", '');
            }
            if(duedate > reminder){
                if(today > reminder){
                    errorFlag = 'true';                   
                    component.set("v.reminderdateError", 'Reminder date should not less than today date'); 
                }            
                else{
                    component.set("v.reminderdateError", '');
                }
            }
        }
        if(errorFlag == 'false') {  
            var action = component.get("c.insertTaskfortransaction");
            action.setParams({ 
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "tenantId" :  localStorage.getItem("LoggeduserTenantID") ,
                "userlists" : Userlist,
                "tasktype" : taskType,
                "tasksubject" : subject,
                "duedate" : duedate,
                "notes" : comments,
                "priority" : priority,
                "status" : status,
                "reminder" : reminder,
                "remindertime" : remindertime,
                "transactionId" : component.get("v.Transactions.Trans.Id"),
                "transrefnumber" : component.get("v.Transactions.Trans.TransactionRefNumber__c")
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') {   
                    component.set("v.Transactionstask",result); 
                    component.set("v.taskCreate", false);
                    if(result.Tasks.length > 0){
                        component.set("v.Taskcreated", true);
                        component.set("v.noTaskcreated", false);    
                        component.set("v.Taskprogress", "process");
                        component.set("v.Taskbox", "p-card progress-border");
                        component.set("v.hTaskstatus","spinneryes");
                        component.set("v.isTaskclosedstatus", false);
                        
                    }
                } else {
                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(action); 
        }
    },
    
    viewclosedtask: function(component, event) {
        debugger;
        component.set("v.taskviews",true);        
        component.set("v.taskId", event.currentTarget.getAttribute("data-recId")); 
        component.find("txtcomments").set("v.disabled", true);
        component.find("selectOptions").set("v.disabled", true);
        var action = component.get("c.getTaskfortransaction");
        action.setParams({                      
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "tenantId" :  localStorage.getItem("LoggeduserTenantID") ,              
            "transactionId" : component.get("v.Transactions.Trans.Id"), 
            "taskId" : component.get("v.taskId")
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state == 'SUCCESS') {   
                component.set("v.managetask",result.taskdetails); 
                component.set("v.Transactionstask",result);
                //component.set("v.Transactionstask.TaskActivity_Log",result.TaskActivity_Log); 
                //component.set("v.Transactionstask.FileUploadUrl2",result.FileUploadUrl2); 
                component.set("v.Transactionstask.ContentVer",result.ContentVer);
                var options = [];
                result.userslist1.forEach(function(CPdata)  { 
                    //alert(CPdata.Id);
                    options.push({ value: CPdata.Id, label: CPdata.First_Name__c});
                    
                });
                component.set("v.listOptions", options);
                if(result.ContentVer.length > 0){
                    component.set("v.taskownerattachment", true);
                }
                if(result.TaskActivity_Log.length > 0){
                    component.set("v.taskownerhistory", true);
                }
                if(result.isTaskclose == true){
                    component.set("v.closedtask", true);
                }
                component.set("v.selectedOptions",result.taskdetails.Entity_User__c);
            } else {
                
                console.log('Failed with state: ' + state);
            }
        });
        $A.enqueueAction(action); 
    },
    
    updateTask : function(component, event) {
        debugger;
        component.set("v.taskUpdate",true);        
        component.set("v.taskId", event.target.id); 
        var action = component.get("c.getFileuploadurlforTask");
            action.setParams({ 
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "tenantId" :  localStorage.getItem("LoggeduserTenantID") ,
                "taskId" : component.get("v.taskId"),
                "transactionId" : component.get("v.Transactions.Trans.Id") 
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') { 
                    component.set("v.Transactions.FileUploadUrl2",result.FileUploadUrl2);
                    component.set("v.Transactions.TaskActivity_Log",result.TaskActivity_Log); 
                    component.set("v.Transactions.ContentVer",result.ContentVer);
                    if(result.TaskActivity_Log.length > 0){
                        component.set("v.taskcphistory", true);
                    }
                    if(result.ContentVer.length > 0){
                        component.set("v.taskcpattachment", true);
                    }
                    
                } else {
                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(action); 
    },
    updatetasktransaction : function(component, event, helper) {
        debugger;
        
        component.set("v.taskcommentsError", ''); 
        var errorFlag = 'false';
        var comments = component.find("txttaskcomments").get("v.value");
        if(comments == '' || comments == null || comments == undefined || comments.trim().length*1 == 0) {
            errorFlag = 'true';           
            component.set("v.taskcommentsError", 'Input field required'); 
        } else { 
            component.set("v.taskcommentsError", ''); 
        }
          if(errorFlag == 'false') {  
            var action = component.get("c.updateTaskfortransaction");
            action.setParams({ 
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "tenantId" :  localStorage.getItem("LoggeduserTenantID") ,               
                "notes" : comments,               
                "taskId" : component.get("v.taskId")
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') { 
                        component.set("v.Transactions.TaskActivity_Log", result.TaskActivity_Log);
                        component.set("v.taskUpdate",false);
                  
                } else {

                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(action); 
        }
    },
    
    taskupdatecancel : function(component, event, helper) {
        debugger;
        component.set("v.taskUpdate",false);
        
    },
    updatemanagetask : function(component, event) {
        debugger;
        component.set("v.taskownerupdate",true);        
        component.set("v.taskId", event.target.id); 
        component.find("updatetaskType").set("v.disabled", true);
        component.find("txtsubject").set("v.disabled", true);
        component.find("txtcomments").set("v.disabled", true);
        component.find("selectOptions").set("v.disabled", true);
        var action = component.get("c.getTaskfortransaction");
        action.setParams({                      
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "tenantId" :  localStorage.getItem("LoggeduserTenantID") ,              
            "transactionId" : component.get("v.Transactions.Trans.Id"), 
            "taskId" : component.get("v.taskId")
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state == 'SUCCESS') {   
                component.set("v.managetask",result.taskdetails); 
                component.set("v.Transactionstask.TaskActivity_Log",result.TaskActivity_Log); 
                component.set("v.Transactionstask.FileUploadUrl2",result.FileUploadUrl2); 
                component.set("v.Transactionstask.ContentVer",result.ContentVer);
                if(result.ContentVer.length > 0){
                    component.set("v.taskownerattachment", true);
                }
                if(result.TaskActivity_Log.length > 0){
                    component.set("v.taskownerhistory", true);
                }
                component.set("v.selectedOptions",result.taskdetails.Entity_User__c);
            } else {
                
                console.log('Failed with state: ' + state);
            }
        });
        $A.enqueueAction(action); 
    },
    
    taskownerupdatecancel : function(component, event){
        debugger;
        component.set("v.taskownerupdate",false);
    },
    taskviewclose : function(component, event){
        debugger;
        component.set("v.taskviews",false);
    },
    sellertaskviewclose : function(component, event){
        debugger;
        component.set("v.taskviews",false);
        var CP = component.get("c.getCPusers");
        CP.setParams({ 
                "tenantId" :  localStorage.getItem("LoggeduserTenantID"),
                "transId" : component.get("v.Transactions.Trans.Id") 
            }); 
            CP.setCallback(this, function(response) {
                var state = response.getState();
                if (component.isValid() && state == 'SUCCESS') {   
                    var CP = response.getReturnValue();
                    var options = [];
                    CP.forEach(function(CPdata)  { 
                        //alert(CPdata.Id);
                        options.push({ value: CPdata.Id, label: CPdata.First_Name__c});
                        
                    });
                    component.set("v.listOptions", options);
                } else {
                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(CP); 
            
            var action = component.get("c.getTransactiontasks");
            action.setParams({ 
                "transId" : component.get("v.Transactions.Trans.Id")             
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') {   
                    component.set("v.Transactionstask",result);
                    component.set("v.managetransaction", true);
                    component.set("v.AmendBillingstatus", true);
                    component.set("v.combox1", "p-card");
                    component.set("v.combox2", "p-card");
                    if(result.Tasks.length > 0){
                        component.set("v.Taskcreated", true);
                        component.set("v.noTaskcreated", false);    
                        component.set("v.Taskprogress", "process");
                        component.set("v.Taskbox", "p-card progress-border");
                        component.set("v.hTaskstatus","spinneryes");
                        
                    }else{
                        component.set("v.Taskcreated", false);
                        component.set("v.noTaskcreated", true); 
                    }
                } else {
                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(action); 
        
    },
    managetaskownerupdate: function(component, event){
        debugger;
        component.set("v.taskcommentsError", ''); 
        var errorFlag = 'false';
        var comments = component.find("txttaskupdatecomments").get("v.value");        
        var duedate = component.find("duedateupdate").get("v.value");
        var priority = component.find("priorityTypesupdate").get("v.value");
        var status = component.find("statusTypesupdate").get("v.value");
        var reminder = component.find("reminderdateupdate").get("v.value");
        var remindertime = component.find("auratimevalueupdate").get("v.value");
        if(comments == '' || comments == null || comments == undefined || comments.trim().length*1 == 0) {
            errorFlag = 'true';           
            component.set("v.taskcommentsError", 'Input field required'); 
        } else { 
            component.set("v.taskcommentsError", ''); 
        }
        if(duedate < reminder){
            errorFlag = 'true';                   
            component.set("v.reminderdateError", 'Reminder date Should not greater than due date'); 
        }else{
            component.set("v.reminderdateError", '');
        }
        if(errorFlag == 'false') {  
            var action = component.get("c.updateTaskownerfortransaction");
            action.setParams({ 
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "tenantId" :  localStorage.getItem("LoggeduserTenantID") ,               
                "notes" : comments,               
                "taskId" : component.get("v.taskId"),
                "status" : status,
                "duedate" : duedate,                
                "priority" : priority,                
                "reminder" : reminder,
                "remindertime" : remindertime
                
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') {  
                    component.set("v.Transactionstask",result); 
                    component.set("v.taskCreate", false);
                    if(result.Tasks.length > 0){
                        component.set("v.Taskcreated", true);
                        component.set("v.noTaskcreated", false);   
                        component.set("v.taskownerupdate",false);
                        component.set("v.Taskprogress", "process");
                        component.set("v.Taskbox", "p-card progress-border");
                        component.set("v.hTaskstatus","spinneryes");                        
                    }
                    if(result.isTaskclosedstatus == true){
                        component.set("v.Taskbox", "p-card active-border");
                        component.set("v.isTaskclosedstatus", true);
                    }else{
                        component.set("v.isTaskclosedstatus", false);
                    }                    
                    //if(result == 'Success'){
                    
                   // component.set("v.Transactions",result);
                    //}
                } else {

                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(action); 
        }
        
    },
    
    Editamend : function(component, event){
        debugger;   
        
        var action = component.get('c.editviewTransaction'); 
        var self = this;
        action.setParams({
            "transid" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Transactions.TransAtt", result.TransAtt); 
                var i;
                for (i = 0; i < result.TransAtt.length; i++) {
                    if(result.TransAtt[i].Attribute_Name__c == 'Date of Expiry'){
                        component.set("v.shipmentexpirydate", result.TransAtt[i].Id+':'+result.TransAtt[i].Attribute_Name__c+':'+result.TransAtt[i].Attribute_Value__c);
                    }
                    if(result.TransAtt[i].Attribute_Name__c == 'Date of Shipment'){
                        component.set("v.shipmentdate", result.TransAtt[i].Id+':'+result.TransAtt[i].Attribute_Name__c+':'+result.TransAtt[i].Attribute_Value__c);                        
                    }
                }
                component.set("v.Amendoffer", true);
                component.find("seller_notesib").set("v.disabled", true);
            }
            
        });        
        $A.enqueueAction(action);
        
    },
    
    Editamendcancel : function(component, event){
        debugger;
        component.set("v.Amendoffer", false);
    },
    SaveAmendTransaction:function(component, event, helper){
        debugger;   
        var errorFlag = 'false';
        var errorFlag1 = 'false'; 
        var Errormsg ='';
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();
        if(dd > 9){var date = dd;}else{var date = '0'+dd;}
        if(mm > 9){var month = mm;}else{var month = '0'+mm;}
        component.set("v.currentDate",yyyy+'-'+month+'-'+date);
        var prevshipmentdate = component.get("v.shipmentdate");
        var prevshipmentexpirydate = component.get("v.shipmentexpirydate");
        var currentshipmentdate;
        var currentshipmentexpirydate;
        var comments = component.find("amendnotes").get("v.value");
        //var SellNotesib = component.find("seller_notesib").get("v.value");
        var Attoptions = [];
        var tempatt = component.get("v.editattributesstring");
        for(var i = 0; i < tempatt.length; i++) {
            if(i !=''){
                if(prevshipmentexpirydate != undefined && prevshipmentdate != undefined){  
                    if(prevshipmentexpirydate.split(":")[1] ==  'Date of Expiry'){
                        if(tempatt[i].split(":")[1] == 'Date of Expiry'){
                            currentshipmentexpirydate = tempatt[i].split(":")[1]+':'+tempatt[i].split(":")[2];
                        }  
                    }
                    if(prevshipmentdate.split(":")[1] ==  'Date of Shipment'){
                        if(tempatt[i].split(":")[1] == 'Date of Shipment'){
                            currentshipmentdate = tempatt[i].split(":")[1]+':'+tempatt[i].split(":")[2];
                        }
                    }
                    
                }
                 Attoptions.push(tempatt[i]);
            }            
        }
        if(errorFlag == 'false') {
        if(currentshipmentexpirydate != null){
            if(Date.parse(currentshipmentexpirydate.split(":")[1]) < Date.parse(component.get("v.currentDate")))
                    {
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth()+1;
                        var yyyy = today.getFullYear();
                        if(dd > 9){var date = dd;}else{var date = '0'+dd;}
                        if(mm > 9){var month = mm;}else{var month = '0'+mm;}
                        Errormsg = currentshipmentexpirydate.split(":")[0]+' must be '+date+'-'+month+'-'+yyyy+' or later , ';
                        errorFlag = 'true';        
                        component.set("v.DatevalidationError",Errormsg);
                    }else{
                        component.set("v.DatevalidationError",'');
                    }
        }
        }
        if(errorFlag == 'false') {
         if(currentshipmentdate != null){
            if(Date.parse(currentshipmentdate.split(":")[1]) < Date.parse(component.get("v.currentDate")))
                    {
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth()+1;
                        var yyyy = today.getFullYear();
                        if(dd > 9){var date = dd;}else{var date = '0'+dd;}
                        if(mm > 9){var month = mm;}else{var month = '0'+mm;}
                        Errormsg = currentshipmentdate.split(":")[0]+' must be '+date+'-'+month+'-'+yyyy+' or later , ';
                        errorFlag = 'true';        
                        component.set("v.DatevalidationError",Errormsg);
                    }else{
                        component.set("v.DatevalidationError",'');
                    }
         }
        }
        /*if(errorFlag == 'false') {
            if(currentshipmentexpirydate != null){
                if(Date.parse(currentshipmentexpirydate.split(":")[1]) < Date.parse(component.get("v.currentDate")))
                {
                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth()+1;
                    var yyyy = today.getFullYear();
                    if(dd > 9){var date = dd;}else{var date = '0'+dd;}
                    if(mm > 9){var month = mm;}else{var month = '0'+mm;}
                    Errormsg = currentshipmentexpirydate.split(":")[0]+' must be '+date+'-'+month+'-'+yyyy+' or later , ';
                    errorFlag = 'true';        
                    component.set("v.DatevalidationError",Errormsg);                    
                }else{
                    component.set("v.DatevalidationError",'');
                }
            }
        } */
         if(errorFlag == 'false') {
        if(currentshipmentexpirydate == undefined && currentshipmentdate != null ){
            if(Date.parse(prevshipmentexpirydate.split(":")[2]) < Date.parse(currentshipmentdate.split(":")[1])){
                errorFlag = 'true';   
                component.set("v.Datevalidation1Error",'Date of Shipment should not be greater than Date of Expiry');
            }else{
                component.set("v.Datevalidation1Error",'');
            }
        }
         }
        if(errorFlag == 'false') {
            if(currentshipmentexpirydate != null && currentshipmentdate != null ){
                if(Date.parse(currentshipmentexpirydate.split(":")[2]) < Date.parse(currentshipmentdate.split(":")[1])){
                    errorFlag = 'true';   
                    component.set("v.Datevalidation1Error",'Date of Shipment should not be greater than Date of Expiry');
                }else{
                    component.set("v.Datevalidation1Error",'');
                }
            }            
        }
        if(errorFlag == 'false') {
    if(comments == '' || comments == null || comments == undefined || comments.trim().length*1 == 0) {
            errorFlag = 'true';           
            component.set("v.AmendNotesError", 'Input field required'); 
        } else { 
            component.set("v.AmendNotesError", ''); 
        }
        }
        if(errorFlag == 'true' ){
            component.set("v.editattributesstring",[]);
        }
        var idListJSON=JSON.stringify(Attoptions);
        if(idListJSON == "[]"){
            errorFlag = 'true';            
        }
        if(errorFlag == 'false') {
            
            var Action = component.get("c.amendTransaction");        
            Action.setParams({
                "transid" : component.get("v.TransId"),
                "TransAttribute" : idListJSON,
                "userid" : component.get("v.Loggeduserid"),
                "notes" : comments
            });        
            Action.setCallback(this, function(actionResult) {
                debugger;
                var state = actionResult.getState(); 
                var result1 = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.Amendoffer", false);
                    component.set("v.Transactionstask",result1);
                    component.set("v.editattributesstring",[]);
                    if(result1.TransAttAmendgroup.length > 0){
                        component.set("v.Amendcreated", true);
                        component.set("v.AmendBillingstatus", false);
                        component.set("v.Amendbox", "p-card progress-border");
                    }else{
                        component.set("v.Amendcreated", false);
                        component.set("v.AmendBillingstatus", true);
                    }
=======
                component.set("v.Created",result.Trans.CreatedDate);
                component.set("v.approveddate",result.Trans.CreatedDate);
                if(result.CounterParties.length > 0){  
                    component.set("v.Declinedflag",result.CounterParties[0].Declined__c);
                }
                else{
                    component.set("v.Declinedflag",false);
                }
                if(result.Tasks.length > 0){
                    component.set("v.Istask", true);
                }else{
                    component.set("v.Istask", false);
                }
                component.set("v.isAmendoffer", result.isAmend);
                component.set("v.isAmendAcceptoffer",result.isAmendAccept);
                if(localStorage.getItem("TaskAssigneeloggeduser") == "true"){
                    component.set("v.taskassigneeuserregister", true);
                    //component.set("v.managetransaction", true);
                    component.set("v.Taskcreated", true);
                    component.set("v.noTaskcreated", false);    
                    component.set("v.Taskprogress", "process");
                    component.set("v.Taskbox", "p-card progress-border");
                    component.set("v.hTaskstatus","spinneryes");
                }
                document.title = result.Trans.TransactionRefNumber__c;
                var Transstatus=result.Trans.Status__c;
                component.set("v.Tstatus",Transstatus);
                var Bidstatus='';
                var Bidstatus1='';
                if(result.Quotes.length > 0)  
                {
                    component.set("v.MorethanoneQuotecountCHK",true);
                    if(result.Quotes[0].Bid_Status__c=='Quote Closed'){
                        Bidstatus ='Quote Closed';
                    }
                    if(result.Quotes[0].Bid_Status__c=='Quote Accepted'){
                        Bidstatus1 ='Quote Accepted';
                    }
                    
                }
                
                if(Transstatus==='Transaction Created' || Transstatus==='Pending Validation' || Transstatus==='Level 1 - Pending Approval' 
                   || Transstatus==='Pending Approval' || Transstatus==='Validation Rejected' || Transstatus==='Transaction Rejected')
                {
                    component.set("v.Tprogress", "process");
                    component.set("v.Tbox", "p-card progress-border");
                    component.set("v.hTstatus","spinneryes");
                } 
                else if(Transstatus == 'Inactive'){
                    component.set("v.Tprogress", "red");
                    component.set("v.Tbox", "p-card red-border");
                }
                    else if(Transstatus==='Transaction Approved' || Transstatus==='Requested For Info')
                    {   
                        if(Transstatus==='Transaction Approved'){
                            component.set("v.RFI","Requested For Info / Quote");
                        }else{
                            component.set("v.Requesteddate",result.Trans.Published_Date__c);
                            component.set("v.RequestedBy",result.Trans.Tenant__r.Tenant_Site_Name__c);
                            component.set("v.RFI","Requested For Info");
                        }
                        component.set("v.Tprogress", "active");
                        component.set("v.Tbox", "p-card active-border");
                        component.set("v.RFIprogress", "process");
                        component.set("v.RFIbox", "p-card progress-border");
                        component.set("v.hTstatus","spinnerno");
                        component.set("v.hRFQstatus","spinneryes");
                        component.set("v.FullRFIstatus",true);
                        component.set("v.Fullcomstatus1",true);
                        component.set("v.combox1", "p-card");
                        component.set("v.Fullcomstatus2",true);
                        component.set("v.combox2", "p-card");
                        
                    }      
                        else if(Transstatus==='Requested For Quote' || Transstatus==='Quote Withdrawn' || Transstatus==='Quote Submitted' || Transstatus==='Quote Rejected')
                        {
                            debugger;
                            if(localStorage.getItem("TaskAssigneeloggeduser") == "true"){
                                if(localStorage.getItem("TaskviewAssigneeloggeduser") == "true"){
                                    component.set("v.taskassigneeuser", false);
                                }else{
                                    component.set("v.taskassigneeuser", true);
                                }
                            }   
                            component.set("v.Requesteddate",result.Trans.Published_Date__c);                            
                            component.set("v.RequestedBy",result.Trans.Tenant__r.Tenant_Site_Name__c);
                            component.set("v.Tprogress", "active");
                            component.set("v.RFIprogress", "active");
                            component.set("v.RFIbox", "p-card active-border");
                            component.set("v.hTstatus","spinnerno");
                            component.set("v.hRFQstatus","spinnerno");
                            component.set("v.Tbox", "p-card active-border");
                            component.set("v.Bidprogress", "process");
                            component.set("v.Bidbox", "p-card progress-border");
                            component.set("v.hTstatus","spinnerno");
                            component.set("v.RFI","Requested For Quote");
                            component.set("v.hRFQstatus","spinnerno");
                            component.set("v.hbidstatus","spinneryes");
                            component.set("v.FullRFIstatus",true);
                            component.set("v.FullBidstatus",true); 
                            component.set("v.Fullcomstatus1",true);
                            component.set("v.combox1", "p-card");
                            /*if(result.OfferExtended.Syndications.length > 0 && result.LogeduserCBId !=''){
                                component.set("v.Tstatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                            }*/
                            if(Transstatus==='Quote Submitted')
                            {
                                if(result.Quotes.length > 0){
                                    component.set("v.BidStatus",result.Quotes[0].Bid_Status__c);
                                    component.set("v.LastBidDate",result.Quotes[0].CreatedDate);
                                    component.set("v.HighestBidder",result.Quotes[0].CreatedBy__r.First_Name__c+' '+result.Quotes[0].CreatedBy__r.Last_Name__c);
                                    component.set("v.progressBidamount",result.Quotes[0].Bid_Amount__c);
                                }
                            }
                        }
                            else if(Transstatus==='Quote Accepted')
                            {
                                
                                //if(result.Quotes.length > 0 && result.LogeduserCBId !=''){
                                //  component.set("v.Tstatus",result.Quotes[0].Bid_Status__c);
                                //}
                                //if(Bidstatus1 =='Bid Accepted' && result.LogeduserCBId !='' || result.LogeduserCBId==''){
                                if(Transstatus =='Quote Accepted'){
                                    if(result.Quotes.length > 0){
                                        
                                        component.set("v.BidStatus",result.Quotes[0].Bid_Status__c);
                                        component.set("v.LastBidDate",result.Quotes[0].CreatedDate);
                                        component.set("v.HighestBidder",result.Quotes[0].CreatedBy__r.First_Name__c+' '+result.Quotes[0].CreatedBy__r.Last_Name__c);
                                        component.set("v.progressBidamount",result.Quotes[0].Bid_Amount__c);
                                    }
                                    component.set("v.Requesteddate",result.Trans.Published_Date__c);                                
                                    component.set("v.RequestedBy",result.Trans.Tenant__r.Tenant_Site_Name__c);
                                    component.set("v.Tprogress", "active");
                                    component.set("v.RFIprogress", "active");
                                    component.set("v.RFIbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.hRFQstatus","spinnerno");
                                    component.set("v.Tbox", "p-card active-border");
                                    component.set("v.Bidprogress", "active");
                                    component.set("v.Bidbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.RFI","Requested For Quote");
                                    component.set("v.comprogress", "process");
                                    component.set("v.combox", "p-card progress-border");
                                    component.set("v.hcomstatus","spinneryes");
                                    component.set("v.hbidstatus","spinnerno");
                                    component.set("v.FullRFIstatus",true);
                                    component.set("v.FullBidstatus",true); 
                                    component.set("v.Fullcomstatus",true);
                                }else{
                                    //component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                                    //component.set("v.RequestedBy",result.OfferExtended.CreditUnion.CU_Name__c);
                                    component.set("v.Tprogress", "active");
                                    component.set("v.RFIprogress", "active");
                                    component.set("v.RFIbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.hRFQstatus","spinnerno");
                                    component.set("v.Tbox", "p-card active-border");
                                    component.set("v.Bidprogress", "active");
                                    component.set("v.Bidbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.RFI","Requested For Quote");
                                    component.set("v.comprogress", "process");
                                    component.set("v.combox", "p-card progress-border");
                                    component.set("v.hcomstatus","spinneryes");
                                    component.set("v.hbidstatus","spinnerno");
                                    component.set("v.FullRFIstatus",true);
                                    component.set("v.FullBidstatus",true); 
                                    component.set("v.Fullcomstatus",false);
                                }
                            }
                                else if(Transstatus==='Completed Transaction')
                                {
                                    
                                    //if(result.OfferExtended.Syndications.length > 0 && result.LogeduserCBId !=''){
                                    //    component.set("v.Tstatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                    //}
                                    if(Bidstatus =='Quote Closed' && result.LogeduserCBId !='' || result.LogeduserCBId==''){                                
                                        component.set("v.FullRFIstatus",true);
                                        component.set("v.FullBidstatus",true); 
                                        component.set("v.Fullcomstatus",true);
                                        debugger;
                                        component.set("v.Completed",result.Trans.Published_Date__c);
                                        component.set("v.Requesteddate",result.Trans.Published_Date__c);
                                        component.set("v.RequestedBy",result.Trans.Tenant__r.Tenant_Site_Name__c);
                                        component.set("v.Tprogress", "active");
                                        component.set("v.RFIprogress", "active");
                                        component.set("v.RFIbox", "p-card active-border");
                                        component.set("v.hTstatus","spinnerno");
                                        component.set("v.hRFQstatus","spinnerno");
                                        component.set("v.Tbox", "p-card active-border");
                                        component.set("v.Bidprogress", "active");
                                        component.set("v.Bidbox", "p-card active-border");
                                        component.set("v.hTstatus","spinnerno");
                                        component.set("v.RFI","Requested For Quote");
                                        component.set("v.hcomstatus","spinnerno");
                                        component.set("v.comprogress", "active");
                                        component.set("v.combox", "p-card active-border");
                                        component.set("v.hbidstatus","spinnerno");
                                        if(result.Quotes.length > 0){
                                            {
                                                //if(result.LogeduserCBId !=''){
                                                //    component.set("v.Tstatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                                //}
                                                component.set("v.BidStatus",result.Quotes[0].Bid_Status__c);
                                                component.set("v.LastBidDate",result.Quotes[0].CreatedDate);
                                                component.set("v.HighestBidder",result.Quotes[0].CreatedBy__r.First_Name__c+' '+result.Quotes[0].CreatedBy__r.Last_Name__c);
                                                component.set("v.progressBidamount",result.Quotes[0].Bid_Amount__c);
                                            }
                                        }
                                    }else{
                                        component.set("v.FullRFIstatus",true);
                                        component.set("v.FullBidstatus",true); 
                                        component.set("v.Fullcomstatus",false);
                                        debugger;
                                        component.set("v.Completed",result.Trans.Published_Date__c);
                                        component.set("v.Requesteddate",result.Trans.Published_Date__c);                        
                                        component.set("v.RequestedBy",result.Trans.Tenant__r.Tenant_Site_Name__c);
                                        component.set("v.Tprogress", "active");
                                        component.set("v.RFIprogress", "active");
                                        component.set("v.RFIbox", "p-card active-border");
                                        component.set("v.hTstatus","spinnerno");
                                        component.set("v.hRFQstatus","spinnerno");
                                        component.set("v.Tbox", "p-card active-border");
                                        component.set("v.Bidprogress", "active");
                                        component.set("v.Bidbox", "p-card active-border");
                                        component.set("v.hTstatus","spinnerno");
                                        component.set("v.RFI","Requested For Quote");
                                        component.set("v.comprogress", "process");
                                        component.set("v.combox", "p-card progress-border");
                                        component.set("v.hcomstatus","spinneryes");
                                        component.set("v.hbidstatus","spinnerno");
                                    }                                    
                                 
                                }
                if(component.get("v.IsBuyerManager") && !result.Trans.RFQChk__c)
                {
                    var json = JSON.parse(result.Trans.Request_For_Information__c);
                    for(var i = 0; i < json.length; i++) {
                        var obj = json[i];
                        if(obj.Flag=='TRUE'){
                            debugger; 
                            // alert(obj.ID);
                            //alert(document.getElementById(obj.ID).value);
                            var hidediv = component.find("Amount").get("v.value");
                            
                            if(hidediv){
                                $A.util.addClass(hidediv, 'hide');
                            }
                        }
                        
                    }
                }
                
            
            component.set("v.managevalue",false);
              if(managetassk == "y"){
                  debugger;
                  
            component.set("v.managevalue",true);
            var checkbox = event.getSource();
            var checked = checkbox.get("v.value");            
                
                var CP = component.get("c.getCPusers");
                CP.setParams({ 
                    "tenantId" :  localStorage.getItem("LoggeduserTenantID"),
                    "transId" : component.get("v.Transactions.Trans.Id") 
                }); 
                CP.setCallback(this, function(response) {
                    var state = response.getState();
                    if (component.isValid() && state == 'SUCCESS') {   
                        var CP = response.getReturnValue();
                        var options = [];
                        CP.forEach(function(CPdata)  { 
                            //alert(CPdata.Id);
                            options.push({ value: CPdata.Id, label: CPdata.First_Name__c});
                            
                        });
                        component.set("v.listOptions", options);
                    } else {
                        console.log('Failed with state: ' + state);
                    }
                });
                $A.enqueueAction(CP); 
                
                var action = component.get("c.getTransactiontasks");
                action.setParams({ 
                    "transId" : component.get("v.Transactions.Trans.Id")             
                }); 
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    var result = response.getReturnValue();
                    if (state == 'SUCCESS') {   
                        component.set("v.Transactionstask",result);
                        component.set("v.managetransaction", true);
                        component.set("v.AmendBillingstatus", true);
                        component.set("v.combox1", "p-card");
                        component.set("v.combox2", "p-card");
                        if(result.Tasks.length > 0){
                            component.set("v.Taskcreated", true);
                            component.set("v.noTaskcreated", false);    
                            component.set("v.Taskprogress", "process");
                            component.set("v.Taskbox", "p-card progress-border");
                            component.set("v.hTaskstatus","spinneryes");
                            
                        }else{
                            component.set("v.Taskcreated", false);
                            component.set("v.noTaskcreated", true); 
                        }
                        if(result.TransAttAmendgroup.length > 0){
                            component.set("v.Amendcreated", true);
                            component.set("v.AmendBillingstatus", false);
                            component.set("v.Amendbox", "p-card progress-border");
                        }else{
                            component.set("v.Amendcreated", false);
                            component.set("v.AmendBillingstatus", true);
                        }
                        if(result.isTaskclosedstatus == true){
                            component.set("v.Taskbox", "p-card active-border");
                            component.set("v.isTaskclosedstatus", true);
                        }else{
                            component.set("v.isTaskclosedstatus", false);
                        }
                    } else {
                        console.log('Failed with state: ' + state);
                    }
                });
                $A.enqueueAction(action); 
            
        }
            }
        }); 
        $A.enqueueAction(action);
        debugger;
        //Add for Direct Manage Task        
      
        
    },
     doneRendering: function(component, event, helper) {
         debugger;
        var action = component.get('c.Chkunregisteruser');  
        action.setParams({ 
            "creditUnionId" :  localStorage.getItem("LoggeduserTenantID"),
            "GID" : component.get("v.TransId")
        }); 
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState();  
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                if(result.CPSelection.length>0){
                    for (var k = 0; k < result.CPSelection.length; k++) {
                        document.getElementById(result.CPSelection[k].Tenant_User__c).checked = true;                  
                    }
                }
                
            }
        }) 
        $A.enqueueAction(action);
         
        /* 
        var action = component.get('c.TransactionDetails');
        var self = this; 
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "offerId" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            debugger;
            if(state === "SUCCESS"){
                component.set("v.Transactions",result);
          if(component.get("v.IsBuyerManager"))// && !result.Trans.RFQChk__c)
                {
                    var json = JSON.parse(result.Trans.Request_For_Information__c);
                    for(var i = 0; i < json.length; i++) {
                        var obj = json[i];
                        if(obj.Flag=='TRUE'){
                            debugger; 
                            // alert(obj.ID);
                            document.getElementById(obj.ID).style.display = "none";
                            //var hidediv = document.getElementById(obj.ID).value; 
                            //var hidediv = component.find(obj.ID);  
                            
                            //if(hidediv){
                             //   $A.util.addClass(hidediv, 'hide');
                           // }
                        }
                        
                    }
                }
            }  }) 
        $A.enqueueAction(action);*/
     },
    onchangechkbox : function(component, event, helper){
        debugger;
        var lfckv = document.getElementById(event.target.id).checked;
        
        var action = component.get('c.addremoveunregisteredCP');  
        action.setParams({
            "UserID" : event.target.id,
            "GId" : component.get("v.TransId"),
            "Flag" : lfckv
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
            }
        });
        $A.enqueueAction(action);
        
    },
    registeruser: function(component, event) { 
        component.set("v.Customer", true);
        component.set("v.benefits",true);
        component.set("v.clrbenefits",'active1');
        component.set("v.userinfo",false);
        component.set("v.setpwd",false);
        component.set("v.finish",false);
    },
	registeruser: function(component, event) { 
        component.set("v.Customer", true);
        component.set("v.benefits",true);
        component.set("v.clrbenefits",'active1');
        component.set("v.userinfo",false);
        component.set("v.setpwd",false);
        component.set("v.finish",false);
    },
    bennext: function(component, event) { 
        
        
        var action = component.get('c.GetLoggedUserInfo');  
        action.setParams({
            "LoggedUserID" : localStorage.getItem('UserSession')
        });
        debugger;
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.UM",result);
                component.set("v.benefits",false);
                component.set("v.userinfo",true);
                component.set("v.clrbenefits",'completed');
                component.set("v.clrud",'active1');
            }
        });
        debugger;
        $A.enqueueAction(action);
        
    },
    bennext1 : function(component, event) {
        debugger;
        var action = component.get('c.viewRequestUsers1');  
        action.setParams({
            "GId" : component.get("v.TransId")                               
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){                
                component.set("v.Transactions", result.CounterParties);
               
                if(result.CounterParties.length>0){
                    component.set("v.SelectedCPlength",true);
                }else{
                    component.set("v.SelectedCPlength",false);
                }
                component.set("v.UnRegisterviewList",result.CPSelection); 
                if(result.CPSelection.length>0){
                    component.set("v.UnRegisterviewListlength",true);
                }else{
                    component.set("v.UnRegisterviewListlength",false);
                }
                
                if(!component.get("v.UnRegisterviewListlength") && !component.get("v.SelectedCPlength"))
                {
                   
                   component.set("v.errorchk", true);
                }else{
                    component.set("v.errorchk", false);
                    component.set("v.benefits1",false);
                    component.set("v.userinfo1",true);
                    component.set("v.clrbenefits1",'completed');
                    component.set("v.clrud1",'active1');
                }
                
            }
        });
        $A.enqueueAction(action); 
        
    },
    
    
    backtoben: function(component, event) { 
        component.set("v.benefits",true);
        component.set("v.userinfo",false);
        component.set("v.clrbenefits",'active1');
        component.set("v.clrud",'');
    }, backtoben1: function(component, event) { 
        component.set("v.benefits1",true);
        component.set("v.userinfo1",false);
        component.set("v.clrbenefits1",'active1');
        component.set("v.clrud",'');
    },
    nexttosetpwd: function(component, event) { 
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        
        //var FirstName = component.find("FirstName").get("v.value");
        //var sitename = component.find("sitename").get("v.value");
       // var email = component.find("email").get("v.value");
        //var lastname = component.find("lastname").get("v.value");
        //var swiftcode = component.find("swiftcode").get("v.value");
        //var location = component.find("location").get("v.value");
        var phone = component.find("phone").get("v.value");   
        var Mobile = component.find("Mobile").get("v.value");  
        var countrycode = component.find("auracountrycode").get("v.value");
        var mobilecountrycode = component.find("auramobilecountrycode").get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        
        
        /*if(FirstName == '' || FirstName == 'undefined' || FirstName == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.FirstNameError", 'Input field required'); 
        } else { 
            component.set("v.FirstNameError", ''); 
        } 
        
        if(sitename == '' || sitename == 'undefined' || sitename == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LegalNameError", 'Input field required'); 
        } else { 
            component.set("v.LegalNameError", ''); 
        } 
        
        if(lastname == '' || lastname == 'undefined' || lastname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LastNameError", 'Input field required'); 
        } else { 
            component.set("v.LastNameError", ''); 
        } 
        if(swiftcode == '' || swiftcode == 'undefined' || swiftcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.SwiftCodeError", 'Input field required'); 
        } else { 
            component.set("v.SwiftCodeError", ''); 
        } */
        if(!$A.util.isEmpty(phone))
        {   
            if(countrycode == '' || countrycode =='--Select--' || countrycode == 'undefined' || countrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.countrycodeError", 'Input field required'); 
            } else { 
                component.set("v.countrycodeError", ''); 
            }
            
            if(!phone.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please enter valid phone number'); 
            } 
            else if(phone.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.PhoneError", ''); 
                }
        }
        if(!$A.util.isEmpty(Mobile))
        {   
            if(mobilecountrycode == '' || mobilecountrycode =='--Select--' || mobilecountrycode == 'undefined' || mobilecountrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.mobilecountrycodeError", 'Input field required'); 
            } else { 
                component.set("v.mobilecountrycodeError", ''); 
            }            
            if(!Mobile.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please enter valid Mobile number'); 
            } 
            else if(Mobile.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.MobileError", ''); 
                }
        }
        if(errorFlag == 'false') {
            
            
            
            var action = component.get('c.UpdateRegForm');  
            action.setParams({
                "LoggedUserID" : localStorage.getItem('UserSession'),            
                "UM" : component.get("v.UM")
            });
            debugger;
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.newcounterPopup", false);
                    component.set("v.onboard", true);
                    
                    component.set("v.userinfo",false);
                    component.set("v.setpwd",true);
                    component.set("v.clrbenefits",'completed');
                    component.set("v.clrud",'completed');
                    component.set("v.clrspwd",'active1');
                    
                }
            });
            debugger;
            $A.enqueueAction(action);
        }
    },
    declinequote: function(component, event) { 
        
         component.set("v.rejectpopup",true);
    },
    rerequestquote: function(component, event) { 
        component.set("v.rereqRFQ",event.target.id);
        component.set("v.reRFQ",true);
    },
    RFQrecancel: function(component, event) { 
        
        component.set("v.reRFQ",false);
    },
    RFQresave: function(component, event) { 
        
         var action = component.get('c.RerequstforRFQ');
            var self = this; 
            action.setParams({
                "offerId" : component.get("v.TransId"),
                "LoggedUserId" : localStorage.getItem('UserSession'),
                "RreqRFQID" :component.get("v.rereqRFQ")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                debugger;
                if(state === "SUCCESS"){  
                    component.set("v.reRFQ",true);
                    $A.get('e.force:refreshView').fire();
                }
            }); 
            $A.enqueueAction(action);   
        
    },
    
    rejectsuccess: function(component, event) { 
        var errorFlag = 'false';
        var deccmt = component.find("declinecmt").get("v.value");
        
        
        if(deccmt == undefined || deccmt == ""){
            errorFlag = 'true';
            component.set("v.saverejectnotes",'Input field required');
        }else{
            component.set("v.saverejectnotes",'');
        }
        if(errorFlag == 'false') {
            debugger;
            var action = component.get('c.declinequotemethod');
            var self = this; 
            action.setParams({
                "offerId" : component.get("v.TransId"),
                "LoggedUserId" : localStorage.getItem('UserSession'),
                "comments" :deccmt
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                debugger;
                if(state === "SUCCESS"){  
                    component.set("v.rejectpopup",false);
                    $A.get('e.force:refreshView').fire();
                }
            }); 
            $A.enqueueAction(action);   
        }
    },
    
    rejectcancel: function(component, event) { 
        
         component.set("v.rejectpopup",false);
    },
    backtouser: function(component, event) { 
        debugger;
        component.set("v.userinfo",true);
        component.set("v.setpwd",false);
        component.set("v.clrbenefits",'completed');
        component.set("v.clrud",'active1');
        component.set("v.clrspwd",'');
    },
    nexttofinish: function(component, event) { 
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var newPassword = component.find("newPassword").get("v.value");
        var confirmPassword = component.find("confirmPassword").get("v.value"); 
        var re = new RegExp("^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$");
        
        if(newPassword == undefined || newPassword == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.newpassworderror",'Input field required');
        }else{
            component.set("v.newpassworderror",'');
        }
        if(confirmPassword == undefined || confirmPassword == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.confirmpassworderror",'Input field required');
        }
        else if(newPassword != confirmPassword){  
            errorFlag = 'true';
            component.set("v.confirmpassworderror",'');
            component.set("v.changepasswordvalidation",true);
            component.set("v.validation", false);
            component.set("v.combinationvalidation",false);
        }
            else if (!re.test(confirmPassword)) {
                errorFlag = 'true';
                component.set("v.combinationvalidation",true);
                component.set("v.changepasswordvalidation",false);
                component.set("v.validation",false);
            }
        
        
        if(errorFlag == 'false') {
            var action = component.get('c.UpdateLBFROMMain');
            var self = this; 
            action.setParams({
                "ID" : localStorage.getItem("UID"),               
                "newPassword" : newPassword
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                debugger;
                if(state === "SUCCESS"){       
                    component.set("v.setpwd",false);
                    component.set("v.finish",true);
                    component.set("v.clrbenefits",'completed');
                    component.set("v.clrud",'completed');
                    component.set("v.clrspwd",'completed');
                    component.set("v.clrfinish",'active1');
                }
            }); 
            $A.enqueueAction(action);    
        } 
    },
    finishben: function(component, event) { 
        debugger;
        component.set("v.clrbenefits",'');
        component.set("v.clrud",'');
        component.set("v.clrspwd",'');
        component.set("v.clrfinish",'');
        component.set("v.Customer",false);
        $A.get('e.force:refreshView').fire();
    },
    closeregistration: function(component, event) { 
        component.set("v.clrbenefits",'');
        component.set("v.clrud",'');
        component.set("v.clrspwd",'');
        component.set("v.clrfinish",'');
        component.set("v.Customer",false);
    },
    getOfferDetails: function(component, event, helper) { 
        debugger;
        // component.set("v.bindtabnamesforoff",'Offers');
        var action = component.get('c.OfferDetails');
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        };
        
        var offerId = getUrlParameter('transactionId');
        
        var status = getUrlParameter('Status');
        
        var self = this; 
        action.setParams({
            "offerId" : offerId
        });
        
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            debugger;
            if(state === "SUCCESS"){
                
                var nextAction = component.get("c.secondarymarketingflag"); 
                debugger;
                nextAction.setCallback(this, function(actionResult1) {
                    var state1 = actionResult1.getState(); 
                    var results = actionResult1.getReturnValue();
                    if(state1 === "SUCCESS"){
                        debugger;
                        if(results)
                        {
                            component.set("v.bindtabnamesforoff",'Quotes');
                            component.set("v.bindtabnamesfornotes",'Internal Memos');
                            component.set("v.bindtabnamesforPart",'Participant');
                            
                        }else{
                            component.set("v.bindtabnamesforoff",'Quotes');
                            component.set("v.bindtabnamesfornotes",'Internal Memos');
                            component.set("v.bindtabnamesforPart",'Counterparties');
                        }
                        component.set("v.issecondaryMarket",results);
                        component.set("v.issecondaryMarketflag",true);
                    }
                });
                $A.enqueueAction(nextAction);
                //alert(result.OfferExtended.Offer.Offer_Status__c);
                var Transstatus=result.OfferExtended.Offer.Offer_Status__c;
                component.set("v.Created",result.OfferExtended.Offer.CreatedDate);
                
                var appdate = result.OfferExtended.Offer.Approvedate__c;
                //alert(appdate);
                if(appdate!='undefined' || appdate!='' )
                {
                    component.set("v.approveddate",appdate);
                }
                
                
                var valdate = result.OfferExtended.Offer.validateddate__c;
                if(valdate=='undefined' || valdate!='' )
                {  
                    component.set("v.Validateddate",valdate);
                }
                //alert(Transstatus);
                component.set("v.Tstatus",Transstatus);
                
                debugger;
                var Bidstatus='';
                var Bidstatus1='';
                if(result.OfferExtended.Syndications.length > 0)
                {
                    if(result.OfferExtended.Syndications[0].Syndication.Bid_Status__c=='Bid Closed'){
                        Bidstatus ='Bid Closed';
                    }
                    if(result.OfferExtended.Syndications[0].Syndication.Bid_Status__c=='Bid Accepted'){
                        Bidstatus1 ='Bid Accepted';
                    }
                }
                if(Transstatus==='Transaction Created' || Transstatus==='Pending Validation' || Transstatus==='Level 1 - Pending Approval' 
                   || Transstatus==='Pending Approval' || Transstatus==='Validation Rejected' || Transstatus==='Transaction Rejected')
                {
                    component.set("v.Tprogress", "process");
                    component.set("v.Tbox", "p-card progress-border");
                    component.set("v.hTstatus","spinneryes");
                } else if(Transstatus == 'Inactive'){
                    component.set("v.Tprogress", "red");
                    component.set("v.Tbox", "p-card red-border");
                    //component.set("v.hTstatus","spinneryes"); 
                    
                }
                
                    else if(Transstatus==='Transaction Approved' || Transstatus==='Requested For Info')
                    {   
                        if(Transstatus==='Transaction Approved'){
                            component.set("v.RFI","Requested For Info / Quote");
                        }else{
                            component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                            component.set("v.RequestedBy",result.OfferExtended.CreditUnion.CU_Name__c);
                            
                            component.set("v.RFI","Requested For Info");
                        }
                        component.set("v.Tprogress", "active");
                        component.set("v.Tbox", "p-card active-border");
                        component.set("v.RFIprogress", "process");
                        component.set("v.RFIbox", "p-card progress-border");
                        component.set("v.hTstatus","spinnerno");
                        component.set("v.hRFQstatus","spinneryes");
                        component.set("v.FullRFIstatus",true);
                    }      
                        else if(Transstatus==='Requested For Quote' || Transstatus==='Bid Submitted')
                        {
                            
                            component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                            component.set("v.RequestedBy",result.OfferExtended.CreditUnion.CU_Name__c);
                            component.set("v.Tprogress", "active");
                            component.set("v.RFIprogress", "active");
                            component.set("v.RFIbox", "p-card active-border");
                            component.set("v.hTstatus","spinnerno");
                            component.set("v.hRFQstatus","spinnerno");
                            component.set("v.Tbox", "p-card active-border");
                            component.set("v.Bidprogress", "process");
                            component.set("v.Bidbox", "p-card progress-border");
                            component.set("v.hTstatus","spinnerno");
                            component.set("v.RFI","Requested For Quote");
                            component.set("v.hRFQstatus","spinnerno");
                            component.set("v.hbidstatus","spinneryes");
                            component.set("v.FullRFIstatus",true);
                            component.set("v.FullBidstatus",true); 
                            if(result.OfferExtended.Syndications.length > 0 && result.LogeduserCBId !=''){
                                component.set("v.Tstatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                            }
                            if(Transstatus==='Bid Submitted')
                            {
                                if(result.OfferExtended.Syndications.length > 0){
                                    component.set("v.BidStatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                    component.set("v.LastBidDate", result.OfferExtended.Syndications[0].Syndication.submitted_Date__c);
                                    component.set("v.HighestBidder",result.OfferExtended.Syndications[0].Syndication.Buyer_Name__c);
                                    component.set("v.progressBidamount",result.OfferExtended.Syndications[0].Syndication.Buyer_Counter_Price__c);
                                }
                            }
                            
                            
                        }
                            else if(Transstatus==='Bid Accepted')
                            {
                                
                                if(result.OfferExtended.Syndications.length > 0 && result.LogeduserCBId !=''){
                                    component.set("v.Tstatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                }
                                if(Bidstatus1 =='Bid Accepted' && result.LogeduserCBId !='' || result.LogeduserCBId==''){
                                    if(result.OfferExtended.Syndications.length > 0){
                                        
                                        component.set("v.BidStatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                        component.set("v.LastBidDate", result.OfferExtended.Syndications[0].Syndication.submitted_Date__c);
                                        component.set("v.HighestBidder",result.OfferExtended.Syndications[0].Syndication.Buyer_Name__c);
                                        component.set("v.progressBidamount",result.OfferExtended.Syndications[0].Syndication.Buyer_Counter_Price__c);
                                        
                                    }
                                    component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                                    component.set("v.RequestedBy",result.OfferExtended.CreditUnion.CU_Name__c);
                                    component.set("v.Tprogress", "active");
                                    component.set("v.RFIprogress", "active");
                                    component.set("v.RFIbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.hRFQstatus","spinnerno");
                                    component.set("v.Tbox", "p-card active-border");
                                    component.set("v.Bidprogress", "active");
                                    component.set("v.Bidbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.RFI","Requested For Quote");
                                    component.set("v.comprogress", "process");
                                    component.set("v.combox", "p-card progress-border");
                                    component.set("v.hcomstatus","spinneryes");
                                    component.set("v.hbidstatus","spinnerno");
                                    component.set("v.FullRFIstatus",true);
                                    component.set("v.FullBidstatus",true); 
                                    component.set("v.Fullcomstatus",true);
                                }else{
                                    //component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                                    //component.set("v.RequestedBy",result.OfferExtended.CreditUnion.CU_Name__c);
                                    component.set("v.Tprogress", "active");
                                    component.set("v.RFIprogress", "active");
                                    component.set("v.RFIbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.hRFQstatus","spinnerno");
                                    component.set("v.Tbox", "p-card active-border");
                                    component.set("v.Bidprogress", "active");
                                    component.set("v.Bidbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.RFI","Requested For Quote");
                                    component.set("v.comprogress", "process");
                                    component.set("v.combox", "p-card progress-border");
                                    component.set("v.hcomstatus","spinneryes");
                                    component.set("v.hbidstatus","spinnerno");
                                    component.set("v.FullRFIstatus",true);
                                    component.set("v.FullBidstatus",true); 
                                    component.set("v.Fullcomstatus",false);
                                }
                            }else if(Transstatus==='Completed Transaction')
                            {
                                
                                if(result.OfferExtended.Syndications.length > 0 && result.LogeduserCBId !=''){
                                    component.set("v.Tstatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                }
                                if(Bidstatus =='Bid Closed' && result.LogeduserCBId !='' || result.LogeduserCBId==''){                                
                                    component.set("v.FullRFIstatus",true);
                                    component.set("v.FullBidstatus",true); 
                                    component.set("v.Fullcomstatus",true);
                                    debugger;
                                    component.set("v.Completed",result.finalizedOfferdate);
                                    component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                                    component.set("v.RequestedBy",result.OfferExtended.CreditUnion.CU_Name__c);
                                    component.set("v.Tprogress", "active");
                                    component.set("v.RFIprogress", "active");
                                    component.set("v.RFIbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.hRFQstatus","spinnerno");
                                    component.set("v.Tbox", "p-card active-border");
                                    component.set("v.Bidprogress", "active");
                                    component.set("v.Bidbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.RFI","Requested For Quote");
                                    component.set("v.hcomstatus","spinnerno");
                                    component.set("v.comprogress", "active");
                                    component.set("v.combox", "p-card active-border");
                                    component.set("v.hbidstatus","spinnerno");
                                    if(result.OfferExtended.Syndications.length > 0)
                                    {
                                        if(result.LogeduserCBId !=''){
                                            component.set("v.Tstatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                        }
                                        component.set("v.BidStatus",result.OfferExtended.Syndications[0].Syndication.Bid_Status__c);
                                        component.set("v.LastBidDate", result.OfferExtended.Syndications[0].Syndication.submitted_Date__c);
                                        component.set("v.HighestBidder",result.OfferExtended.Syndications[0].Syndication.Buyer_Name__c);
                                        component.set("v.progressBidamount",result.OfferExtended.Syndications[0].Syndication.Buyer_Counter_Price__c);
                                    }
                                }else{
                                    component.set("v.FullRFIstatus",true);
                                    component.set("v.FullBidstatus",true); 
                                    component.set("v.Fullcomstatus",false);
                                    debugger;
                                    component.set("v.Completed",result.finalizedOfferdate);
                                    component.set("v.Requesteddate",result.OfferExtended.Offer.PublishedDate__c);
                                    component.set("v.RequestedBy",result.OfferExtended.CreditUnion.CU_Name__c);
                                    component.set("v.Tprogress", "active");
                                    component.set("v.RFIprogress", "active");
                                    component.set("v.RFIbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.hRFQstatus","spinnerno");
                                    component.set("v.Tbox", "p-card active-border");
                                    component.set("v.Bidprogress", "active");
                                    component.set("v.Bidbox", "p-card active-border");
                                    component.set("v.hTstatus","spinnerno");
                                    component.set("v.RFI","Requested For Quote");
                                    component.set("v.comprogress", "process");
                                    component.set("v.combox", "p-card progress-border");
                                    component.set("v.hcomstatus","spinneryes");
                                    component.set("v.hbidstatus","spinnerno");
                                }
                            }
                
                debugger;
                debugger;
                var container = component.find("container");
                $A.util.removeClass(container, "slds-hide");
                $A.util.addClass(container, "slds-show");
                
                $A.createComponent("c:"+result.OfferExtended.Offer.Component_Name__c,
                                   {
                                       "aura:id": 'MyCustomComponent',
                                       "edit": 'false',
                                       "mode": 'VIEW',
                                       "objectApiName": result.OfferExtended.Offer.SObject_Name__c,
                                       "recordId": result.OfferExtended.Offer.Record_Id__c
                                   },
                                   function(newComponent, status, errorMessage){ 
                                       debugger;
                                       var targetCmp = component.find('placeHolder');
                                       
                                       targetCmp.set("v.body", []); 
                                       var body = targetCmp.get("v.body");
                                       console.log('@body'+body);          
                                       if((result.isBuyer || result.isBuyerValidator) && !result.OfferExtended.Offer.RFQChk__c && !(result.OfferExtended.Offer.Granted__c))
                                       {
                                           //if(result.OfferExtended.Offer.Request_For_Information__c!=NULL){
                                           debugger;
                                           var json = JSON.parse(result.OfferExtended.Offer.Request_For_Information__c);
                                           for(var i = 0; i < json.length; i++) {
                                               var obj = json[i];
                                               if(obj.Flag=='TRUE'){
                                                   var hidediv = newComponent.find('div'+obj.ID);
                                                   if(hidediv){
                                                       $A.util.addClass(hidediv, 'hide');
                                                   }
                                               }
                                               
                                           }
                                           //}
                                       }
                                       body.push(newComponent);
                                       targetCmp.set("v.body", body); 
                                       
                                       
                                   });   
                debugger;
                component.set("v.Offer", result);
                
                // if(result.isBuyerValidator || result.isBuyer){
                //     component.set("v.RNSize",result.requestNotes.length);
                // }
                
                if(result.OfferExtended.Offer.Seller_Notes__c !=''){
                    //component.set("v.corporateKeyword",result.Offers.BankMSg);
                    component.set("v.editcall",'no');
                }
                else
                {
                    component.set("v.editcall",'yes');
                }
                //alert(result.LogeduserID);
                component.set("v.Loggeduserid",result.LogeduserID);
                
                document.title = result.OfferExtended.Offer.TransactionRefNumber__c;
                component.set("v.shareSubject",'TradeAix : Transaction Details - '+result.OfferExtended.Offer.TransactionRefNumber__c);
                component.set("v.sharebodytxt",'Hello,  I am sharing this transaction from TradeAix platform and would like you to have a look at the attached PDF document and let me know your feedback.  Regards , '+result.OfferExtended.CreditUnion.CU_Name__c);
                
                component.set("v.offerId", result.offerId); 
                component.set("v.parentId", result.offerId);
                component.set("v.SellerUserId", result.OfferExtended.Offer.CreatedById);
                debugger;
                component.set("v.validation", false);
                component.set("v.loanError", '');
                component.set("v.templateError", '');
                component.set("v.nameError", '');    
                component.set("v.participationAmountError", '');
                component.set("v.participationPercentageError", '');
                component.set("v.LogeduserCUnionId", result.LogeduserCBId);
                component.set("v.credtiunionchk", result.credtiunionchk);
                component.set("v.PartyID", result.OfferExtended.CreditUnion.Id);
                component.set("v.Partname", result.OfferExtended.CreditUnion.CU_Name__c);
                if(result.isSeller) {
                    debugger;    
                    component.set("v.Quotelength", result.OfferExtended.Syndications.length);
                    component.set("v.attlen", result.OfferExtended.AttachmentRequests.length);
                    try{ component.set("v.selectedTabId", "0"); } catch(ex){}
                } else {
                    component.set("v.selectedTabId", "1");
                }
                //changeParticipationAmount(component, event, helper);
                //changeParticipationPercentage(component, event, helper);
            }
            var menu = document.getElementById('Transactions');
            var classArr = menu.className.split(" ");
            if (classArr.indexOf("current") == -1) {
                menu.className += " " + "current";
            }
            
        }); 
        $A.enqueueAction(action);
    },  
    savecorporatetxt : function(component, event,helper) { 
        debugger;
        var txtcorporate = component.find("cptext").get("v.value"); 
        // alert(txtcorporate);
        var errorFlag = 'false';
        if(txtcorporate == '' || txtcorporate == null || txtcorporate == undefined || txtcorporate.trim().length*1 == 0) 
        {  
            errorFlag = 'true';
            component.set("v.txtcor", 'Input field required'); 
        }
        else{  
            component.set("v.txtcor", ''); 
        }  
        if(errorFlag == 'false')
        { 
            var action = component.get('c.savesavecorporatetxt'); 
            action.setParams({ 
                "txtcor" : txtcorporate,
                "offerId" : component.get("v.offerId")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    debugger;
                    $A.get('e.force:refreshView').fire();
                    
                } 
            });
            $A.enqueueAction(action);
        }
    }, 
    handleResponse : function(component, event, helper) {
        var response = event.getParam("response");
    },
    uploadfilepopup: function(component, event) {     
        debugger;
        component.set("v.opendocvault", true); 
    },
    editclick: function(component){
        //component.set("v.BankMSg",'');
        component.set("v.editcall",'yes');
    },
    cancelcorporatetxt: function(component){
        debugger;
        //component.set("v.BankMSg",'');
        component.set("v.editcall",'no');
    },
    btncancelfor: function(component, event) {     
        debugger;
        component.set("v.opendocvault", false); 
    },
    CPrevokeCheck: function(component, event, helper) {
        /*  if(component.find("CPrevokeAccess").get('v.value')==true)
        {
            component.find("CPrevoke").set("v.disabled", false);
        }
        else{
            component.find("CPrevoke").set("v.disabled", true);
        }*/
    },
    CPConfirmcan: function(component, event) {
        component.set("v.CPConfirm", false);
    },
    downloadCP: function(component, event) {
        var action = component.get("c.downloadCP"); 
        action.setParams({
            "offerId" : event.target.id           
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){
            }
        });
    },
    CPConfirmpopup : function(component, event) { 
        //alert(event.target.id);
        component.set("v.counterPopup", false);
        component.set("v.CPConfirm", true);
        
    }, 
    
    CPrevokecancel: function(component, event) {
        component.set("v.CPrevokeCheckpop", false);
    },
    
    CPrevokepopup : function(component, event) { 
        //alert(event.target.id);
        debugger;
        component.set("v.CPID",event.target.id);
        component.set("v.CPrevokeCheckpop", true);       
    },  
    removeCP: function(component, event) {     
        debugger;       
        var action = component.get("c.removeCPMethod"); 
        action.setParams({
            "offerId" : component.get("v.TransId"),
            "counterids" :component.get("v.CPID"),
            "LoggedUserId" : localStorage.getItem('UserSession')
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){
                $A.get('e.force:refreshView').fire();
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Counterparties Sucessfully Revoked',
                        'type': 'Success'
                    }
                );
                showToast.fire();
            }});
        $A.enqueueAction(action);
        
    },
    requestForInfo : function(component, event) {
        
        debugger;
        console.log('@inside info@');
        component.set("v.requestForInfo", true); 
        var contentVersion = component.get("v.Offer.content");
        var attachment = component.get("v.Offer.attachment");
        var errorFlag = 'false';
        var counterParties = component.get("v.Transactions.CounterParties");
        if(errorFlag == 'true') 
        {
            component.set("v.PubEditError", 'One or more fields below do not have appropriate data');
        }
        else{ 
            /*
            if((contentVersion.length > 0 || attachment.length > 0) && counterParties.length > 0 && errorFlag == 'false') 
            {
                component.set("v.publishOffer", true); 
            }         
            else
            {
                if(counterParties.length <= 0){
                    component.set("v.counterError", true);
                } else {
                    component.set("v.publishError", true);
                }
            }
            */
            
            if((counterParties.length > 0 || component.get("v.Transactions.CPSelection").length >0) && errorFlag == 'false') 
            {
                component.set("v.POPrequestForInfo", true); 
                //component.set("v.publishOffer", true); 
            }         
            else
            {
                if(counterParties.length <= 0 || component.get("v.Transactions.CPSelection").length <=0){
                    component.set("v.counterError", true);
                    component.find("tabs").set("v.Activetab",3);
                } 
            }
        }
        var action = component.get('c.getTransactionfields');
        //var Tid='a07m0000008DkAbAAK';
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        };
        var Tid=getUrlParameter('transactionId'); 
        console.log('@@'+Tid);
        debugger;
        var self = this;
        action.setParams({
            "Transid" : Tid          
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                console.log('@@'+result);
                //component.set("v.templateforRFI", result);
                var custs = [];
                for(var key in result){
                    debugger;
                    custs.push({value:result[key], key:key}); //Here we are creating the list to show on UI.
                }
                component.set("v.lensize",custs.length%2); 
                component.set("v.templateforRFI",custs); 
            }
            else if(state === "ERROR") {
                debugger;
            }
        });
        $A.enqueueAction(action);
    },
    onCheck: function(component, event) { 
        debugger;
        var checkboxes = document.getElementsByClassName("chk");
        
        var chkTF = [];
        for(var i = 0; i < checkboxes.length; i++)
        {  
            if (checkboxes[i].checked) {
                
                console.log('##'+event.target.value);
                
                chkTF.push(event.target.value);
            }
            
            
        } 
        
        component.set("v.CheckedTfields", chkTF);
        var d=component.get("v.CheckedTfields");
        onsole.log('#&#'+d);
    },	
    saveTR: function(component, event) {
        debugger;
        var jsonObj='';
        var getAllId = component.find("boxPack");
        var len=getAllId.length-1;
        var j = 0;
        for (var i = 0; i < getAllId.length; i++) {        
            
            if (getAllId[i].get("v.value") == true) {
                jsonObj=jsonObj+getAllId[i].get("v.text")+',';
                j++;
            }
            
        }  
       
        component.set('v.maskcount',j);
        debugger;
        var action = component.get('c.SaveRFI'); 
        
        action.setParams({
            "RFI" : jsonObj,
            "Transid" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.POPrequestForInfo", false); 
                component.set("v.publishOffer", true); 
            }
        });
        $A.enqueueAction(action);
    },
    publishcancel : function(component, event) {
        component.set("v.publishOffer", false);
    },
    btnpublish: function(component, event, helper) {
        
        var action = component.get('c.publish');
        var self = this; 
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "offerId" : component.get("v.TransId"),
            "requestForInfo": component.get("v.requestForInfo")            
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.publishOffer", false);
            component.set("v.SecondpublishPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);
                component.set("v.offerId", result.offerId);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    publishingOffer : function(component, event) {
        debugger;
        component.set("v.requestForInfo", false); 
        var contentVersion = component.get("v.Offer.content");
        var attachment = component.get("v.Offer.attachment");
        var errorFlag = 'false';
        
        var counterParties = component.get("v.Transactions.CounterParties");
        
        if(errorFlag == 'true') 
        {
            component.set("v.PubEditError", 'One or more fields below do not have appropriate data');
        }
        else{ 
            /*
            if((contentVersion.length > 0 || attachment.length > 0) && counterParties.length > 0 && errorFlag == 'false') 
            {
                component.set("v.publishOffer", true); 
            }         
            else
            {
                if(counterParties.length <= 0){
                    component.set("v.counterError", true);
                } else {
                    component.set("v.publishError", true);
                }
            }
            */
            
            if((counterParties.length > 0 || component.get("v.Transactions.CPSelection").length >0) && errorFlag == 'false') 
            {
                //component.set("v.requestForInfo", true); 
                component.set("v.publishOfferRFQ", true); 
            }         
            else
            {
                if(counterParties.length <= 0 || component.get("v.Transactions.CPSelection").length <=0){
                    debugger;
                    component.set("v.counterError", true);
                    component.find("tabs").set("v.Activetab","3");
                    
                } 
            }
        }
    },
    counterErrorcancel : function(component, event) {
        debugger;
        component.set("v.counterError", false);
        component.find("tabs").set("v.Activetab","3");
    },
    SecondpublishPopupcancel : function(component, event) {
        component.set("v.SecondpublishPopup", false);
        $A.get('e.force:refreshView').fire();
    },
    completePopupcancel : function(component, event) {
        component.set("v.CompletePopup", false);
        $A.get('e.force:refreshView').fire();
    },
    publishErrorcancel : function(component, event) {
        component.set("v.publishError", false);
    }, 
    publishcancel : function(component, event) {
        component.set("v.publishOffer", false);
    }, 
    selectdocuvault : function(component, event) {
        component.set("v.popdocuvault", true);
    },
    canceldocuvault : function(component, event) {
        component.set("v.popdocuvault", false);
    },
    selectChange : function(component, event, helper) {
        if(component.find("chk1").get('v.value')==true)
        {
            component.find("btn1").set("v.disabled", false);
        }
        else{
            component.find("btn1").set("v.disabled", true);
        }
    }, 
    
    btnpublishRFQ: function(component, event, helper) {
        debugger;
        component.find("btn1").set("v.disabled", true);
        var action = component.get('c.publishRFQ'); 
        var self = this; 
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "offerId" : component.get("v.TransId"),
            "requestForInfo": component.get("v.requestForInfo")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.publishOfferRFQ", false);
            component.set("v.SecondpublishPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);
                component.set("v.offerId", result.offerId);  
                component.set("v.parentId", result.offerId);
                $A.get('e.force:refreshView').fire();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    publishcancel : function(component, event) {
        component.set("v.publishOffer", false);
    }, 
    publishcancelRFQ : function(component, event) {
        component.set("v.publishOfferRFQ", false);
    }, 
    UnpublishOffer : function(component, event) { 
        component.set("v.Unpublish", true);
    }, 
    unpublishCheck: function(component, event, helper) {
        if(component.find("chkUnpublish").get('v.value')==true)
        {
            component.find("Unpublishbtn1").set("v.disabled", false);
        }
        else{
            component.find("Unpublishbtn1").set("v.disabled", true);
        }
    }, 
    SecondUnpublishPopupcancel : function(component, event) {
        component.set("v.SecondUnpublishPopup", false);
    },
    btnUnpublish: function(component, event, helper) {
        var action = component.get('c.Unpublish'); 
        var self = this; 
        action.setParams({
            "offerId" : component.get("v.offerId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.Unpublish", false);
            component.set("v.SecondUnpublishPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);
                component.set("v.offerId", result.offerId);  
                component.set("v.parentId", result.offerId);               
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    Unpublishcancel: function(component, event) {
        component.set("v.Unpublish", false);
    },
    InactiveOffer1 : function(component, event) { 
        component.set("v.Inactive1", true);       
    },    
    inactiveCheck: function(component, event, helper) {
        if(component.find("chkInactive").get('v.value')==true)
        {
            debugger;
            component.find("Inactivebtn1").set("v.disabled", "false");
        }
        else{
            debugger;
            component.find("Inactivebtn1").set("v.disabled", "true");
        }
    },
    btnInactive: function(component, event, helper) {
        var action = component.get('c.Inactive'); 
        var self = this; 
        action.setParams({
            "offerId" : component.get("v.offerId"),
            "body5" : component.find("body5").get("v.value")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.Inactive1", false);
            component.set("v.SecondInactive", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);
                component.set("v.offerId", result.offerId); 
                component.set("v.parentId", result.offerId);                
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    Inactivecancel: function(component, event) {
        component.set("v.Inactive1", false);
    },
    SecondInactivecancel: function(component, event) {
        component.set("v.SecondInactive", false);
    },
    reqAccess : function(component, event) { 
        component.set("v.reqAccess", true);       
    },    
    reqCheck: function(component, event, helper) {
        if(component.find("ChkreqAccess").get('v.value')==true)
        {
            component.find("Accessbtn1").set("v.disabled", false);
        }
        else{
            component.find("Accessbtn1").set("v.disabled", true);
        }
    },
    btnreqAccess: function(component, event, helper) {
        var action = component.get('c.RequestAccess'); 
        var self = this; 
        action.setParams({
            "offerId" : component.get("v.offerId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.reqAccess", false);
            component.set("v.SecondRequestPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);
                component.set("v.offerId", result.offerId);  
                component.set("v.parentId", result.offerId);               
            }
            else if(state === "ERROR") { 
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },  
    reqAccesscancel: function(component, event) {
        component.set("v.reqAccess", false);
    },
    SecondRequestPopupcancel: function(component, event) {
        component.set("v.SecondRequestPopup", false);
    },
    grantAccess : function(component, event) {
        component.set("v.docId", event.target.id); 
        component.set("v.grantAccess", true);       
    },     
    grantCheck: function(component, event, helper) {
        if(component.find("chkGrant").get('v.value')==true)
        {
            component.find("Grantbtn1").set("v.disabled", false);
        }
        else{
            component.find("Grantbtn1").set("v.disabled", true);
        }
    }, 
    btnRequestForQuote: function(component, event, helper) {
        debugger;
        component.set("v.docId", event.target.id); 
        var action = component.get('c.GrantAccessToRFQ');
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "offerId" : component.get("v.TransId"),
            "docId" : component.get("v.docId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.SecondGrantPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }             
        });
        
        $A.enqueueAction(action);
    }, 
    btngrantAccess: function(component, event, helper) {
        var action = component.get('c.GrantAccess'); 
        var self = this; 
        action.setParams({
            "docId" : component.get("v.docId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.grantAccess", false);
            component.set("v.SecondGrantPopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }             
        });
        $A.enqueueAction(action);
    }, 
    grantAccesscancel: function(component, event) {
        component.set("v.grantAccess", false);
        $A.get('e.force:refreshView').fire();
    },
    SecondGrantPopupcancel: function(component, event) {
        component.set("v.SecondGrantPopup", false);
        $A.get('e.force:refreshView').fire();
    },
    RejectAccess : function(component, event) { 
        component.set("v.docId", event.target.id);
        component.set("v.RejectAccess", true);       
    },    
    rejectCheck: function(component, event, helper) {
        if(component.find("chkReject").get('v.value')==true)
        {
            component.find("Rejectbtn1").set("v.disabled", false);
        }
        else{
            component.find("Rejectbtn1").set("v.disabled", true);
        }
    },
    btnrejectAccess: function(component, event, helper) {
        var action = component.get('c.rejectAccess'); 
        var self = this; 
        action.setParams({
            "docId" : component.get("v.docId"),
            "body3" : component.find("body3").get("v.value")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue(); 
            component.set("v.RejectAccess", false);
            component.set("v.SecondRejectPopup", true);
            $A.log(actionResult);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    rejectAccesscancel: function(component, event) {
        component.set("v.RejectAccess", false);
    },
    POPrequestForInfocel: function(component, event) {
        component.set("v.POPrequestForInfo", false);
    },
    
    SecondRejectPopupcancel: function(component, event) {
        component.set("v.SecondRejectPopup", false);
    },
    RevokeAccess : function(component, event) { 
        component.set("v.docId", event.target.id);
        component.set("v.RevokeAccess", true);       
    },   
    revokeCheck: function(component, event, helper) {
        if(component.find("chkRevoke").get('v.value')==true)
        {
            component.find("Revokebtn1").set("v.disabled", false);
        }
        else{
            component.find("Revokebtn1").set("v.disabled", true);
        }
    }, 
    btnrevokeAccess: function(component, event, helper) {
        var action = component.get('c.revokeAccess'); 
        var self = this; 
        action.setParams({
            "docId" : component.get("v.docId"),
            "body4" : component.find("body4").get("v.value")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.RevokeAccess", false);
            component.set("v.SecondRevokePopup", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    revokeAccesscancel: function(component, event) {
        component.set("v.RevokeAccess", false);
    }, 
    SecondRevokePopupcancel: function(component, event) {
        component.set("v.SecondRevokePopup", false);
    },
    BidWithdrawn : function(component, event) { 
        component.set("v.syndId", event.target.id); 
        component.set("v.BidWithdrawn", true);       
    },     
    BidWidrawnCheck: function(component, event, helper) {
        if(component.find("chkBidWidrawn").get('v.value')==true)
        {
            component.find("BidWidrawnbtn1").set("v.disabled", false);
        }
        else{
            component.find("BidWidrawnbtn1").set("v.disabled", true);
        }
        
    }, 
    btnBidWidrawn: function(component, event, helper) {
        var action = component.get('c.postWithdrawn'); 
        var self = this; 
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "syndId" : component.get("v.syndId"),
            "Title" : localStorage.getItem("LoggeduserName"),
            "body1" : component.find("body1").get("v.value")
        });
        action.setCallback(self, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.BidWithdrawn", false);
            component.set("v.SecondBidWithdrawn", true);
            $A.log(actionResult);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);
                $A.get('e.force:refreshView').fire();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    BidWidrawncancel: function(component, event) {
        component.set("v.BidWithdrawn", false);
        $A.get('e.force:refreshView').fire();
    },
    SecondBidWithdrawncancel: function(component, event) {
        component.set("v.SecondBidWithdrawn", false);
        $A.get('e.force:refreshView').fire();
    },
    PreWithdraw : function(component, event) { 
        component.set("v.syndId", event.target.id); 
        component.set("v.PreWithdrawn", true);       
    }, 
    PreWithdrawnCheck: function(component, event, helper) {
        if(component.find("chkPreWithdrawn").get('v.value')==true)
        {
            component.find("PreWithdrawnbtn1").set("v.disabled", false);
        }
        else{
            component.find("PreWithdrawnbtn1").set("v.disabled", true);
        }
    }, 
    btnPreWithdrawn: function(component, event, helper) {
        debugger;
        var action = component.get('c.PreWithdraw1'); 
        var self = this; 
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "syndId" : component.get("v.syndId"),
            "Title" : localStorage.getItem("LoggeduserName"),
            "body2" : component.find("body2").get("v.value")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.PreWithdrawn", false);
            component.set("v.SecondPreWithdraw", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result); 
                component.set("v.parentId", result.offerId); 
                $A.get('e.force:refreshView').fire();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        
        $A.enqueueAction(action);
    }, 
    PreWithdrawncancel: function(component, event) {
        component.set("v.PreWithdrawn", false);
        $A.get('e.force:refreshView').fire();
    },    
    SecondPreWithdrawcancel: function(component, event) {
        component.set("v.SecondPreWithdraw", false);
    }, 
    AgreeBid: function(component, event) { 
        
        component.set("v.syndId", event.target.id); 
        component.set("v.AgreetoTerms", true);       
    }, 
    AgreeBidcheck: function(component, event, helper) {
        debugger;
        //var ACDate = component.find("AntDate").get("v.value");
        if(component.find("chkAgreeBid").get('v.value') == true) {// && ACDate != "" && ACDate != undefined){
            component.find("AgreeBidbtn1").set("v.disabled", false);
        } else {
            component.find("AgreeBidbtn1").set("v.disabled", true);
        }
    }, 
    btnAgreeBid: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        if(errorFlag == 'false')
        { 
            var action = component.get('c.AgreeBid1'); 
            var self = this; 
            action.setParams({
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
                "IsManager" : component.get("v.IsSellerManager"),
                "syndId" : component.get("v.syndId"),
                "Title" : localStorage.getItem("LoggeduserName"),
                "body4" : component.find("body4").get("v.value")                
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.AgreetoTerms", false);
                component.set("v.SecondAgree", true);
                if(state === "SUCCESS"){
                    component.set("v.Offer", result);  
                    component.set("v.parentId", result.offerId);
                    $A.get('e.force:refreshView').fire();
                }
                else if(state === "ERROR"){
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    }, 
    AgreeBidcancel: function(component, event) {
        component.set("v.AgreetoTerms", false);
        // $A.get('e.force:refreshView').fire();
    },
    SecondAgreecancel: function(component, event) {
        component.set("v.SecondAgree", false);
        //$A.get('e.force:refreshView').fire();
    },
    BidReject: function(component, event) { 
        component.set("v.syndId", event.target.id); 
        component.set("v.BidReject", true);       
    },    
    BidRejectcheck: function(component, event, helper) {
        if(component.find("chkBidReject").get('v.value')==true)
        {
            component.find("BidRejectbtn1").set("v.disabled", false);
        }
        else{
            component.find("BidRejectbtn1").set("v.disabled", true);
        }
    },
    btnBidReject: function(component, event, helper) {
        var action = component.get('c.BidReject1'); 
        var self = this; 
        action.setParams({
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
            "IsManager" : component.get("v.IsSellerManager"),
            "syndId" : component.get("v.syndId"),
            "Title" : localStorage.getItem("LoggeduserName"),
            "body3" : component.find("body3").get("v.value")/*jaya*/
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.BidReject", false);
            component.set("v.SecondBidReject", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result); 
                component.set("v.parentId", result.offerId); 
                $A.get('e.force:refreshView').fire();
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    BidRejectcancel: function(component, event) {
        component.set("v.BidReject", false);
        // $A.get('e.force:refreshView').fire();
    },
    SecondBidRejectcancel: function(component, event) {
        component.set("v.SecondBidReject", false);
    },
    CmpTrans: function(component, event) { 
        component.set("v.syndId", event.target.id);
        component.set("v.CmpTrans", true);       
    },    
    CmpTranscheck: function(component, event, helper) {
        if(component.find("chkCmpTrans").get('v.value')==true)
        {
            component.find("CmpTransbtn1").set("v.disabled", false);
        }
        else{
            component.find("CmpTransbtn1").set("v.disabled", true);
        }
    },
    btnCmpTrans: function(component, event, helper) {
        
        var img = component.find("pdfImgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');
        
        debugger;
        //component.find(event.target.id).set("v.disabled", true);
        var action = component.get('c.createFinalized'); 
        var self = this;
        action.setParams({
            "offerId" : component.get("v.TransId"),
            "syndId" : event.target.id
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                $A.get('e.force:refreshView').fire();
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Transaction Completed Successfully',
                        'type': 'Success'
                    }
                );
                showToast.fire();
                component.set("v.CompletePopup", true);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        
        $A.enqueueAction(action);
    }, 
    CmpTranscancel: function(component, event) {
        component.set("v.CmpTrans", false);
    },
    EditofferNotes : function(component, event, helper) { 
        component.set("v.EditofferNotes", true);       
    }, 
    EditcancelNotes : function(component, event) {
        component.set("v.EditofferNotes", false);
    },
    
    updateSellerNotes: function(component, event, helper) {
        
        var OfferObjectnote = component.get("v.Offer.OfferExtended.Offer");
        // Create the action
        var action = component.get("c.updateNotes");
        action.setParams({ "saveSellerOffersNotes": OfferObjectnote });
        
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Offer.OfferExtended.Offer", response.getReturnValue());
                component.set("v.showpopup", false);
                $A.get('e.force:refreshView').fire();
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Transaction notes Updated Sucessfully',
                        'type': 'Success'
                    }
                );
                showToast.fire();
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        
        // Send action off to be executed
        $A.enqueueAction(action);
    },
    Editoffer : function(component, event, helper) { 
        debugger;              
        var action = component.get('c.editviewTransaction'); 
        var self = this;
        action.setParams({
            "transid" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
               component.set("v.Transactions.TransAtt", result.TransAtt); 
               component.set("v.Editoffer", true); 
            }
            
        });        
        $A.enqueueAction(action);
    }, 
    Editcancel : function(component, event) {
        component.set("v.Editoffer", false);
        component.set("v.validation", false);
        component.set("v.loanError", '');
        component.set("v.templateError", '');
        component.set("v.nameError", '');
        component.set("v.participationAmountError", '');
        component.set("v.participationPercentageError", '');
        // $A.get('e.force:refreshView').fire();
    },
    changeParticipationAmount: function(component, event, helper) { 
        var ParticipationAmount = component.find("ParticipationAmount").get("v.value");        
        if(ParticipationAmount != undefined)
        { 
            var val = helper.numberWithoutCommas(ParticipationAmount); 
            var val1 = helper.numberWithCommas(val);
            //component.find("ParticipationAmount").set("v.value",val1);
            component.set("v.Offer.OfferExtended.Offer.ParticipationAmount__c", val1);
        }  
    },
    changeParticipationPercentage: function(component, event, helper) {  
        var ParticipationPercentage = component.find("ParticipationPercentage").get("v.value"); 
        if(ParticipationPercentage != undefined)
        { 
            var val = helper.numberWithoutCommas(ParticipationPercentage); 
            var val1 = helper.round(val, 3); 
            var val2 = helper.numberWithCommas(val1);
            //component.find("ParticipationPercentage").set("v.value", val2);
            component.set("v.Offer.OfferExtended.Offer.ParticipationPercentage__c", val2); 
        }         
    }, 
    ParticipationAmount: function(component, event, helper) {
        var inp = component.find("ParticipationAmount").get("v.value"); 
        if(inp == 0)
            //component.find("ParticipationAmount").set("v.value", "");
            component.set("v.Offer.OfferExtended.Offer.ParticipationAmount__c", null);
    },
    ParticipationPercentage: function(component, event, helper) {
        var inp = component.find("ParticipationPercentage").get("v.value"); 
        if(inp == 0.000)
            //component.find("ParticipationPercentage").set("v.value","");
            component.set("v.Offer.OfferExtended.Offer.ParticipationPercentage__c", null);
    },
    SaveEditTransaction:function(component, event, helper){
        debugger;   
        
        var SellNotesib = component.find("seller_notesib").get("v.value");
        var Attoptions = [];
        var tempatt = component.get("v.editattributesstring");
        for(var i = 0; i < tempatt.length; i++) {
            if(i !=''){
                Attoptions.push(tempatt[i]);
            }            
        }
        /*var TemObj = component.get("v.Transactions.TransAtt");
        for(var k = 0; k < TemObj.length; k++) {
            var I = TemObj[k].Id;
            var T = TemObj[k].Attribute_Name__c;
            var D = document.getElementById(T).value; 
            
            Attoptions.push(I+':'+T+':'+D);
        } */
        var idListJSON=JSON.stringify(Attoptions);
        var Action = component.get("c.updateTransaction");        
        Action.setParams({
            "transid" : component.get("v.TransId"),
            "TransAttribute" : idListJSON,
            "userid" : component.get("v.Loggeduserid"),
            "notes" : SellNotesib
        });        
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Attributes Updated Successfully',
                        'type': 'Success'
                    }
                );
                showToast.fire();
                $A.get('e.force:refreshView').fire();
                
            }
        });
        $A.enqueueAction(Action);
        
        
        
    },
    SaveEditOffer : function(component, event, helper) { 
        debugger;
        var errorFlag = 'false';
        //var offerName = component.find("offername").get("v.value");
        /*
         * commented by jaya since name field is not needed
         * 
        if(offerName == '' || offerName == null || offerName == undefined || offerName.trim().length*1 == 0) 
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.nameError", 'Input field required'); 
        }
        else
        { 
            if(helper.checkSpecialCharecter1(offerName))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.nameError", 'The product name has special characters. These are not allowed.'); 
            } 
            else{  
                component.set("v.nameError", ''); 
            }  
        }
        */
        
        /*
         * commented by jaya since participation amount is not needed
         * 
        if(participationAmount == undefined || participationAmount == ''){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.participationAmountError", 'Input field required'); 
        } else {   
            var numWithoutComma = helper.numberWithoutCommas(participationAmount);
            if(helper.checkSpecialCharecter(numWithoutComma) || helper.checkCharecter(numWithoutComma)) { 
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.participationAmountError", 'Please enter a numeric value.'); 
            } else {
                if(numWithoutComma <= 0)
                {
                    errorFlag = 'true';
                    component.set("v.validation", true);
                    component.set("v.participationAmountError", 'Enter a value greater than 0'); 
                } else {
                    component.set("v.participationAmountError", '');
                }
            }
        }
		*/
        
        /*
         * commented by jaya since participation percentage is not needed
         * 
        if(participationPercentage == undefined || participationPercentage == '') {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.participationPercentageError", 'Input field required'); 
        } else {
            var numWithoutComma = helper.numberWithoutCommas(participationPercentage);
            if(helper.checkSpecialCharecter(numWithoutComma) || helper.checkCharecter(numWithoutComma)) { 
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.participationPercentageError", 'Please enter a numeric value.'); 
            } else {
                if(isNaN(numWithoutComma) || numWithoutComma <= 0 || numWithoutComma > 99)
                {
                    errorFlag = 'true';
                    component.set("v.validation", true);
                    component.set("v.participationPercentageError", 'Enter a value between 1% - 99%'); 
                } else {
                    component.set("v.participationPercentageError", '');  
                }
            }
        }
        */
        
        if(errorFlag == 'false')
        { 
            var img = component.find("pdfImgloading");
            
            $A.util.removeClass(img,'slds-hide');
            $A.util.addClass(img,'slds-show');  
            
            component.find("btnSaveTrans").set("v.disabled", true); 
            debugger;            
            var customComponent = component.find("MyCustomComponentEdit");
            customComponent.find("recordCreator").saveRecord(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    customComponent.set("v.recordId", saveResult.recordId);
                    component.set("v.Offer.OfferExtended.Offer.Record_Id__c", customComponent.get("v.recordId")); 
                    var OfferObject = component.get("v.Offer.OfferExtended.Offer"); 
                    var action = component.get("c.editSellerOffersItems");
                    action.setParams({ "saveSellerOffers": OfferObject });
                    action.setCallback(this, function(response) {
                        debugger;
                        var offerId = response.getReturnValue();
                        if(offerId != '') {
                            if(offerId == 'Already Used'){
                                component.find("btnSaveTrans").set("v.disabled", true); 
                                errorFlag = 'true';
                                component.set("v.validation", true);
                                component.set("v.nameError", 'This Transaction Name has already been used. Please enter a new Transaction Name.');
                                
                                $A.util.addClass(img,'slds-hide'); 
                                $A.util.removeClass(img,'slds-show');
                                
                            } else { 
                                $A.util.addClass(img,'slds-hide'); 
                                $A.util.removeClass(img,'slds-show');
                                component.set("v.showpopup", false);
                                $A.get('e.force:refreshView').fire();
                                var showToast = $A.get('e.force:showToast');
                                showToast.setParams(
                                    {
                                        'title': 'Success: ',
                                        'message': 'Transaction Updated Sucessfully',
                                        'type': 'Success'
                                    }
                                );
                                showToast.fire();
                            }
                        } else { 
                            component.find("btnSaveTrans").set("v.disabled", false); 
                            component.set("v.validation", false);
                            component.set("v.Editoffer", false);
                            
                            $A.util.addClass(img,'slds-hide'); 
                            $A.util.removeClass(img,'slds-show');
                            
                            var showToast = $A.get('e.force:showToast');
                            showToast.setParams(
                                {
                                    'title': 'Failure : ',
                                    'message': 'Unable to Update Transaction',
                                    'type': 'Success'
                                }
                            );
                            showToast.fire();
                        } 
                    });
                    $A.enqueueAction(action);
                } else if (saveResult.state === "INCOMPLETE") {
                    component.find("btnSaveTrans").set("v.disabled", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    component.find("btnSaveTrans").set("v.disabled", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    console.log('Problem saving transaction, error: ' + JSON.stringify(saveResult.error));
                } else {
                    component.find("btnSaveTrans").set("v.disabled", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                }
            });
        }
    },
    partcipation: function(component, event, helper) {
        var selected = component.find("levels").get("v.value"); 
    }, 
    LoanType: function(component, event, helper) {
        var selected = component.find("loan").get("v.value"); 
    },
    delete1: function(component, event, helper) {
        var action = component.get('c.DeleteFile');  
        action.setParams({
            "attId" : event.target.id 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.Offer", result);
                component.set("v.DeleteFile", true);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action); 
    },
    delAttach: function(component, event, helper) {
        var action = component.get('c.DeleteAttach');  
        action.setParams({
            "attId" : event.target.id 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.Offer", result);
                component.set("v.DeleteFile", true);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action); 
    },
    DeleteFilecancel: function(component, event, helper) {
        component.set('v.DeleteFile', false);
        $A.get('e.force:refreshView').fire();
    }, 
    loadFirstTab: function(component, event, helper) {
        //component.set("v.hideDiv", true);
    },
    loadSecondTab: function(component, event, helper) {
        component.set("v.hideDiv", false);
    },
    
    hoverCreditUnion: function(component, event) { 
        var action = component.get('c.CUDetails'); 
        action.setParams({
            "CUId" : event.target.id
        });       
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.CUUser", result); 
                component.set("v.popoverCreditUnion", true);
            } 
        });
        $A.enqueueAction(action);
    },
    hoverCreditUnionhide: function(component, event) { 
        component.set("v.popoverCreditUnion", false);
    },  
    hoverBuyerUser: function(component, event) { 
        debugger;
        var action = component.get('c.UserDetails1'); 
        action.setParams({
            "attachId" : event.target.id
        });       
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.user", result);
                component.set("v.popoverBidBuyerName", true);
            } 
        });
        $A.enqueueAction(action);
    },
    hoverBidBuyerUser: function(component, event) { 
        var action = component.get('c.UserDetails2'); 
        action.setParams({
            "syndId1" : event.target.id
        });       
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.user", result); 
                component.set("v.popoverBidBuyerName", true);
            } 
        });
        $A.enqueueAction(action);
    },
    hoverBidbuyerhide: function(component, event) { 
        component.set("v.popoverBidBuyerName", false); 
    },
    showNotes:function(component, event, helper) {
        component.set("v.docId", event.target.id);
        component.set("v.ShowNotes", true);
        component.set("v.rerequestflag", false);
    },
    showrerequestNotes:function(component, event, helper) {
        component.set("v.docId", event.target.id);
        component.set("v.ShowNotes", true);
        component.set("v.rerequestflag", true);
    },
    showNotesCancel: function(component, event) {
        component.set("v.ShowNotes", false);
        component.set("v.showSaveNote", '');
    }, 
    Bidhideshow:function(component, event) {
        debugger;
        var alltrs = document.getElementsByClassName('hidden-tr');
        var alltrs1 = document.getElementsByClassName('fas fa-plus-circle');
        var alltrs2 = document.getElementsByClassName('fas fa-minus-circle');        
        for (var i = 0; i < alltrs.length; i++) {
            alltrs[i].style.display = 'none';
            alltrs1[i].style.display = 'block';
            alltrs2[i].style.display = 'none';
        }
        var elementId ='tr_'+event.target.id;
        document.getElementById(elementId).style.display = 'table-row';
        var MelementId =event.target.id;
        var Replaceval = MelementId.replace("P_", "M_");
        var n = MelementId.includes("P_");
        if(n){
            document.getElementById(MelementId).style.display = 'none';
            document.getElementById(Replaceval).style.display = 'block';
        }else{
            document.getElementById(MelementId).style.display = 'block';
            document.getElementById(Replaceval).style.display = 'none';  
        }
    },  
    Amendhideshow:function(component, event) {
        debugger;
        var alltrs = document.getElementsByClassName('hidden-tr');
        var alltrs1 = document.getElementsByClassName('fas fa-plus-circle');
        var alltrs2 = document.getElementsByClassName('fas fa-minus-circle');        
        for (var i = 0; i < alltrs.length; i++) {
            alltrs[i].style.display = 'none';
            alltrs1[i].style.display = 'block';
            alltrs2[i].style.display = 'none';
        }
        var elementId ='tr_'+event.target.id;
        document.getElementById(elementId).style.display = 'table-row';
        var MelementId =event.target.id;
        var Replaceval = MelementId.replace("PL_", "MI_");
        var n = MelementId.includes("PL_");
        if(n){
            document.getElementById(MelementId).style.display = 'none';
            document.getElementById(Replaceval).style.display = 'block';
        }else{
            document.getElementById(MelementId).style.display = 'block';
            document.getElementById(Replaceval).style.display = 'none';  
        }
    },  
    loadNotes:function(component, event) {
        debugger;
        component.set("v.docId", event.target.id); 
        component.set("v.showRequestNotes", []);
        var alltrs = document.getElementsByClassName('hidden-tr');
        var alltrs1 = document.getElementsByClassName('fas fa-plus-circle');
        var alltrs2 = document.getElementsByClassName('fas fa-minus-circle');    
        for (var i = 0; i < alltrs.length; i++) {
            alltrs[i].style.display = 'none';
            alltrs1[i].style.display = 'block';
            alltrs2[i].style.display = 'none';
        }
        var action = component.get('c.showLoadNotes');
        action.setParams({
            "offerId" : component.get("v.TransId"),
            "docId" : component.get("v.docId"),
        });
        action.setCallback(this, function(actionResult) { 
            debugger;
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.showRequestNotes", result);
                var showselleernotechk = component.get("v.showsellernotes");
                if(showselleernotechk){
                    component.set("v.showsellernotes", false);
                }else
                {
                    component.set("v.showsellernotes", true);
                }
                
                var elementId ='tr_'+event.target.id;
                document.getElementById(elementId).style.display = 'table-row';
                var MelementId =event.target.id;
                var Replaceval = MelementId.replace("RP_", "RM_");
                var n = MelementId.includes("RP_");
                if(n){
                    document.getElementById(MelementId).style.display = 'none';
                    document.getElementById(Replaceval).style.display = 'block';
                }else{
                    document.getElementById(MelementId).style.display = 'block';
                    document.getElementById(Replaceval).style.display = 'none';  
                }
            }
        });
        $A.enqueueAction(action);
    },
    showNotesSave:function(component, event) {
        // alert('inside Notes');
        debugger;
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || savNote == null || savNote == undefined || savNote.trim().length*1 ==0)
        {            
            component.set("v.showSaveNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        }
        else{
            
            component.set("v.showSaveNote", ''); 
            var action = component.get('c.showSaveNotes');
            action.setParams({
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
                "IsManager" : component.get("v.IsSellerManager"),
                "offerId" : component.get("v.TransId"),
                "docId" : component.get("v.docId"),
                "body" : component.find("body").get("v.value")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.ShowNotes", false);
                if(state === "SUCCESS"){
                    component.set("v.showRequestNotes", result);
                    //var elementId ='tr_'+event.target.id;
                    //document.getElementById(elementId).style.display = 'table-row';
                    $A.get('e.force:refreshView').fire();
                }
            });
            
            $A.enqueueAction(action);
        }
    },
    
    requestPopNotes: function(component, event) {
        component.set("v.RequestNotes", true);
        component.set("v.WtQyes", true);
        component.set("v.WtQno", false);
    },
    RequestNotesCancel: function(component, event) {
        component.set("v.RequestNotes", false);
        component.set("v.requestSaveNote", '');
    },
    handleno: function(component, event) {
        //var yescall = component.find("rbtnyes").get("v.value"); 
        var nocall = component.find("rbtnno").get("v.value"); 
        if(nocall)
        {
            component.set("v.WtQyes", false);
            component.set("v.WtQno", true);
        }
    },
    handleyes: function(component, event) {
        
        var yescall = component.find("rbtnyes").get("v.value"); 
        if(yescall)
        {
            component.set("v.WtQyes", true);
            component.set("v.WtQno", false);
        }
    }, 
    RequestNotesSave: function(component, event, helper) {
        debugger;
        var savNote = component.find("responsebody").get("v.value");
        if(savNote == '' || savNote == null || savNote == undefined || savNote.trim().length*1 ==0)
        {            
            component.set("v.requestSaveNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        }
        else{
            debugger; 
            component.set("v.requestSaveNote", ''); 
            var action = component.get('c.requestSaveNotes');
            var RFI = component.find("rbtnyes").get('v.value');
            action.setParams({
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
                "IsManager" : component.get("v.IsSellerManager"),
                "offerId" : component.get("v.TransId"),
                "docId" : component.get("v.docId"),
                "body" : component.find("responsebody").get("v.value"),
                "CRTQ" : RFI
            });
            
            action.setCallback(this, function(actionResult) { 
                debugger;
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.RequestNotes", false);
                if(state === "SUCCESS"){
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            
            $A.enqueueAction(action);
        }
    }, 
    popNotes: function(component, event) {
        component.set("v.Notes", true);
    },
    NotesCancel: function(component, event) {
        component.set("v.Notes", false);
        component.set("v.saveNote", '');
    },
    NotesSave: function(component, event, helper) {
        debugger;
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || savNote == null || savNote == undefined || savNote.trim().length*1 ==0)
        {            
            component.set("v.saveNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        }
        else{
            component.set("v.saveNote", ''); 
            var action = component.get('c.saveNotes');
            action.setParams({
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
                "IsManager" : component.get("v.IsSellerManager"),
                "offerId" : component.get("v.TransId"),
                "Title" : localStorage.getItem("LoggeduserName"),
                "body" : component.find("body").get("v.value")  
            });
            
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                component.set("v.Notes", false);
                $A.log(actionResult);
                if(state === "SUCCESS"){
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            
            $A.enqueueAction(action);
        }
    }, 
    OriginalPrincipal: function(component, event, helper) {
        var inp = component.find("oPrincipal").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.OriginalPrincipal__c","");
    },
    Wac: function(component, event, helper) {
        var inp = component.find("wac").get("v.value"); 
        if(inp == 0.000)
            component.set("v.Offer.OfferExtended.Offer.WAC__c","");
    },
    CurrentPrincipal: function(component, event, helper) {
        var inp = component.find("cPrincipal").get("v.value"); ;
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.OutstandingPrincipal__c","");
    },
    WAFICO: function(component, event, helper) {
        var inp = component.find("fico").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.FICO_WA__c","");
    },
    NumOfLoan: function(component, event, helper) {
        var inp = component.find("nof").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.NumberOfLoans__c","");
    },
    MinFICO: function(component, event, helper) {
        var inp = component.find("minfico").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.MinFICO__c","");
    },
    AvgLoanAmount: function(component, event, helper) {
        var inp = component.find("avgloan").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.AverageLoanAmount__c","");
    },
    WAOriginal: function(component, event, helper) {
        var inp = component.find("oTerm").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.Term_WA__c","");
    },
    WALTV: function(component, event, helper) {
        var inp = component.find("ltv").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.LoanToValue__c","");
    },
    WARemaining: function(component, event, helper) {
        var inp = component.find("Rmonth").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.RemainingTerm__c","");
    },
    WADTI: function(component, event, helper) {
        var inp = component.find("dti").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.WA_DTI__c","");
    },
    New: function(component, event, helper) {
        var inp = component.find("newper").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.NewPercentage__c","");
    },
    Indirect: function(component, event, helper) {
        var inp = component.find("indirect").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.IndirectPercentage__c","");
    },
    AvgVehAge: function(component, event, helper) {
        var inp = component.find("AvgVhclAge").get("v.value"); 
        if(inp == 0.0)
            component.set("v.Offer.OfferExtended.Offer.AverageVehicleAge__c","");
    },
    ChargeOff: function(component, event, helper) {
        var inp = component.find("ChrgOff").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.Charge_Off__c","");
    },
    WALoan: function(component, event, helper) {
        var inp = component.find("WaLifeLoan").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.Average_Life_of_Loan__c","");
    },
    PortPrice: function(component, event, helper) {
        var inp = component.find("PPrice").get("v.value"); 
        if(inp == 0.00)
            component.set("v.Offer.OfferExtended.Offer.PortfolioPrice__c","");
    },
    ServFee: function(component, event, helper) {
        var inp = component.find("SFee").get("v.value"); 
        if(inp == 0)
            component.set("v.Offer.OfferExtended.Offer.ServicingFee__c","");
    },
    bidNameClick: function(component, event, helper) {
        debugger;
        var address = "/bid-detail?offerId=&syndId="+ event.target.id;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": address,
            "isredirect" :false
        });
        urlEvent.fire();
    },
    refreshModel: function (component, event, helpler) {
        debugger;
        var action = component.get('c.getContentslist');  
        action.setParams({
            "parentId" : component.get("v.TransId")
            //"description" : ''
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                debugger;
                component.set("v.Transactions.ContentVer", result.ContentVer);
                
                component.set("v.opendocvault", false);
                component.set("v.popdocuvault", false);
                var img = component.find("imgloading");
                $A.util.addClass(img,'slds-hide'); 
                $A.util.removeClass(img,'slds-show');
                //component.set("v.FileUploadMsg", 'File(s) have been uploaded successfully');
                //component.set("v.FileUploadMsgSorE", 'Success');
                //component.set("v.FileUpload", true);
                //component.set("v.Offer.attachment", result); 
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
    refreshModel1: function (component, event, helpler) {
        debugger;
        var action = component.get('c.getContentslisttask');  
        action.setParams({
            "parentId" : component.get("v.TransId")
            //"description" : ''
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                debugger;
                component.set("v.taskattachment", true);
                component.set("v.Transactionstask.ContentVer", result.ContentVer);                
                var img = component.find("imgloading");
                $A.util.addClass(img,'slds-hide'); 
                $A.util.removeClass(img,'slds-show');                
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
    refreshModel2: function (component, event, helpler) {
        debugger;
        var action = component.get('c.getContentslistcptask');  
        action.setParams({
            "parentId" : component.get("v.TransId"),
            "taskId" : component.get("v.taskId")
            //"description" : ''
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                debugger;
                component.set("v.taskcpattachment", true);
                component.set("v.Transactions.ContentVer", result.ContentVer);                
                var img = component.find("imgloading");
                $A.util.addClass(img,'slds-hide'); 
                $A.util.removeClass(img,'slds-show');                
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
    refreshModel3: function (component, event, helpler) {
        debugger;
        var action = component.get('c.getContentslistownertask');  
        action.setParams({
            "parentId" : component.get("v.TransId"),
            "taskId" : component.get("v.taskId")            
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                debugger;
                component.set("v.taskownerattachment", true);
                component.set("v.TransactionsTask.ContentVer", result.ContentVer);                
                var img = component.find("imgloading");
                $A.util.addClass(img,'slds-hide'); 
                $A.util.removeClass(img,'slds-show');                
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
    refreshModelDue: function (component, event, helpler) {
        var img = component.find("imgloadingDue");
        $A.util.addClass(img,'slds-hide'); 
        $A.util.removeClass(img,'slds-show');
        component.set("v.FileUploadMsg", 'File(s) have been uploaded successfully');
        component.set("v.FileUploadMsgSorE", 'Success');
        component.set("v.FileUpload", true);
        var action = component.get('c.getContentVersions');  
        action.setParams({
            "parentId" : component.get("v.parentId"),
            "description" : 'due'
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){      
                component.set("v.Offer.duecontent", result);
                //component.set("v.Offer.dueAttachment", result); 
            }
            else if(state === "ERROR") {
                $A.log('error');
            }
        });
        $A.enqueueAction(action); 
    },
    ChooseFile : function(component, event) {        
        component.set("v.FileUploadMsg", 'Please choose the file(s)');
        component.set("v.FileUploadMsgSorE", 'Error');
        component.set("v.FileUpload", true); 
    },
    ChooseFailure : function(component, event) {        
        component.set("v.FileUploadMsg", 'An error has occurred. Please contact us at\nLoanParticipationBeta@cunamutual.com.');
        component.set("v.FileUploadMsgSorE", 'Error');        
        component.set("v.FileUpload", true); 
    },
    ChooseFile1 : function(component, event) {        
        component.set("v.FileUploadMsg", 'Please choose the file(s)');
        component.set("v.FileUploadMsgSorE", 'Error');
        component.set("v.FileUpload", true); 
    },
    ChooseFile2 : function(component, event) {        
        component.set("v.FileUploadMsg", 'Please choose the file(s)');
        component.set("v.FileUploadMsgSorE", 'Error');
        component.set("v.FileUpload", true); 
    },
    ChooseFile3 : function(component, event) {        
        component.set("v.FileUploadMsg", 'Please choose the file(s)');
        component.set("v.FileUploadMsgSorE", 'Error');
        component.set("v.FileUpload", true); 
    },
    fileExtension : function(component, event) {        
        component.set("v.FileUploadMsg", 'This file format not allowed to upload');
        component.set("v.FileUploadMsgSorE", 'Error');        
        component.set("v.FileUpload", true); 
    },
    LoaderDisplay : function(component, event) {
        var img = component.find("imgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    LoaderDisplay1 : function(component, event) {
        var img = component.find("imgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    LoaderDisplay2 : function(component, event) {
        var img = component.find("imgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    LoaderDisplay3 : function(component, event) {
        var img = component.find("imgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    LoaderDisplayDue : function(component, event) {
        var img = component.find("imgloadingDue");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    FileUploadcancel : function(component, event) {
        var img = component.find("imgloading");
        $A.util.addClass(img,'slds-hide'); 
        $A.util.removeClass(img,'slds-show');
        var img1 = component.find("imgloadingDue");
        $A.util.addClass(img1,'slds-hide'); 
        $A.util.removeClass(img1,'slds-show');
        component.set("v.FileUpload", false); 
        $A.get('e.force:refreshView').fire();
    },    
    BidDue : function(component, event) {
        component.set("v.syndId", event.target.id); 
        component.set("v.BidDue", true); 
    },
    BidDueCheck : function(component, event, helper) {
        if(component.find("chkBidDue").get('v.value') == true) {
            component.find("BidDuebtn1").set("v.disabled", false);
        }
        else{
            component.find("BidDuebtn1").set("v.disabled", true);
        }
    },
    btnBidDue: function(component, event, helper) {
        debugger;
        var action = component.get('c.BidDue1'); 
        action.setParams({
            "syndId" : component.get("v.syndId"),
            "body6" : component.find("body6").get("v.value")
        });
        action.setCallback(self, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.BidDue", false);
            component.set("v.SecondBidDue", true);
            if(state === "SUCCESS"){
                component.set("v.Offer", result);  
                component.set("v.parentId", result.offerId);  
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    BidDuecancel : function(component, event) {
        component.set("v.BidDue", false);
    },
    SecondBidDuecancel : function(component, event) {
        component.set("v.SecondBidDue", false);
    },
    ShareSave: function(component, event) {
        var errorFlag = 'false';
        var vsharerecip =component.find("sharerecip").get("v.value");
        var vsharesub =component.find("sharesub").get("v.value");
        var vsharebody =component.find("sharebody").get("v.value");
        // var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(vsharerecip == '' || vsharerecip == 'undefined' || vsharerecip == null)
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.sharerecipError", 'Input field required'); 
        }
        else
        {component.set("v.sharerecipError", ''); }
        /*else if(!$A.util.isEmpty(vsharerecip))
        {   
            if(!vsharerecip.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.sharerecipError", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.sharerecipError", ''); 
            }
        } */
        if(vsharesub == '' || vsharesub == 'undefined' || vsharesub == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.sharesubError", 'Input field required'); 
        } else { 
            component.set("v.sharesubError", ''); 
        } 
        if(vsharebody == '' || vsharebody == 'undefined' || vsharebody == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.sharebodyError", 'Input field required'); 
        } else { 
            component.set("v.cpError", ''); 
        }  
        if(errorFlag == 'false') {
            var action = component.get('c.SaveShareTransaction');  
            action.setParams({
                "offerId" : component.get("v.Offer.OfferExtended.Offer.Id"),
                "Recepit" : vsharerecip,
                "sub" : vsharesub,
                "body" : vsharebody
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                }
            });
            $A.enqueueAction(action);
            component.set("v.SharePopup", false);
            $A.get('e.force:refreshView').fire();
        }
        
    },
    counterPopupCancel1 : function(component, event) {
        $A.get('e.force:refreshView').fire();
        //component.set("v.counterPopup", false);
        //component.set("v.counterpartyselectionPopup", false);
        component.set("v.counterpartyselectionPopupnew", false);
    },
    counterPopupCancel : function(component, event) {
        var action = component.get('c.getCounterParties');  
        action.setParams({
            "offerId" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                 component.set("v.Offer.OfferExtended.CounterParties", result);
                //component.set("v.counterPopup", false);
                component.set("v.counterpartyselectionPopup", false);
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
        component.set("v.counterPopup", false);
    },
      /*showCounterParties: function(component, event) {
        debugger;
        var action = component.get('c.getApprovedCreditUnions');  
        action.setParams({
            "creditUnionId" : component.get("v.Transactions.Trans.Tenant__c"),
            "offerId" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                component.set("v.ApprovedCounterParties", result);
                //component.set("v.counterPopup", true);
                component.set("v.counterpartyselectionPopup", true);
            }
        });
        $A.enqueueAction(action);
    },*/
    showCounterParties: function(component, event) {
        debugger;
        var action = component.get('c.counterPartylist');  
        action.setParams({
            "creditUnionId" : component.get("v.Transactions.Trans.Tenant__c"),
            "offerId" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                component.set("v.Tusers",actionResult.getReturnValue());               
                //component.set("v.counterPopup", true);
                component.set("v.counterpartyselectionPopup", true);
            }
        });
        $A.enqueueAction(action);
    }, 
    showCounterPartiesnew: function(component, event) {
        debugger;
        var action = component.get('c.OrgcounterPartylist');  
        action.setParams({
            "creditUnionId" : component.get("v.Transactions.Trans.Tenant__c"),
            "offerId" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                component.set("v.Orgusers",result.Org);  
                component.set("v.UnRegisterList",result.userslist); 
                if(result.userslist.length>0){
                    component.set("v.UnRegisterListlength",true);
                }else{
                    component.set("v.UnRegisterListlength",false);
                }
                //component.set("v.counterPopup", true);
                component.set("v.counterpartyselectionPopupnew", true);
                component.set("v.benefits1", true);
                component.set("v.userinfo1", false);
                component.set("v.clrbenefits1",'active');
                component.set("v.clrud1",'');
                
            }
        });
        $A.enqueueAction(action);
    },
    sharepopupopen : function(component, event) { 
        debugger;
        component.set("v.SharePopup", true);       
    },
    sharecancel : function(component, event) { 
        debugger;
        component.set("v.SharePopup", false);       
    },
    addCounterPartiespop : function(component, event) { 
        debugger;
        component.set("v.newcounterPopup", true);       
    },
    popcounterPopupCancel : function(component, event) { 
        debugger;
        component.set("v.newcounterPopup", false);       
    },
    addCounterParties: function(component, event) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var Bankname = component.find("Banknameval").get("v.value");
        var emval = component.find("emailval").get("v.value");
        var mval = component.find("mobileval").get("v.value");
        var vcode =  component.find("code").get("v.value");
        var vcity =component.find("city").get("v.value");
        var vpcontact =component.find("pcontact").get("v.value");
        var vwebsite =component.find("website").get("v.value");
        var vstate =component.find("state").get("v.value");
        
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        if(mval == '' || mval == 'undefined')
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.mobileError", 'Input field required'); 
        }
        else if(!$A.util.isEmpty(mval))
        {   
            if(!mval.match(regPhoneNo))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.mobileError", 'Please enter valid phone number.'); 
            } 
            else{
                component.set("v.mobileError", ''); 
            }
        }
        if(emval == '' || emval == 'undefined')
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.emailError", 'Input field required'); 
        }
        else if(!$A.util.isEmpty(emval))
        {   
            if(!emval.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.emailError", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.emailError", ''); 
            }
        }
        
        if(Bankname == '' || Bankname == 'undefined' || Bankname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.cpError", 'Input field required'); 
        } else { 
            component.set("v.cpError", ''); 
        } 
        if(vcode == '' || vcode == 'undefined' || vcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.codeerror", 'Input field required'); 
        } else { 
            component.set("v.codeerror", ''); 
        } 
        if(vcity == '' || vcity == 'undefined'|| vcity == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.cityerror", 'Input field required'); 
        } else { 
            component.set("v.cityerror", ''); 
        } 
        if(vpcontact == '' || vpcontact == 'undefined' || vpcontact == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Pcontacterror", 'Input field required'); 
        } else { 
            component.set("v.Pcontacterror", ''); 
        } 
        if(vwebsite == '' || vwebsite == 'undefined' || vwebsite == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.weberror", 'Input field required'); 
        } else { 
            component.set("v.weberror", ''); 
        } 
        if(vstate == '' || vstate == 'undefined' || vstate == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Stateerror", 'Input field required'); 
        } else { 
            component.set("v.Stateerror", ''); 
        } 
        if(errorFlag == 'false') {
            console.log('@@'+component.get("v.counterpartyname"));
            var action = component.get('c.addCounterPartiesname');  
            action.setParams({
                "saveCreditunion" : component.get("v.Creditunion"),
                "PName" : component.get("v.Partname"),
                "PID" : component.get("v.PartyID")
            });
            debugger;
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.newcounterPopup", false);
                    component.set("v.onboard", true);
                }
            });
            debugger;
            $A.enqueueAction(action);
        }
    },
    onboard: function(component, event) {
        component.set("v.onboard", false);
    },
    selectCounterParties: function(component, event) {
        debugger;
        var checkbox = event.getSource();
        var checked = checkbox.get("v.value");
        if(checked) {
            component.find("btnSelect").set("v.disabled", false);
        }
        else{
            component.find("btnSelect").set("v.disabled", true);
        }
        var creditUnionId = checkbox.get("v.text");
        debugger;
        var action1 = component.get('c.Chkissuingbank');  
        action1.setParams({
            "creditUnionId" : creditUnionId,
            "offerId" : component.get("v.Transactions.Trans.Id")
        });
        action1.setCallback(this, function(actionResult1) { 
            var state1 = actionResult1.getState(); 
            var result1 = actionResult1.getReturnValue();
            if(state1 === "SUCCESS"){
                
                if(result1 =='MATCH')
                {
                    component.set("v.issuingpopup",true);
                }else{
                    var action = component.get('c.addRemoveCounterParty');  
                    action.setParams({
                        "creditUnionId" : creditUnionId,
                        "offerId" : component.get("v.Transactions.Trans.Id"),
                        "forAdd" : checked
                    });
                    action.setCallback(this, function(actionResult) { 
                        var state = actionResult.getState(); 
                        var result = actionResult.getReturnValue();
                        if(state === "SUCCESS"){
                        }
                    });
                    $A.enqueueAction(action);
                }
            }
        });
        $A.enqueueAction(action1);
        
        //alert(creditUnionId);
        
    },
    issuingcancel : function(component, event) { 
        component.set("v.issuingpopup",false);     
    },
    approveValidation : function(component, event) { 
        component.set("v.ApproveValidation", true);       
    },
    approveValidationCancel : function(component, event) { 
        component.set("v.ApproveValidation", false);       
    },
    Validationresubmit : function(component, event) { 
        component.set("v.resubmitValidationpop", true);       
    },
    Validationsubmit : function(component, event) { 
        component.set("v.submitValidationpop", true);       
    },
    resubmitValidationcancel : function(component, event) { 
        component.set("v.resubmitValidationpop", false);       
    },
    submitValidationcancel : function(component, event) { 
        component.set("v.submitValidationpop", false);       
    },
    approveValidationre : function(component, event) { 
        component.set("v.ApproveValidationre", true);       
    },
    approveValidationCancelre : function(component, event) { 
        component.set("v.ApproveValidationre", false);       
    },
    resubmitValidationCancel : function(component, event) { 
        component.set("v.ResubmitValidation", false);       
    },
    submitForApproval: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyValidation").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteValidation", 'Input field required'); 
            component.find("bodyValidation").set("v.value", '')
        } else {
            component.set("v.saveNoteValidation", ''); 
            var action = component.get('c.SubmitForApprove'); 
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" :component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.ApproveValidation", false); 
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction is validated Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    }, 
    sellerApproval1m : function(component, event) { 
        component.set("v.SellerApproval1m", true);       
    },
    sellerApproval1mCancel : function(component, event) { 
        component.set("v.SellerApproval1m", false);       
    },
    approve1m: function(component, event, helper) {
        debugger;        
        var savNote = component.find("bodyApproval1").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteApproval1m", 'Input field required'); 
            component.find("bodyApproval1m").set("v.value", '')
        } else {
            component.set("v.saveNoteApproval1m", ''); 
            var action = component.get('c.SellerApproval1m');            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" : component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.SellerApproval1m", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction is approved Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    resubmitForApproval: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyValidation").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteValidation", 'Input field required'); 
            component.find("bodyValidation").set("v.value", '')
        } else {
            component.set("v.saveNoteValidation", ''); 
            var action = component.get('c.reSubmitForApprove1'); 
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" :component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.ApproveValidation", false); 
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Re-Submitted Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    resubmitValidationsave: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyValidation").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteValidation", 'Input field required'); 
            component.find("bodyValidation").set("v.value", '')
        } else {
            component.set("v.saveNoteValidation", ''); 
            var action = component.get('c.reSubmitForvalidation'); 
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" :component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.ApproveValidation", false); 
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Re-Submitted Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    submitValidationsave: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyValidation").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteValidation", 'Input field required'); 
            component.find("bodyValidation").set("v.value", '')
        } else {
            component.set("v.saveNoteValidation", ''); 
            var action = component.get('c.SubmitForvalidation'); 
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" :component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.ApproveValidation", false); 
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Submitted Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    rejectValidation : function(component, event) { 
        component.set("v.RejectValidation", true);       
    },
    rejectValidationCancel : function(component, event) { 
        component.set("v.RejectValidation", false);       
    },
    rejectForApproval: function(component, event, helper) {
        debugger;        
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        } else {
            component.set("v.saveNote", ''); 
            var action = component.get('c.RejectValidation');
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" :component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.RejectValidation", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Rejected: ',
                            'message': 'Transaction is rejected Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            
            $A.enqueueAction(action);
        }        
    }, 
    Resubmitforvalidate : function(component, event) { 
        debugger;
        component.set("v.ResubmitValidation", true);
        component.set("v.setTargetids",event.target.id)
    },
    Resubmitforvalidatesave : function(component, event) { 
        debugger;
        
        var action = component.get('c.Resubmitforvalidatecall'); 
        action.setParams({
            "PCPids" : component.get("v.setTargetids")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){ 
                var savNote = component.find("bodyValidation").get("v.value");
                if(savNote == '' || 
                   savNote == null || 
                   savNote == undefined || 
                   savNote.trim().length*1 == 0) {            
                    component.set("v.saveNoteValidation", 'Input field required'); 
                    component.find("bodyValidation").set("v.value", '')
                } else {
                    component.set("v.saveNoteValidation", ''); 
                    var action = component.get('c.reSubmitForApprove'); 
                    
                    action.setParams({
                        "offerId" : component.get("v.offerId"),
                        "isSellerOrBuyer" : true,
                        "body" : savNote,
                        "counterids" :component.get("v.setTargetids")
                    });
                    action.setCallback(this, function(actionResult) { 
                        var state = actionResult.getState(); 
                        if(state === "SUCCESS"){
                            var result = actionResult.getReturnValue();
                            component.set("v.ResubmitValidation", false); 
                            component.set("v.Offer", result);
                            component.set("v.offerId", result.offerId);  
                            component.set("v.parentId", result.offerId);  
                            
                            $A.get('e.force:refreshView').fire();
                            var showToast = $A.get('e.force:showToast');
                            showToast.setParams(
                                {
                                    'title': 'Success: ',
                                    'message': 'Transaction Updated Sucessfully',
                                    'type': 'Success'
                                }
                            );
                            showToast.fire();
                        }
                        else if(state === "ERROR") {
                            var emptyTask = component.get("v.Offer");
                            emptyTask.Subject = "";
                            emptyTask.Description = "";  
                            component.set("v.Offer", emptyTask);
                        }
                    });
                    $A.enqueueAction(action);
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    //jaya
    approveCounterValidation : function(component, event) { 
        component.set("v.ApproveCounterValidation", true);       
    },
    approveCounterValidationCancel : function(component, event) { 
        component.set("v.ApproveCounterValidation", false);       
    },
    submitForCounterApproval: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyCounterValidation").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteCounterValidation", 'Input field required'); 
            component.find("bodyCounterValidation").set("v.value", '')
        } else {
            component.set("v.saveNoteCounterValidation", ''); 
            var action = component.get('c.SubmitForCounterApprove'); 
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : false,
                "body" : savNote,
                "LogedCUnionId" : component.get("v.LogeduserCUnionId")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.ApproveCounterValidation", false); 
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction is validated successfully.',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    rejectCounterValidation : function(component, event) { 
        component.set("v.RejectCounterValidation", true);       
    },
    rejectCounterValidationCancel : function(component, event) { 
        component.set("v.RejectCounterValidation", false);       
    },
    rejectForCounterApproval: function(component, event, helper) {
        var savNote = component.find("body").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveCounterNote", 'Input field required'); 
            component.find("body").set("v.value", '')
        } else {
            component.set("v.saveCounterNote", ''); 
            var action = component.get('c.RejectCounterValidation');
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : false,
                "body" : savNote,
                "LogedCUnionId" : component.get("v.LogeduserCUnionId")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.RejectCounterValidation", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Reject: ',
                            'message': 'Transaction is rejected successfully.',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            
            $A.enqueueAction(action);
        }        
    }, 
    //jaya
    
    sellerApproval1 : function(component, event) { 
        component.set("v.SellerApproval1", true);       
    },
    sellerApproval1Cancel : function(component, event) { 
        component.set("v.SellerApproval1", false);       
    },
    approve1: function(component, event, helper) {
        debugger;        
        var savNote = component.find("bodyApproval1").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteApproval1", 'Input field required'); 
            component.find("bodyApproval1").set("v.value", '')
        } else {
            component.set("v.saveNoteApproval1", ''); 
            var action = component.get('c.SellerApproval1');            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" :component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.SellerApproval1", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction is approved Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });            
            $A.enqueueAction(action);
        }        
    },
    
    sellerReject1 : function(component, event) { 
        component.set("v.SellerReject1", true);       
    },
    sellerReject1Cancel : function(component, event) { 
        component.set("v.SellerReject1", false);       
    },
    reject1: function(component, event, helper) {
        debugger;        
        var savNote = component.find("bodyReject1").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteReject1", 'Input field required'); 
            component.find("bodyReject1").set("v.value", '')
        } else {
            component.set("v.saveNoteReject1", ''); 
            var action = component.get('c.SellerReject1');
            
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote,
                "PID" :component.get("v.PartyID")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.SellerReject1", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Reject: ',
                            'message': 'Transaction is rejected Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });            
            $A.enqueueAction(action);
        }        
    },  
    
    sellerApproval2 : function(component, event) { 
        component.set("v.SellerApproval2", true);       
    },
    sellerApproval2Cancel : function(component, event) { 
        component.set("v.SellerApproval2", false);       
    },
    approve2: function(component, event, helper) {
        debugger;        
        var savNote = component.find("bodyApproval2").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteApproval2", 'Input field required'); 
            component.find("bodyApproval2").set("v.value", '')
        } else {
            component.set("v.saveNoteApproval2", ''); 
            var action = component.get('c.SellerApproval2');
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote  
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.SellerApproval2", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });            
            $A.enqueueAction(action);
        }        
    },
    
    sellerReject2 : function(component, event) { 
        component.set("v.SellerReject2", true);       
    },
    sellerReject2Cancel : function(component, event) { 
        component.set("v.SellerReject2", false);       
    },
    reject2: function(component, event, helper) {
        debugger;        
        var savNote = component.find("bodyReject2").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteReject2", 'Input field required'); 
            component.find("bodyReject2").set("v.value", '')
        } else {
            component.set("v.saveNoteReject2", ''); 
            var action = component.get('c.SellerReject2');
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                component.set("v.SellerReject2", false);       
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.Offer", result);
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });            
            $A.enqueueAction(action);
        }        
    },
    
    sellerApproval3 : function(component, event) { 
        component.set("v.SellerApproval3", true);       
    },
    sellerApproval3Cancel : function(component, event) { 
        component.set("v.SellerApproval3", false);       
    },
    approve3: function(component, event, helper) { 
        debugger;        
        var savNote = component.find("bodyApproval3").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteApproval3", 'Input field required'); 
            component.find("bodyApproval3").set("v.value", '')
        } else {
            component.set("v.saveNoteApproval3", ''); 
            var action = component.get('c.Approve'); 
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.SellerApproval3", false);
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Updated Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    }, 
    selectAll: function(component, event, helper) {
        var getAllId = component.find("boxPack");
        for (var i = 0; i < getAllId.length; i++) {
            component.find("boxPack")[i].set("v.value", true);
            
        }
    },
    deselectAll: function(component, event, helper) {
        var getAllId = component.find("boxPack");
        for (var i = 0; i < getAllId.length; i++) {
            component.find("boxPack")[i].set("v.value", false);
            
        }
    },
    sellerReject3 : function(component, event) { 
        component.set("v.SellerReject3", true);       
    },
    sellerReject3Cancel : function(component, event) { 
        component.set("v.SellerReject3", false);       
    },
    reject3: function(component, event, helper) {
        debugger;
        var savNote = component.find("bodyReject3").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saveNoteReject3", 'Input field required'); 
            component.find("bodyReject3").set("v.value", '')
        } else {
            component.set("v.saveNoteReject3", ''); 
            var action = component.get('c.Reject'); 
            action.setParams({
                "offerId" : component.get("v.offerId"),
                "isSellerOrBuyer" : true,
                "body" : savNote
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.SellerReject3", false);
                    component.set("v.Offer", result);
                    component.set("v.offerId", result.offerId);  
                    component.set("v.parentId", result.offerId);  
                    
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Transaction Updated Sucessfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
                else if(state === "ERROR") {
                    var emptyTask = component.get("v.Offer");
                    emptyTask.Subject = "";
                    emptyTask.Description = "";  
                    component.set("v.Offer", emptyTask);
                }
            });
            $A.enqueueAction(action);
        }
    },
    textChange: function(component, event,helper) {  
        debugger;
        if(event.getSource){
            var target = event.getSource();  
            var txtVal = target.get("v.value") ;
            component.set("v.selectedItem",txtVal);   
        }else{
            var target = event.target;  
            var dataEle = target.getAttribute("data-selected-Index"); 
            var Attoptions = [];
            //component.set("v.selectedItem", "Component at index "+dataEle+" has value "+target.value); 
            if(component.get("v.editattributesstring").length > 0){                
                var TemObj = component.get("v.Transactions.TransAtt"); 
                var tempatt = component.get("v.editattributesstring");
                for(var i = 0; i < tempatt.length; i++) {
                    Attoptions.push(tempatt[i]);
                }
                for(var k = 0; k < TemObj.length; k++) {
                    if(dataEle == k){
                        var I = TemObj[k].Id;
                        var T = TemObj[k].Attribute_Name__c;
                        var D = target.value;
                        Attoptions.push(I+':'+T+':'+D);
                    }
                    component.set("v.editattributesstring",Attoptions) ;
                }                
            }else{                
                var TemObj = component.get("v.Transactions.TransAtt");
                Attoptions.push(component.get("v.editattributesstring"));
                for(var k = 0; k < TemObj.length; k++) {
                    if(dataEle == k){
                        var I = TemObj[k].Id;
                        var T = TemObj[k].Attribute_Name__c;
                        var D = target.value;
                        Attoptions.push(I+':'+T+':'+D);
                    }
                    component.set("v.editattributesstring",Attoptions) ;
                }
            }           
            
            
        }
        
    },
     toggleSection : function(component, event, helper) {
        debugger;
        // dynamically get aura:id name from 'data-auraId' attribute
        var sectionAuraId = event.target.getAttribute("data-auraId");
        // get section Div element using aura:id
        var sectionDiv = component.find(sectionAuraId).getElement();
        /* The search() method searches for 'slds-is-open' class, and returns the position of the match.
         * This method returns -1 if no match is found.
        */
        var sectionState = sectionDiv.getAttribute('class').search('slds-is-open'); 
        
        // -1 if 'slds-is-open' class is missing...then set 'slds-is-open' class else set slds-is-close class to element
        if(sectionState == -1){
            sectionDiv.setAttribute('class' , 'slds-section slds-is-open');
        }else{
            sectionDiv.setAttribute('class' , 'slds-section slds-is-close');
        }
    },
    CPhideshow:function(component, event) {
        debugger;
        var alltrs = document.getElementsByClassName('hidden-tr');
        var alltrs1 = document.getElementsByClassName('fas fa-plus-circle');
        var alltrs2 = document.getElementsByClassName('fas fa-minus-circle');        
        for (var i = 0; i < alltrs.length; i++) {
            //alltrs[i].style.display = 'none';
            //alltrs1[i].style.display = 'block';
            //alltrs2[i].style.display = 'none';
            //alert(event.target.id.replace("P_", ""));
        var action = component.get('c.viewTenantUsersList');  
        action.setParams({
            "tenantId" : event.target.id.replace("P_", "")
            
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                    component.set("v.Transactions",result);
                  }
        });
        $A.enqueueAction(action);    
        }
        
        
        var elementId ='tr_'+event.target.id;
        document.getElementById(elementId).style.display = 'table-row';
        var MelementId =event.target.id;
        var Replaceval = MelementId.replace("P_", "M_");
        var n = MelementId.includes("P_");
        if(n){
            document.getElementById(MelementId).style.display = 'none';
            document.getElementById(Replaceval).style.display = 'block';
        }else{
            document.getElementById(MelementId).style.display = 'block';
            document.getElementById(Replaceval).style.display = 'none';  
        }
    }, 
    
    Orghideshow:function(component, event) {
        debugger;
        var alltrs = document.getElementsByClassName('hidden-tr'+event.target.id);
        var alltrs1 = document.getElementsByClassName('fas fa-plus-circle');
        var alltrs2 = document.getElementsByClassName('fas fa-minus-circle'); 
        for (var i = 0; i < alltrs.length; i++) {
            alltrs[i].style.display = 'table-row';
            alltrs1[i].style.display = 'block';
            alltrs2[i].style.display = 'none';
        }
       
        var MelementId =event.target.id;
       
        
        var n = MelementId.includes("PO_");
        if(n){
            var Replaceval = MelementId.replace("PO_", "MO_");
            document.getElementById(MelementId).style.display = 'none';
            document.getElementById(Replaceval).style.display = 'block';
        }else{
            var Replaceval = MelementId.replace("MO_", "PO_");
            document.getElementById(MelementId).style.display = 'none';
            document.getElementById(Replaceval).style.display = 'block'; 
            
        }
    }, 
    
    selectAll1: function(component, event, helper) {
        debugger;
        //get the header checkbox value  
        var selectedHeaderCheck = event.getSource().get("v.value");       
        var getAllId = component.find("checkdoc");        
        if(! Array.isArray(getAllId)){
            if(selectedHeaderCheck == true){ 
                component.find("checkdoc").set("v.value", true);                
            }else{
                component.find("checkdoc").set("v.value", false);               
            }
        }else{           
            if (selectedHeaderCheck == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("checkdoc")[i].set("v.value", true);                    
                }
            } else {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("checkdoc")[i].set("v.value", false);                    
                }
            } 
        }
        
    },
    onchangecheckbox :  function(component, event, helper) {
        debugger;
        var selectedContacts = [];
        var checkvalue = component.find("checkdoc");
        
        if(!Array.isArray(checkvalue)){
            if (checkvalue.get("v.value") == true) {
                selectedContacts.push(checkvalue.get("v.text"));
            }
        }
    },
    CPConfirmationpopup : function(component, event, helper) {
        debugger;
        /*var selectedUser = [];
        var checkvalue = component.find("checkdoc");
        
        if(!Array.isArray(checkvalue)){
            if (checkvalue.get("v.value") == true) {
                selectedUser.push(checkvalue.get("v.text"));
            }
        }else{
            for (var i = 0; i < checkvalue.length; i++) {
                if (checkvalue[i].get("v.value") == true) {
                    selectedUser.push(checkvalue[i].get("v.text"));
                }
            }
        }        
        console.log('selectedUser-' + selectedUser);
        component.set("v.publishedTenantuser",selectedUser);
        component.set("v.counterpartyselectionPopup",false);
        component.set("v.CPConfirm",true);*/
        component.set("v.counterpartyselectionPopupnew", false);
         $A.get('e.force:refreshView').fire();
        
    },
    
    AddcounterPartyusers :  function(component, event, helper) {
        debugger;   
        var tenantUser = component.get("v.publishedTenantuser");
        var action = component.get('c.addCPTenantUsers');  
        action.setParams({
            "tenantusers" : tenantUser,
            "offerId" : component.get("v.Transactions.Trans.Id")                        
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                if(result == 'Success'){
                    component.set("v.CPConfirm",false);
                    $A.get('e.force:refreshView').fire();
                }
            }
        });
        $A.enqueueAction(action);       
        
    },
    viewRequestedCounterParties  :  function(component, event, helper) {
        debugger; 
        var idx = event.target.id;
        var action = component.get('c.viewRequestUsers');  
        action.setParams({
            "publishtenantid" : idx                               
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){                
                component.set("v.Transactions", result.CounterPartiesUsers);
                component.set("v.counterpartyrequestviewPopup", true);               
            }
        });
        $A.enqueueAction(action);   
    },
    closecp: function(component, event, helper) {
        component.set("v.benefits1", true);
        component.set("v.userinfo1", false);
        component.set("v.counterpartyselectionPopupnew", false); 
        debugger;
        $A.get('e.force:refreshView').fire();
    },
    viewrequestuserclose: function(component, event, helper) {
        debugger;
        component.set("v.counterpartyrequestviewPopup", false);
        $A.get('e.force:refreshView').fire();
    },
    selectManagetransaction:function(component, event, helper) {
        debugger;
        var checkbox = event.getSource();
        var checked = checkbox.get("v.value");
        if(checked){
            
            var CP = component.get("c.getCPusers");
            CP.setParams({ 
                "tenantId" :  localStorage.getItem("LoggeduserTenantID"),
                "transId" : component.get("v.Transactions.Trans.Id") 
            }); 
            CP.setCallback(this, function(response) {
                var state = response.getState();
                if (component.isValid() && state == 'SUCCESS') {   
                    var CP = response.getReturnValue();
                    var options = [];
                    CP.forEach(function(CPdata)  { 
                        //alert(CPdata.Id);
                        options.push({ value: CPdata.Id, label: CPdata.First_Name__c});
                        
                    });
                    component.set("v.listOptions", options);
                } else {
                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(CP); 
            
            var action = component.get("c.getTransactiontasks");
            action.setParams({ 
                "transId" : component.get("v.Transactions.Trans.Id")             
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') {   
                    component.set("v.Transactionstask",result);
                    component.set("v.managetransaction", true);
                    component.set("v.AmendBillingstatus", true);
                    component.set("v.combox1", "p-card");
                    component.set("v.combox2", "p-card");
                    if(result.Tasks.length > 0){
                        component.set("v.Taskcreated", true);
                        component.set("v.noTaskcreated", false);    
                        component.set("v.Taskprogress", "process");
                        component.set("v.Taskbox", "p-card progress-border");
                        component.set("v.hTaskstatus","spinneryes");
                        component.set("v.isTaskclosedstatus", false);
                        
                    }else{
                        component.set("v.Taskcreated", false);
                        component.set("v.noTaskcreated", true); 
                    }
                    if(result.isTaskclosedstatus == true){
                        component.set("v.Taskbox", "p-card active-border");
                        component.set("v.isTaskclosedstatus", true);
                    }else{
                        component.set("v.isTaskclosedstatus", false);
                    }
                    if(result.TransAttAmendgroup.length > 0){
                        component.set("v.Amendcreated", true);
                        component.set("v.AmendBillingstatus", false);
                        component.set("v.Amendbox", "p-card progress-border");
                    }else{
                        component.set("v.Amendcreated", false);
                        component.set("v.AmendBillingstatus", true);
                    }
                } else {
                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(action); 
        }else{
            component.set("v.managetransaction", false);
        }
    },
    createnewtask:function(component, event, helper) {
        debugger;
        component.set("v.taskCreate", true);
    },
    taskcancel:function(component, event, helper) {
        debugger;
        component.set("v.taskCreate", false);
    },
    saveTask:function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        var taskType = component.find("taskType").get("v.value"); 
        var Userlist = component.get("v.selectedOptions");
        var subject = component.find("txtsubject").get("v.value"); 
        var duedate = component.find("duedate").get("v.value");
        var comments = component.find("txtcomments").get("v.value");
        var priority = component.find("priorityTypes").get("v.value");
        var status = component.find("statusTypes").get("v.value");
        var reminder = component.find("reminderdate").get("v.value");
        var remindertime = component.find("auratimevalue").get("v.value");
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        
        if(subject == '' || subject == null || subject == undefined || subject.trim().length*1 == 0) {
            errorFlag = 'true';           
            component.set("v.txtsubjectError", 'Input field required'); 
        } else { 
            component.set("v.txtsubjectError", ''); 
        }
        if(duedate == '' || duedate == null || duedate == undefined ) {
            errorFlag = 'true';           
            component.set("v.duedateError", 'Input field required'); 
        } else { 
            component.set("v.duedateError", ''); 
        }
        if(comments == '' || comments == null || comments == undefined || comments.trim().length*1 == 0) {
            errorFlag = 'true';           
            component.set("v.commentsError", 'Input field required'); 
        } else { 
            component.set("v.commentsError", ''); 
        }
        if(Userlist.length ==0){
            if(Userlist == '' || Userlist == null || Userlist == undefined) {
                errorFlag = 'true';                   
                component.set("v.AssigneeError", 'Input field required'); 
            }
        } else { 
            component.set("v.AssigneeError", ''); 
        }
        if(today > duedate){
            errorFlag = 'true';  
            component.set("v.duedateError", 'Due date should not less than today date');
        }else{
            component.set("v.duedateError", '');
        }
        if(reminder == ''){
            reminder = '2019-10-10';
        }else{
            if(duedate < reminder){
                errorFlag = 'true';                   
                component.set("v.reminderdateError", 'Reminder date should not greater than due date'); 
            }else{
                component.set("v.reminderdateError", '');
            }
            if(duedate > reminder){
                if(today > reminder){
                    errorFlag = 'true';                   
                    component.set("v.reminderdateError", 'Reminder date should not less than today date'); 
                }            
                else{
                    component.set("v.reminderdateError", '');
                }
            }
        }
        if(errorFlag == 'false') {  
            var action = component.get("c.insertTaskfortransaction");
            action.setParams({ 
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "tenantId" :  localStorage.getItem("LoggeduserTenantID") ,
                "userlists" : Userlist,
                "tasktype" : taskType,
                "tasksubject" : subject,
                "duedate" : duedate,
                "notes" : comments,
                "priority" : priority,
                "status" : status,
                "reminder" : reminder,
                "remindertime" : remindertime,
                "transactionId" : component.get("v.Transactions.Trans.Id"),
                "transrefnumber" : component.get("v.Transactions.Trans.TransactionRefNumber__c")
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') {   
                    component.set("v.Transactionstask",result); 
                    component.set("v.taskCreate", false);
                    if(result.Tasks.length > 0){
                        component.set("v.Taskcreated", true);
                        component.set("v.noTaskcreated", false);    
                        component.set("v.Taskprogress", "process");
                        component.set("v.Taskbox", "p-card progress-border");
                        component.set("v.hTaskstatus","spinneryes");
                        component.set("v.isTaskclosedstatus", false);
                        
                    }
                } else {
                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(action); 
        }
    },
    
    viewclosedtask: function(component, event) {
        debugger;
        component.set("v.taskviews",true);        
        component.set("v.taskId", event.currentTarget.getAttribute("data-recId")); 
        component.find("txtcomments").set("v.disabled", true);
        component.find("selectOptions").set("v.disabled", true);
        var action = component.get("c.getTaskfortransaction");
        action.setParams({                      
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "tenantId" :  localStorage.getItem("LoggeduserTenantID") ,              
            "transactionId" : component.get("v.Transactions.Trans.Id"), 
            "taskId" : component.get("v.taskId")
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state == 'SUCCESS') {   
                component.set("v.managetask",result.taskdetails); 
                component.set("v.Transactionstask",result);
                //component.set("v.Transactionstask.TaskActivity_Log",result.TaskActivity_Log); 
                //component.set("v.Transactionstask.FileUploadUrl2",result.FileUploadUrl2); 
                component.set("v.Transactionstask.ContentVer",result.ContentVer);
                var options = [];
                result.userslist1.forEach(function(CPdata)  { 
                    //alert(CPdata.Id);
                    options.push({ value: CPdata.Id, label: CPdata.First_Name__c});
                    
                });
                component.set("v.listOptions", options);
                if(result.ContentVer.length > 0){
                    component.set("v.taskownerattachment", true);
                }
                if(result.TaskActivity_Log.length > 0){
                    component.set("v.taskownerhistory", true);
                }
                if(result.isTaskclose == true){
                    component.set("v.closedtask", true);
                }
                component.set("v.selectedOptions",result.taskdetails.Entity_User__c);
            } else {
                
                console.log('Failed with state: ' + state);
            }
        });
        $A.enqueueAction(action); 
    },
    
    updateTask : function(component, event) {
        debugger;
        component.set("v.taskUpdate",true);        
        component.set("v.taskId", event.target.id); 
        var action = component.get("c.getFileuploadurlforTask");
            action.setParams({ 
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "tenantId" :  localStorage.getItem("LoggeduserTenantID") ,
                "taskId" : component.get("v.taskId"),
                "transactionId" : component.get("v.Transactions.Trans.Id") 
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') { 
                    component.set("v.Transactions.FileUploadUrl2",result.FileUploadUrl2);
                    component.set("v.Transactions.TaskActivity_Log",result.TaskActivity_Log); 
                    component.set("v.Transactions.ContentVer",result.ContentVer);
                    if(result.TaskActivity_Log.length > 0){
                        component.set("v.taskcphistory", true);
                    }
                    if(result.ContentVer.length > 0){
                        component.set("v.taskcpattachment", true);
                    }
                    
                } else {
                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(action); 
    },
    updatetasktransaction : function(component, event, helper) {
        debugger;
        
        component.set("v.taskcommentsError", ''); 
        var errorFlag = 'false';
        var comments = component.find("txttaskcomments").get("v.value");
        if(comments == '' || comments == null || comments == undefined || comments.trim().length*1 == 0) {
            errorFlag = 'true';           
            component.set("v.taskcommentsError", 'Input field required'); 
        } else { 
            component.set("v.taskcommentsError", ''); 
        }
          if(errorFlag == 'false') {  
            var action = component.get("c.updateTaskfortransaction");
            action.setParams({ 
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "tenantId" :  localStorage.getItem("LoggeduserTenantID") ,               
                "notes" : comments,               
                "taskId" : component.get("v.taskId")
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') { 
                        component.set("v.Transactions.TaskActivity_Log", result.TaskActivity_Log);
                        component.set("v.taskUpdate",false);
                  
                } else {

                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(action); 
        }
    },
    
    taskupdatecancel : function(component, event, helper) {
        debugger;
        component.set("v.taskUpdate",false);
        
    },
    updatemanagetask : function(component, event) {
        debugger;
        component.set("v.taskownerupdate",true);        
        component.set("v.taskId", event.target.id); 
        component.find("updatetaskType").set("v.disabled", true);
        component.find("txtsubject").set("v.disabled", true);
        component.find("txtcomments").set("v.disabled", true);
        component.find("selectOptions").set("v.disabled", true);
        var action = component.get("c.getTaskfortransaction");
        action.setParams({                      
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "tenantId" :  localStorage.getItem("LoggeduserTenantID") ,              
            "transactionId" : component.get("v.Transactions.Trans.Id"), 
            "taskId" : component.get("v.taskId")
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state == 'SUCCESS') {   
                component.set("v.managetask",result.taskdetails); 
                component.set("v.Transactionstask.TaskActivity_Log",result.TaskActivity_Log); 
                component.set("v.Transactionstask.FileUploadUrl2",result.FileUploadUrl2); 
                component.set("v.Transactionstask.ContentVer",result.ContentVer);
                if(result.ContentVer.length > 0){
                    component.set("v.taskownerattachment", true);
                }
                if(result.TaskActivity_Log.length > 0){
                    component.set("v.taskownerhistory", true);
                }
                component.set("v.selectedOptions",result.taskdetails.Entity_User__c);
            } else {
                
                console.log('Failed with state: ' + state);
            }
        });
        $A.enqueueAction(action); 
    },
    
    taskownerupdatecancel : function(component, event){
        debugger;
        component.set("v.taskownerupdate",false);
    },
    taskviewclose : function(component, event){
        debugger;
        component.set("v.taskviews",false);
    },
    sellertaskviewclose : function(component, event){
        debugger;
        component.set("v.taskviews",false);
        var CP = component.get("c.getCPusers");
        CP.setParams({ 
                "tenantId" :  localStorage.getItem("LoggeduserTenantID"),
                "transId" : component.get("v.Transactions.Trans.Id") 
            }); 
            CP.setCallback(this, function(response) {
                var state = response.getState();
                if (component.isValid() && state == 'SUCCESS') {   
                    var CP = response.getReturnValue();
                    var options = [];
                    CP.forEach(function(CPdata)  { 
                        //alert(CPdata.Id);
                        options.push({ value: CPdata.Id, label: CPdata.First_Name__c});
                        
                    });
                    component.set("v.listOptions", options);
                } else {
                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(CP); 
            
            var action = component.get("c.getTransactiontasks");
            action.setParams({ 
                "transId" : component.get("v.Transactions.Trans.Id")             
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') {   
                    component.set("v.Transactionstask",result);
                    component.set("v.managetransaction", true);
                    component.set("v.AmendBillingstatus", true);
                    component.set("v.combox1", "p-card");
                    component.set("v.combox2", "p-card");
                    if(result.Tasks.length > 0){
                        component.set("v.Taskcreated", true);
                        component.set("v.noTaskcreated", false);    
                        component.set("v.Taskprogress", "process");
                        component.set("v.Taskbox", "p-card progress-border");
                        component.set("v.hTaskstatus","spinneryes");
                        
                    }else{
                        component.set("v.Taskcreated", false);
                        component.set("v.noTaskcreated", true); 
                    }
                } else {
                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(action); 
        
    },
    managetaskownerupdate: function(component, event){
        debugger;
        component.set("v.taskcommentsError", ''); 
        var errorFlag = 'false';
        var comments = component.find("txttaskupdatecomments").get("v.value");        
        var duedate = component.find("duedateupdate").get("v.value");
        var priority = component.find("priorityTypesupdate").get("v.value");
        var status = component.find("statusTypesupdate").get("v.value");
        var reminder = component.find("reminderdateupdate").get("v.value");
        var remindertime = component.find("auratimevalueupdate").get("v.value");
        if(comments == '' || comments == null || comments == undefined || comments.trim().length*1 == 0) {
            errorFlag = 'true';           
            component.set("v.taskcommentsError", 'Input field required'); 
        } else { 
            component.set("v.taskcommentsError", ''); 
        }
        if(duedate < reminder){
            errorFlag = 'true';                   
            component.set("v.reminderdateError", 'Reminder date Should not greater than due date'); 
        }else{
            component.set("v.reminderdateError", '');
        }
        if(errorFlag == 'false') {  
            var action = component.get("c.updateTaskownerfortransaction");
            action.setParams({ 
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "tenantId" :  localStorage.getItem("LoggeduserTenantID") ,               
                "notes" : comments,               
                "taskId" : component.get("v.taskId"),
                "status" : status,
                "duedate" : duedate,                
                "priority" : priority,                
                "reminder" : reminder,
                "remindertime" : remindertime
                
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') {  
                    component.set("v.Transactionstask",result); 
                    component.set("v.taskCreate", false);
                    if(result.Tasks.length > 0){
                        component.set("v.Taskcreated", true);
                        component.set("v.noTaskcreated", false);   
                        component.set("v.taskownerupdate",false);
                        component.set("v.Taskprogress", "process");
                        component.set("v.Taskbox", "p-card progress-border");
                        component.set("v.hTaskstatus","spinneryes");                        
                    }
                    if(result.isTaskclosedstatus == true){
                        component.set("v.Taskbox", "p-card active-border");
                        component.set("v.isTaskclosedstatus", true);
                    }else{
                        component.set("v.isTaskclosedstatus", false);
                    }                    
                    //if(result == 'Success'){
                    
                   // component.set("v.Transactions",result);
                    //}
                } else {

                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(action); 
        }
        
    },
    
    Editamend : function(component, event){
        debugger;   
        
        var action = component.get('c.editviewTransaction'); 
        var self = this;
        action.setParams({
            "transid" : component.get("v.TransId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Transactions.TransAtt", result.TransAtt); 
                var i;
                for (i = 0; i < result.TransAtt.length; i++) {
                    if(result.TransAtt[i].Attribute_Name__c == 'Date of Expiry'){
                        component.set("v.shipmentexpirydate", result.TransAtt[i].Id+':'+result.TransAtt[i].Attribute_Name__c+':'+result.TransAtt[i].Attribute_Value__c);
                    }
                    if(result.TransAtt[i].Attribute_Name__c == 'Date of Shipment'){
                        component.set("v.shipmentdate", result.TransAtt[i].Id+':'+result.TransAtt[i].Attribute_Name__c+':'+result.TransAtt[i].Attribute_Value__c);                        
                    }
                }
                component.set("v.Amendoffer", true);
                component.find("seller_notesib").set("v.disabled", true);
            }
            
        });        
        $A.enqueueAction(action);
        
    },
    
    Editamendcancel : function(component, event){
        debugger;
        component.set("v.Amendoffer", false);
    },
    SaveAmendTransaction:function(component, event, helper){
        debugger;   
        var errorFlag = 'false';
        var Errormsg ='';
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();
        if(dd > 9){var date = dd;}else{var date = '0'+dd;}
        if(mm > 9){var month = mm;}else{var month = '0'+mm;}
        component.set("v.currentDate",yyyy+'-'+month+'-'+date);
        var prevshipmentdate = component.get("v.shipmentdate");
        var prevshipmentexpirydate = component.get("v.shipmentexpirydate");
        var currentshipmentdate;
        var currentshipmentexpirydate;
        var comments = component.find("amendnotes").get("v.value");
        //var SellNotesib = component.find("seller_notesib").get("v.value");
        var Attoptions = [];
        var tempatt = component.get("v.editattributesstring");
        for(var i = 0; i < tempatt.length; i++) {
            if(i !=''){
                if(prevshipmentdate.split(":")[1] ==  'Date of Expiry'){
                    currentshipmentexpirydate = tempatt[i].split(":")[1]+':'+tempatt[i].split(":")[2];
                   
                }
                if(prevshipmentdate.split(":")[1] ==  'Date of Shipment'){
                    currentshipmentdate = tempatt[i].split(":")[1]+':'+tempatt[i].split(":")[2];
                }
                Attoptions.push(tempatt[i]);
            }            
        }
        if(currentshipmentexpirydate != null){
            if(Date.parse(currentshipmentexpirydate.split(":")[1]) < Date.parse(component.get("v.currentDate")))
                    {
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth()+1;
                        var yyyy = today.getFullYear();
                        if(dd > 9){var date = dd;}else{var date = '0'+dd;}
                        if(mm > 9){var month = mm;}else{var month = '0'+mm;}
                        Errormsg = currentshipmentexpirydate.split(":")[0]+' must be '+date+'-'+month+'-'+yyyy+' or later , ';
                        errorFlag = 'true';        
                        component.set("v.DatevalidationError",Errormsg);
                    }else{
                        component.set("v.DatevalidationError",'');
                    }
        }
         if(currentshipmentdate != null){
            if(Date.parse(currentshipmentdate.split(":")[1]) < Date.parse(component.get("v.currentDate")))
                    {
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth()+1;
                        var yyyy = today.getFullYear();
                        if(dd > 9){var date = dd;}else{var date = '0'+dd;}
                        if(mm > 9){var month = mm;}else{var month = '0'+mm;}
                        Errormsg = currentshipmentdate.split(":")[0]+' must be '+date+'-'+month+'-'+yyyy+' or later , ';
                        errorFlag = 'true';        
                        component.set("v.DatevalidationError",Errormsg);
                    }else{
                        component.set("v.DatevalidationError",'');
                    }
         }
        if(currentshipmentexpirydate == undefined && currentshipmentdate != null ){
            if(Date.parse(prevshipmentexpirydate.split(":")[2]) < Date.parse(currentshipmentdate.split(":")[1])){
                errorFlag = 'true';   
                component.set("v.Datevalidation1Error",'Date of Shipment should not be greater than Date of Expiry');
            }else{
                component.set("v.Datevalidation1Error",'');
            }
        }
    if(comments == '' || comments == null || comments == undefined || comments.trim().length*1 == 0) {
            errorFlag = 'true';           
            component.set("v.AmendNotesError", 'Input field required'); 
        } else { 
            component.set("v.AmendNotesError", ''); 
        }
        var idListJSON=JSON.stringify(Attoptions);
        if(errorFlag == 'false') {
            
            var Action = component.get("c.amendTransaction");        
            Action.setParams({
                "transid" : component.get("v.TransId"),
                "TransAttribute" : idListJSON,
                "userid" : component.get("v.Loggeduserid"),
                "notes" : comments
            });        
            Action.setCallback(this, function(actionResult) {
                debugger;
                var state = actionResult.getState(); 
                var result1 = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.Amendoffer", false);
                    component.set("v.Transactionstask",result1);
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-clientdemo.git
                    /*var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Attributes Amended Successfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                    $A.get('e.force:refreshView').fire();
                    compoment.set("v.Amendoffer", false);*/
                    
                }
            });
            $A.enqueueAction(Action);
        }
        
        
    },
    onChangetasktype : function(component, event, helper){
        debugger;
        var taskType = component.find("taskType").get("v.value");  
        if(taskType == 'Amendment'){
            var CP = component.get("c.getPartyusers");
            CP.setParams({ 
                "tenantId" :  localStorage.getItem("LoggeduserTenantID"),
                "transId" : component.get("v.Transactions.Trans.Id") 
            }); 
            CP.setCallback(this, function(response) {
                var state = response.getState();
                if (component.isValid() && state == 'SUCCESS') {   
                    var CP = response.getReturnValue();
                    var options = [];
                    CP.forEach(function(CPdata)  { 
                        //alert(CPdata.Id);
                        options.push({ value: CPdata.Id, label: CPdata.First_Name__c});
                        
                    });
                    component.set("v.listOptions", options);
                } else {
                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(CP); 
        }else{
            var CP1 = component.get("c.getCPusers");
            CP1.setParams({ 
                "tenantId" :  localStorage.getItem("LoggeduserTenantID"),
                "transId" : component.get("v.Transactions.Trans.Id") 
            }); 
            CP1.setCallback(this, function(response) {
                var state = response.getState();
                if (component.isValid() && state == 'SUCCESS') {   
                    var CP1 = response.getReturnValue();
                    var options = [];
                    CP1.forEach(function(CPdata)  { 
                        //alert(CPdata.Id);
                        options.push({ value: CPdata.Id, label: CPdata.First_Name__c});
                        
                    });
                    component.set("v.listOptions", options);
                } else {
                    console.log('Failed with state: ' + state);
                }
            });
            $A.enqueueAction(CP1); 
        }
        
    },
    
    Amendsubmit: function(component, event, helper){
        debugger;
        var amendvalue = event.currentTarget.getAttribute("data-recId");
         var action = component.get("c.amendapprovalrequest");
            action.setParams({ 
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "transId" : component.get("v.TransId"),
                "amendId" : amendvalue
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') {  
                    
                        component.set("v.Transactionstask",result);
                        var showToast = $A.get('e.force:showToast');
                        showToast.setParams(
                            {
                                'title': 'Success: ',
                                'message': 'Amendment Submitted for Approval',
                                'type': 'Success'
                            }
                        );
                        showToast.fire();
                       // $A.get('e.force:refreshView').fire();
                    
                } else {
                    console.log('Failed with Amendsubmit: ' + state);
                }
            });
            $A.enqueueAction(action); 
    },
    
    acceptamendment : function(component, event, helper){
        debugger;
        component.set("v.AmendId",event.currentTarget.getAttribute("data-recId"));
        component.set("v.AcceptRejectNotes", true);
        component.set("v.AcceptNotes" , true);
    },
    
    acceptamendmentsubmit : function(component, event, helper){
        debugger;
        var amendvalue = component.get("v.AmendId");
        var errorFlag = 'false';
        var comments = component.find("acceptrejectbody").get("v.value");
        if(comments == '' || comments == null || comments == undefined || comments.trim().length*1 == 0) {
            errorFlag = 'true';           
            component.set("v.acceptrejectNoteError", 'Input field required'); 
        } else { 
            component.set("v.acceptrejectNoteError", ''); 
        }
        if(errorFlag == 'false') {  
            
            var action = component.get("c.amendapprove");
            action.setParams({ 
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "transId" : component.get("v.TransId"),
                "amendId" : amendvalue,
                "notes" : comments
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') {  
                    if(result == 'Success'){
                        var showToast = $A.get('e.force:showToast');
                        showToast.setParams(
                            {
                                'title': 'Success: ',
                                'message': 'Amendment Accepted Successfully',
                                'type': 'Success'
                            }
                        );
                        showToast.fire();
                        $A.get('e.force:refreshView').fire();
                    }
                } else {
                    console.log('Failed with Amendsubmit: ' + state);
                }
            });
            $A.enqueueAction(action); 
        }
        
    },
    
    rejectamendment: function(component, event, helper){
        debugger;
        component.set("v.AmendId",event.currentTarget.getAttribute("data-recId"));
        component.set("v.AcceptRejectNotes", true);
        component.set("v.RejectNotes" , true);
    },
    
    rejectamendmentsubmit: function(component, event, helper){
        debugger;
        var amendvalue = component.get("v.AmendId");
        var errorFlag = 'false';
        var comments = component.find("acceptrejectbody").get("v.value");
        if(comments == '' || comments == null || comments == undefined || comments.trim().length*1 == 0) {
            errorFlag = 'true';           
            component.set("v.acceptrejectNoteError", 'Input field required'); 
        } else { 
            component.set("v.acceptrejectNoteError", ''); 
        }
        if(errorFlag == 'false') {             
            var action = component.get("c.amendreject");
            action.setParams({ 
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "transId" : component.get("v.TransId"), 
                "amendId" : amendvalue,
                "notes" : comments
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') {  
                    if(result == 'Success'){
                        var showToast = $A.get('e.force:showToast');
                        showToast.setParams(
                            {
                                'title': 'Success: ',
                                'message': 'Amendment Rejected Successfully',
                                'type': 'Success'
                            }
                        );
                        showToast.fire();
                        $A.get('e.force:refreshView').fire();
                    }
                } else {
                    console.log('Failed with Amendsubmit: ' + state);
                }
            });
            $A.enqueueAction(action); 
        }
        
    },
    compareamendment : function(component, event, helper){
        debugger;
        var amendvalue = event.currentTarget.getAttribute("data-recId");
        component.set("v.showCompareattributes", true);
        var action = component.get("c.compareamends");
            action.setParams({ 
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "transId" : component.get("v.TransId"), 
                "amendId" : amendvalue
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') {  
                    component.set("v.Transactions.TransAttAmendAccepted",result.TransAttAmendAccepted);
                    component.set("v.Transactions.TransAttOldValues",result.TransAttOldValues);
                    
                } else {
                    console.log('Failed with Amendsubmit: ' + state);
                }
            });
            $A.enqueueAction(action); 
        
    },
    viewacceptedAmendments : function(component, event, helper){
        debugger;
        //var amendvalue = event.currentTarget.getAttribute("data-recId");
        //var amendvalue = event.currentTarget.getAttribute("data-recId");
        var amendvalue = event.target.id;
        var action = component.get("c.viewacceptedamend");
            action.setParams({ 
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "transId" : component.get("v.TransId"), 
                "amendId" : amendvalue
            }); 
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state == 'SUCCESS') {  
                    component.set("v.AcceptedAmendoffer", true);
                    component.set("v.Transactions.TransAttAmendAccepted",result.TransAttAmendAccepted); 
                } else {
                    console.log('Failed with Amendsubmit: ' + state);
                }
            });
            $A.enqueueAction(action);         
        
    },
    
    closeacceptedamend: function(component, event, helper){
        debugger;
        component.set("v.AcceptedAmendoffer", false);
    },
  
    closecompareamend: function(component, event, helper){
        debugger;
        component.set("v.showCompareattributes", false);
    },
    AcceptRejectNotesCancel : function(component, event, helper){
        debugger;
        component.set("v.AcceptRejectNotes", false);
    },
})
({
     tabhighlight: function(component) {  
         debugger;
         var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
         document.title =sitename +' : Clone Product Template';
         component.find("rbtnFunded").set("v.disabled", true);
         component.find("rbtnUnFunded").set("v.disabled", true);
         component.set("v.TenantIDs",localStorage.getItem('IDTenant'));
         var action = component.get('c.getCloneProductTemplate');
      var getUrlParameter = function getUrlParameter(sParam) {
          try{
              var searchString = document.URL.split('?')[1];
              var sPageURL = decodeURIComponent(searchString),
                  sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                  sParameterName,
                  i;
              for (i = 0; i < sURLVariables.length; i++) {
                  sParameterName = sURLVariables[i].split('=');                 
                  if (sParameterName[0] === sParam)  {
                      return sParameterName[1] === undefined ? true : sParameterName[1];
                  }
              }
          }catch(ex){}
      };
      
      var templateId = getUrlParameter('templateId');
      action.setParams({
          "templateId" : templateId,
           "LoggeduserTenantID" : localStorage.getItem('LoggeduserTenantID')
      });
      action.setCallback(this, function(actionResult) {
          debugger;
          var state = actionResult.getState(); 
          if(state === "SUCCESS"){  
              var result = actionResult.getReturnValue();
              component.set("v.template", result.ProductTemplateVExtended);
              debugger;
              if(result.ProductTemplateVExtended.ProductType=='Funded')
              {
                  var pTyperbtnFunded = component.find("rbtnFunded").set("v.value",true); 
                  
              }else{
                  var pTyperbtnUnFunded = component.find("rbtnUnFunded").set("v.value",true); 
              }
              if(result.ProductTemplateVExtended.IsPrimary)
              {
                  var mTyperbtnPrimary = component.find("rbtnPrimary").set("v.value",true); 
              }else{
                  var mTyperbtnSecondary = component.find("rbtnSecondary").set("v.value",true); 
              }
              var approveLevel = component.find("dropApproveLevel");
              approveLevel.set("v.value", result.ProductTemplateVExtended.ApproveLevel);
          } 
          
          else if(state === "ERROR") {
              component.set("v.template", component.get("v.template"));
          }
      }); 
      $A.enqueueAction(action);   
     },
     CloneProductTEmp : function(component, event, helper) { 
      debugger;
      var errorFlag = 'false';
      component.set("v.validation", false);
      var txtTemplateName = component.find("txtTemplateName").get("v.value");
      var mTyperbtnPrimary = component.find("rbtnPrimary").get("v.value"); 
      var mTyperbtnSecondary = component.find("rbtnSecondary").get("v.value"); 
      
      if(mTyperbtnPrimary){
          var market='Primary';
      }
      else if(mTyperbtnSecondary){
          var market='Secondary';
      }
      
      var pTyperbtnFunded = component.find("rbtnFunded").get("v.value"); 
      var pTyperbtnUnFunded = component.find("rbtnUnFunded").get("v.value"); 
      
      if(pTyperbtnFunded){
          var loanType='Funded';
      }
      else if(pTyperbtnUnFunded){
          var loanType='UnFunded';
      } 
      if(loanType == '' || loanType == null || loanType == undefined || loanType.trim().length*1 == 0) {
          errorFlag = 'true';
          component.set("v.validation", true);
          component.set("v.loanError", 'Input field required'); 
      } else { 
          component.set("v.loanError", ''); 
      }
      if(market == '' || market == null || market == undefined || market.trim().length*1 == 0) {
          errorFlag = 'true';
          component.set("v.validation", true);
          component.set("v.MytypeError", 'Input field required'); 
      } else { 
          component.set("v.MytypeError", ''); 
      }
      
      if(txtTemplateName == undefined || txtTemplateName == '')
      {
          errorFlag = 'true';
          component.set("v.validation", true); 
          component.set("v.nameError", 'Input field required'); 
      }
      else
      { 
          if(helper.checkSpecialCharecter(txtTemplateName))
          {
              errorFlag = 'true';
              component.set("v.validation", true); 
              component.set("v.nameError", 'Please enter without special characters.'); 
          } 
          else{  
              component.set("v.nameError", ''); 
          }  
      } 
      
      if(errorFlag == 'false') {
          var action = component.get("c.CheckProductTemplate");
          action.setParams({ "txtTemplateName": txtTemplateName,
                            "TenantId": localStorage.getItem("LoggeduserTenantID"),
                            "Type" :loanType});
          action.setCallback(this, function(response) {
              var state = response.getState();
              var requestId = response.getReturnValue(); 
              if(requestId != '') {
                  if(requestId == 'Already Used') { 
                      errorFlag = 'true';
                      component.set("v.validation", true); 
                      component.set("v.nameError", 'This Product Template Name has already been used. Please enter a new Product Template Name.');
                  } else {
                      component.set("v.nameError", ''); 
                  }
              } else { 
                  component.set("v.nameError", ''); 
              }
              if(errorFlag == 'false') {
                  component.set("v.CloneProductTEmp", true); 
              }
          });
          $A.enqueueAction(action); 
      }
  },
    getTemplateList: function(component, event, helper) { 
        debugger;
        var action = component.get('c.getProductTemplateList');
         var self = this; 
        component.set("v.selectedItem","Active");
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem")
        }); 
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){
                debugger;
                component.set("v.showAttrubutes",false);
                
                helper.setGridOption(component, actionResult.getReturnValue());
                
			}
            
        }); 
        $A.enqueueAction(action);      
        

    }, 
     cancelCloneProductTEmp : function(component, event) {
      component.set("v.CloneProductTEmp", false); 
  },
    CloneProductTEmpCheck : function(component, event, helper) {
      if(component.find("chkClone").get('v.value')==true)
      {
          component.find("btnSaveProdTemp").set("v.disabled", false);
      }
      else{
          component.find("btnSaveProdTemp").set("v.disabled", true);
      }
  },
    Beginning: function(component, event, helper) {
        var action = component.get('c.getBeginning'); 
        action.setParams({
            "counter" : '0',
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem") 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    Previous: function(component, event, helper) {
        debugger;
        
        var action = component.get('c.getPrevious'); 
        var list_size = component.get('v.list_size');
        
        var counter = component.get('v.counter');
        var res = counter - list_size;  
        if(res < 0) { res = 0; }
        action.setParams({ 
            "selected" : component.get("v.selectedItem"), 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection")
            
        });
       
        action.setCallback(this, function(actionResult) { 
            
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                
                Helper.setGridOption(component, actionResult.getReturnValue());
                
            }
        });
        $A.enqueueAction(action);
    },
    Next: function(component, event, helper) {
        var action = component.get('c.getNext'); 
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var res = list_size + counter;
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem") 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    End: function(component, event, helper) {
        var action = component.get('c.getEnd');  
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var total_size = component.get('v.total_size');
        var res = total_size - (total_size % list_size); 
        if(res == total_size) { res -= list_size; }
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem") 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    sort: function(component, event, helper) {
        var action = component.get('c.SortTable'); 
        var sortfield = event.currentTarget.getAttribute("data-recId");  
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : sortfield,
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem") 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    changePageSizes: function(component, event, helper) {
        var action = component.get('c.changelist_size');  
        var list_size = component.find("recordSize").get("v.value"); 
        action.setParams({
            "counter" : '0',
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem"), 
            "list_size" : list_size
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
               helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    changePageOptions: function(component, event, helper) {
        var action = component.get('c.changePage'); 
        var showpage = component.find("pageOptions").get("v.value"); 
        var list_size = component.find("recordSize").get("v.value"); 
        var counter = (parseInt(showpage)-1)*list_size;
        action.setParams({
            "counter" : counter.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem") 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    handleChange: function (component, event, helper) {
        var selectedFields = event.getParam("value");
        var targetName = event.getSource().get("v.name");
        if(targetName == 'srcFields'){ 
            component.set("v.template.SelectedFields", selectedFields);
        }
    },
    createTemplate: function(component, event, helper) { 
        var action = component.get('c.getProductTemplate');
        
        action.setParams({
            "templateId" : "",
            "productType" : "Funded",
            "marketType" : "false",
            "templateName": ""
        }); 
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                var result = actionResult.getReturnValue();
                component.set("v.template", result);
                component.set("v.showCreate", true);
            }
            else if(state === "ERROR") {
                component.set("v.template", component.get("v.template"));
            }
        }); 
        $A.enqueueAction(action);      
    },
    cancel: function(component, event, helper) {
        component.set("v.showCreate", false);
        component.set("v.validation", false);
        component.set("v.nameError", '');
        component.set("v.loanError", '');
        component.set("v.MytypeError", '');
    },
     saveTemplate: function(component, event, helper) { 
        debugger;
        var errorFlag = 'false';
		 component.set("v.CloneProductTEmp", false);
        component.set("v.validation", false);  
        var template = component.get("v.template");
        var tempname = component.find("txtTemplateName").get("v.value");
        var mTyperbtnPrimary = component.find("rbtnPrimary").get("v.value"); 
        //var mTyperbtnSecondary = component.find("rbtnSecondary").get("v.value"); 
        if(tempname == '' || tempname == null || tempname == undefined || tempname.trim().length*1 == 0) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.nameError", 'Input field required'); 
        } else { 
            if(helper.checkSpecialCharecter(tempname)) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.nameError", 'Please enter without special characters.'); 
            } else {  
                component.set("v.nameError", ''); 
            } 
        }
        
        //template.IsPrimary=mTyperbtnPrimary;
		var pTyperbtnFunded = component.find("rbtnFunded").get("v.value"); 
        var pTyperbtnUnFunded = component.find("rbtnUnFunded").get("v.value"); 
            
        if(pTyperbtnFunded){
          //  template.ProductType='UnFunded';
            var loanType='Funded';
		}
        else if(pTyperbtnUnFunded){
           //  template.ProductType='UnFunded';
            var loanType='UnFunded';
		} 
         if(loanType == '' || loanType == null || loanType == undefined || loanType.trim().length*1 == 0) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.loanError", 'Input field required'); 
        } else { 
            component.set("v.loanError", ''); 
        }
       
        
        if(errorFlag == 'false') {
            template.IsPrimary=mTyperbtnPrimary;
            if(pTyperbtnFunded){
                template.ProductType='UnFunded';
                
            }
            else if(pTyperbtnUnFunded){
               template.ProductType='UnFunded';
              
            } 
            template.CreatedByName = component.get("v.TenantIDs");
            template.LastModifiedByName = component.get("v.TenantIDs");
            template.ApproveLevel=1;
            template.icclonechk = true;
            template.TemplateName=tempname;
            debugger;
            component.set("v.showProgress",true);
            var img = component.find("pdfImgloading");
            $A.util.removeClass(img,'slds-hide');
            $A.util.addClass(img,'slds-show');  

            component.find("btnSaveTemplate").set("v.disabled", true); 
            component.find("btnCancleTemplate").set("v.disabled", true); 
            var action = component.get('c.saveProductTemplate');
            action.setParams({ "template": JSON.stringify(template),
                              "TenantId": localStorage.getItem("LoggeduserTenantID")});
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){  
                    
                    if(result != 'Already Used' && result !='Attributes Zero' && result != '')
                    {
                        /*var action1 = component.get('c.emailProductTemplate'); 
                        action1.setParams({
                            "templateId" : result  
                        });
                        action1.setCallback(this, function(actionResult1) {
                            var state1 = actionResult1.getState();
                        });
                        $A.enqueueAction(action1); 
                        */
                        
                        component.find("btnSaveTemplate").set("v.disabled", false); 
                        component.find("btnCancleTemplate").set("v.disabled", false); 
                        
                        component.set("v.validation", false);
                        var site = $A.get("{!$Label.c.Org_URL}");
                        window.location = '/'+site+'/s/product-template-detail?templateId='+ result;
                                 
                        $A.get('e.force:refreshView').fire();
                    }
                    else if(result == 'Already Used')
                    {
                        component.set("v.validation", true);
                        component.set("v.nameError", 'This Template Name has already been used for this tenant. Please enter a new Template Name.');
                        component.find("btnSaveTemplate").set("v.disabled", false);
                        component.find("btnCancleTemplate").set("v.disabled", false);   
                    } 
                    else if(result == 'Attributes Zero'){
                            component.set("v.validation", true);
                            component.set("v.selectatt", 'Please select at least one attributes.');
                            component.find("btnSaveTemplate").set("v.disabled", false);
                            component.find("btnCancleTemplate").set("v.disabled", false);   
                            
                        }
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    component.set("v.showProgress",false);
                }
                else if(state === "ERROR") {
                    component.find("btnSaveTemplate").set("v.disabled", false);
                    component.find("btnCancleTemplate").set("v.disabled", false); 
                    component.set("v.template", component.get("v.template"));
                    component.set("v.validation", false);
                    component.set("v.showCreate", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Failure : ',
                            'message': 'Unable to Created Template',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
            });
            $A.enqueueAction(action);
        }
    },
        
     View: function(component, event, helper) {
        debugger;
        var action = component.get('c.Views'); 
        
        var selected = event.getSource().get("v.text");
        
        component.set("v.selectedItem", selected)
        var self = this; 
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem") 
        });
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
			}
        }); 
        $A.enqueueAction(action); 
    },  
    handleProductTypeChange: function(component, event, helper) {
            debugger;
        var templateName = component.find("txtTemplateName").get("v.value");
         var productType = event.getSource().get("v.label");
        if(productType != '')
        {
            component.set("v.loanError", '');    
        }
        debugger;
        var templateName = component.find("txtTemplateName").get("v.value");
        var mTyperbtnPrimary = component.find("rbtnPrimary").get("v.value"); 
        var mTyperbtnSecondary = component.find("rbtnSecondary").get("v.value"); 
            
        if(mTyperbtnPrimary){
            var market='Primary';
		}
        else if(mTyperbtnSecondary){
            var market='Secondary';
		}
        if(mTyperbtnPrimary || mTyperbtnSecondary) {
            component.set("v.showAttrubutes",true);
            var isPrimary = market; 
        var action = component.get('c.getProductTemplate');
        action.setParams({
            "templateId" : "",
            "productType": productType,
            "marketType": isPrimary.toString(),
            "templateName": templateName
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                debugger;
                var result = actionResult.getReturnValue();
                component.set("v.template", result);
            }
        });
        $A.enqueueAction(action);
            }
    },
    handleMarkerTypeChange: function(component, event, helper) {
            debugger;
        var market = event.getSource().get("v.label");
        debugger;
        var templateName = component.find("txtTemplateName").get("v.value");
        var pTyperbtnFunded = component.find("rbtnFunded").get("v.value"); 
        var pTyperbtnUnFunded = component.find("rbtnUnFunded").get("v.value"); 
             if(market != '')
        {
            component.set("v.MytypeError", '');    
        }
        if(pTyperbtnFunded){
            var prodType='Funded';
		}
        else if(pTyperbtnUnFunded){
            var prodType='UnFunded';
		}
        if(pTyperbtnFunded || pTyperbtnUnFunded) {
            component.set("v.showAttrubutes",true);
        var isPrimary = market; 
        var action = component.get('c.getProductTemplate');
        action.setParams({
            "templateId" : "",
            "productType": prodType,
            "marketType": isPrimary.toString(),
            "templateName": templateName
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                debugger;
                var result = actionResult.getReturnValue();
                component.set("v.template", result);
            }
        });
        $A.enqueueAction(action);
            }
    }, 
})
({
	steponedelete : function(component, event, helper) {
		debugger;       
        var Action = component.get("c.getAlltransactions");       
        Action.setParams({           
            "TId" : localStorage.getItem("LoggeduserTenantID"),
            "userId" : localStorage.getItem("IDTenant")
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                //component.set("v.Brandingpopupwithtransactions", true);
                component.set("v.Brandingpopupwithtransactions", false); 
                component.set("v.Brandingsteponedetails", true); 
                component.set("v.transactioncount",results.TransactionsCount);
                component.set("v.quotecount",results.QuotesCount);  
                component.set("v.notificationcount",results.NotificationCount);  
                component.set("v.activitylogcount",results.ActivitylogsCount);
                component.set("v.documentcount",results.documentcount);
                
            }
        });
        $A.enqueueAction(Action);
    },
    cancelstepone: function(component, event, helper){
        debugger;
        component.set("v.Brandingsteponedetails", false); 
    },
    deletealltransactions : function(component, event, helper){
        debugger;
        component.set("v.Brandingpopupwithtransactions", false);
        component.set("v.Brandingsteponedetails", false); 
        component.set("v.Deleteconfirmation", true);        
    },
    canceldelete: function(component, event, helper){
        debugger;
        component.set("v.Deleteconfirmation", false);        
    },
    deleteconfirm : function(component, event, helper){
        debugger;
        var Action = component.get("c.deletetransactions");
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                //if(results == 'Success'){
                component.set("v.Deleteconfirmation", false); 
                component.set("v.Brandcreation", true);                     
                //}                                            
            }
        });
        $A.enqueueAction(Action);
    },
    cancelbrand: function(component, event, helper){
        debugger;
        component.set("v.Brandcreation", false);
    },
    createbrand: function(component, event, helper){
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/brandcreation';
    },
    steptwocustomize : function(component, event, helper){
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/brandcreation';
    },
    stepthreeupdate: function(component, event, helper){
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/brandreview';
    }
})
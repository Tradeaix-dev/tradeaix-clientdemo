({
    initialize: function(component, event, helper) {
        debugger;
        $A.get("e.siteforce:registerQueryEventMap").setParams({"qsToEvent" : helper.qsToEventMap}).fire();    
        component.set('v.isUsernamePasswordEnabled', helper.getIsUsernamePasswordEnabled(component, event, helper));
        component.set("v.isSelfRegistrationEnabled", helper.getIsSelfRegistrationEnabled(component, event, helper));
        component.set("v.communityForgotPasswordUrl", helper.getCommunityForgotPasswordUrl(component, event, helper));
        component.set("v.communitySelfRegisterUrl", helper.getCommunitySelfRegisterUrl(component, event, helper));
    },
    ForgotPwd: function(component, event, helper) {
        debugger;
        
        
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };
        component.set("v.setTenant",getUrlParameter('tenant')); 
        
        var Action = component.get("c.getTenant"); 
        Action.setParams({
            "TenantId" : component.get("v.setTenant")               
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.LogoURL",results.Userlists.Tenant_Logo_Url__c); 
                var site = $A.get("{!$Label.c.Org_URL}");
                component.set("v.loginurl",'/'+site+'/s/'+component.get("v.setTenant"));
            }
        });
        $A.enqueueAction(Action); 
        
    },
    cancel:function(component, event, helper) {
        debugger;
            var site = $A.get("{!$Label.c.Org_URL}");
            window.location ='/'+site+'/s/'+component.get("v.setTenant");
        
    },
    
    handleForgotPassword : function(component, event, helper) {
        debugger;
        var username = component.find("username").get("v.value");
        var checkEmailUrl = "https://stratizant12345--tradeaix99.cs95.my.salesforce.com";
        if(username == undefined || username == ""){
            component.set("v.errorMessage",'Enter Username');
            component.set("v.showError",true);
            component.set("v.alertmessage",false);
        }else{
            var action = component.get("c.forgotPassword");
            action.setParams({username:username, checkEmailUrl:checkEmailUrl, tenant:component.get("v.setTenant")});
            action.setCallback(this, function(a) {
                var rtnValue = a.getReturnValue();
                if (rtnValue == 'Success'){
                    component.set("v.showForgot",false);
                    component.set("v.showError",false);
                }
                else if (rtnValue == 'Invalid User') {
                    component.set("v.errorMessage",'Invalid Username');
                    component.set("v.showError",true);
                    component.set("v.alertmessage",false);
                }
                    else if (rtnValue == 'Please contact your Administrator') {
                        component.set("v.showForgot",false);
                        component.set("v.errorMessage",rtnValue);
                        component.set("v.passwordattempt",true);
                        component.set("v.alertmessage",false);
                    }else{
                        if(rtnValue == 'Invalid Email Address'){
                            component.set("v.errorMessage",'Invalid User Name');
                        }
                        
                        component.set("v.showError",true);
                        component.set("v.alertmessage",false);
                    }
                
            });
            $A.enqueueAction(action);
        }
        //helpler.handleForgotPassword(component, event, helpler);
    },
    
    resendEmail : function(component, event, helper) {
        debugger;
       component.set("v.showForgot",true);        
	},
     setStartUrl: function (component, event, helpler) {
        var startUrl = event.getParam('startURL');
        if(startUrl) {
            component.set("v.startUrl", startUrl);
        }
    },
    
})
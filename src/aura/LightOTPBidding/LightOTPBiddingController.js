({
    initialize: function(component, event, helper) {
        debugger;
        $A.get("e.siteforce:registerQueryEventMap").setParams({"qsToEvent" : helper.qsToEventMap}).fire();    
        component.set('v.isUsernamePasswordEnabled', helper.getIsUsernamePasswordEnabled(component, event, helper));
        component.set("v.isSelfRegistrationEnabled", helper.getIsSelfRegistrationEnabled(component, event, helper));
        component.set("v.communityForgotPasswordUrl", helper.getCommunityForgotPasswordUrl(component, event, helper));
        component.set("v.communitySelfRegisterUrl", helper.getCommunitySelfRegisterUrl(component, event, helper));
    },
    afterScriptLoaded: function(component, event, helper) {
        debugger;
        try{ 
            var getUrlParameter = function getUrlParameter(sParam) {  
                try{
                    var searchString = document.URL.split('?')[1];
                    var sPageURL = decodeURIComponent(searchString),
                        sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                        sParameterName,
                        i;
                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');                 
                        if (sParameterName[0] === sParam)  {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                } catch(ex){}
            };
            component.set("v.Invitedby",getUrlParameter('i'));
            component.set("v.Flag",getUrlParameter('F'));
            if(getUrlParameter('flag') == 'TA'){
                component.set("v.taskAssignee",true);
            }
            if(component.get("v.Flag")!=undefined){
                component.set("v.OTPPopup", true);
                var a1 = component.get('c.resend');
                $A.enqueueAction(a1); 
            }else{
                
            }
            localStorage.setItem("Flag",getUrlParameter('F'));
            //alert(component.get("v.Flag"));
            var BMCP = $A.get("{!$Label.c.BMCP_ProfileID}");
            component.set("v.id",getUrlParameter('id'));
            component.set("v.LBid",getUrlParameter('cp'));
            sforce.connection.serverUrl =$A.get("{!$Label.c.LoginURL}").replace('/"/g', '\'');
            var result = sforce.connection.login($A.get("{!$Label.c.LoginUNAME}").replace('/"/g', '\''),$A.get("{!$Label.c.LoginPWD}").replace('/"/g', '\''));
            sforce.connection.sessionId = result.sessionId; 
            var LB =  sforce.connection.query("SELECT id,agree__c,Do_Not_Agree__c,CreatedBy__c,Not_the_Apropriate_User__c,Tenant_User__c,Transaction__c,Transaction_Reference__c FROM Limited_Bidding__c WHERE id='"+ getUrlParameter('cp') +"'");
            var User =  sforce.connection.query("SELECT id,CreatedFromrapid__c,Profiles__c,First_Name__c,User_Email__c,Last_Name__c,Street__c,Phone__c,Tenant__r.Tenant_Swift_Code__c,Tenant__r.Tenant_Site_Name__c,City__c,State_Province__c,Zip_Postal_Code__c,Country__c,Mobile__c,Title__c,User_Title__c,Department__c,Country_Code__c, SiteName__c, Country_List__c FROM User_Management__c WHERE id='"+ component.get("v.id") +"'");
            if(LB.size>0){
                component.set("v.transrefnumber",LB.records.Transaction_Reference__c);
            }
            if(User.size>0){
                component.set("v.emailid",User.records.User_Email__c);
            }
            if(User.records.Profiles__c == BMCP && User.records.CreatedFromrapid__c=="true")
            {
                 
                component.set("v.Ruser",true);
                
            }else{
                component.set("v.Ruser",false);
                if(LB.records.agree__c=="true"){
                    /******browser check-START******/
                    var browserType = navigator.sayswho= (function(){
                        var ua= navigator.userAgent, tem,
                            M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
                        if(/trident/i.test(M[1])){
                            tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
                            return 'IE '+(tem[1] || '');
                        }
                        if(M[1]=== 'Chrome'){
                            tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
                            if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
                        }
                        M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
                        if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
                        return M.join(' ');
                    })();
                    var action = component.get("c.login"); 
                    localStorage.setItem("UID",component.get("v.id"));
                    localStorage.setItem("LBID",component.get("v.LBid"));
                    //alert(LB.records.Limited_Bidding__r.Transaction__c);
                    action.setParams({Username:'' ,
                                      ID:component.get("v.id"),
                                      LBIds:component.get("v.LBid"), 
                                      browsername:browserType,ipAddress:'',
                                      LoginURL:document.URL,RetURL:' ',
                                      TId:LB.records.Transaction__c});
                    
                    action.setCallback(this, function(a){
                        var rtnValue = a.getReturnValue();
                        if (rtnValue =='Failed') {
                            alert('Error');
                            component.set("v.errorMessage",'Your login attempt has failed. Make sure the username and password are correct.');
                            component.set("v.showError",true);
                        }
                        else
                        {
                            //window.location='/demo/s/transactionviewdetails?transactionId=a0P6C000000BpvAUAS';
                            
                            localStorage.setItem("UserSession", rtnValue);
                            var Action = component.get("c.getUsername");
                            Action.setParams({
                                "username" : localStorage.getItem("UserSession")                
                            });
                            Action.setCallback(this, function(actionResult) {
                                debugger;
                                var state = actionResult.getState(); 
                                var results = actionResult.getReturnValue();
                                if(state === "SUCCESS"){
                                    localStorage.setItem("LoggeduserProfile", results[0].Profiles__c);
                                    localStorage.setItem("IDTenant", results[0].Id);
                                    localStorage.setItem("LoggeduserTenantID", results[0].Tenant__c);
                                    localStorage.setItem("LoggeduserOrgID", results[0].Tenant__r.Organization__c);
                                    localStorage.setItem("LoggeduserName", results[0].First_Name__c+' '+results[0].Last_Name__c);
                                    localStorage.setItem("TSiteName", results[0].Tenant__r.Tenant_Short_Name__c);
                                    localStorage.setItem("FromRaidUserPWD", results[0].Password__c);
                                    localStorage.setItem("FromRaidCHK", results[0].CreatedFromrapid__c);
                                    localStorage.setItem("LoggeduserTenantType", results[0].Tenant__r.Tenant_Type__c);
                                    localStorage.setItem("BPuser",results[0].Become_a_Premium_user__c);
                                    var OBCP = $A.get("{!$Label.c.OBCP_ProfileID}");
                                    if(localStorage.getItem("LoggeduserProfile")==OBCP)
                                    {
                                        component.set("v.OTPUser",true);
                                    }else{
                                        component.set("v.OTPUser",false); 
                                    }
                                    
                                }
                            });
                            $A.enqueueAction(Action);  
                        }
                    });
                    $A.enqueueAction(action);
                    //window.location='/dev/s/transactionviewdetails?transactionId='+LB.records.Transaction__c;
                    component.set("v.transrefnumber",LB.records.Transaction_Reference__c);
                    component.set("v.emailid",User.records.User_Email__c);
                }
                else{
                    component.set("v.Eula", false);
                    component.set("v.Contactinfo", true);
                    component.set("v.rejectcon", true);
                    component.set("v.colorde",'completed');
                    
                    component.set("v.UM",User.records);
                    component.set("v.CreatedBYID",User.records.CreatedBy__c);
                    component.set("v.emailid",User.records.User_Email__c);
                    if(LB.size>0){
                        component.set("v.transrefnumber",LB.records.Transaction_Reference__c);
                        if(component.get("v.taskAssignee") == true){
                            document.title =LB.records.Transaction_Reference__c+' - TASK ASSIGNEE';
                        }else{
                            document.title =LB.records.Transaction_Reference__c+' - CP ON BOARDING WIZARD';
                        }
                        component.set("v.agree", LB.records.agree__c);
                        component.set("v.donotagree", LB.records.Do_Not_Agree__c);
                        component.set("v.notappuser", LB.records.Not_the_Apropriate_User__c);
                    }
                    else{
                        component.set("v.agree", "false");
                        component.set("v.donotagree", "false");
                        component.set("v.notappuser", "false");
                    }
                }
                debugger;
            }
        }catch(ex){
            debugger;
        }
    },
    ctologin : function(component, event, helper) {
        window.location='/demo/s/admin';
    },
    reject: function(component, event, helper) {
        debugger; 
        component.set("v.rejectpopup", true);
    },
    rejectcancel: function(component, event, helper) {
        debugger; 
        component.set("v.rejectpopup", false);
    },
    rejectsuccess: function(component, event, helper) {
        debugger; 
        
        var savNote = component.find("bodyValidation").get("v.value");
        if(savNote == '' || 
           savNote == null || 
           savNote == undefined || 
           savNote.trim().length*1 == 0) {            
            component.set("v.saverejectnotes", 'Input field required'); 
            component.find("bodyValidation").set("v.value", '')
        }
        else{
            var ids=component.get("v.LBid");
            var updateCU = new sforce.SObject('Limited_Bidding__c');
            updateCU.id=ids;
            updateCU.Not_the_Apropriate_User__c=true;
            updateCU.Reject_Reason__c=savNote;
            sforce.connection.serverUrl =$A.get("{!$Label.c.LoginURL}");
            var result = sforce.connection.login($A.get("{!$Label.c.LoginUNAME}"),$A.get("{!$Label.c.LoginPWD}"));
            sforce.connection.sessionId = result.sessionId; 
            sforce.connection.update([updateCU], 
                                     {
                                         onSuccess : function(result)
                                         {
                                             var showToast = $A.get('e.force:showToast');
                                             showToast.setParams(
                                                 {
                                                     'title': 'Reject'	,
                                                     'message': 'Successfully Declined.',
                                                     'type': 'Success'
                                                 }
                                             );
                                             showToast.fire();
                                             $A.get('e.force:refreshView').fire();
                                             component.set("v.Eula", fase);
                                             component.set("v.rejectpopup", false);
                                             component.set("v.Contactinfo", true);                                                 
                                             debugger;
                                             sforce.connection.serverUrl =$A.get("{!$Label.c.LoginURL}");
                                             var result = sforce.connection.login($A.get("{!$Label.c.LoginUNAME}"),$A.get("{!$Label.c.LoginPWD}"));sforce.connection.sessionId = result.sessionId; 
                                             var User =  sforce.connection.query("SELECT id,First_Name__c,User_Email__c,Last_Name__c,Street__c,Phone__c,Tenant__r.Tenant_Swift_Code__c,Tenant__r.Tenant_Site_Name__c,City__c,State_Province__c,Zip_Postal_Code__c,Country__c,Country_List__c FROM User_Management__c WHERE id='"+ component.get("v.id") +"'");
                                             component.set("v.UM",User.records.CreatedBy__c);
                                             var AL = new sforce.SObject('Activity_Log__c');        
                                             AL.Action_By__c = component.get("v.id");
                                             AL.Action__c = 'EULA Agreement';
                                             AL.Action_To__c=component.get("v.CreatedBYID");
                                             AL.RecordID__c ='';
                                             AL.Message__c = 'Agree the EULA Agreement';
                                             AL.ReadFlag__c = true;
                                             
                                             sforce.connection.insert([AL], 
                                                                      {
                                                                          onSuccess : function(result)
                                                                          {
                                                                              //alert('Your OTP has been expired. Please click "Resend One-Time Password".');
                                                                              debugger;
                                                                              if (result[0].getBoolean("success")) 
                                                                              {
                                                                              }
                                                                          },onFailure : function(error)
                                                                          {
                                                                              debugger;
                                                                              //alert('error occured.');
                                                                          }
                                                                          
                                                                      });
                                             
                                             if (result[0].getBoolean("success")) 
                                             {
                                             }
                                         },onFailure : function(error)
                                         {
                                             debugger;
                                             //alert('error occured.');
                                         }
                                         
                                     });
        }     
        
        
    },
    onCheckagree : function(component, event, helper) {
        debugger; 
        if(component.find("chkagree").get('v.value')==true)
        {
            component.find("chknotagree").set("v.value", false);
            component.find("chknotusr").set("v.value", false);
            
        }
    },
    onCheckdonotagree : function(component, event, helper) {
        debugger;
        if(component.find("chknotagree").get('v.value')==true)
        {
            component.find("chkagree").set("v.value", false);
            component.find("chknotusr").set("v.value", false);
            
        }
    },
    onChecknotuser : function(component, event, helper) {
        debugger;
        if(component.find("chknotusr").get('v.value')==true)
        {
            component.find("chkagree").set("v.value", false);
            component.find("chknotagree").set("v.value", false);
            
        }
    },
    submit : function(component, event, helper) {
        debugger;
        component.set("v.colorde",'active');
        component.set("v.coloreula",'active');
        component.set("v.colorotp",'completed');
        
        sforce.connection.serverUrl =$A.get("{!$Label.c.LoginURL}");
        var result = sforce.connection.login($A.get("{!$Label.c.LoginUNAME}"),$A.get("{!$Label.c.LoginPWD}")); sforce.connection.sessionId = result.sessionId; 
<<<<<<< HEAD
        var LB1 =  sforce.connection.query("SELECT id,agree__c,Do_Not_Agree__c,Not_the_Apropriate_User__c,Tenant_User__c,Transaction__c,Tenant__c FROM Limited_Bidding__c WHERE Tenant_User__c='"+ component.get("v.id") +"'");
        var tenantId = LB1.records.Tenant__c;
        var updateLB = new sforce.SObject('Limited_Bidding__c');
        updateLB.Id=LB1.records.Id;
        component.set("v.LBid",LB1.records.Id);
        updateLB.agree__c=true;
        sforce.connection.update([updateLB], 
                                 {
                                     onSuccess : function(result)
                                     {
                                         
                                         
                                         component.set("v.colorde",'completed');
                                         component.set("v.coloreula",'completed');
                                         component.set("v.colorotp",'active');
                                         component.set("v.OTPPopup",true);
                                         
                                         component.set("v.endTime",new Date().getTime()+5*60000);
                                         var timer = setInterval(function() { 
                                             var now = new Date().getTime();
                                             var fiveminus = new Date(component.get("v.endTime"));
                                             var distance = fiveminus - now;
                                             var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                             var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                                             var result =  minutes + "m " + seconds + "s ";
                                             component.set("v.expOTPtxt",false);
                                             component.set("v.OTPtxt",true);
                                             if(result=='0m 0s '){ 
                                                 component.set("v.chkerrorotpexpire",'0');
                                                 clearInterval(timer);
                                                 
                                                 component.set("v.expOTPtxt",true);
                                                 component.set("v.OTPtxt",false);
                                                 sforce.connection.serverUrl =$A.get("{!$Label.c.LoginURL}");
                                                 var result = sforce.connection.login($A.get("{!$Label.c.LoginUNAME}"),$A.get("{!$Label.c.LoginPWD}"));  sforce.connection.sessionId = result.sessionId; 
                                                 var LB2 =  sforce.connection.query("SELECT id FROM OTP_History__c WHERE Limited_Bidding__c='"+ component.get("v.LBid") +"' Order by createdDate desc LIMIT 1");
                                                 var updateCU = new sforce.SObject('OTP_History__c');
                                                 updateCU.Id=LB2.records.Id;
                                                 updateCU.isActive__c=false;
                                                 sforce.connection.update([updateCU], 
                                                                          {
                                                                              onSuccess : function(result)
                                                                              {
                                                                                  //alert('Your OTP has been expired. Please click "Resend One-Time Password".');
                                                                                  debugger;
                                                                                  if (result[0].getBoolean("success")) 
                                                                                  {
                                                                                  }
                                                                              },onFailure : function(error)
                                                                              {
                                                                                  debugger;
                                                                                  //alert('error occured.');
                                                                              }
                                                                              
                                                                          });
                                             }else{
                                                 component.set("v.chkerrorotpexpire",result);
                                             }
                                             
                                         }, 500);
                                         debugger;
                                         if (result[0].getBoolean("success")) 
                                         {
                                         }
                                     },onFailure : function(error)
                                     {
                                         debugger;
                                         //alert('error occured.');
                                     }
                                     
                                 });
        
        //Update Entity active is true
        sforce.connection.serverUrl =$A.get("{!$Label.c.LoginURL}");
        var result = sforce.connection.login($A.get("{!$Label.c.LoginUNAME}"),$A.get("{!$Label.c.LoginPWD}")); sforce.connection.sessionId = result.sessionId; 
        var TU1 =  sforce.connection.query("SELECT id,IsActive__c FROM Tenant__c WHERE Id='"+ tenantId +"'");
        var updateTB = new sforce.SObject('Tenant__c');
        updateTB.Id=TU1.records.Id;
        updateTB.IsActive__c=true;
        sforce.connection.update([updateTB], 
                                 {
                                     onSuccess : function(result)
                                     {
                                         //alert('Your OTP has been expired. Please click "Resend One-Time Password".');
                                         debugger;
                                         if (result[0].getBoolean("success")) 
                                         {
                                         }
                                     },onFailure : function(error)
                                     {
                                         debugger;
                                         //alert('error occured.');
                                     }
                                     
                                 });
        
        // Update activity logs                         
=======
        var LB1 =  sforce.connection.query("SELECT id,agree__c,Do_Not_Agree__c,Not_the_Apropriate_User__c,Tenant_User__c,Transaction__c FROM Limited_Bidding__c WHERE Tenant_User__c='"+ component.get("v.id") +"'");
        var updateLB = new sforce.SObject('Limited_Bidding__c');
        updateLB.Id=LB1.records.Id;
        component.set("v.LBid",LB1.records.Id);
        updateLB.agree__c=true;
        sforce.connection.update([updateLB], 
                                 {
                                     onSuccess : function(result)
                                     {
                                         
                                         
                                         component.set("v.colorde",'completed');
                                         component.set("v.coloreula",'completed');
                                         component.set("v.colorotp",'active');
                                         component.set("v.OTPPopup",true);
                                         
                                         component.set("v.endTime",new Date().getTime()+5*60000);
                                         var timer = setInterval(function() { 
                                             var now = new Date().getTime();
                                             var fiveminus = new Date(component.get("v.endTime"));
                                             var distance = fiveminus - now;
                                             var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                             var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                                             var result =  minutes + "m " + seconds + "s ";
                                             component.set("v.expOTPtxt",false);
                                             component.set("v.OTPtxt",true);
                                             if(result=='0m 0s '){ 
                                                 component.set("v.chkerrorotpexpire",'0');
                                                 clearInterval(timer);
                                                 
                                                 component.set("v.expOTPtxt",true);
                                                 component.set("v.OTPtxt",false);
                                                 sforce.connection.serverUrl =$A.get("{!$Label.c.LoginURL}");
                                                 var result = sforce.connection.login($A.get("{!$Label.c.LoginUNAME}"),$A.get("{!$Label.c.LoginPWD}"));  sforce.connection.sessionId = result.sessionId; 
                                                 var LB2 =  sforce.connection.query("SELECT id FROM OTP_History__c WHERE Limited_Bidding__c='"+ component.get("v.LBid") +"' Order by createdDate desc LIMIT 1");
                                                 var updateCU = new sforce.SObject('OTP_History__c');
                                                 updateCU.Id=LB2.records.Id;
                                                 updateCU.isActive__c=false;
                                                 sforce.connection.update([updateCU], 
                                                                          {
                                                                              onSuccess : function(result)
                                                                              {
                                                                                  //alert('Your OTP has been expired. Please click "Resend One-Time Password".');
                                                                                  debugger;
                                                                                  if (result[0].getBoolean("success")) 
                                                                                  {
                                                                                  }
                                                                              },onFailure : function(error)
                                                                              {
                                                                                  debugger;
                                                                                  //alert('error occured.');
                                                                              }
                                                                              
                                                                          });
                                             }else{
                                                 component.set("v.chkerrorotpexpire",result);
                                             }
                                             
                                         }, 500);
                                         debugger;
                                         if (result[0].getBoolean("success")) 
                                         {
                                         }
                                     },onFailure : function(error)
                                     {
                                         debugger;
                                         //alert('error occured.');
                                     }
                                     
                                 });
        
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-clientdemo.git
        sforce.connection.serverUrl =$A.get("{!$Label.c.LoginURL}");
        var result = sforce.connection.login($A.get("{!$Label.c.LoginUNAME}"),$A.get("{!$Label.c.LoginPWD}")); sforce.connection.sessionId = result.sessionId; 
        var AL = new sforce.SObject('Activity_Log__c');        
        AL.Action_By__c = component.get("v.id");
        AL.Operation__c = 'EULA Agreement';
        AL.RecordID__c ='';
        AL.Operation_Log__c = 'Agree the EULA Agreement';
        AL.ErrorCall__c = false;
        
        sforce.connection.insert([AL], 
                                 {
                                     onSuccess : function(result)
                                     {
                                         //alert('Your OTP has been expired. Please click "Resend One-Time Password".');
                                         debugger;
                                         if (result[0].getBoolean("success")) 
                                         {
                                         }
                                     },onFailure : function(error)
                                     {
                                         debugger;
                                         //alert('error occured.');
                                     }
                                     
                                 });
        
        
        
    },
    valdate : function(component, event, helper) {
        debugger;
        if(component.find("txt1").get('v.value') !='' && component.find("txt2").get('v.value') !='' && component.find("txt3").get('v.value') !='' && component.find("txt4").get('v.value') !='' && component.find("txt5").get('v.value') !='' && component.find("txt6").get('v.value') !='')
        {
            var regPhoneNo = /^[0-9]*$/;
            if(component.find("txt1").get('v.value').match(regPhoneNo) && component.find("txt2").get('v.value').match(regPhoneNo) && component.find("txt3").get('v.value').match(regPhoneNo) && component.find("txt4").get('v.value').match(regPhoneNo) && component.find("txt5").get('v.value').match(regPhoneNo) && component.find("txt6").get('v.value').match(regPhoneNo))
            {
                sforce.connection.serverUrl =$A.get("{!$Label.c.LoginURL}");
                var result = sforce.connection.login($A.get("{!$Label.c.LoginUNAME}"),$A.get("{!$Label.c.LoginPWD}")); sforce.connection.sessionId = result.sessionId; 
                var LB =  sforce.connection.query("SELECT id,OTP__c,isActive__c,Limited_Bidding__r.Transaction__c FROM OTP_History__c WHERE Limited_Bidding__c='"+ component.get("v.LBid") +"' AND isActive__c=true Order by createdDate desc LIMIT 1");
                var BidderOTP = component.find("txt1").get('v.value') + component.find("txt2").get('v.value') +component.find("txt3").get('v.value')+component.find("txt4").get('v.value')+component.find("txt5").get('v.value') + component.find("txt6").get('v.value');
                if(LB.size>0)
                {
                    if(BidderOTP == LB.records.OTP__c)
                    {
                        /******browser check-START******/
                        var browserType = navigator.sayswho= (function(){
                            var ua= navigator.userAgent, tem,
                                M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
                            if(/trident/i.test(M[1])){
                                tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
                                return 'IE '+(tem[1] || '');
                            }
                            if(M[1]=== 'Chrome'){
                                tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
                                if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
                            }
                            M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
                            if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
                            return M.join(' ');
                        })();
                        sforce.connection.serverUrl =$A.get("{!$Label.c.LoginURL}");
                        var result = sforce.connection.login($A.get("{!$Label.c.LoginUNAME}"),$A.get("{!$Label.c.LoginPWD}")); sforce.connection.sessionId = result.sessionId; 
                        var LB2 =  sforce.connection.query("SELECT id FROM OTP_History__c WHERE Limited_Bidding__c='"+ component.get("v.LBid") +"' Order by createdDate desc LIMIT 1");
                        var updateCU = new sforce.SObject('OTP_History__c');
                        updateCU.Id=LB2.records.Id;
                        updateCU.OTP_Validated_On__c=$A.localizationService.formatDate(new Date(), "yyyy-MM-ddTHH:mm:ss");
                        sforce.connection.update([updateCU], 
                                                 {
                                                     onSuccess : function(result)
                                                     {
                                                         //alert('Your OTP has been expired. Please click "Resend One-Time Password".');
                                                         debugger;
                                                         if (result[0].getBoolean("success")) 
                                                         {
                                                         }
                                                     },onFailure : function(error)
                                                     {
                                                         debugger;
                                                         //alert('error occured.');
                                                     }
                                                     
                                                 });
                        
                        var action = component.get("c.login"); 
                        localStorage.setItem("UID",component.get("v.id"));
                        localStorage.setItem("LBID",component.get("v.LBid"));
                        //alert(LB.records.Limited_Bidding__r.Transaction__c);
                        action.setParams({Username:component.get("v.UM.First_Name__c")+' '+component.get("v.UM.Last_Name__c") ,ID:component.get("v.id"),LBIds:component.get("v.LBid"), browsername:browserType,ipAddress:'',LoginURL:document.URL,RetURL:' ',TId:LB.records.Limited_Bidding__r.Transaction__c});
                        
                        action.setCallback(this, function(a){
                            var rtnValue = a.getReturnValue();
                            if (rtnValue =='Failed') {
                                component.set("v.errorMessage",'Your login attempt has failed. Make sure the username and password are correct.');
                                component.set("v.showError",true);
                            }
                            else
                            {
                                //window.location='/demo/s/transactionviewdetails?transactionId=a0P6C000000BpvAUAS';
                                //alert(rtnValue);
                                localStorage.setItem("UserSession", rtnValue);
                            }
                        });
                        $A.enqueueAction(action);
                        
                        
                    }else{
                        component.set("v.chkotperror","Error: Please Entire Correct OTP.");
                        //alert('Your OTP has been expired. Please click "Resend One-Time Password".');
                    }
                }else{
                    //alert('Your OTP has been expired. Please click "Resend One-Time Password".');
                }
                
            }else{
                component.set("v.chkotperror","Error: 6 digit OTP should be in only digits.");
                
            }
        }
        else{
            component.set("v.chkotperror","Error: Please Enter the 6 digit OTP.");
        }
        
    },
    resend: function(component, event, helper) {
        debugger;
        //alert('resent');
        sforce.connection.serverUrl =$A.get("{!$Label.c.LoginURL}");
        var result = sforce.connection.login($A.get("{!$Label.c.LoginUNAME}"),$A.get("{!$Label.c.LoginPWD}")); sforce.connection.sessionId = result.sessionId; 
        var LB2 =  sforce.connection.query("SELECT id FROM OTP_History__c WHERE Limited_Bidding__c ='"+ component.get("v.LBid") +"' Order by createdDate desc LIMIT 1");
        var updateCU = new sforce.SObject('OTP_History__c');
        updateCU.Id=LB2.records.Id;
        updateCU.isActive__c=false;
        sforce.connection.update([updateCU], 
                                 {
                                     onSuccess : function(result)
                                     {
                                         
                                         sforce.connection.serverUrl =$A.get("{!$Label.c.LoginURL}");
                                         var result = sforce.connection.login($A.get("{!$Label.c.LoginUNAME}"),$A.get("{!$Label.c.LoginPWD}"));  sforce.connection.sessionId = result.sessionId; 
                                         //var LB2 =  sforce.connection.query("SELECT id FROM OTP_History__c WHERE Limited_Bidding__c='"+ component.get("v.LBid") +"' Order by createdDate desc LIMIT 1");
                                         var updateCU = new sforce.SObject('OTP_History__c');
                                         updateCU.Limited_Bidding__c= component.get("v.LBid");
                                         updateCU.isActive__c=true;
                                         var random = Math.round(Math.random() * 1000000);
                                         var Finalrandom ='';
                                         if(random.toString().length == 4)
                                         {
                                             Finalrandom = '91'+random;
                                         }else if(random.toString().length == 5)
                                         { 
                                             Finalrandom = '9'+random;
                                         }else{
                                             Finalrandom = random;
                                         }
                                         updateCU.OTP__c =Finalrandom;
                                         sforce.connection.create([updateCU], 
                                                                  {
                                                                      onSuccess : function(result)
                                                                      {
                                                                          component.set("v.endTime",new Date().getTime()+5*60000);
                                                                          var timer = setInterval(function() { 
                                                                              var now = new Date().getTime();
                                                                              var fiveminus = new Date(component.get("v.endTime"));
                                                                              var distance = fiveminus - now;
                                                                              var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                                                              var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                                                                              var result =  minutes + "m " + seconds + "s ";
                                                                              component.set("v.expOTPtxt",false);
                                                                              component.set("v.OTPtxt",true);
                                                                              if(result=='0m 0s '){                                                                                   component.set("v.chkerrorotpexpire",'0');
                                                                                                   clearInterval(timer);
                                                                                                   
                                                                                                   component.set("v.expOTPtxt",true);
                                                                                                   component.set("v.OTPtxt",false);
                                                                                                   sforce.connection.serverUrl =$A.get("{!$Label.c.LoginURL}");
                                                                                                   var result = sforce.connection.login($A.get("{!$Label.c.LoginUNAME}"),$A.get("{!$Label.c.LoginPWD}"));
                                                                                                   sforce.connection.sessionId = result.sessionId; 
                                                                                                   var LB2 =  sforce.connection.query("SELECT id FROM OTP_History__c WHERE Limited_Bidding__c='"+ component.get("v.LBid") +"' Order by createdDate desc LIMIT 1");
                                                                                                   var updateCU = new sforce.SObject('OTP_History__c');
                                                                                                   updateCU.Id=LB2.records.Id;
                                                                                                   updateCU.isActive__c=false;
                                                                                                   sforce.connection.update([updateCU], 
                                                                                                                            {
                                                                                                                                onSuccess : function(result)
                                                                                                                                {
                                                                                                                                    //alert('Your OTP has been expired. Please click "Resend One-Time Password".');
                                                                                                                                    debugger;
                                                                                                                                    if (result[0].getBoolean("success")) 
                                                                                                                                    {
                                                                                                                                    }
                                                                                                                                },onFailure : function(error)
                                                                                                                                {
                                                                                                                                    debugger;
                                                                                                                                    //alert('error occured.');
                                                                                                                                }
                                                                                                                                
                                                                                                                            });
                                                                                                  }else{
                                                                                                      component.set("v.chkerrorotpexpire",result);
                                                                                                  }
                                                                              
                                                                          }, 500);
                                                                          //alert('Your OTP has been expired. Please click "Resend One-Time Password".');
                                                                          debugger;
                                                                          if (result[0].getBoolean("success")) 
                                                                          {
                                                                          }
                                                                      },onFailure : function(error)
                                                                      {
                                                                          debugger;
                                                                          //alert('error occured.');
                                                                      }
                                                                      
                                                                  });
                                         //alert('Your OTP has been expired. Please click "Resend One-Time Password".');
                                         debugger;
                                         if (result[0].getBoolean("success")) 
                                         {
                                         }
                                     },onFailure : function(error)
                                     {
                                         debugger;
                                         //alert('error occured.');
                                     }
                                     
                                 });
    },
    ontxt1 : function(component, event, helper) {
        component.find('txt2').focus();
    },
    ontxt2 : function(component, event, helper) {
        component.find('txt3').focus();
    },
    ontxt3 : function(component, event, helper) {
        component.find('txt4').focus();
    },
    ontxt4 : function(component, event, helper) {
        component.find('txt5').focus();
    },
    ontxt5 : function(component, event, helper) {
        component.find('txt6').focus();
    },
    updateinfo : function(component, event, helper) {
        debugger;
        component.set("v.colorde",'active');
        component.set("v.coloreula",'completed');
        
        component.set("v.rejectcon", false);
        var errorFlag = 'false';
        component.set("v.validation", false); 
        
        //var FirstName = component.find("FirstName").get("v.value");
        //var sitename = component.find("sitename").get("v.value");
        
        //var lastname = component.find("lastname").get("v.value");
        //var swiftcode = component.find("swiftcode").get("v.value");
        // var location = component.find("location").get("v.value");
        var phone = component.find("phone").get("v.value");
        var Mobile = component.find("Mobile").get("v.value");  
        var countrycode = component.find("auracountrycode").get("v.value");
        var mobilecountrycode = component.find("auramobilecountrycode").get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        
        
        /*if(FirstName == '' || FirstName == 'undefined' || FirstName == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.FirstNameError", 'Input field required'); 
        } else { 
            component.set("v.FirstNameError", ''); 
        } 
        
        if(sitename == '' || sitename == 'undefined' || sitename == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LegalNameError", 'Input field required'); 
        } else { 
            component.set("v.LegalNameError", ''); 
        } 
        
        if(lastname == '' || lastname == 'undefined' || lastname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LastNameError", 'Input field required'); 
        } else { 
            component.set("v.LastNameError", ''); 
        } 
        if(swiftcode == '' || swiftcode == 'undefined' || swiftcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.SwiftCodeError", 'Input field required'); 
        } else { 
            component.set("v.SwiftCodeError", ''); 
        } */
        /* if(location == '' || location == 'undefined' || location == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LocationError", 'Input field required'); 
        } else { 
            component.set("v.LocationError", ''); 
        } */
        
        if(!$A.util.isEmpty(phone))
        {   
            if(countrycode == '' || countrycode =='--Select--' || countrycode == 'undefined' || countrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.countrycodeError", 'Input field required'); 
            } else { 
                component.set("v.countrycodeError", ''); 
            } 
            
            if(!phone.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please enter valid phone number'); 
            } 
            else if(phone.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.PhoneError", ''); 
                }
        }
        if(!$A.util.isEmpty(Mobile))
        {   
            if(mobilecountrycode == '' || mobilecountrycode =='--Select--' || mobilecountrycode == 'undefined' || mobilecountrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.mobilecountrycodeError", 'Input field required'); 
            } else { 
                component.set("v.mobilecountrycodeError", ''); 
            }
            
            if(!Mobile.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please enter valid Mobile number'); 
            } 
            else if(Mobile.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.MobileError", ''); 
                }
        }
        
        
        if(errorFlag == 'false') {
            
            var updateCU = new sforce.SObject('User_Management__c');
            updateCU.id=component.get("v.id");
            updateCU.First_Name__c =component.get("v.UM.First_Name__c");
            updateCU.Last_Name__c =component.get("v.UM.Last_Name__c");
            updateCU.Street__c =component.get("v.UM.Street__c");
            updateCU.City__c =component.get("v.UM.City__c");
            updateCU.State_Province__c =component.get("v.UM.State_Province__c");
            updateCU.Zip_Postal_Code__c =component.get("v.UM.Zip_Postal_Code__c");
            updateCU.Country__c =component.get("v.UM.Country__c");
            updateCU.Phone__c =component.get("v.UM.Phone__c");
            updateCU.Mobile__c=component.get("v.UM.Mobile__c");
            updateCU.Department__c=component.get("v.UM.Department__c");
            updateCU.Country_Code__c=component.get("v.UM.Country_Code__c");
            sforce.connection.update([updateCU], 
                                     {
                                         onSuccess : function(result)
                                         {
                                             //component.set("v.OTPPopup",true);
                                             /*if(component.get("v.taskAssignee") == true){
                                                 component.set("v.Eula", false);
                                                 component.set("v.Contactinfo", false);
                                                 component.set("v.Eula", false);
                                                 component.set("v.colorde",'completed');
                                                 component.set("v.coloreula",'completed');
                                                 component.set("v.colorotp",'active');
                                                 component.set("v.OTPPopup",true);
                                             }else{ */
                                                 component.set("v.Eula", true);
                                                 component.set("v.Contactinfo", false);
                                             //}                                             
                                             debugger;
                                             if (result[0].getBoolean("success")) 
                                             {                                               
                                                 
                                             } 
                                             else 
                                             {
                                                 
                                             }
                                         }, 
                                         onFailure : function(error)
                                         {
                                             debugger;
                                             //alert('error occured.');
                                         }
                                     });            
        }
        
        
    },
    updateinfoassignee : function(component, event, helper) {
        debugger;
        component.set("v.colorde",'active');
        component.set("v.coloreula",'completed');
        
        component.set("v.rejectcon", false);
        var errorFlag = 'false';
        component.set("v.validation", false); 
       
        var phone = component.find("phone").get("v.value");
        var Mobile = component.find("Mobile").get("v.value");  
        var countrycode = component.find("auracountrycode").get("v.value");
        var mobilecountrycode = component.find("auramobilecountrycode").get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        
        if(!$A.util.isEmpty(phone))
        {   
            if(countrycode == '' || countrycode =='--Select--' || countrycode == 'undefined' || countrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.countrycodeError", 'Input field required'); 
            } else { 
                component.set("v.countrycodeError", ''); 
            } 
            
            if(!phone.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please enter valid phone number'); 
            } 
            else if(phone.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.PhoneError", ''); 
                }
        }
        if(!$A.util.isEmpty(Mobile))
        {   
            if(mobilecountrycode == '' || mobilecountrycode =='--Select--' || mobilecountrycode == 'undefined' || mobilecountrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.mobilecountrycodeError", 'Input field required'); 
            } else { 
                component.set("v.mobilecountrycodeError", ''); 
            }
            
            if(!Mobile.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please enter valid Mobile number'); 
            } 
            else if(Mobile.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.MobileError", ''); 
                }
        }
        
        
        if(errorFlag == 'false') {
            
            var updateCU = new sforce.SObject('User_Management__c');
            updateCU.id=component.get("v.id");
            updateCU.First_Name__c =component.get("v.UM.First_Name__c");
            updateCU.Last_Name__c =component.get("v.UM.Last_Name__c");
            updateCU.Street__c =component.get("v.UM.Street__c");
            updateCU.City__c =component.get("v.UM.City__c");
            updateCU.State_Province__c =component.get("v.UM.State_Province__c");
            updateCU.Zip_Postal_Code__c =component.get("v.UM.Zip_Postal_Code__c");
            updateCU.Country__c =component.get("v.UM.Country__c");
            updateCU.Phone__c =component.get("v.UM.Phone__c");
            updateCU.Mobile__c=component.get("v.UM.Mobile__c");
            updateCU.Department__c=component.get("v.UM.Department__c");
            updateCU.Country_Code__c=component.get("v.UM.Country_Code__c");
            sforce.connection.update([updateCU], 
                                     {
                                         onSuccess : function(result)
                                         {
                                             debugger;
                                             var LB1 =  sforce.connection.query("SELECT id,agree__c,Do_Not_Agree__c,Not_the_Apropriate_User__c,Tenant_User__c,Transaction__c FROM Limited_Bidding__c WHERE Tenant_User__c='"+ component.get("v.id") +"' order by createddate desc limit 1");
                                             var updateLB = new sforce.SObject('Limited_Bidding__c');
                                             updateLB.Id=LB1.records.Id;
                                             component.set("v.LBid",LB1.records.Id);
                                             updateLB.agree__c=true;
                                             sforce.connection.update([updateLB],     
                                                                      {
                                                                          onSuccess : function(result)
                                                                          {    
                                                                              component.set("v.Eula", false);
                                                                              component.set("v.Contactinfo", false);
                                                                              component.set("v.colorde",'completed');
                                                                              component.set("v.coloreula",'completed');
                                                                              component.set("v.colorotp",'active');
                                                                              component.set("v.OTPPopup",true);                                     
                                                                              
                                                                          }, 
                                                                          onFailure : function(error)
                                                                          {
                                                                              debugger;                                           
                                                                          }
                                                                      }); 
                                             //component.set("v.OTPPopup",true);
                                             /*if(component.get("v.taskAssignee") == true){
                                                 component.set("v.Eula", false);
                                                 component.set("v.Contactinfo", false);
                                                 component.set("v.Eula", false);
                                                 component.set("v.colorde",'completed');
                                                 component.set("v.coloreula",'completed');
                                                 component.set("v.colorotp",'active');
                                                 component.set("v.OTPPopup",true);
                                             }else{ */
                                                 //component.set("v.Eula", true);
                                                 component.set("v.Contactinfo", false);
                                             //}                                             
                                             debugger;
                                             if (result[0].getBoolean("success")) 
                                             {                                               
                                                 
                                             } 
                                             else 
                                             {
                                                 
                                             }
                                         }, 
                                         onFailure : function(error)
                                         {
                                             debugger;
                                             //alert('error occured.');
                                         }
                                     });            
        }
        
        
    },
    setStartUrl: function (component, event, helpler) {
        var startUrl = event.getParam('startURL');
        if(startUrl) {
            component.set("v.startUrl", startUrl);
        }
    },
    onKeyUp: function(component, event, helpler){
        //checks for "enter" key
        if (event.getParam('keyCode')===13) {
            helpler.handleLogin(component, event, helpler);
        }
    },
    backtoviewinfo : function(component, event, helpler){
        component.set("v.Eula", false);
        component.set("v.Contactinfo", true);
        component.set("v.rejectcon", true);
        component.set("v.coloreula",'');
        component.set("v.colorde",'completed');
        
        
    }, 
    navigateToForgotPassword: function(cmp, event, helper) {
        var forgotPwdUrl = cmp.get("v.communityForgotPasswordUrl");
        if ($A.util.isUndefinedOrNull(forgotPwdUrl)) {
            forgotPwdUrl = cmp.get("v.forgotPasswordUrl");
        }
        var attributes = { url: forgotPwdUrl };
        $A.get("e.force:navigateToURL").setParams(attributes).fire();
    },
    
    navigateToSelfRegister: function(cmp, event, helper) {
        var selrRegUrl = cmp.get("v.communitySelfRegisterUrl");
        if (selrRegUrl == null) {
            selrRegUrl = cmp.get("v.selfRegisterUrl");
        }
        
        var attributes = { url: selrRegUrl };
        $A.get("e.force:navigateToURL").setParams(attributes).fire();
    } 
})
({
    canceladd: function(component, event, helper) { 
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/transactionviewdetails?transactionId='+component.get("v.TIDs");
    },
    getBanks: function(component, event, helper) { 
        var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
        document.title =sitename +' : Onboard a Counterparty(CP)';
        debugger;
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        };
        
        var flagchk = getUrlParameter('flag');
        component.set("v.flagchk", flagchk); 
        var TID = getUrlParameter('transactionId');
        component.set("v.TIDs", TID); 
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();
        component.set("v.currentDate",yyyy+'-'+mm+'-'+dd);
        var ActionTitle = component.get("c.getloggeduserBrowserTitle");
        ActionTitle.setParams({
            "transid" : TID                
        });   
        ActionTitle.setCallback(this, function(actionResultTitle) {
            var statetitle = actionResultTitle.getState(); 
            var resulttitle = actionResultTitle.getReturnValue();
            if(statetitle === "SUCCESS"){
                debugger;
                component.set("v.transactionrefnumber",resulttitle.tranRefnumber);
                component.set("v.countrycode", resulttitle.countrycodelist);
                component.set("v.countrylist", resulttitle.countrylist);
                /*component.set("v.PartyID",resulttitle.Id);
                component.set("v.Partname",resulttitle.CU_Name__c);
                component.find('Description__c').set("v.value",'Dear Sir, This Request is for the onboarding process. Thank you.');
                if(component.get("v.flagchk") =='T' || component.get("v.flagchk") == 'C'){
                    document.title ='Counterparty Banks';
                }else{
                    document.title =resulttitle.Browser_Title__c +' :: Party Banks';
                }*/
            }
        });
        $A.enqueueAction(ActionTitle);
        
    },
    popcounterPopupCancel : function(component, event) { 
        debugger;
        component.set("v.newcounterPopup", false);       
    },
    addCounterParties: function(component, event) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var img = component.find("pdfImgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');
        
        var FirstName = component.find("FirstName").get("v.value");
        var sitename = component.find("sitename").get("v.value");
        var email = component.find("email").get("v.value");
        var lastname = component.find("lastname").get("v.value");
        var swiftcode = component.find("swiftcode").get("v.value");
        var Tenanttitle = component.find("Tenanttitle").get("v.value");
        var phone = component.find("phone").get("v.value");   
        var Mobile = component.find("Mobile").get("v.value");  
        var countrycode = component.find("auracountrycode").get("v.value");
        var mobilecountrycode = component.find("auramobilecountrycode").get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        
        
        if(Tenanttitle == '' || Tenanttitle =='--Select--' || Tenanttitle == 'undefined' || Tenanttitle == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.SalutationError", 'Input field required'); 
        } else { 
            component.set("v.SalutationError", ''); 
        } 
        if(FirstName == '' || FirstName == 'undefined' || FirstName == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.FirstNameError", 'Input field required'); 
        } else { 
            component.set("v.FirstNameError", ''); 
        } 
        
        if(sitename == '' || sitename == 'undefined' || sitename == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LegalNameError", 'Input field required'); 
        } else { 
            component.set("v.LegalNameError", ''); 
        } 
        
        if(lastname == '' || lastname == 'undefined' || lastname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LastNameError", 'Input field required'); 
        } else { 
            component.set("v.LastNameError", ''); 
        } 
        if(swiftcode == '' || swiftcode == 'undefined' || swiftcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.SwiftCodeError", 'Input field required'); 
        } else { 
            component.set("v.SwiftCodeError", ''); 
        } 
        
        /*if(component.get("v.flagchk") == 'IC'){
            var dept = component.find("department").get("v.value");
            if(dept == '' || dept == 'undefined' || dept == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.departmentError", 'Input field required'); 
            } else { 
                component.set("v.departmentError", ''); 
            } 
        }*/
        /* if(location == '' || location == 'undefined' || location == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LocationError", 'Input field required'); 
        } else { 
            component.set("v.LocationError", ''); 
        } */
        
        if(!$A.util.isEmpty(phone))
        {   
            if(countrycode == '' || countrycode =='--Select--' || countrycode == 'undefined' || countrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.countrycodeError", 'Input field required'); 
            } else { 
                component.set("v.countrycodeError", ''); 
            } 
            
            if(!phone.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please enter valid phone number'); 
            } 
            else if(phone.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.PhoneError", ''); 
                }
        }
        if(!$A.util.isEmpty(Mobile))
        {   
            if(mobilecountrycode == '' || mobilecountrycode =='--Select--' || mobilecountrycode == 'undefined' || mobilecountrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.mobilecountrycodeError", 'Input field required'); 
            } else { 
                component.set("v.mobilecountrycodeError", ''); 
            }
            
            if(!Mobile.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please enter valid Mobile number'); 
            } 
            else if(Mobile.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.MobileError", ''); 
                }
        }
        
        
        if(email == '' || email == 'undefined' || email == null)
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.EmailError", 'Input field required'); 
        }
        else if(!$A.util.isEmpty(email))
        {   
            if(!email.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.EmailError", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.EmailError", ''); 
            }
        }
        
        var bankd = component.get("v.bank");
        var user =component.get("v.UM");
        var actionchk = component.get("c.CheckBank");
        var bname = bankd.Tenant_Site_Name__c.trim();
        var bscode = bankd.Tenant_Swift_Code__c.trim();
        var email = user.User_Email__c.trim();
        actionchk.setParams({ "bankName": bname,
                             "swiftcode": bscode,
                             "email" : email});
        actionchk.setCallback(this, function(response) {
            var statechk = response.getState();
            var bankchk = response.getReturnValue(); 
            if(bankchk != '' && bankchk != undefined) {
                if(bankchk == 'Already Used LName') { 
                    errorFlag = 'true';
                    
<<<<<<< HEAD
                    component.set("v.legnameerror", 'Error: This Entity Legal Name and Swift Code has already used. Please enter a new Entity Legal Name or Swift Code.');
                } else if(bankchk =='Already Used Email') {
                    errorFlag = 'true';
                    component.set("v.legnameerror", 'Error: This Email has already used. Please enter a new Email.');
                }
                    else{
                        component.set("v.legnameerror", ''); 
                        
                        
                    }
            } 
            else { 
                component.set("v.legnameerror", ''); 
            }
            if(errorFlag == 'false') { 
                
                
                component.set("v.showProgress",true);
                console.log('@@'+component.get("v.counterpartyname"));
                var action = component.get('c.addCounterPartiesname');  
                action.setParams({
                    "LoggedUserID" : localStorage.getItem("IDTenant"),
                    "saveCreditunion" : component.get("v.bank"),
                    "UM" : component.get("v.UM"),
                    "PName" : component.get("v.Partname"),
                    "PID" : component.get("v.PartyID"),
                    "Flag" : component.get("v.flagchk"),
                    "TransId" : component.get("v.TIDs")
                });
                debugger;
                action.setCallback(this, function(actionResult) { 
                    var state = actionResult.getState(); 
                    var result = actionResult.getReturnValue();
                    if(state === "SUCCESS"){
                        component.set("v.showProgress",false);
                        component.set("v.newcounterPopup", false);
                        component.set("v.onboard", true);
                    }
                });
                debugger;
                $A.enqueueAction(action);
            }
        });
        $A.enqueueAction(actionchk);
    },
    saveassignee: function(component, event){
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var img = component.find("pdfImgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');
        var viewtrans  = component.find("chkboxviewTransaction").get("v.value");
        var FirstName = component.find("FirstName").get("v.value");
        var sitename = component.find("sitename").get("v.value");
        var email = component.find("email").get("v.value");
        var lastname = component.find("lastname").get("v.value");
        var swiftcode = component.find("swiftcode").get("v.value");
        var Tenanttitle = component.find("Tenanttitle").get("v.value");
        var phone = component.find("phone").get("v.value");   
        var Mobile = component.find("Mobile").get("v.value");  
        var countrycode = component.find("auracountrycode").get("v.value");
        var countrylist = component.find("auracountrylistcode").get("v.value");
        var mobilecountrycode = component.find("auramobilecountrycode").get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        
        
        if(Tenanttitle == '' || Tenanttitle =='--Select--' || Tenanttitle == 'undefined' || Tenanttitle == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.SalutationError", 'Input field required'); 
        } else { 
            component.set("v.SalutationError", ''); 
        } 
        if(FirstName == '' || FirstName == 'undefined' || FirstName == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.FirstNameError", 'Input field required'); 
        } else { 
            component.set("v.FirstNameError", ''); 
        } 
        
        if(sitename == '' || sitename == 'undefined' || sitename == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LegalNameError", 'Input field required'); 
        } else { 
            component.set("v.LegalNameError", ''); 
        } 
        
        if(lastname == '' || lastname == 'undefined' || lastname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LastNameError", 'Input field required'); 
        } else { 
            component.set("v.LastNameError", ''); 
        } 
        /*if(swiftcode == '' || swiftcode == 'undefined' || swiftcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.SwiftCodeError", 'Input field required'); 
        } else { 
            component.set("v.SwiftCodeError", ''); 
        } */
        if(countrylist == '' || countrylist =='--Select--' || countrylist == 'undefined' || countrylist == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.countrylistError", 'Input field required'); 
        } else { 
            component.set("v.countrylistError", ''); 
        } 
       
        if(!$A.util.isEmpty(phone))
        {   
            if(countrycode == '' || countrycode =='--Select--' || countrycode == 'undefined' || countrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.countrycodeError", 'Input field required'); 
            } else { 
                component.set("v.countrycodeError", ''); 
            } 
            
            if(!phone.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please enter valid phone number'); 
            } 
            else if(phone.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.PhoneError", ''); 
                }
        }
        if(!$A.util.isEmpty(Mobile))
        {   
            if(mobilecountrycode == '' || mobilecountrycode =='--Select--' || mobilecountrycode == 'undefined' || mobilecountrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.mobilecountrycodeError", 'Input field required'); 
            } else { 
                component.set("v.mobilecountrycodeError", ''); 
            }
            
            if(!Mobile.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please enter valid Mobile number'); 
            } 
            else if(Mobile.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.MobileError", ''); 
                }
        }
        
        
        if(email == '' || email == 'undefined' || email == null)
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.EmailError", 'Input field required'); 
        }
        else if(!$A.util.isEmpty(email))
        {   
            if(!email.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.EmailError", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.EmailError", ''); 
            }
        }
       
        
        var bankd = component.get("v.bank");
        var user =component.get("v.UM");
        var actionchk = component.get("c.CheckBank");
        var bname = bankd.Tenant_Site_Name__c.trim();
        var bscode;
        if(swiftcode == undefined){
            bscode = 'TS1234';
        }else{
            bscode = bankd.Tenant_Swift_Code__c.trim();
        }
        
        var email = user.User_Email__c.trim();
        actionchk.setParams({ "bankName": bname,
                             "swiftcode": bscode,
                             "email" : email});
        actionchk.setCallback(this, function(response) {
            var statechk = response.getState();
            var bankchk = response.getReturnValue(); 
            if(bankchk != '' && bankchk != undefined) {
                if(bankchk == 'Already Used LName') { 
                    errorFlag = 'true';
                    
                    component.set("v.legnameerror", 'Error: This Entity Legal Name and Swift Code has already used. Please enter a new Entity Legal Name or Swift Code.');
=======
                    component.set("v.legnameerror", 'Error: This Legal Name and Swift Code has already used. Please enter a new Legal Name or Swift Code.');
                } else if(bankchk =='Already Used Email') {
                    errorFlag = 'true';
                    component.set("v.legnameerror", 'Error: This Email has already used. Please enter a new Email.');
                }
                    else{
                        component.set("v.legnameerror", ''); 
                        
                        
                    }
            } 
            else { 
                component.set("v.legnameerror", ''); 
            }
            if(errorFlag == 'false') { 
                
                
                component.set("v.showProgress",true);
                console.log('@@'+component.get("v.counterpartyname"));
                var action = component.get('c.addCounterPartiesname');  
                action.setParams({
                    "LoggedUserID" : localStorage.getItem("IDTenant"),
                    "saveCreditunion" : component.get("v.bank"),
                    "UM" : component.get("v.UM"),
                    "PName" : component.get("v.Partname"),
                    "PID" : component.get("v.PartyID"),
                    "Flag" : component.get("v.flagchk"),
                    "TransId" : component.get("v.TIDs")
                });
                debugger;
                action.setCallback(this, function(actionResult) { 
                    var state = actionResult.getState(); 
                    var result = actionResult.getReturnValue();
                    if(state === "SUCCESS"){
                        component.set("v.showProgress",false);
                        component.set("v.newcounterPopup", false);
                        component.set("v.onboard", true);
                    }
                });
                debugger;
                $A.enqueueAction(action);
            }
        });
        $A.enqueueAction(actionchk);
    },
    saveassignee: function(component, event){
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var img = component.find("pdfImgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');
        var viewtrans  = component.find("chkboxviewTransaction").get("v.value");
        var FirstName = component.find("FirstName").get("v.value");
        var sitename = component.find("sitename").get("v.value");
        var email = component.find("email").get("v.value");
        var lastname = component.find("lastname").get("v.value");
        var swiftcode = component.find("swiftcode").get("v.value");
        var Tenanttitle = component.find("Tenanttitle").get("v.value");
        var phone = component.find("phone").get("v.value");   
        var Mobile = component.find("Mobile").get("v.value");  
        var countrycode = component.find("auracountrycode").get("v.value");
        var countrylist = component.find("auracountrylistcode").get("v.value");
        var mobilecountrycode = component.find("auramobilecountrycode").get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        
        
        if(Tenanttitle == '' || Tenanttitle =='--Select--' || Tenanttitle == 'undefined' || Tenanttitle == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.SalutationError", 'Input field required'); 
        } else { 
            component.set("v.SalutationError", ''); 
        } 
        if(FirstName == '' || FirstName == 'undefined' || FirstName == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.FirstNameError", 'Input field required'); 
        } else { 
            component.set("v.FirstNameError", ''); 
        } 
        
        if(sitename == '' || sitename == 'undefined' || sitename == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LegalNameError", 'Input field required'); 
        } else { 
            component.set("v.LegalNameError", ''); 
        } 
        
        if(lastname == '' || lastname == 'undefined' || lastname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.LastNameError", 'Input field required'); 
        } else { 
            component.set("v.LastNameError", ''); 
        } 
        /*if(swiftcode == '' || swiftcode == 'undefined' || swiftcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.SwiftCodeError", 'Input field required'); 
        } else { 
            component.set("v.SwiftCodeError", ''); 
        } */
        if(countrylist == '' || countrylist =='--Select--' || countrylist == 'undefined' || countrylist == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.countrylistError", 'Input field required'); 
        } else { 
            component.set("v.countrylistError", ''); 
        } 
       
        if(!$A.util.isEmpty(phone))
        {   
            if(countrycode == '' || countrycode =='--Select--' || countrycode == 'undefined' || countrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.countrycodeError", 'Input field required'); 
            } else { 
                component.set("v.countrycodeError", ''); 
            } 
            
            if(!phone.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please enter valid phone number'); 
            } 
            else if(phone.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.PhoneError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.PhoneError", ''); 
                }
        }
        if(!$A.util.isEmpty(Mobile))
        {   
            if(mobilecountrycode == '' || mobilecountrycode =='--Select--' || mobilecountrycode == 'undefined' || mobilecountrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.mobilecountrycodeError", 'Input field required'); 
            } else { 
                component.set("v.mobilecountrycodeError", ''); 
            }
            
            if(!Mobile.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please enter valid Mobile number'); 
            } 
            else if(Mobile.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.MobileError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.MobileError", ''); 
                }
        }
        
        
        if(email == '' || email == 'undefined' || email == null)
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.EmailError", 'Input field required'); 
        }
        else if(!$A.util.isEmpty(email))
        {   
            if(!email.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.EmailError", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.EmailError", ''); 
            }
        }
       
        
        var bankd = component.get("v.bank");
        var user =component.get("v.UM");
        var actionchk = component.get("c.CheckBank");
        var bname = bankd.Tenant_Site_Name__c.trim();
        var bscode;
        if(swiftcode == undefined){
            bscode = 'TS1234';
        }else{
            bscode = bankd.Tenant_Swift_Code__c.trim();
        }
        
        var email = user.User_Email__c.trim();
        actionchk.setParams({ "bankName": bname,
                             "swiftcode": bscode,
                             "email" : email});
        actionchk.setCallback(this, function(response) {
            var statechk = response.getState();
            var bankchk = response.getReturnValue(); 
            if(bankchk != '' && bankchk != undefined) {
                if(bankchk == 'Already Used LName') { 
                    errorFlag = 'true';
                    
                    component.set("v.legnameerror", 'Error: This Legal Name and Swift Code has already used. Please enter a new Legal Name or Swift Code.');
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-clientdemo.git
                } else if(bankchk =='Already Used Email') {
                    errorFlag = 'true';
                    component.set("v.legnameerror", 'Error: This Email has already used. Please enter a new Email.');
                }
                    else{
                        component.set("v.legnameerror", ''); 
                        
                        
                    }
            } 
            else { 
                component.set("v.legnameerror", ''); 
            }
            if(errorFlag == 'false') { 
                
                
                component.set("v.showProgress",true);
                console.log('@@'+component.get("v.counterpartyname"));
                var action = component.get('c.addassignee');  
                action.setParams({
                    "LoggedUserID" : localStorage.getItem("IDTenant"),
                    "LoggedUserTenantId" : localStorage.getItem("LoggeduserTenantID"),
                    "saveCreditunion" : component.get("v.bank"),
                    "UM" : component.get("v.UM"),
                    "PName" : component.get("v.Partname"),
                    "PID" : component.get("v.PartyID"),
                    "Flag" : component.get("v.flagchk"),
                    "TransId" : component.get("v.TIDs"),
                    "TransFlag" : viewtrans
                });
                debugger;
                action.setCallback(this, function(actionResult) { 
                    var state = actionResult.getState(); 
                    var result = actionResult.getReturnValue();
                    if(state === "SUCCESS"){
                        component.set("v.showProgress",false);
                        component.set("v.newcounterPopup", false);
                        component.set("v.onboardassignee", true);
                    }
                });
                debugger;
                $A.enqueueAction(action);
            }
        });
        $A.enqueueAction(actionchk);
    },
    onboard: function(component, event) {
        debugger;
        component.set("v.onboard", false);
        var site = $A.get("{!$Label.c.Org_URL}");
        //window.location = '/'+site+'/s/adminusers';
	    window.location = '/'+site+'/s/transactionviewdetails?transactionId='+component.get("v.TIDs");

        
    },
    onboardassigneeclose: function(component, event) {
        debugger;
        component.set("v.onboardassignee", false);
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/adminusers';
        
    },
})
({
    getentitydetails : function(component, event, helper) {
        debugger;
        var Action = component.get("c.Branddetails"); 
        Action.setCallback(this, function(actionResult1) {
            debugger;
            var state = actionResult1.getState(); 
            var results = actionResult1.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Banks",results);
            }
            
        }); 
        $A.enqueueAction(Action);
    },
    editparty : function(component, event, helper) {
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/brandupdate?flag=P&Id=a0jg000000Ble4e';
    },
    editcparty1 : function(component, event, helper) {
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/brandupdate?flag=CP1&Id=a0jg000000Ble5I';
    },
    editcparty2 : function(component, event, helper) {
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/brandupdate?flag=CP2&Id=a0jg000000Ble58';
    },
    cancel : function(component, event, helper) {
        debugger;        
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/adminconsole';
    },
    continue: function(component, event, helper) {
        debugger; 
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/brandreview';
	}
 })
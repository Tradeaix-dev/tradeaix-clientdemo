({
	getTenantlist : function(component, event, helper) {
        var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
        document.title =sitename +' : Tenant Detail';
        debugger;
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };
        
        var orgId = getUrlParameter('tenantid');        
        var Action = component.get("c.getTenant");
        var sessionname = localStorage.getItem("UserSession");       
        Action.setParams({
            "username" : sessionname,
            "tenantname" : orgId
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Tenantlist",results.tenant);  
                component.set("v.bankId", results.tenant.Id);
                component.set("v.Counterparty",results.TMCP);
                component.set("v.IB",results.TMIB);
                if(results.tenant.Tenant_Type__c == 'Corporate'){
                    component.set("v.EnableBanktype", false);
                }else{
                    component.set("v.EnableBanktype", true);
                }
                component.set("v.tenant", results);
            }
        });
        $A.enqueueAction(Action);
    },
    counterPopupCancel1 : function(component, event) {
        debugger;
        component.set("v.counterPopup", false);
    }, counterPopupCancel : function(component, event) {        
        component.set("v.counterPopup", false);
        $A.get('e.force:refreshView').fire();
        component.set("v.key","two");
    },
     CPConfirmpopup : function(component, event) {       
        component.set("v.counterPopup", false);
        component.set("v.CPConfirm", true);
        
    }, 
    selectCounterParties: function(component, event) {
        debugger;
        var checkbox = event.getSource();
        var checked = checkbox.get("v.value");
        if(checked) {
            component.find("btnSelect").set("v.disabled", false);
        }
        else{
            component.find("btnSelect").set("v.disabled", true);
        }
        var creditUnionId = checkbox.get("v.text");
        debugger;
        
        var action = component.get('c.addRemoveCounterParty');  
        action.setParams({
            "creditUnionId" : creditUnionId,
            "forAdd" : checked,
            "TId" : component.get("v.bankId"),
            "LoggedUserId" : localStorage.getItem('UserSession')
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
            }
        });
        $A.enqueueAction(action);
        
        
    },
      CPrevokepopup: function(component, event) {
        debugger;
        var action = component.get('c.addRemoveCounterParty');  
        action.setParams({
            "creditUnionId" : event.target.id,
            "forAdd" : false,
            "TId" : component.get("v.bankId"),
            "LoggedUserId" : localStorage.getItem('UserSession')
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
        
        
    },
    CPConfirmcan: function(component, event) {
        component.set("v.CPConfirm", false);
    },
     showCounterParties: function(component, event) {
        debugger;
        var action = component.get('c.getTMapping');
        action.setParams({
            "creditUnionId" : component.get("v.bankId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                component.set("v.ApprovedCounterParties", result);
                component.set("v.counterPopup", true);
            }
        });
        $A.enqueueAction(action);
    },counterPopupCancel1IB : function(component, event) {
        debugger;
        component.set("v.counterPopupIB", false);
    }, 
    counterPopupCancelIB : function(component, event) {        
        component.set("v.counterPopupIB", false);
        $A.get('e.force:refreshView').fire();
        component.set("v.key","two");
    },
     CPConfirmpopupIB : function(component, event) {       
        component.set("v.counterPopupIB", false);
        component.set("v.CPConfirmIB", true);
        
    }, 
    selectCounterPartiesIB: function(component, event) {
        debugger;
        var checkbox = event.getSource();
        var checked = checkbox.get("v.value");
        if(checked) {
            component.find("btnSelectIB").set("v.disabled", false);
        }
        else{
            component.find("btnSelectIB").set("v.disabled", true);
        }
        var creditUnionId = checkbox.get("v.text");
        debugger;
        
        var action = component.get('c.addRemoveCounterParty');  
        action.setParams({
            "creditUnionId" : creditUnionId,
            "forAdd" : checked,
            "TId" : component.get("v.bankId"),
            "LoggedUserId" : localStorage.getItem('UserSession')
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
            }
        });
        $A.enqueueAction(action);
        
        
    },
      CPrevokepopupIB: function(component, event) {
        debugger;
        var action = component.get('c.addRemoveCounterParty');  
        action.setParams({
            "creditUnionId" : event.target.id,
            "forAdd" : false,
            "TId" : component.get("v.bankId"),
            "LoggedUserId" : localStorage.getItem('UserSession')
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
        
        
    },
    CPConfirmcanIB: function(component, event) {
        component.set("v.CPConfirmIB", false);
    },
     showCounterPartiesIB: function(component, event) {
        debugger;
        var action = component.get('c.getTMappingIB');
        action.setParams({
            "creditUnionId" : component.get("v.bankId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                component.set("v.ApprovedIB", result);
                component.set("v.counterPopupIB", true);
            }
        });
        $A.enqueueAction(action);
    },
    editTenant :  function(component, event, helper) {
        debugger;
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };        
        var tenantId = getUrlParameter('tenantid');  
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/tenantupdate?tenantid='+tenantId;
    },
    
    backtoAdmin : function(component, event, helper) {
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/administration';
    }
    
})
<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AmendRequestEmailAlert</fullName>
        <description>AmendRequestEmailAlert</description>
        <protected>false</protected>
        <recipients>
            <field>TaskAssigneeEmail__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>TaskOwnerEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/AmendRequestEmail</template>
    </alerts>
    <rules>
        <fullName>AmendRequestRule</fullName>
        <actions>
            <name>AmendRequestEmailAlert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Notification__c.Action__c</field>
            <operation>equals</operation>
            <value>Amendment Request</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

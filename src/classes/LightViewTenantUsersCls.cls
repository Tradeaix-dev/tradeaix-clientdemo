public class LightViewTenantUsersCls {
    @AuraEnabled
    public static LightViewTenantUsersClsV viewTenantUsersList(String tenantId, String transactionId){
        LightViewTenantUsersClsV viewuser = new LightViewTenantUsersClsV(); 
        //String transactionId = '';
        viewuser.userslist = [SELECT Id, Name, First_Name__c, Last_Name__c, User_Email__c, Title__c, Phone__c,Department__c from User_Management__c WHERE Tenant__c =: tenantId AND Task_Assignee__c = false];
        List<String> ls = new List<String>();
        List<Published_Tenant__c> lstPT = [SELECT Id, Name from Published_Tenant__c WHERE Tenant__c =: tenantId AND Transaction__c =: transactionId];
        Set<Id> PTId = new Set<Id>();
        for(Published_Tenant__c PT : lstPT){
            PTId.add(PT.Id);
        }
        system.debug('==PTId=='+PTId);
        for(Published_Tenant_User__c PTU: [SELECT Id, Tenant_User__c from Published_Tenant_User__c WHERE Published_Tenant__c =: PTId]){
            ls.add(PTU.Tenant_User__c);
        }
        system.debug('==ls=='+ls);
        if(ls.size() >0){
            viewuser.idString = ls;
        }
        
        return viewuser;
    }
    
    @AuraEnabled
    public static LightViewTenantUsersClsV viewTenantUsersListrefresh(String tenantId, String transactionId){
        LightViewTenantUsersClsV viewuser = new LightViewTenantUsersClsV(); 
        //String transactionId = '';
        viewuser.userslist = [SELECT Id, Name, First_Name__c, Last_Name__c, User_Email__c, Title__c, Phone__c,Department__c from User_Management__c WHERE Tenant__c =: tenantId];
        List<String> ls = new List<String>();
        List<Published_Tenant__c> lstPT = [SELECT Id, Name from Published_Tenant__c WHERE Transaction__c =: transactionId];
        Set<Id> PTId = new Set<Id>();
        for(Published_Tenant__c PT : lstPT){
            PTId.add(PT.Id);
        }
        system.debug('==PTId=='+PTId);
        for(Published_Tenant_User__c PTU: [SELECT Id, Tenant_User__c from Published_Tenant_User__c WHERE Published_Tenant__c =: PTId]){
            
            ls.add(PTU.Tenant_User__c);
        }
        system.debug('==ls=='+ls);
        if(ls.size() >0){
            viewuser.idString = ls;
        }
        
        return viewuser;
    }
    @AuraEnabled
    public static LightViewTenantUsersClsV GetPTenantusers(String tenantId, String transactionId){
        LightViewTenantUsersClsV viewuser = new LightViewTenantUsersClsV(); 
  
        List<String> ls = new List<String>();
        for(Published_Tenant_User__c PTU: [SELECT Id, Tenant_User__c from Published_Tenant_User__c WHERE Tenant__c =: tenantId AND Published_Tenant__r.Transaction__C=:transactionId]){
            ls.add(PTU.Tenant_User__c);
        }
        system.debug('==ls=='+ls);
        if(ls.size() >0){
            viewuser.idString = ls;
        }
        
        return viewuser;
    }
    
    @AuraEnabled
    public static String addCPTenantUsers(String Tenant, string offerId){
        try{
            string PID = '';
            List<Published_Tenant__c> PT = new List<Published_Tenant__c>();
            List<Published_Tenant_User__c> listPTU = new List<Published_Tenant_User__c>();
            Set<Id> TId = new Set<Id>();            
            List<User_Management__c> UMlist = [SELECT Id, Name,Tenant__c,Tenant__r.Organization__c from User_Management__c WHERE Tenant__c=: Tenant order by Tenant__r.Organization__c limit 1];
            
            List<Published_Tenant__c> lstPT = [SELECT Id, Name,Organizations__c from Published_Tenant__c WHERE Organizations__c = : UMlist[0].Tenant__r.Organization__c AND Transaction__r.GroupID__c =: offerId];
            if(lstPT.size() > 0){
                TId.add(lstPT[0].Organizations__c);
            }
            else{
                for(User_Management__c UM: UMlist){
                    
                        Published_Tenant__c P = new Published_Tenant__c();
                        P.Transaction__c = offerId;//
                        P.Organizations__c = UM.Tenant__r.Organization__c;
                        P.Tenant__c = UM.Tenant__c;
                        PT.add(P);   
                        
                        TId.add(UM.Tenant__r.Organization__c);
                   
                }
            }
            if(PT.size() > 0){
                insert PT;  
                PID = PT[0].Id;
            }
            
            system.debug('==AfterInsert PT List=='+TId);
            List<Published_Tenant__c> PPT = [SELECT Id, Name, Organizations__c from Published_Tenant__c WHERE Organizations__c =: TId AND Transaction__c=: offerId];
            system.debug('==AfterInsert PT List=='+PPT);
            for(Published_Tenant__c PTC : PPT){
                for(User_Management__c UMC : [SELECT Id, Tenant__c,Tenant__r.Organization__c from User_Management__c WHERE Tenant__c=: Tenant AND Tenant__r.Organization__c =: PTC.Organizations__c order by Tenant__r.Organization__c]){
                    Published_Tenant_User__c PTU = new Published_Tenant_User__c();
                    PTU.Published_Tenant__c = PTC.Id;
                    PTU.Tenant_User__c = UMC.Id;
                    PTU.Tenant__c =Tenant;
                    listPTU.add(PTU);
                }
            }
            system.debug('==listPTU=='+listPTU);
            if(listPTU.size()>0){
                insert listPTU;
            }
            
            List<Published_Tenant__c> pubCUResult = [
                SELECT 
                Id, Tenant__c,Tenant__r.Tenant_Site_Name__c, 
                Transaction__c,Transaction__r.CreatedBy__c,Transaction__r.Id,
                RequestForInfo__c
                FROM Published_Tenant__c
                WHERE Transaction__c =: offerId AND Id =: PID];
            for(Published_Tenant__c PTR : pubCUResult){
                Helper.ActivityLogInsertCallForRecord(PTR.Transaction__r.CreatedBy__c,PTR.Transaction__r.Id,'CP - '+PTR.Tenant__r.Tenant_Site_Name__c+'- Selected','',false);
            }
            
            return 'Success';
            
        }catch(exception ex){
            system.debug('==addCPTenantUsers=='+ex.getMessage());
            return ex.getMessage();
        }
        
    }
    
    @AuraEnabled
    public static String addremovesingleCPTenantUser(List<String> tenantusers,string tenantID, string offerId){
        try{
            string PTId = '';
            Transaction__c T=[Select Tenant__c FROM Transaction__c WHERE id=:offerId Limit 1];
            Tenant_Mapping__c TM =[Select Email_Notification__c FROM Tenant_Mapping__c WHERE Tenant__c=:T.Tenant__c AND Tenant_Approved__c=:tenantID Limit 1];
            List<User_Management__c> UMlist = [SELECT Id, Name,Tenant__c,User_Email__c,Tenant__r.Tenant_Site_Name__c,Tenant__r.Organization__c from User_Management__c WHERE Id IN: tenantusers order by Tenant__r.Organization__c limit 1];
            system.debug('==UMlist=='+UMlist);
            List<Published_Tenant_User__c> listPTU = new List<Published_Tenant_User__c>();
            if(UMlist.size() > 0){
                system.debug('===IF Condition==');
                List<Published_Tenant__c> lstPT = [SELECT Id,Transaction__c,Transaction__r.CreatedBy__c,Name,Organizations__c from Published_Tenant__c WHERE Organizations__c = : UMlist[0].Tenant__r.Organization__c AND Transaction__c =: offerId];
                if(lstPT.size() > 0){
                    system.debug('===IF IF Condition==');
                    for(Published_Tenant__c PTC : lstPT){
                        for(User_Management__c UMC : [SELECT Id,User_Email__c,Tenant__c,Tenant__r.Tenant_Site_Name__c,Tenant__r.Organization__c from User_Management__c WHERE Id IN: tenantusers AND Tenant__r.Organization__c =: PTC.Organizations__c order by Tenant__r.Organization__c]){
                            Published_Tenant_User__c PTU = new Published_Tenant_User__c();
                            PTU.Published_Tenant__c = PTC.Id;
                            PTU.Tenant__c= tenantID;
                            PTU.Tenant_User__c = UMC.Id;
                            listPTU.add(PTU);
                             String Tid=[SELECT id FROM Transaction__c where Id=:offerId].CreatedBy__c;
                            Helper.ActivityLogInsertCallForRecord(PTC.Transaction__r.CreatedBy__c,PTC.Transaction__c,'CP - '+UMC.User_Email__c+' ('+UMC.Tenant__r.Tenant_Site_Name__c+')- Selected','',false);
 
                        }
                    }
                    system.debug('==listPTU=='+listPTU);
                    if(listPTU.size()>0){
                        insert listPTU;
                    }
                }else{
                    
                    system.debug('===IF ELSE Condition==');
                    //List<Published_Tenant__c> PTL = New List<Published_Tenant__c>();
                    Set<Id> TIds = new Set<Id>();
                    system.debug('---UMLIST---'+UMlist);
                    for(User_Management__c UM: UMlist){
                       
                            Published_Tenant__c P = new Published_Tenant__c();
                            P.Transaction__c =offerId;
                            P.Organizations__c = UM.Tenant__r.Organization__c;
                            P.Tenant__c = UM.Tenant__c;
                            P.Tenant_Name__c = UM.Id;
                            P.Email_Notification__c=TM.Email_Notification__c;
                            Insert P;   
                            PTId = P.Id;
                        
                            system.debug('--published tenant id--'+P.Id);
                            Published_Tenant_User__c PTU = new Published_Tenant_User__c();
                            PTU.Published_Tenant__c = P.Id;
                            PTU.Tenant__c= tenantID;
                            PTU.Tenant_User__c = UM.Id;
                            Insert PTU; 
                            String Tid=[SELECT CreatedBy__c FROM Transaction__c where Id=:offerId].CreatedBy__c;
                            Helper.ActivityLogInsertCallForRecord(Tid,offerId,'CP - '+UM.User_Email__c+' ('+UM.Tenant__r.Tenant_Site_Name__c+')- Selected','',false);
 
                      
                    }
                    
                    List<Published_Tenant__c> pubCUResult = [
                        SELECT 
                        Tenant__c,Tenant__r.Tenant_Site_Name__c, 
                        Transaction__c,Transaction__r.CreatedBy__c,Transaction__r.Id,
                        RequestForInfo__c
                        FROM Published_Tenant__c
                        WHERE Transaction__c =: offerId AND Id =: PTId];
                    for(Published_Tenant__c PTR : pubCUResult){
                        //Helper.ActivityLogInsertCallForRecord(PTR.Transaction__r.CreatedBy__c,PTR.Transaction__r.Id,'CP - '+PTR.Tenant__r.Tenant_Site_Name__c+'- Selected','',false);
                    }
                    
                }
            }                    
            return 'Success'; 
            
        }catch(exception ex){
            system.debug('==addCPTenantUsers=='+ex.getMessage());
            return ex.getMessage();
        }       
    }
    
    @AuraEnabled
    public static String removesingleCPTenantUser(List<String> tenantdeselectusers,string tenantID, string offerId, string LoggedUserId){
        try{
            system.debug('----removesingleCPTenantUser---'+tenantdeselectusers);
            system.debug('----tenantID---'+tenantID);
            system.debug('----removesingleCPTenantUser---'+offerId+'---ewe-----'+LoggedUserId);
            List<User_Management__c> UMlist = [SELECT Id,Tenant__r.Tenant_Site_Name__c,User_Email__c,Name,Tenant__c from User_Management__c WHERE Id IN: tenantdeselectusers order by Tenant__c ];
            system.debug('----removesingleCPTenantUser---'+offerId+'ewe'+LoggedUserId);
            if(UMlist.size() > 0){
                
                SET<ID> PTUSET =New SET<ID>(); 
                List<String> PTList =New List<String>();
                SET<ID> PTSET =New SET<ID>(); 
                for(Published_Tenant_User__c lstPTU : [SELECT Id,Published_Tenant__c from Published_Tenant_User__c WHERE Tenant_User__c =: tenantdeselectusers AND Tenant__c =:tenantID AND Published_Tenant__r.Transaction__c=:offerId])
                {
                    PTUSET.add(lstPTU.Id);
                    PTSET.add(lstPTU.Published_Tenant__c);
                    PTList.add(lstPTU.Published_Tenant__c);
                }
                /*List<Published_Tenant__c> lstPT = [SELECT Id, Name, Tenant__c,CounterPartyUserCount__c, FTenant_Name__c from Published_Tenant__c WHERE Id=:PTSET];
                if(PTList.size() == lstPT.size()){
                    delete lstPT;
                    Helper.ActivityLogInsertCallForRecord(LoggedUserId,offerId,'CP - '+lstPT[0].FTenant_Name__c+'- Un-Selected','',false);
                    
                }else{*/
                if(PTUSET.size()>0){
                    Published_Tenant_User__c[] PTU = [SELECT Id, Name,Tenant_User__r.User_Email__c,Tenant__r.Tenant_Site_Name__c FROM Published_Tenant_User__c 
WHERE ID =:PTUSET]; 

for(Published_Tenant_User__c UM: PTU)
{
for(Transaction__c T:[SELECT Id,CreatedBy__c FROM Transaction__c WHERE id=: offerId]){
Helper.ActivityLogInsertCallForRecord(T.CreatedBy__c,T.id,'CP - '+UM.Tenant_User__r.User_Email__c+' ('+UM.Tenant__r.Tenant_Site_Name__c+')- UnSelected','',false);

}
}
                    delete PTU;
                    Published_Tenant__c[] PTDel = [SELECT Id,CounterPartyUserCount__c,Name FROM Published_Tenant__c WHERE Id=:PTSET AND CounterPartyUserCount__c=0]; 

Delete PTDel ;
                }
                //}
            } 
        
        return 'Success';
    }catch(exception ex){
        system.debug('==addCPTenantUsers=='+ex.getMessage());
        return ex.getMessage();
    }
    
}
}
public class MetadataPort {
        public static String SOAP_M_URI = 'http://soap.sforce.com/2006/04/metadata';
        public String endpoint_x = URL.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/m/42.0';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        public MetadataWrapper.SessionHeader_element SessionHeader;
        public MetadataWrapper.DebuggingInfo_element DebuggingInfo;
        public MetadataWrapper.DebuggingHeader_element DebuggingHeader;
        public MetadataWrapper.CallOptions_element CallOptions;
        public MetadataWrapper.AllOrNoneHeader_element AllOrNoneHeader;
        private String SessionHeader_hns = 'SessionHeader=http://soap.sforce.com/2006/04/metadata';
        private String DebuggingInfo_hns = 'DebuggingInfo=http://soap.sforce.com/2006/04/metadata';
        private String DebuggingHeader_hns = 'DebuggingHeader=http://soap.sforce.com/2006/04/metadata';
        private String CallOptions_hns = 'CallOptions=http://soap.sforce.com/2006/04/metadata';
        private String AllOrNoneHeader_hns = 'AllOrNoneHeader=http://soap.sforce.com/2006/04/metadata';
        //private String[] ns_map_type_info = new String[]{SOAP_M_URI, 'MetadataWrapper'};
        public MetadataWrapper.DeleteResult[] deleteMetadata(String type_x,String[] fullNames) {
            MetadataWrapper.deleteMetadata_element request_x = new MetadataWrapper.deleteMetadata_element();
            request_x.type_x = type_x;
            request_x.fullNames = fullNames;
            MetadataWrapper.deleteMetadataResponse_element response_x;
            Map<String, MetadataWrapper.deleteMetadataResponse_element> response_map_x = new Map<String, MetadataWrapper.deleteMetadataResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'deleteMetadata',
              SOAP_M_URI,
              'deleteMetadataResponse',
              'MetadataWrapper.deleteMetadataResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
        public MetadataWrapper.SaveResult renameMetadata(String type_x,String oldFullName,String newFullName) {
            MetadataWrapper.renameMetadata_element request_x = new MetadataWrapper.renameMetadata_element();
            request_x.type_x = type_x;
            request_x.oldFullName = oldFullName;
            request_x.newFullName = newFullName;
            MetadataWrapper.renameMetadataResponse_element response_x;
            Map<String, MetadataWrapper.renameMetadataResponse_element> response_map_x = new Map<String, MetadataWrapper.renameMetadataResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'renameMetadata',
              SOAP_M_URI,
              'renameMetadataResponse',
              'MetadataWrapper.renameMetadataResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
        public MetadataWrapper.SaveResult[] updateMetadata(MetadataWrapper.Metadata[] metadata) {
            MetadataWrapper.updateMetadata_element request_x = new MetadataWrapper.updateMetadata_element();
            request_x.metadata = metadata;
            MetadataWrapper.updateMetadataResponse_element response_x;
            Map<String, MetadataWrapper.updateMetadataResponse_element> response_map_x = new Map<String, MetadataWrapper.updateMetadataResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'updateMetadata',
              SOAP_M_URI,
              'updateMetadataResponse',
              'MetadataWrapper.updateMetadataResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
        public MetadataWrapper.DescribeMetadataResult describeMetadata(Double asOfVersion) {
            MetadataWrapper.describeMetadata_element request_x = new MetadataWrapper.describeMetadata_element();
            request_x.asOfVersion = asOfVersion;
            MetadataWrapper.describeMetadataResponse_element response_x;
            Map<String, MetadataWrapper.describeMetadataResponse_element> response_map_x = new Map<String, MetadataWrapper.describeMetadataResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'describeMetadata',
              SOAP_M_URI,
              'describeMetadataResponse',
              'MetadataWrapper.describeMetadataResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
        public MetadataWrapper.DeployResult checkDeployStatus(String asyncProcessId,Boolean includeDetails) {
            MetadataWrapper.checkDeployStatus_element request_x = new MetadataWrapper.checkDeployStatus_element();
            request_x.asyncProcessId = asyncProcessId;
            request_x.includeDetails = includeDetails;
            MetadataWrapper.checkDeployStatusResponse_element response_x;
            Map<String, MetadataWrapper.checkDeployStatusResponse_element> response_map_x = new Map<String, MetadataWrapper.checkDeployStatusResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'checkDeployStatus',
              SOAP_M_URI,
              'checkDeployStatusResponse',
              'MetadataWrapper.checkDeployStatusResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
        public MetadataWrapper.RetrieveResult checkRetrieveStatus(String asyncProcessId,Boolean includeZip) {
            MetadataWrapper.checkRetrieveStatus_element request_x = new MetadataWrapper.checkRetrieveStatus_element();
            request_x.asyncProcessId = asyncProcessId;
            request_x.includeZip = includeZip;
            MetadataWrapper.checkRetrieveStatusResponse_element response_x;
            Map<String, MetadataWrapper.checkRetrieveStatusResponse_element> response_map_x = new Map<String, MetadataWrapper.checkRetrieveStatusResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'checkRetrieveStatus',
              SOAP_M_URI,
              'checkRetrieveStatusResponse',
              'MetadataWrapper.checkRetrieveStatusResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
        /*public MetadataWrapper.AsyncResult retrieve(MetadataWrapper.RetrieveRequest retrieveRequest) {
            MetadataWrapper.retrieve_element request_x = new MetadataWrapper.retrieve_element();
            request_x.retrieveRequest = retrieveRequest;
            MetadataWrapper.retrieveResponse_element response_x;
            Map<String, MetadataWrapper.retrieveResponse_element> response_map_x = new Map<String, MetadataWrapper.retrieveResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'retrieve',
              SOAP_M_URI,
              'retrieveResponse',
              'MetadataWrapper.retrieveResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }*/
        public MetadataWrapper.CancelDeployResult cancelDeploy(String String_x) {
            MetadataWrapper.cancelDeploy_element request_x = new MetadataWrapper.cancelDeploy_element();
            request_x.String_x = String_x;
            MetadataWrapper.cancelDeployResponse_element response_x;
            Map<String, MetadataWrapper.cancelDeployResponse_element> response_map_x = new Map<String, MetadataWrapper.cancelDeployResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'cancelDeploy',
              SOAP_M_URI,
              'cancelDeployResponse',
              'MetadataWrapper.cancelDeployResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
        public String deployRecentValidation(String validationId) {
            MetadataWrapper.deployRecentValidation_element request_x = new MetadataWrapper.deployRecentValidation_element();
            request_x.validationId = validationId;
            MetadataWrapper.deployRecentValidationResponse_element response_x;
            Map<String, MetadataWrapper.deployRecentValidationResponse_element> response_map_x = new Map<String, MetadataWrapper.deployRecentValidationResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'deployRecentValidation',
              SOAP_M_URI,
              'deployRecentValidationResponse',
              'MetadataWrapper.deployRecentValidationResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
        public MetadataWrapper.DescribeValueTypeResult describeValueType(String type_x) {
            MetadataWrapper.describeValueType_element request_x = new MetadataWrapper.describeValueType_element();
            request_x.type_x = type_x;
            MetadataWrapper.describeValueTypeResponse_element response_x;
            Map<String, MetadataWrapper.describeValueTypeResponse_element> response_map_x = new Map<String, MetadataWrapper.describeValueTypeResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'describeValueType',
              SOAP_M_URI,
              'describeValueTypeResponse',
              'MetadataWrapper.describeValueTypeResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
        public MetadataWrapper.SaveResult[] createMetadata(MetadataWrapper.Metadata[] metadata) {
            MetadataWrapper.createMetadata_element request_x = new MetadataWrapper.createMetadata_element();
            request_x.metadata = metadata;
            MetadataWrapper.createMetadataResponse_element response_x;
            Map<String, MetadataWrapper.createMetadataResponse_element> response_map_x = new Map<String, MetadataWrapper.createMetadataResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'createMetadata',
              SOAP_M_URI,
              'createMetadataResponse',
              'MetadataWrapper.createMetadataResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
        /*public MetadataWrapper.AsyncResult deploy(String ZipFile,MetadataWrapper.DeployOptions DeployOptions) {
            MetadataWrapper.deploy_element request_x = new MetadataWrapper.deploy_element();
            request_x.ZipFile = ZipFile;
            request_x.DeployOptions = DeployOptions;
            MetadataWrapper.deployResponse_element response_x;
            Map<String, MetadataWrapper.deployResponse_element> response_map_x = new Map<String, MetadataWrapper.deployResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'deploy',
              SOAP_M_URI,
              'deployResponse',
              'MetadataWrapper.deployResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }*/
        public MetadataWrapper.IReadResult readMetadata(String type_x,String[] fullNames) {
            MetadataWrapper.readMetadata_element request_x = new MetadataWrapper.readMetadata_element();
            request_x.type_x = type_x;
            request_x.fullNames = fullNames;
            MetadataWrapper.IReadResponseElement response_x;
            Map<String, MetadataWrapper.IReadResponseElement> response_map_x = new Map<String, MetadataWrapper.IReadResponseElement>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'readMetadata',
              SOAP_M_URI,
              'readMetadataResponse',
              'MetadataWrapper.read' + type_x + 'Response_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.getResult();
        }
        public MetadataWrapper.UpsertResult[] upsertMetadata(MetadataWrapper.Metadata[] metadata) {
            MetadataWrapper.upsertMetadata_element request_x = new MetadataWrapper.upsertMetadata_element();
            request_x.metadata = metadata;
            MetadataWrapper.upsertMetadataResponse_element response_x;
            Map<String, MetadataWrapper.upsertMetadataResponse_element> response_map_x = new Map<String, MetadataWrapper.upsertMetadataResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'upsertMetadata',
              SOAP_M_URI,
              'upsertMetadataResponse',
              'MetadataWrapper.upsertMetadataResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
        public MetadataWrapper.FileProperties[] listMetadata(MetadataWrapper.ListMetadataQuery[] queries,Double asOfVersion) {
            MetadataWrapper.listMetadata_element request_x = new MetadataWrapper.listMetadata_element();
            request_x.queries = queries;
            request_x.asOfVersion = asOfVersion;
            MetadataWrapper.listMetadataResponse_element response_x;
            Map<String, MetadataWrapper.listMetadataResponse_element> response_map_x = new Map<String, MetadataWrapper.listMetadataResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              SOAP_M_URI,
              'listMetadata',
              SOAP_M_URI,
              'listMetadataResponse',
              'MetadataWrapper.listMetadataResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.result;
        }
    }
public class EmailManager {  
    @future
    public static void sendMailWithTemplate1(String address, String[] CcAddresses, String templateId, String targetObjectId, String whatId,String TSN){  
        try{
            system.debug('==sendMailWithTemplate=='+address+'::'+templateId+'::'+targetObjectId+'::'+whatId);
            Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();        
            String[] toAddresses = new String[] {address}; 
            email.setToAddresses(toAddresses); 
            
            List<String> GmailList = New List<String>();
            String[] Groupmail = System.label.GroupEmail.split(',');
            for (String str : Groupmail) {
            GmailList.add(str);
            }
            email.setCcAddresses(GmailList);
            
            email.setBccAddresses(CcAddresses);
            email.setTemplateId(templateId);
            email.setTargetObjectId(targetObjectId);
            email.setWhatId(whatId);
            //List<User_Management__c> UM = [Select Id,Tenant__r.Tenant_Short_Name__c,Profiles__c FROM User_Management__c WHERE User_Email__c=:address Limit 1];
            if(TSN !=''){
            if(TSN == 'ebt')
            {email.setReplyTo('ebt@1usjf1esiyg7n3ntkq4c2kw0rswcc180mt7r14ijsswpbvxjyp.g-6vtsjeag.cs17.apex.sandbox.salesforce.com');
            
            }
            else if(TSN == 'indusind')
            {email.setReplyTo('indusind_notification@25skkvcjlpdpi4mqx8qlw5gzue7qoljdn6jmkirgagb43yuu5.g-6vtsjeag.cs17.apex.sandbox.salesforce.com');
            }
            else if(TSN == 'db')
            {email.setReplyTo('db_notification@2bxntoh2jimx15r3xbaumxoh5wzyxtsrzegdsw27257fopvfwi.2d-8b2luaq.cs69.apex.sandbox.salesforce.com');
            }
            else{}
            }
            email.setSaveAsActivity(false);            
            Messaging.SendEmailResult[] results = Messaging.sendEmail(
            new Messaging.SingleEmailMessage[] { email });            
        } catch(exception ex){
            system.debug('//****// '+ex.getMessage());
        } 
    }
    public void sendMailWithTemplate(String address, String[] CcAddresses, String templateId, String targetObjectId, String whatId){  
        try{
            system.debug('==sendMailWithTemplate=='+address+'::'+templateId+'::'+targetObjectId+'::'+whatId);
            Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();        
            String[] toAddresses = new String[] {address}; 
            email.setToAddresses(toAddresses); 
            //email.setCcAddresses(CcAddresses);
            email.setBccAddresses(CcAddresses);
            email.setTemplateId(templateId);
            email.setTargetObjectId(targetObjectId);
            email.setWhatId(whatId);
            List<User_Management__c> UM = [Select Id,Tenant__r.Tenant_Short_Name__c,Profiles__c FROM User_Management__c WHERE User_Email__c=:address Limit 1];
            if(UM.size()>0){
            if(UM[0].Tenant__r.Tenant_Short_Name__c=='ebt')
            {email.setReplyTo('ebt@1usjf1esiyg7n3ntkq4c2kw0rswcc180mt7r14ijsswpbvxjyp.g-6vtsjeag.cs17.apex.sandbox.salesforce.com');
            
            }
            else if(UM[0].Tenant__r.Tenant_Short_Name__c=='indusind')
            {email.setReplyTo('indusind_notification@25skkvcjlpdpi4mqx8qlw5gzue7qoljdn6jmkirgagb43yuu5.g-6vtsjeag.cs17.apex.sandbox.salesforce.com');
            }
            else if(UM[0].Tenant__r.Tenant_Short_Name__c=='db')
            {email.setReplyTo('db_notification@2bxntoh2jimx15r3xbaumxoh5wzyxtsrzegdsw27257fopvfwi.2d-8b2luaq.cs69.apex.sandbox.salesforce.com');
            }
            else{}
            }
            email.setSaveAsActivity(false);            
            Messaging.SendEmailResult[] results = Messaging.sendEmail(
            new Messaging.SingleEmailMessage[] { email });            
        } catch(exception ex){
            system.debug('//****// '+ex.getMessage());
        } 
    }
    public void sendMailWithTemplateQuote(String address, String[] CcAddresses, String templateId, String targetObjectId, String whatId,String TSN){  
        try{
            system.debug('==sendMailWithTemplate=='+address+'::'+templateId+'::'+targetObjectId+'::'+whatId);
            Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();        
            String[] toAddresses = new String[] {address}; 
            email.setToAddresses(toAddresses); 
            //email.setCcAddresses(CcAddresses);
            email.setBccAddresses(CcAddresses);
            email.setTemplateId(templateId);
            email.setTargetObjectId(targetObjectId);
            email.setWhatId(whatId);
            if(TSN !=''){
            if(TSN == 'ebt')
            {email.setReplyTo('ebt@1usjf1esiyg7n3ntkq4c2kw0rswcc180mt7r14ijsswpbvxjyp.g-6vtsjeag.cs17.apex.sandbox.salesforce.com');
            
            }
            else if(TSN == 'indusind')
            {email.setReplyTo('indusind_notification@25skkvcjlpdpi4mqx8qlw5gzue7qoljdn6jmkirgagb43yuu5.g-6vtsjeag.cs17.apex.sandbox.salesforce.com');
            }
            else if(TSN == 'db')
            {email.setReplyTo('db_notification@2bxntoh2jimx15r3xbaumxoh5wzyxtsrzegdsw27257fopvfwi.2d-8b2luaq.cs69.apex.sandbox.salesforce.com');
            }
            else{}
            }
            email.setSaveAsActivity(false);            
            Messaging.SendEmailResult[] results = Messaging.sendEmail(
            new Messaging.SingleEmailMessage[] { email });            
        } catch(exception ex){
            system.debug('//****// '+ex.getMessage());
        } 
    }
}
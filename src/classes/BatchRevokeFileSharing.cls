/**
 * Class Name - BatchRevokeFileSharing
 * Purpose : To revoke sharing of document after expiration
 **/

global class BatchRevokeFileSharing implements Database.Batchable<sObject>
{
    public String query; 
    
   
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       
        try
        {
                    
            query='Select id from File_Sharing__c Where Expired_On__c=today';
            
            if(test.isRunningTest()) 
                query += ' limit 200';           
              
            system.debug('--Query'+query);
              
            return Database.getQueryLocator(query);     
        }
          
        catch(exception e)
        {
          throw e;
        }
    }
    
    global void execute(Database.BatchableContext BC, List<File_Sharing__c> scope)
    {
        
       
        List<File_Sharing__c> fileShareToDelete = new List<File_Sharing__c>();
        system.debug('inside execute');
        
        
        if (scope.size()>0)
        {
            system.debug('scope=' +scope);
            for(File_Sharing__c fsc : scope)
            {                                   
                fileShareToDelete.add(fsc);                    
                
            }
        }
        try
        {
            system.debug('fileShareToDelete=' +fileShareToDelete);
            delete fileShareToDelete;
        }
        catch(exception e)
        {
            system.debug('exception: ' +e);            
        
        }         
        
    }    
     
    
    global void finish(Database.BatchableContext BC)
    {
    
        
        
    }
}
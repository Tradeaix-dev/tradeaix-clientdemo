public class TransactionTriggerHandler {
<<<<<<< HEAD
    
    Public static void SentMail(List<Transaction__c> NewTrans,Map<ID,Transaction__c> OldTrans)
    { 
        for(Transaction__c t : NewTrans){   
            
            try{ 
                List<String> CcAddresses = new List<String>(); 
                List<String> bCcAddresses = new List<String>(); 
                set<Id> tenantId = new set<Id>();
                System.debug('====Transaction Trigger To Email===='+t.Tenant__c +':::'+system.label.SMCP_ProfileId+':::'+system.label.AMCP);
                List<User_Management__c> lstUM = [SELECT User_Email__c from User_Management__c WHERE Tenant__c=: t.Tenant__c AND (Profiles__c=:system.label.SMCP_ProfileId OR Profiles__c=:system.label.AMCP)];
                String toEmail = lstUM[0].User_Email__c ;
                /*if(t.Status__c.equals('Transaction Approved')){                     
List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: t.CreatedBy__c];                 
for( User_Management__c lstStr: listUserEmail){
CcAddresses.add(lstStr.User_Email__c) ;
}
String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionCreateEmailTemp' LIMIT 1].Id;
em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(t.Id));
}                
If(Trigger.isAfter && Trigger.isUpdate){
if(t.Status__c.equals('Transaction Created')){
List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: t.CreatedBy__c];                 
for( User_Management__c lstStr: listUserEmail){
CcAddresses.add(lstStr.User_Email__c) ;
}
String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionCreateEmailTemp' LIMIT 1].Id;
em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(t.Id));
}
}*/
                
                system.debug('####'+t.RFQChk__c+'####'+OldTrans.get(t.Id).RFQChk__c);
                if((t.RFQChk__c == true && OldTrans.get(t.Id).RFQChk__c == false && t.RFIChk__c == false) || (t.recall__c == false && OldTrans.get(t.Id).recall__c == true && t.RFIChk__c == false && t.RFQChk__c == true) || (t.recall__c == false && OldTrans.get(t.Id).recall__c == true && t.RFIChk__c == true && t.RFQChk__c == true)){ 
                    bCcAddresses.add(toEmail);
                    List<Published_Tenant__c> listPT = [SELECT Id, Name, Tenant__c from Published_Tenant__c WHERE Transaction__c =: t.Id];
                    for(Published_Tenant__c PT:listPT){
                        tenantId.add(PT.Tenant__c);
                    }
                    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionRequestQuoteEmailTemp' LIMIT 1].Id;
                    String temaplateIdlink = [SELECT Id from EmailTemplate WHERE Name = 'TransactionRequestQuoteEmailTempwithlink' LIMIT 1].Id;
                    set<Id> PTId = new set<Id>();
                    set<Id> tuserid = new set<Id>();
                    List<Published_Tenant__c> lstPT = [SELECT Id, Name from Published_Tenant__c WHERE Transaction__c =: t.Id];
                    for(Published_Tenant__c PT: lstPT){
                        PTId.add(PT.Id);
                    }
                    List<Published_Tenant_User__c> lstPTU = [SELECT Id,Published_Tenant__r.Email_Notification__c, Name,Tenant_User__c from Published_Tenant_User__c WHERE Published_Tenant__c =: PTId];
                    set<Id> Emailnoti = new set<Id>();
                    for(Published_Tenant_User__c PTU: lstPTU){
                        if(PTU.Published_Tenant__r.Email_Notification__c)
                        {
                            Emailnoti.add(PTU.Tenant_User__c);
                        }
                        tuserid.add(PTU.Tenant_User__c);
                    }
                    List<CPSelection__c> LstCP = [SELECT id,Tenant_User__c FROM CPSelection__c WHERE Transaction__c=:t.Id];
                    for(CPSelection__c Cp: LstCP ){
                        tuserid.add(Cp.Tenant_User__c);
                    }
                    List<User_Management__c> listUserEmail= [SELECT Id,Tenant__r.Tenant_Short_Name__c,User_Email__c,Profiles__c from User_Management__c WHERE Id =: tuserid];
                    for( User_Management__c lstStr: listUserEmail){
                        //CcAddresses.add(lstStr.User_Email__c) ;
                        if(lstStr.Profiles__c == system.label.OBCP_ProfileID){
                            if(t.recall__c == false && OldTrans.get(t.Id).recall__c == true && t.RFIChk__c == false && t.RFQChk__c == true){
                                Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Re-Request For Quote','',t.TransactionRefNumber__c,t.id);
                            }else{
                                Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Request For Quote','',t.TransactionRefNumber__c,t.id);
                            }      
                            EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , temaplateIdlink , userinfo.getuserid(), string.valueOf(t.Id),'');
                        }else{
                            system.debug('RE-RFQ'+t.recall__c +' &&'+ OldTrans.get(t.Id).recall__c  +' &&'+ t.RFIChk__c  +' &&'+ t.RFQChk__c  );
                            if(t.recall__c == false && OldTrans.get(t.Id).recall__c == true && t.RFIChk__c == false && t.RFQChk__c == true){
                                Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Re-Request For Quote','',t.TransactionRefNumber__c,t.id);
                            }else{
                                Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Request For Quote','',t.TransactionRefNumber__c,t.id);
                            }
                            List<EmailTemplate> ET= [SELECT Id from EmailTemplate WHERE Name =:lstStr.Tenant__r.Tenant_Short_Name__c+'_RFQEmailTemp' LIMIT 1];
                            //if(ET.size()>0 && Emailnoti.contains(lstStr.Id))
                            if(ET.size()>0)
                            {
                                EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , ET[0].Id, userinfo.getuserid(), string.valueOf(t.Id),lstStr.Tenant__r.Tenant_Short_Name__c);
                            }else{
                                EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(t.Id),'');
                            }  
                        }                      
                        
                    }}else{
                        
                        
                        
                    }
                
                if((t.RFIChk__c == true && OldTrans.get(t.Id).RFIChk__c == false) || (t.recall__c == false && OldTrans.get(t.Id).recall__c == true && t.RFIChk__c == true && t.RFQChk__c == false)){ 
                    bCcAddresses.add(toEmail);
                    List<Request_Information_Quote__c> listPT = [SELECT Id, Name, Tenant__c from Request_Information_Quote__c WHERE Transaction__c =: t.Id];
                    for(Request_Information_Quote__c PT:listPT){
                        tenantId.add(PT.Tenant__c);
                    }
                    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionRequestforInfoEmailTemp' LIMIT 1].Id;
                    String temaplateIdFOROTP = [SELECT Id from EmailTemplate WHERE Name = 'TransactionRequestforInfoEmailTempFOROTP' LIMIT 1].Id;
                    set<Id> PTId = new set<Id>();
                    set<Id> tuserid = new set<Id>();
                    List<Published_Tenant__c> lstPT = [SELECT Id, Name from Published_Tenant__c WHERE Transaction__c =: t.Id];
                    for(Published_Tenant__c PT: lstPT){
                        PTId.add(PT.Id);
                    }
                    List<Published_Tenant_User__c> lstPTU = [SELECT Id, Name,Tenant_User__c from Published_Tenant_User__c WHERE Published_Tenant__c =: PTId];
                    for(Published_Tenant_User__c PTU: lstPTU){
                        tuserid.add(PTU.Tenant_User__c);
                    }
                    List<CPSelection__c> LstCP = [SELECT id,Tenant_User__c FROM CPSelection__c WHERE Transaction__c=:t.Id];
                    for(CPSelection__c Cp: LstCP ){
                        tuserid.add(Cp.Tenant_User__c);
                    }
                    List<User_Management__c> listUserEmail= [SELECT Id,Tenant__r.Tenant_Short_Name__c,User_Email__c,Profiles__c from User_Management__c WHERE Id =: tuserid];   
                    for( User_Management__c lstStr: listUserEmail){
                        //CcAddresses.add(lstStr.User_Email__c) ;
                        if(lstStr.Profiles__c == system.label.OBCP_ProfileID){
                            if(t.recall__c == false && OldTrans.get(t.Id).recall__c == true && t.RFIChk__c == true && t.RFQChk__c == false){
                                Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Re-Request For Info','',t.TransactionRefNumber__c,t.id);
                            }else{
                                Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Request For Info','',t.TransactionRefNumber__c,t.id);
                            }
                            
                            EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , temaplateIdFOROTP , userinfo.getuserid(), string.valueOf(t.Id),'');
                        }else{
                            if(t.recall__c == false && OldTrans.get(t.Id).recall__c == true && t.RFIChk__c == true && t.RFQChk__c == false){
                                Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Re-Request For Info','',t.TransactionRefNumber__c,t.id);
                            }else{
                                Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Request For Info','',t.TransactionRefNumber__c,t.id);
                            }
                            List<EmailTemplate> ET= [SELECT Id from EmailTemplate WHERE Name =:lstStr.Tenant__r.Tenant_Short_Name__c+'_RFIEmailTemp' LIMIT 1];
                            if(ET.size()>0)
                            {
                                EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , ET[0].Id, userinfo.getuserid(), string.valueOf(t.Id),lstStr.Tenant__r.Tenant_Short_Name__c);
                            }else{
                                EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(t.Id),'');
                            }
                        }
                    }
                    
                    
                }
                
                if(t.recall__c && OldTrans.get(t.Id).recall__c == false)
                {
                    
                    if(t.RFIChk__c==true && t.RFQChk__c==false)
                    {
                        bCcAddresses.add(toEmail);
                        List<Request_Information_Quote__c> listPT = [SELECT Id, Name, Tenant__c from Request_Information_Quote__c WHERE Transaction__c =: t.Id];
                        for(Request_Information_Quote__c PT:listPT){
                            tenantId.add(PT.Tenant__c);
                        }
                        //String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionRequestforInfoEmailTemprecall' LIMIT 1].Id;
                        
                        set<Id> PTId = new set<Id>();
                        set<Id> tuserid = new set<Id>();
                        List<Published_Tenant__c> lstPT = [SELECT Id, Name from Published_Tenant__c WHERE Transaction__c =: t.Id];
                        for(Published_Tenant__c PT: lstPT){
                            PTId.add(PT.Id);
                        }
                        List<Published_Tenant_User__c> lstPTU = [SELECT Id, Name,Tenant_User__c from Published_Tenant_User__c WHERE Published_Tenant__c =: PTId];
                        for(Published_Tenant_User__c PTU: lstPTU){
                            tuserid.add(PTU.Tenant_User__c);
                        }
                        List<CPSelection__c> LstCP = [SELECT id,Tenant_User__c FROM CPSelection__c WHERE Transaction__c=:t.Id];
                        for(CPSelection__c Cp: LstCP ){
                            tuserid.add(Cp.Tenant_User__c);
                        }
                        List<User_Management__c> listUserEmail= [SELECT Id,Tenant__r.Tenant_Short_Name__c,User_Email__c,Profiles__c from User_Management__c WHERE Id =: tuserid];   
                        for( User_Management__c lstStr: listUserEmail){
                            List<EmailTemplate> ET= [SELECT Id from EmailTemplate WHERE Name =:lstStr.Tenant__r.Tenant_Short_Name__c+'_RecallRFIEmailTemp' LIMIT 1];
                            if(ET.size()>0)
                            {
                                Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Request For Info','',t.TransactionRefNumber__c,t.id);
                                
                                EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , ET[0].Id , userinfo.getuserid(), string.valueOf(t.Id),lstStr.Tenant__r.Tenant_Short_Name__c);
                            }
                        }
                    }
                    else if(t.RFQChk__c)
                    { bCcAddresses.add(toEmail);
                     List<Published_Tenant__c> listPT = [SELECT Id, Name, Tenant__c from Published_Tenant__c WHERE Transaction__c =: t.Id];
                     for(Published_Tenant__c PT:listPT){
                         tenantId.add(PT.Tenant__c);
                     }
                     //String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionRequestQuoteEmailTemprecall' LIMIT 1].Id;
                     
                     set<Id> PTId = new set<Id>();
                     set<Id> tuserid = new set<Id>();
                     List<Published_Tenant__c> lstPT = [SELECT Id, Name from Published_Tenant__c WHERE Transaction__c =: t.Id];
                     for(Published_Tenant__c PT: lstPT){
                         PTId.add(PT.Id);
                     }
                     List<Published_Tenant_User__c> lstPTU = [SELECT Id,Published_Tenant__r.Email_Notification__c, Name,Tenant_User__c from Published_Tenant_User__c WHERE Published_Tenant__c =: PTId];
                     set<Id> Emailnoti = new set<Id>();
                     for(Published_Tenant_User__c PTU: lstPTU){
                         if(PTU.Published_Tenant__r.Email_Notification__c)
                         {
                             Emailnoti.add(PTU.Tenant_User__c);
                         }
                         tuserid.add(PTU.Tenant_User__c);
                     }
                     List<CPSelection__c> LstCP = [SELECT id,Tenant_User__c FROM CPSelection__c WHERE Transaction__c=:t.Id];
                     for(CPSelection__c Cp: LstCP ){
                         tuserid.add(Cp.Tenant_User__c);
                     }
                     List<User_Management__c> listUserEmail= [SELECT Id,Tenant__r.Tenant_Short_Name__c,User_Email__c,Profiles__c from User_Management__c WHERE Id =: tuserid];
                     for( User_Management__c lstStr: listUserEmail){
                         //CcAddresses.add(lstStr.User_Email__c) ;
                         List<EmailTemplate> ET= [SELECT Id from EmailTemplate WHERE Name =:lstStr.Tenant__r.Tenant_Short_Name__c+'_RecallRFQEmailTemp' LIMIT 1];
                         if(ET.size()>0)
                         {
                             Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Recalled','',t.TransactionRefNumber__c,t.id);
                             EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , ET[0].Id , userinfo.getuserid(), string.valueOf(t.Id),lstStr.Tenant__r.Tenant_Short_Name__c);
                         }
                     }                      
                     
                    }
                    
                    
                    
                }
                system.debug('###'+t.Status__c);
                if(t.Status__c.equals('Completed Transaction')){
                    
                    List<Quotes__c> listQuote =[SELECT Bid_Status__c,CounteParty__c,Id,Transaction__c FROM Quotes__c WHERE Transaction__c =: t.Id AND Bid_Status__c = 'Quote Accepted'];
                    system.debug('###'+listQuote);
                    List<User_Management__c> listUserEmail= [SELECT Id,User_Email__c from User_Management__c WHERE Tenant__c=: listQuote[0].CounteParty__c AND Profiles__r.Name ='Buyer Manager Community Plus'];                 
                    for( User_Management__c lstStr: listUserEmail){
                        CcAddresses.add(lstStr.User_Email__c) ;                       
                        Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Execute Agreement','',t.TransactionRefNumber__c,t.id);
                    }
                    List<User_Management__c> listUserEmail1= [SELECT Id,User_Email__c from User_Management__c WHERE Tenant__c=: listQuote[0].CounteParty__c ];   
                    for( User_Management__c lstStr1: listUserEmail1){
                        CcAddresses.add(lstStr1.User_Email__c) ;
                    }
                    //String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionCompletedEmailTemp' LIMIT 1].Id;
                    //EmailManager.sendMailWithTemplate1(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(t.Id));
                    
                    List<String> bcc = new List<String>(); 
                    List<Quotes__c> listOtherBidderQuote =[SELECT CreatedBy__r.User_Email__c,CreatedBy__c,Bid_Status__c,CounteParty__c,Id,Transaction__c,Transaction__r.TransactionRefNumber__c FROM Quotes__c WHERE Transaction__c =: t.Id AND Bid_Status__c = 'Quote Submitted'];
                    String closedtemaplateId = [SELECT Id from EmailTemplate WHERE Name = 'BidClosedEmailForOtherBidder' LIMIT 1].Id;
                    system.debug('====closedtemaplateId=='+closedtemaplateId);
                    for( Quotes__c lstOtherbidder: listOtherBidderQuote){
                        Helper.NotificationInsertCall(t.CreatedBy__c,lstOtherbidder.CreatedBy__c,'Quotes Closed','',t.TransactionRefNumber__c,t.id);
                        system.debug('====enter for loop other bidder=='+lstOtherbidder.CreatedBy__r.User_Email__c);
                        bcc.add(lstOtherbidder.CreatedBy__r.User_Email__c);
                        EmailManager.sendMailWithTemplate1(lstOtherbidder.CreatedBy__r.User_Email__c , bcc , closedtemaplateId , userinfo.getuserid(), string.valueOf(lstOtherbidder.Id),'');
                        bcc.clear();
                    }
                    
                    
                }
            }catch(exception ex){
                system.debug('===TransactionTrigger==='+ex.getMessage());
                system.debug('===TransactionTrigger line number==='+ex.getLineNumber());
            }
        }
    }
    Public static void SentMailAttributeUpdate(List<Transaction_Attributes__c> NewTransAtt,Map<ID,Transaction_Attributes__c> OldTransAtt)
    { 
        
        for(Transaction_Attributes__c t : NewTransAtt){   
            List<Transaction__c> lstTrans = [SELECT Id, Name, Tenant__c, Status__c FROM Transaction__c WHERE Id =: OldTransAtt.get(t.Id).Transaction__c];
            if(lstTrans[0].Status__c == 'Requested For Quote'){
                List<String> CcAddresses = new List<String>(); 
                List<String> bCcAddresses = new List<String>(); 
                set<Id> tenantId = new set<Id>();
                System.debug('====Transaction Trigger To Email===='+t.Tenant__c +':::'+system.label.SMCP_ProfileId+':::'+system.label.AMCP);
                List<User_Management__c> lstUM = [SELECT User_Email__c from User_Management__c WHERE Tenant__c=: lstTrans[0].Tenant__c AND (Profiles__c=:system.label.SMCP_ProfileId OR Profiles__c=:system.label.AMCP)];
                String toEmail = lstUM[1].User_Email__c ;
                bCcAddresses.add(toEmail);
                List<Published_Tenant__c> listPT = [SELECT Id, Name, Tenant__c from Published_Tenant__c WHERE Transaction__c =: lstTrans[0].Id];
                for(Published_Tenant__c PT:listPT){
                    tenantId.add(PT.Tenant__c);
                }
                String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionEditEmailTemp' LIMIT 1].Id;
                set<Id> PTId = new set<Id>();
                set<Id> tuserid = new set<Id>();
                List<Published_Tenant__c> lstPT = [SELECT Id, Name from Published_Tenant__c WHERE Transaction__c =: lstTrans[0].Id];
                for(Published_Tenant__c PT: lstPT){
                    PTId.add(PT.Id);
                }
                List<Published_Tenant_User__c> lstPTU = [SELECT Id,Published_Tenant__r.Email_Notification__c, Name,Tenant_User__c from Published_Tenant_User__c WHERE Published_Tenant__c =: PTId];
                for(Published_Tenant_User__c PTU: lstPTU){
                    tuserid.add(PTU.Tenant_User__c);
                }
                List<CPSelection__c> LstCP = [SELECT id,Tenant_User__c FROM CPSelection__c WHERE Transaction__c=: lstTrans[0].Id];
                for(CPSelection__c Cp: LstCP ){
                    tuserid.add(Cp.Tenant_User__c);
                }
                List<User_Management__c> listUserEmail= [SELECT Id,Tenant__r.Tenant_Short_Name__c,User_Email__c,Profiles__c from User_Management__c WHERE Id =: tuserid];
                for( User_Management__c lstStr: listUserEmail){
                    //CcAddresses.add(lstStr.User_Email__c) ;
                    List<EmailTemplate> ET= [SELECT Id from EmailTemplate WHERE Name =:lstStr.Tenant__r.Tenant_Short_Name__c+'_UpdateRFQEmailTemp' LIMIT 1];
                    if(ET.size()>0)
                    {
                        EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , ET[0].Id , userinfo.getuserid(), string.valueOf(lstTrans[0].Id),lstStr.Tenant__r.Tenant_Short_Name__c);
                        //EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(lstTrans[0].Id));
                    }
                }
                
=======
       
    Public static void SentMail(List<Transaction__c> NewTrans,Map<ID,Transaction__c> OldTrans)
    { 
         for(Transaction__c t : NewTrans){   
       
            try{ 
                List<String> CcAddresses = new List<String>(); 
                List<String> bCcAddresses = new List<String>(); 
                set<Id> tenantId = new set<Id>();
                System.debug('====Transaction Trigger To Email===='+t.Tenant__c +':::'+system.label.SMCP_ProfileId+':::'+system.label.AMCP);
                List<User_Management__c> lstUM = [SELECT User_Email__c from User_Management__c WHERE Tenant__c=: t.Tenant__c AND (Profiles__c=:system.label.SMCP_ProfileId OR Profiles__c=:system.label.AMCP)];
                String toEmail = lstUM[0].User_Email__c ;
                /*if(t.Status__c.equals('Transaction Approved')){                     
                    List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: t.CreatedBy__c];                 
                    for( User_Management__c lstStr: listUserEmail){
                        CcAddresses.add(lstStr.User_Email__c) ;
                    }
                    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionCreateEmailTemp' LIMIT 1].Id;
                    em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(t.Id));
                }                
                If(Trigger.isAfter && Trigger.isUpdate){
                    if(t.Status__c.equals('Transaction Created')){
                        List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: t.CreatedBy__c];                 
                        for( User_Management__c lstStr: listUserEmail){
                            CcAddresses.add(lstStr.User_Email__c) ;
                        }
                        String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionCreateEmailTemp' LIMIT 1].Id;
                        em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(t.Id));
                    }
                }*/
                
              system.debug('####'+t.RFQChk__c+'####'+OldTrans.get(t.Id).RFQChk__c);
                if(t.RFQChk__c == true && OldTrans.get(t.Id).RFQChk__c == false && t.RFIChk__c == false){ 
                    bCcAddresses.add(toEmail);
                    List<Published_Tenant__c> listPT = [SELECT Id, Name, Tenant__c from Published_Tenant__c WHERE Transaction__c =: t.Id];
                    for(Published_Tenant__c PT:listPT){
                        tenantId.add(PT.Tenant__c);
                    }
                    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionRequestQuoteEmailTemp' LIMIT 1].Id;
                    String temaplateIdlink = [SELECT Id from EmailTemplate WHERE Name = 'TransactionRequestQuoteEmailTempwithlink' LIMIT 1].Id;
                    set<Id> PTId = new set<Id>();
                    set<Id> tuserid = new set<Id>();
                    List<Published_Tenant__c> lstPT = [SELECT Id, Name from Published_Tenant__c WHERE Transaction__c =: t.Id];
                    for(Published_Tenant__c PT: lstPT){
                        PTId.add(PT.Id);
                    }
                    List<Published_Tenant_User__c> lstPTU = [SELECT Id,Published_Tenant__r.Email_Notification__c, Name,Tenant_User__c from Published_Tenant_User__c WHERE Published_Tenant__c =: PTId];
                    set<Id> Emailnoti = new set<Id>();
                    for(Published_Tenant_User__c PTU: lstPTU){
                    if(PTU.Published_Tenant__r.Email_Notification__c)
                    {
                    Emailnoti.add(PTU.Tenant_User__c);
                    }
                        tuserid.add(PTU.Tenant_User__c);
                    }
                    List<CPSelection__c> LstCP = [SELECT id,Tenant_User__c FROM CPSelection__c WHERE Transaction__c=:t.Id];
                    for(CPSelection__c Cp: LstCP ){
                        tuserid.add(Cp.Tenant_User__c);
                    }
                    List<User_Management__c> listUserEmail= [SELECT Id,Tenant__r.Tenant_Short_Name__c,User_Email__c,Profiles__c from User_Management__c WHERE Id =: tuserid];
                    for( User_Management__c lstStr: listUserEmail){
                        //CcAddresses.add(lstStr.User_Email__c) ;
                        if(lstStr.Profiles__c == system.label.OBCP_ProfileID){
                            Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Request For Quote','',t.TransactionRefNumber__c,t.id);
                            EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , temaplateIdlink , userinfo.getuserid(), string.valueOf(t.Id),'');
                        }else{
                            Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Request For Quote','',t.TransactionRefNumber__c,t.id);
                            List<EmailTemplate> ET= [SELECT Id from EmailTemplate WHERE Name =:lstStr.Tenant__r.Tenant_Short_Name__c+'_RFQEmailTemp' LIMIT 1];
                            //if(ET.size()>0 && Emailnoti.contains(lstStr.Id))
                            if(ET.size()>0)
                            {
                             EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , ET[0].Id, userinfo.getuserid(), string.valueOf(t.Id),lstStr.Tenant__r.Tenant_Short_Name__c);
                            }else{
                            EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(t.Id),'');
                            }  
                        }                      
                        
                    }
                    
                    
                }
                if(t.RFIChk__c == true && OldTrans.get(t.Id).RFIChk__c == false){ 
                    bCcAddresses.add(toEmail);
                    List<Request_Information_Quote__c> listPT = [SELECT Id, Name, Tenant__c from Request_Information_Quote__c WHERE Transaction__c =: t.Id];
                    for(Request_Information_Quote__c PT:listPT){
                        tenantId.add(PT.Tenant__c);
                    }
                    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionRequestforInfoEmailTemp' LIMIT 1].Id;
                    String temaplateIdFOROTP = [SELECT Id from EmailTemplate WHERE Name = 'TransactionRequestforInfoEmailTempFOROTP' LIMIT 1].Id;
                    set<Id> PTId = new set<Id>();
                    set<Id> tuserid = new set<Id>();
                    List<Published_Tenant__c> lstPT = [SELECT Id, Name from Published_Tenant__c WHERE Transaction__c =: t.Id];
                    for(Published_Tenant__c PT: lstPT){
                        PTId.add(PT.Id);
                    }
                    List<Published_Tenant_User__c> lstPTU = [SELECT Id, Name,Tenant_User__c from Published_Tenant_User__c WHERE Published_Tenant__c =: PTId];
                    for(Published_Tenant_User__c PTU: lstPTU){
                        tuserid.add(PTU.Tenant_User__c);
                    }
                    List<CPSelection__c> LstCP = [SELECT id,Tenant_User__c FROM CPSelection__c WHERE Transaction__c=:t.Id];
                    for(CPSelection__c Cp: LstCP ){
                        tuserid.add(Cp.Tenant_User__c);
                    }
                    List<User_Management__c> listUserEmail= [SELECT Id,Tenant__r.Tenant_Short_Name__c,User_Email__c,Profiles__c from User_Management__c WHERE Id =: tuserid];   
                    for( User_Management__c lstStr: listUserEmail){
                        //CcAddresses.add(lstStr.User_Email__c) ;
                        if(lstStr.Profiles__c == system.label.OBCP_ProfileID){
                            Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Request For Info','',t.TransactionRefNumber__c,t.id);
                            EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , temaplateIdFOROTP , userinfo.getuserid(), string.valueOf(t.Id),'');
                        }else{
                            Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Request For Info','',t.TransactionRefNumber__c,t.id);
                             List<EmailTemplate> ET= [SELECT Id from EmailTemplate WHERE Name =:lstStr.Tenant__r.Tenant_Short_Name__c+'_RFIEmailTemp' LIMIT 1];
                            if(ET.size()>0)
                            {
                             EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , ET[0].Id, userinfo.getuserid(), string.valueOf(t.Id),lstStr.Tenant__r.Tenant_Short_Name__c);
                            }else{
                            EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(t.Id),'');
                            }
                        }
                    }
                    
                    
                }
                
                system.debug('###'+t.Status__c);
                if(t.Status__c.equals('Completed Transaction')){
                
                    List<Quotes__c> listQuote =[SELECT Bid_Status__c,CounteParty__c,Id,Transaction__c FROM Quotes__c WHERE Transaction__c =: t.Id AND Bid_Status__c = 'Quote Accepted'];
                    system.debug('###'+listQuote);
                    List<User_Management__c> listUserEmail= [SELECT Id,User_Email__c from User_Management__c WHERE Tenant__c=: listQuote[0].CounteParty__c AND Profiles__r.Name ='Buyer Manager Community Plus'];                 
                    for( User_Management__c lstStr: listUserEmail){
                        CcAddresses.add(lstStr.User_Email__c) ;                       
                        Helper.NotificationInsertCall(t.CreatedBy__c,lstStr.Id,'Execute Agreement','',t.TransactionRefNumber__c,t.id);
                    }
                    List<User_Management__c> listUserEmail1= [SELECT Id,User_Email__c from User_Management__c WHERE Tenant__c=: listQuote[0].CounteParty__c ];   
                    for( User_Management__c lstStr1: listUserEmail1){
                        CcAddresses.add(lstStr1.User_Email__c) ;
                    }
                    //String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionCompletedEmailTemp' LIMIT 1].Id;
                    //EmailManager.sendMailWithTemplate1(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(t.Id));
                    
                    List<String> bcc = new List<String>(); 
                    List<Quotes__c> listOtherBidderQuote =[SELECT CreatedBy__r.User_Email__c,Bid_Status__c,CounteParty__c,Id,Transaction__c FROM Quotes__c WHERE Transaction__c =: t.Id AND Bid_Status__c = 'Quote Submitted'];
                    String closedtemaplateId = [SELECT Id from EmailTemplate WHERE Name = 'BidClosedEmailForOtherBidder' LIMIT 1].Id;
                    system.debug('====closedtemaplateId=='+closedtemaplateId);
                    for( Quotes__c lstOtherbidder: listOtherBidderQuote){
                        system.debug('====enter for loop other bidder=='+lstOtherbidder.CreatedBy__r.User_Email__c);
                        bcc.add(lstOtherbidder.CreatedBy__r.User_Email__c);
                        EmailManager.sendMailWithTemplate1(lstOtherbidder.CreatedBy__r.User_Email__c , bcc , closedtemaplateId , userinfo.getuserid(), string.valueOf(lstOtherbidder.Id),'');
                        bcc.clear();
                    }
           
                    
                }
            }catch(exception ex){
                system.debug('===TransactionTrigger==='+ex.getMessage());
                system.debug('===TransactionTrigger line number==='+ex.getLineNumber());
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-clientdemo.git
            }
        }
    }
}
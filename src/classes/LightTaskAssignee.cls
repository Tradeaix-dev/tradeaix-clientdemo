public with sharing class LightTaskAssignee {
    public static String query = ''; 
    public static Integer size = 0;
    
    @AuraEnabled 
    public static Organization__c[] loadorgs(String username) {
        return Helper.loadorgs(username);
    }
    
    @AuraEnabled
    public static LightBankListV getloggeduserBrowserTitle(string transid) { 
        LightBankListV bankdetail = new LightBankListV();
        try{
            string tranRefnumber = [SELECT Id,TransactionRefNumber__c from Transaction__c WHERE Id=:transid].TransactionRefNumber__c;
            //return tranRefnumber;
            bankdetail.tranRefnumber = tranRefnumber;
            List<String> lstring = new List<String>();
            Schema.DescribeFieldResult fieldResult = User_Management__c.Country_Code__c.getDescribe();            
            List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();            
            lstring.add('--Select--');
            for(Schema.picklistEntry f:ple){                
                   lstring.add(f.getLabel());
            } 
            bankdetail.countrycodelist = lstring;
        }catch (Exception e) { 
            system.debug('ERROR ===== '+e.getMessage());
            //return e.getMessage();
        }
        return bankdetail;             
    }
}
public class LightSellerOfferListV1 { 
    @AuraEnabled
    public List <Transaction__c> results = new List <Transaction__c>();
    @AuraEnabled
    public Integer counter = 0; 
    @AuraEnabled
    public Integer Tcount = 0; 
    @AuraEnabled
    public Integer ACount = 0; 
    @AuraEnabled
    public String selectedItem = ''; 
    @AuraEnabled
    public String showpage = '';
    @AuraEnabled
    public String sortbyField = '';
    @AuraEnabled
    public String sortDirection = '';
    @AuraEnabled
    public Integer total_page = 0; 
    @AuraEnabled
    public Integer list_size = 0;
    @AuraEnabled
    public Integer total_size = 0;
       @AuraEnabled
    public Boolean isFilterA = false;
    @AuraEnabled
    public UI_Filter__c filter = new UI_Filter__c();
    @AuraEnabled 
    public List<CPSelection__c> CPSelection = new List<CPSelection__c>();
}
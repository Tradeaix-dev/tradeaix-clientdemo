public class LightDocuSignLoginInfoV {
    @AuraEnabled
    public List<LightLoginAccountsV> loginAccounts = new List<LightLoginAccountsV>();  
    @AuraEnabled
    public string apiPassword = ''; 
}
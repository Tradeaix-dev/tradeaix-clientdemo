/**
* Email services are automated processes that use Apex classes
* to process the contents, headers, and attachments of inbound
* email.
*/
global class CreateQuotesFrmEmail implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email,Messaging.InboundEnvelope env){
        try{
            // Create an InboundEmailResult object for returning the result of the 
            // Apex Email Service
            Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
            
            
            if(email.subject!=''){
                if(email.subject.contains('WITHDRAW'))
                {
                    string TName = email.subject.substringAfter('T-').substringBefore(' : ');
                    system.debug('<##>'+TName);
                    Transaction__C[] Trans =[Select TransactionRefNumber__c,Status__c,id FROM Transaction__c  WHERE Name=:'T-'+TName.trim() Limit 1];
                    system.debug('<##>'+Trans);
                    if(Trans.Size()>0){
                        if(Trans[0].Status__c=='Completed Transaction' || Trans[0].Status__c=='Quote Accepted')
                        { 
                            emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'','','');
                        }else{
                            string Qname ='Q-'+email.subject.substringAfter('Q-').substringBefore('-');
                            string QNotes = email.subject.substringAfter('- <').replace('>','');
                            Quotes__c synd = [SELECT Bid_Amount__c,Reason__c,Note__c,Bid_Expiration_Date__c,CounteParty__c,CounteParty__r.Tenant_Site_Name__c,Bid_Service_Fee_bps__c,Bid_Status__c,CreatedById,CreatedById__r.First_Name__c,CreatedById__r.Last_Name__c,CreatedById__c,CreatedBy__c,CreatedDate,Id,Name,Request_Information_Quote__c,Transaction__c FROM Quotes__c WHERE Name=:Qname.trim()];
                            synd.Bid_Status__c = 'Quote Withdrawn';
                            if(QNotes!=''){
                                Helper.ActivityLogInsertCallForRecord(synd.CreatedBy__c,synd.Transaction__c,synd.Name+'- Quote Withdrawn ('+QNotes+')','',false);
                            }else{
                                Helper.ActivityLogInsertCallForRecord(synd.CreatedBy__c,synd.Transaction__c,synd.Name+'- Quote Withdrawn','',false);
                            }
                            update synd;   
                        }
                    }
                }else if(email.subject.contains('WILLING TO QUOTE')){
                    
                    string TName = email.subject.substringAfter('T-').substringBefore(' (');
                    system.debug('<##>'+TName);
                    Transaction__C[] Trans =[Select Tenant__c,TransactionRefNumber__c,Status__c,id FROM Transaction__c  WHERE Name=:'T-'+TName.trim() Limit 1];
                    system.debug('<##>'+Trans);
                    if(Trans.Size()>0){
                        if(Trans[0].Status__c=='Completed Transaction' || Trans[0].Status__c=='Quote Accepted')
                        { 
                            emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'','','');
                        }else{
                            
                            
                            string SubAfter = email.subject.substringAfter(' - ');
                            System.debug('<#2#>'+SubAfter);
                            string repsub = SubAfter.replaceAll('- <',' | ');
                            System.debug('<#3#>'+repsub);
                            string Finaltxt = repsub.replaceAll('> -',',');
                            System.debug('<#res#>'+Finaltxt); 
                            List<String> res = Finaltxt.split(',');
                            
                            string Comments='';
                            string WTQ='';
                            
                            for(String r:res) {
                                System.debug('<#res#>'+r);
                                string Finaltxtres = r.replace(' | ',',');
                                //System.debug('<#res#>'+ Finaltxtres.split(',')[1]);
                                if(Finaltxtres.substringAfter(',')!=''){
                                    if(Finaltxtres.split(',')[0].trim()=='WILLING TO QUOTE')
                                    {
                                        WTQ = Finaltxtres.split(',')[1];
                                    }
                                    if(Finaltxtres.split(',')[0].trim()=='COMMENTS')
                                    {
                                        Comments = Finaltxtres.split(',')[1].replace('>','');
                                    }
                                }
                            }
                            system.debug('###'+Comments);
                            system.debug('###'+WTQ);
                            String tid='';
                            string Q= email.subject.substringAfter(' (').substringBefore(') -').toLowerCase();
                            if(Q!=''){
                                Tenant__c[] Ten = [SELECT id FROM Tenant__c WHERE Tenant_Short_Name__c=:Q Limit 1];
                                tid = Ten[0].id;
                                system.debug('##tid##'+tid);
                            }
                            Request_Information_Quote__c[] requests = [
                                SELECT id,
                                Tenant__c,RFI_Reference__c,WTQ__c,
                                Transaction__c,RevokeAccess__c,RFI__c
                                FROM Request_Information_Quote__c
                                WHERE Transaction__c=: Trans[0].id
                                AND Tenant__c= : tid
                                LIMIT 1 
                            ];
                            system.debug('##requests##'+requests);
                            if(WTQ.toLowerCase()=='YES'){
                                requests[0].WTQ__c='Yes';
                                requests[0].RFI__c=true;
                                requests[0].RevokeAccess__c=false;
                            }else
                            {requests[0].WTQ__c='No';
                             
                            }
                            
                            List<User_Management__c> UM = [Select Id,Profiles__c FROM User_Management__c WHERE User_Email__c=:email.fromAddress Limit 1];
                            system.debug('##UM##'+UM);
                            RequestedNote__c note = new RequestedNote__c();
                            note.CreatedBy__c  =  UM[0].Id;   
                            note.Transaction__c = Trans[0].id;
                            note.Body__c= Comments;
                            note.WTQ__c = 'Willing to Quote : '+requests[0].WTQ__c;
                            note.Request_Information_Quote__c = requests[0].id;
                            note.Tenant__c =tid;
                            List<User_Management__c> UM1 = [Select Id,Profiles__c FROM User_Management__c WHERE Tenant__c=:Trans[0].Tenant__c AND (Profiles__c=:System.label.ACP_ProfileID OR Profiles__c=:System.label.AMCP) Limit 1];
                            system.debug('##UM##'+UM1);
                            if(UM1.Size() >0){
                                if(UM1[0].Profiles__c==System.label.ACP_ProfileID || UM1[0].Profiles__c==System.label.AMCP)
                                {
                                    note.Requested_To__c= UM1[0].Id;
                                    
                                }
                                
                            }
                            upsert note;
                            
                            Helper.ActivityLogInsertCallForRecord(UM[0].Id,Trans[0].id,requests[0].RFI_Reference__c+' - Requested For Info ( Willing to Quote : '+requests[0].WTQ__c+')','',false);
                            Update requests;
                        }}
                }
                else if(email.subject.contains('REVISE'))
                {
                    
                    string TName = email.subject.substringAfter('T-').substringBefore(' :');
                    string Tq = email.subject.substringAfter('Q-').substringBefore('-');
                    system.debug('<##>'+TName);
                    system.debug('<##>'+Tq);
                    Transaction__C[] Trans =[Select CreatedBy__c,Tenant__c,TransactionRefNumber__c,Status__c,id FROM Transaction__c  WHERE Name=:'T-'+TName.trim() Limit 1];
                    system.debug('<##>'+Trans);
                    if(Trans.Size()>0){
                        if(Trans[0].Status__c=='Completed Transaction' || Trans[0].Status__c=='Quote Accepted')
                        { 
                            emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'','','');
                        }else{
                            
                            Quotes__c[] Q =[SELECT id,Name,CounteParty__c,Transaction__c 
                                            FROM Quotes__c WHERE Transaction__c=:Trans[0].id AND Name =:'Q-'+Tq.trim() LIMIT 1];
                             system.debug('<##>'+Q);
                            set<Id> PTId = new set<Id>();
                            set<Id> tuserid = new set<Id>();
                            List<Published_Tenant__c> lstPT = [SELECT Id,Name from Published_Tenant__c Where Tenant__c=:Q[0].CounteParty__c AND Transaction__c=:Trans[0].id Limit 1];
                            for(Published_Tenant__c PTt: lstPT){
                                PTId.add(PTt.Id);
                            }
                             system.debug('<##>');
                            List<Published_Tenant_User__c> lstPTU = [SELECT Id, Name,Tenant_User__c from Published_Tenant_User__c WHERE Published_Tenant__c =: PTId];
                            for(Published_Tenant_User__c PTUu: lstPTU){
                                tuserid.add(PTUu.Tenant_User__c);
                            }
                            system.debug('<##>');
                            List<String> CcAddresses = new List<String>(); 
                            List<String> bCcAddresses = new List<String>(); 
                            List<User_Management__c> lstUM = [SELECT User_Email__c from User_Management__c WHERE Tenant__c=: Trans[0].Tenant__c AND (Profiles__c=:system.label.SMCP_ProfileId OR Profiles__c=:system.label.AMCP)] ;
                            String toEmail  = lstUM[0].User_Email__c ;
                            system.debug('<##>');
                            bCcAddresses.add(toEmail);
                            String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionRequestQuoteEmailTemp' LIMIT 1].Id;
                            String temaplateIdlink = [SELECT Id from EmailTemplate WHERE Name = 'TransactionRequestQuoteEmailTempwithlink' LIMIT 1].Id;
                            system.debug('<##>');
                            List<User_Management__c> listUserEmail= [SELECT Id,Tenant__r.Tenant_Short_Name__c,User_Email__c,Profiles__c from User_Management__c WHERE Id =: tuserid];
                            for( User_Management__c lstStr: listUserEmail){
                                if(lstStr.Profiles__c == system.label.OBCP_ProfileID){
                                     EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , temaplateIdlink , userinfo.getuserid(), string.valueOf(Trans[0].Id),'');
                                }else{
                                    system.debug('<##>');
                                    List<EmailTemplate> ET= [SELECT Id from EmailTemplate WHERE Name =:lstStr.Tenant__r.Tenant_Short_Name__c+'_RFQEmailTemp' LIMIT 1];
                                    //if(ET.size()>0 && Emailnoti.contains(lstStr.Id))
                                    if(ET.size()>0)
                                    {
                                        system.debug('<##>');
                                        EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , ET[0].Id, userinfo.getuserid(), string.valueOf(Trans[0].Id),lstStr.Tenant__r.Tenant_Short_Name__c);
                                    }else{
                                        EmailManager.sendMailWithTemplate1(lstStr.User_Email__c , bCcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(Trans[0].Id),'');
                                    }  }                        
                                
                            }
                        }
                    }
                }
                else{
<<<<<<< HEAD
                    System.debug('###Quote From Email###');
                    string T = email.subject.substringAfter('-');
                    string FT= 'T-'+T.substringBefore(' (');
                    System.debug('<#1#>'+email.subject.substringAfter(' - '));
                    string SubAfter = email.subject.substringAfter(' - ');
                    System.debug('<#2#>'+SubAfter);
                    string repsub = SubAfter.replaceAll('- <',' | ');
                    System.debug('<#3#>'+repsub);
                    string Finaltxt = repsub.replaceAll('> -',',');
                    System.debug('<#res#>'+Finaltxt); 
                    List<String> res = Finaltxt.split(',');
                    System.debug('<#res#>'+res);
                    string Qamt='';
                    string Comments='';
                    string bps='';
                    string cof='';
                    
                    for(String r:res) {
                        System.debug('<#res#>'+r);
                        string Finaltxtres = r.replace(' | ',',');
                        //System.debug('<#res#>'+ Finaltxtres.split(',')[1]);
                        if(Finaltxtres.substringAfter(',')!=''){
                            if(Finaltxtres.split(',')[0].trim()=='QUOTE')
                            {
                                Qamt = Finaltxtres.split(',')[1];
                            }
                            if(Finaltxtres.split(',')[0].trim()=='QUOTE FEE')
                            {
                                bps = Finaltxtres.split(',')[1];
                            }
                            if(Finaltxtres.split(',')[0].trim()=='COF')
                            {
                                 cof = Finaltxtres.split(',')[1].trim();
                            }
                            if(Finaltxtres.split(',')[0].trim()=='COMMENTS')
                            {
                                Comments = Finaltxtres.split(',')[1].replace('>','');
                            }
                        }
                    }
                    
                    Quotes__c[] newQuotes = new Quotes__c[0];
                    
                    try {
                        system.debug('<####>'+FT);
                        Transaction__C[] Trans =[Select TransactionRefNumber__c,Status__c,id FROM Transaction__c  WHERE Name=:FT.trim() Limit 1];
                        if(Trans.Size()>0){
                            Transaction_Attributes__c[] TA = [SELECT Attribute_Value__c FROM Transaction_Attributes__c WHERE Attribute_Name__c='Amount' AND Transaction__c=:Trans[0].id LIMIT 1];
                            Boolean chkflag =false;
                            if(Trans[0].Status__c=='Completed Transaction' || Trans[0].Status__c=='Quote Accepted')
                            {
                                chkflag =true;
                                emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'','','');
                            }
                            else
                            {
                                
                                system.debug('<>'+Qamt);
                                system.debug('<>'+bps);
                                system.debug('<>'+Comments); 
                                system.debug('<>'+cof); 
                                
                                if(Qamt=='' && bps=='')
                                {
                                    chkflag =true;
                                    emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'Both','','');  
                                    system.debug('<IF - >');
                                }else if(Qamt==''){
                                    chkflag =true;
                                    emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'Quote','','');  
                                    
                                }else if(bps==''){
                                    chkflag =true;
                                    emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'Bps','','');
                                }
                                else if(cof == ''){
                                    chkflag =true;
                                    emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'cof','','');
                                }
                                else if(!System.Label.CostofFunds.contains(cof)){
                                    chkflag =true;
                                    emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'cofinvalid','','');
                                }
                                else if(TA.Size()>0){
                                    Integer peramt = (Integer.valueof(TA[0].Attribute_Value__c)/100)*10;
                                    Integer Disamt = Integer.valueof(TA[0].Attribute_Value__c) - peramt;
                                    system.debug('<>'+Integer.valueof(Qamt));
                                    system.debug('<>'+peramt);
                                    system.debug('<>'+Disamt);
                                    
                                    if(Integer.valueof(Qamt) > Integer.valueof(TA[0].Attribute_Value__c) || Integer.valueof(Qamt) < Disamt)
                                    {
                                        chkflag =true;
                                        emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'A',TA[0].Attribute_Value__c,string.valueof(Disamt));
                                    }
                                }
                                if(chkflag==false){
                                    String tid='';
                                    string Q= email.subject.substringAfter(' (').substringBefore(') -').toLowerCase();
                                    if(Q!=''){
                                        Tenant__c[] Ten = [SELECT id FROM Tenant__c WHERE Tenant_Short_Name__c=:Q Limit 1];
                                        tid = Ten[0].id;
                                        system.debug('##tid##'+tid);
                                    }
                                    List<User_Management__c> UM = [Select Id,Profiles__c FROM User_Management__c WHERE User_Email__c=:email.fromAddress Limit 1];
                                    
                                    system.debug('<ELSE - >');
                                    newQuotes.add(new 
                                                  Quotes__c(
                                                      Bid_Amount__c = Decimal.ValueOf(Qamt),
                                                      Bid_Expiration_Date__c = system.today()+3,
                                                      Bid_Service_Fee_bps__c = Decimal.ValueOf(bps),
                                                      Transaction__c = Trans[0].id,
                                                      Bid_Status__c = 'Quote Submitted', 
                                                      CounteParty__c = tid,
                                                      Note__c = Comments,
                                                      Cost_of_Funds__c = cof,
                                                      CreatedBy__c =UM[0].Id
                                                  ));
                                    
                                    insert newQuotes;   
                                    Quotes__c[] insersynd =[SELECT id,Name,Bid_Status__c,Transaction__c,Transaction__r.Tenant__c 
                                                            FROM Quotes__c WHERE id=:newQuotes[0].Id ORDER by CreatedDate desc LIMIT 1];
                                    User_Management__c[] userList = [SELECT Id,First_Name__c,Last_Name__c from User_Management__c WHERE Id =:UM[0].Id];
                                    
                                    Helper.ActivityLogInsertCallForRecord(userList[0].Id,Trans[0].id,insersynd[0].Name+'- Quote Submitted',userList[0].First_Name__c+' '+userList[0].Last_Name__c+'- Quote Submitted.',false);  
                                    Quotes__c[] syndupdateinactive =[SELECT id,Name,Bid_Status__c,Transaction__c 
                                                                     FROM Quotes__c WHERE Transaction__c=:Trans[0].id 
                                                                     AND id !=:newQuotes[0].id AND Bid_Status__c ='Quote Submitted' 
                                                                     AND CreatedBy__c=:UM[0].Id ORDER by CreatedDate desc LIMIT 1];
                                    if(syndupdateinactive.Size() >0)
                                    {
                                        Quotes__c sy =new Quotes__c();
                                        sy.Bid_Status__c='Quote Inactivated';
                                        sy.id=syndupdateinactive[0].id;
                                        Helper.ActivityLogInsertCallForRecord(userList[0].Id,syndupdateinactive[0].Transaction__c,syndupdateinactive[0].Name+'- Quote Inactivated',userList[0].First_Name__c+' '+userList[0].Last_Name__c+'- bid inactivated.',false);   
                                        update sy;
                                    } 
                                    System.debug('New Quotes Object: ' + newQuotes);  
                                }
                            } 
                        }
                    }
                    catch (QueryException e) {
                        System.debug('Query Issue: ' + e);
                    }
                    result.success = true;
                }
                return result;
            }
        }
        catch (Exception e) {
            System.debug('Query Issue: ' + e);
        }
        return null;
    }
    public static void emailSendforIncorrectformat(string email, string TRefNo, string Flag,string Tamt,string bidamt){
        // Step 0: Create a master list to hold the emails we'll send
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();  
        
        // Step 1: Create a new Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        // Step 2: Set list of people who should get the email
        List<String> sendTo = new List<String>();
        sendTo.add(email);
        mail.setToAddresses(sendTo);
        
        // Step 3: Set who the email is sent from
        mail.setReplyTo('satheesh@aixchane.co.in');
        mail.setSenderDisplayName('TradeAix');
        String body ='';
        if(Flag=='Both'){
            mail.setSubject('Required QUOTE and QUOTE FEE : '+TRefNo); 
            
            body = 'Hi '+', <br/><br/>';
            body += 'QUOTE and QUOTE FEE Should not be blank.<br/><br/>';            
            body +='<br/><br/>'; 
            body +='Thanks<br/>';
            body +='MNC TradeAix Team';
        }else if(Flag=='Quote'){
            mail.setSubject('Required QUOTE : '+TRefNo); 
            
            body = 'Hi '+', <br/><br/>';
            body += 'QUOTE Should not be blank.<br/><br/>';            
            body +='<br/><br/>'; 
            body +='Thanks<br/>';
            body +='MNC TradeAix Team';
        }else if(Flag=='A'){
            mail.setSubject('Quote Amount Error : '+TRefNo); 
            
            body = 'Hi '+', <br/><br/>';
            body += 'The quote amount should be greater than 90% OR equal to the transaction amount.<br/><br/>';            
            body +='<br/><br/>'; 
            body +='Thanks<br/>';
            body +='MNC TradeAix Team';
        }
        else if(Flag=='Bps'){
            mail.setSubject('Required QUOTE FEE : '+TRefNo); 
            
            body = 'Hi '+', <br/><br/>';
            body += 'QUOTE FEE Should not be blank.<br/><br/>';            
            body +='<br/><br/>'; 
            body +='Thanks<br/>';
            body +='MNC TradeAix Team';
        }
        else if(Flag=='cof'){
            mail.setSubject('Required COF : '+TRefNo); 
            
            body = 'Hi '+', <br/><br/>';
            body += 'COF Should not be blank.<br/><br/>';            
            body +='<br/><br/>'; 
            body +='Thanks<br/>';
            body +='MNC TradeAix Team';
        }
        else if(Flag=='cofinvalid'){
            mail.setSubject('COF Error :: '+TRefNo); 
            
            body = 'Hi '+', <br/><br/>';
            body += 'COF = Cost of Funds must be LIBOR or 1MLibor or 3MLibor or 6 M LIBOR or Not Applicable<br/><br/>';            
            body +='<br/><br/>'; 
            body +='Thanks<br/>';
            body +='MNC TradeAix Team';
        }
        else
        {
            mail.setSubject('Quote Closed : '+TRefNo); 
            
            body = 'Hi '+', <br/><br/>';
            body += 'MNC has closed the quotes for Trade Finance transaction. Your quote was unsuccessful. We thank you for your participation.<br/><br/>';            
=======
                    string T = email.subject.substringAfter('-');
                    string FT= 'T-'+T.substringBefore(' (');
                    System.debug('<#1#>'+email.subject.substringAfter(' - '));
                    string SubAfter = email.subject.substringAfter(' - ');
                    System.debug('<#2#>'+SubAfter);
                    string repsub = SubAfter.replaceAll('- <',' | ');
                    System.debug('<#3#>'+repsub);
                    string Finaltxt = repsub.replaceAll('> -',',');
                    System.debug('<#res#>'+Finaltxt); 
                    List<String> res = Finaltxt.split(',');
                    System.debug('<#res#>'+res);
                    string Qamt='';
                    string Comments='';
                    string bps='';
                    
                    for(String r:res) {
                        System.debug('<#res#>'+r);
                        string Finaltxtres = r.replace(' | ',',');
                        //System.debug('<#res#>'+ Finaltxtres.split(',')[1]);
                        if(Finaltxtres.substringAfter(',')!=''){
                            if(Finaltxtres.split(',')[0].trim()=='QUOTE')
                            {
                                Qamt = Finaltxtres.split(',')[1];
                            }
                            if(Finaltxtres.split(',')[0].trim()=='QUOTE FEE')
                            {
                                bps = Finaltxtres.split(',')[1];
                            }
                            if(Finaltxtres.split(',')[0].trim()=='COMMENTS')
                            {
                                Comments = Finaltxtres.split(',')[1].replace('>','');
                            }
                        }
                    }
                    
                    Quotes__c[] newQuotes = new Quotes__c[0];
                    
                    try {
                        system.debug('<####>'+FT);
                        Transaction__C[] Trans =[Select TransactionRefNumber__c,Status__c,id FROM Transaction__c  WHERE Name=:FT.trim() Limit 1];
                        if(Trans.Size()>0){
                            Transaction_Attributes__c[] TA = [SELECT Attribute_Value__c FROM Transaction_Attributes__c WHERE Attribute_Name__c='Amount' AND Transaction__c=:Trans[0].id LIMIT 1];
                            Boolean chkflag =false;
                            if(Trans[0].Status__c=='Completed Transaction' || Trans[0].Status__c=='Quote Accepted')
                            {
                                chkflag =true;
                                emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'','','');
                            }
                            else
                            {
                                
                                system.debug('<>'+Qamt);
                                system.debug('<>'+bps);
                                system.debug('<>'+Comments); 
                                
                                if(Qamt=='' && bps=='')
                                {
                                    chkflag =true;
                                    emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'Both','','');  
                                    system.debug('<IF - >');
                                }else if(Qamt==''){
                                    chkflag =true;
                                    emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'Quote','','');  
                                    
                                }else if(bps==''){
                                    chkflag =true;
                                    emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'Bps','','');
                                }
                                else if(TA.Size()>0){
                                    Integer peramt = (Integer.valueof(TA[0].Attribute_Value__c)/100)*10;
                                    Integer Disamt = Integer.valueof(TA[0].Attribute_Value__c) - peramt;
                                    system.debug('<>'+Integer.valueof(Qamt));
                                    system.debug('<>'+peramt);
                                    system.debug('<>'+Disamt);
                                    
                                    if(Integer.valueof(Qamt) > Integer.valueof(TA[0].Attribute_Value__c) || Integer.valueof(Qamt) < Disamt)
                                    {
                                        chkflag =true;
                                        emailSendforIncorrectformat(email.fromAddress,Trans[0].TransactionRefNumber__c,'A',TA[0].Attribute_Value__c,string.valueof(Disamt));
                                    }
                                }
                                if(chkflag==false){
                                    String tid='';
                                    string Q= email.subject.substringAfter(' (').substringBefore(') -').toLowerCase();
                                    if(Q!=''){
                                        Tenant__c[] Ten = [SELECT id FROM Tenant__c WHERE Tenant_Short_Name__c=:Q Limit 1];
                                        tid = Ten[0].id;
                                        system.debug('##tid##'+tid);
                                    }
                                    List<User_Management__c> UM = [Select Id,Profiles__c FROM User_Management__c WHERE User_Email__c=:email.fromAddress Limit 1];
                                    
                                    system.debug('<ELSE - >');
                                    newQuotes.add(new 
                                                  Quotes__c(
                                                      Bid_Amount__c = Decimal.ValueOf(Qamt),
                                                      Bid_Expiration_Date__c = system.today()+3,
                                                      Bid_Service_Fee_bps__c = Decimal.ValueOf(bps),
                                                      Transaction__c = Trans[0].id,
                                                      Bid_Status__c = 'Quote Submitted', 
                                                      CounteParty__c = tid,
                                                      Note__c = Comments,
                                                      CreatedBy__c =UM[0].Id
                                                  ));
                                    
                                    insert newQuotes;   
                                    Quotes__c[] insersynd =[SELECT id,Name,Bid_Status__c,Transaction__c,Transaction__r.Tenant__c 
                                                            FROM Quotes__c WHERE id=:newQuotes[0].Id ORDER by CreatedDate desc LIMIT 1];
                                    User_Management__c[] userList = [SELECT Id,First_Name__c,Last_Name__c from User_Management__c WHERE Id =:UM[0].Id];
                                    
                                    Helper.ActivityLogInsertCallForRecord(userList[0].Id,Trans[0].id,insersynd[0].Name+'- Quote Submitted',userList[0].First_Name__c+' '+userList[0].Last_Name__c+'- Quote Submitted.',false);  
                                    Quotes__c[] syndupdateinactive =[SELECT id,Name,Bid_Status__c,Transaction__c 
                                                                     FROM Quotes__c WHERE Transaction__c=:Trans[0].id 
                                                                     AND id !=:newQuotes[0].id AND Bid_Status__c ='Quote Submitted' 
                                                                     AND CreatedBy__c=:UM[0].Id ORDER by CreatedDate desc LIMIT 1];
                                    if(syndupdateinactive.Size() >0)
                                    {
                                        Quotes__c sy =new Quotes__c();
                                        sy.Bid_Status__c='Quote Inactivated';
                                        sy.id=syndupdateinactive[0].id;
                                        Helper.ActivityLogInsertCallForRecord(userList[0].Id,syndupdateinactive[0].Transaction__c,syndupdateinactive[0].Name+'- Quote Inactivated',userList[0].First_Name__c+' '+userList[0].Last_Name__c+'- bid inactivated.',false);   
                                        update sy;
                                    } 
                                    System.debug('New Quotes Object: ' + newQuotes);  
                                }
                            } 
                        }
                    }
                    catch (QueryException e) {
                        System.debug('Query Issue: ' + e);
                    }
                    result.success = true;
                }
                return result;
            }
        }
        catch (Exception e) {
            System.debug('Query Issue: ' + e);
        }
        return null;
    }
    public static void emailSendforIncorrectformat(string email, string TRefNo, string Flag,string Tamt,string bidamt){
        // Step 0: Create a master list to hold the emails we'll send
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();  
        
        // Step 1: Create a new Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        // Step 2: Set list of people who should get the email
        List<String> sendTo = new List<String>();
        sendTo.add(email);
        mail.setToAddresses(sendTo);
        
        // Step 3: Set who the email is sent from
        mail.setReplyTo('satheesh@aixchane.co.in');
        mail.setSenderDisplayName('TradeAix');
        String body ='';
        if(Flag=='Both'){
            mail.setSubject('Required QUOTE and QUOTE FEE : '+TRefNo); 
            
            body = 'Hi '+', <br/><br/>';
            body += 'QUOTE and QUOTE FEE Should not be blank.<br/><br/>';            
            body +='<br/><br/>'; 
            body +='Thanks<br/>';
            body +='JSL TradeAix Team';
        }else if(Flag=='Quote'){
            mail.setSubject('Required QUOTE : '+TRefNo); 
            
            body = 'Hi '+', <br/><br/>';
            body += 'QUOTE Should not be blank.<br/><br/>';            
            body +='<br/><br/>'; 
            body +='Thanks<br/>';
            body +='JSL TradeAix Team';
        }else if(Flag=='A'){
            mail.setSubject('Quote Amount Error : '+TRefNo); 
            
            body = 'Hi '+', <br/><br/>';
            body += 'The quote amount should be greater than 90% OR equal to the transaction amount.<br/><br/>';            
            body +='<br/><br/>'; 
            body +='Thanks<br/>';
            body +='JSL TradeAix Team';
        }
        else if(Flag=='Bps'){
            mail.setSubject('Required QUOTE FEE : '+TRefNo); 
            
            body = 'Hi '+', <br/><br/>';
            body += 'QUOTE FEE Should not be blank.<br/><br/>';            
            body +='<br/><br/>'; 
            body +='Thanks<br/>';
            body +='JSL TradeAix Team';
        }else
        {
            mail.setSubject('Quote Closed : '+TRefNo); 
            
            body = 'Hi '+', <br/><br/>';
            body += 'Jindal has closed the quotes for Trade Finance transaction. Your quote was unsuccessful. We thank you for your participation.<br/><br/>';            
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-clientdemo.git
            body +='<br/><br/>'; 
            body +='Thanks<br/>';
            body +='JSL TradeAix Team';
        }
        mail.setHtmlBody(body);
        
        // Step 5. Add your email to the master list
        mails.add(mail);
        
        // Step 6: Send all emails in the master list
        Messaging.sendEmail(mails);
    }
}
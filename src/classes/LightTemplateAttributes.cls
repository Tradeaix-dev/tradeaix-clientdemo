public with sharing class LightTemplateAttributes {
    public static String query = ''; 
    public static String Countquery = ''; 
    public static Integer size = 0; 
    @AuraEnabled 
    public static Organization__c[] loadorgs(String username) {
        return Helper.loadorgs(username);
    }
    @AuraEnabled
    public static String getloggeduserBrowserTitle() {      
        return '';
    }
    @AuraEnabled
    public static LightTemplateAttributesV Filter(Integer counter, String sortbyField, String sortDirection, String selected,String userid,String Filtervalue) { 
        //Helper.setSessionValue('sessionselected', selected);
        return ShowTable(counter, sortbyField, sortDirection, selected,userid,Filtervalue); 
    }
    private static LightTemplateAttributesV ShowTable(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue){ 
        LightTemplateAttributesV retValue = new LightTemplateAttributesV();
        try{
            system.debug('##'+selected);
            String sessionselectedchk='';
            if((String)Cache.Session.get('sessionselectedTA') != null)  
                sessionselectedchk= '';  
            String whereCondition = ''; 
            if(selected == 'Active' || selected == '' || selected==null){
                whereCondition = ' WHERE IsActive__c = True';
                
            }
            else if(selected == 'All'){
                whereCondition = ' WHERE (IsActive__c = True OR IsActive__c = false)';
            }
            else if(selected == 'Inactive'){
                whereCondition = ' WHERE IsActive__c = false';
            }
            if(Filtervalue!='All Realms' && Filtervalue!=null)
            {
                String setTids='(';
                if(Filtervalue.startsWith('a0M')){
                    
                    for(Tenant__c T:[SELECT id FROM Tenant__c WHERE Organization__c=:Filtervalue])
                    {
                        setTids =setTids +'\''+T.id+'\',';                        
                    }
                    setTids =setTids+')';
                    if(setTids != '(')
                    {
                        system.debug('##'+setTids);
                        String FinalsetTids = setTids.replace(',)', ')');
                        system.debug('#whereCondition#'+whereCondition);
                        if(whereCondition!=''){
                            whereCondition = whereCondition + ' AND Tenant__c IN'+FinalsetTids;
                        }else{
                            whereCondition = whereCondition + ' Tenant__c IN'+FinalsetTids;
                        }
                        
                        
                    }
                }else{
                    if(whereCondition!=''){
                        whereCondition = whereCondition + ' AND Tenant__c=\''+Filtervalue+'\'';
                    }else{
                        whereCondition = whereCondition + ' Tenant__c=\''+Filtervalue+'\'';
                    }
                }
            }
            size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'TemplateAttributes' AND Grid_Name__c =: 'TemplateAttributes']; 
            
            if(size > 0){
                retValue.list_size = Integer.valueOf([SELECT List_Size__c from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'TemplateAttributes' AND Grid_Name__c =: 'TemplateAttributes'][0].List_Size__c);
            }
            else {
                retValue.list_size = 25;
            }
            List<User_Management__c> lstName= [Select Id,Tenant__c,Tenant__r.Tenant_Short_Name__c,Tenant__r.Tenant_Logo_Url__c,First_Name__c,Last_Name__c,Profiles__c from User_Management__c where Id =: userid LIMIT 1];
            String query = 'SELECT Id, Name,Mandatorys__c,Attribute_Size__c,CreatedBy.name, ProductType__c, Attribute_Type__c, IsActive__c, IsPrimary__c, IsSecondary__c, CreatedDate,CreatedByName__r.First_Name__c,CreatedByName__r.Last_Name__c FROM Attributes__c';
            system.debug('@@'+lstName[0].Profiles__c);
            if(lstName[0].Profiles__c==System.label.ACP_ProfileID)
            {
                query = query + whereCondition + ' ORDER BY ' + sortbyField + ' ' + sortDirection + ' LIMIT ' + retValue.list_size + ' offset ' + counter;
                Countquery = 'SELECT count() FROM Attributes__c' + whereCondition;
            }else{
                query = query + whereCondition + ' AND ( Tenant__c=\''+lstName[0].Tenant__c+'\' OR Tenant__c=\'\') ORDER BY ' + sortbyField + ' ' + sortDirection + ' LIMIT ' + retValue.list_size + ' offset ' + counter;
                Countquery = 'SELECT count() FROM Attributes__c' + whereCondition +' AND ( Tenant__c=\''+lstName[0].Tenant__c+'\' OR Tenant__c=\'\')';  
            }system.debug('@@'+query);
            retValue.results = Database.query(query);
            retValue.total_size = Database.countquery(Countquery);
            retValue.counter = counter; 
            retValue.sortbyField = sortbyField; 
            retValue.sortDirection = sortDirection; 
            retValue.total_page = getTotalPages(retValue.list_size, retValue.total_size);
            retValue.selectedItem = (selected == '') ? 'Active': selected;
        } catch(Exception ex){
            System.debug('LightTemplateAttributes - ShowTable() : ' + ex.getMessage());          
            
        }
        
        return retValue;    
    }
    
    @AuraEnabled
    public static LightTemplateAttributesV getTemplateAttributesList(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue){           
        LightTemplateAttributesV retValue = new LightTemplateAttributesV(); 
        try{  
            if((String)Cache.Session.get('sortbyFieldSOL') != null)  
                sortbyField = '';  
            
            if((String)Cache.Session.get('sortDirectionSOL') != null)  
                sortDirection = '';
            retValue = ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue);  
        } catch(Exception ex){
            System.debug('LightTemplateAttributes - ShowTable() : ' + ex.getMessage());            
        }
        return retValue;
    } 
    
    @AuraEnabled
    public static LightTemplateAttributesV getBeginning(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue) { 
        return ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue); 
    }
    
    @AuraEnabled
    public static LightTemplateAttributesV Views(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue) { 
        
        return ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue); 
    }
    
    @AuraEnabled
    public static LightTemplateAttributesV getPrevious(Integer counter, String sortbyField, String sortDirection, String selected,String userid,String Filtervalue) {
        return ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue); 
    }
    
    @AuraEnabled
    public static LightTemplateAttributesV getNext(Integer counter, String sortbyField, String sortDirection, String selected,String userid,String Filtervalue) {  
        return ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue); 
    }
    
    @AuraEnabled
    public static LightTemplateAttributesV getEnd(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue) { 
        return ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue); 
    } 
    
    private static Integer getTotalPages(Integer list_size, Integer total_size) { 
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        }
        else {
            return (total_size/list_size);
        }
    }  
    
    @AuraEnabled
    public static LightTemplateAttributesV changelist_size(Integer counter, String sortbyField, String sortDirection, Integer list_size, String selected, String userid,String Filtervalue) { 
        LightTemplateAttributesV retValue = new LightTemplateAttributesV(); 
        try{
            size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'TemplateAttributes' AND Grid_Name__c =: 'TemplateAttributes'];
            if(size > 0){  
                UI_List_Size__c listSize = new UI_List_Size__c(
                    Id = [SELECT Id from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'TemplateAttributes' AND Grid_Name__c =: 'TemplateAttributes'][0].Id,
                    List_Size__c = list_size  
                );
                update listSize;
            }
            else {
                UI_List_Size__c listSize = new UI_List_Size__c(
                    List_Size__c = list_size,
                    Name = 'TemplateAttributes',
                    Grid_Name__c = 'TemplateAttributes',
                    User__c = UserInfo.getUserId()
                );
                insert listSize;
            }  
            
            retValue = ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue);
            
        } catch(Exception ex){ 
            system.debug('===changelist_size=='+ex.getMessage());
        }
        return retValue;
    }
    
    @AuraEnabled
    public static LightTemplateAttributesV SortTable(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue) { 
        LightTemplateAttributesV retValue = new LightTemplateAttributesV(); 
        try{
            if(sortbyField == '') {
                sortbyField = 'LastModifiedDate';
                sortDirection = 'DESC';
            } else {
                if(sortDirection.equals('ASC')){
                    sortDirection = 'DESC';
                } else {
                    sortDirection = 'ASC';
                }
            }
            //Helper.setSessionValue('sortbyFieldSOL', sortbyField);
            // Helper.setSessionValue('sortDirectionSOL', sortDirection); 
            retValue = ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue); 
        } catch (Exception ex){ 
            system.debug('Error - LightTemplateAttributes - SortTable()'+ex.getMessage());
        }
        return retValue;
    } 
    
    @AuraEnabled
    public static LightTemplateAttributesV changePage(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue) {
        return ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue); 
    }
    
    @AuraEnabled
    public static String CheckTemplateAttributes(String attributeName,String Tenant,String Type) { 
        String response = '';
        try {  
            Integer count = [SELECT count() FROM Attributes__c WHERE Name = : attributeName AND Tenant__c=:Tenant AND ProductType__c=:Type]; 
            if(count > 0){
                response = 'Already Used';
            }
            else { 
                response = '';
            }  
        } catch(Exception ex){
            System.debug('LightTemplateAttributes - CheckTemplateAttributes() : ' + ex.getMessage());           
            
        }
        return response; 
    } 
    
    @AuraEnabled
    public static String SaveAttribute(String Name, String AttributeType, String ProductType, String AttributeSize, String IsActive, String IsPrimary, String IsSecondary,String Ismandatory, String Isfunded, String Isunfunded, String TenantId, String LoggeduserTid) {
        String response = '';
        try { 
            Boolean man;
            if(Ismandatory=='Yes')
            {
                man = true; 
            }
            else{
                man = false; 
            }
                Attributes__c attributeNew = new Attributes__c(
                Name = Name,
                Attribute_Type__c = AttributeType,
                ProductType__c = ProductType,                
                IsActive__c = Boolean.valueOf(IsActive),
                IsPrimary__c = Boolean.valueOf(IsPrimary),    
                IsSecondary__c = Boolean.valueOf(IsSecondary),
                Mandatorys__c = Ismandatory,
                Mandatory__c = man,
                Funded__c = Boolean.valueOf(Isfunded),
                UnFunded__c = Boolean.valueOf(Isunfunded),    
                CreatedByName__c=TenantId,
                LastModifiedByName__c=TenantId,
                Tenant__c = LoggeduserTid
            ); 
            System.debug('AttributeSize'+AttributeSize);
            if(AttributeSize != '')
                attributeNew.Attribute_Size__c = Decimal.valueOf(AttributeSize);
            
            upsert attributeNew;
            response = attributeNew.Id; 
            Helper.ActivityLogInsertCallForRecord(TenantId,attributeNew.Id,'Attribute Insert',Name+' Created.',false);
        } catch(Exception ex){
            System.debug('LightTemplateAttributes - SaveAttribute() : ' + ex.getMessage());
            Helper.ActivityLogInsertCall('','Attribute Insert-Error',ex.getMessage(),true);
        }
        return response; 
    }  
}
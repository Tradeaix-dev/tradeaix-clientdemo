public with sharing class LightCounterpartybank {
    public static String query = ''; 
    public static Integer size = 0; 
     @AuraEnabled 
    public static Organization__c[] loadorgs(String username) {
        return Helper.loadorgs(username);
    }
    @AuraEnabled
    public static LightBankListV getloggeduserBrowserTitle(string transid) { 
        LightBankListV bankdetail = new LightBankListV();
        try{
            string tranRefnumber = [SELECT Id,TransactionRefNumber__c from Transaction__c WHERE Id=:transid].TransactionRefNumber__c;
            //return tranRefnumber;
            bankdetail.tranRefnumber = tranRefnumber;
            List<String> lstring = new List<String>();
            Schema.DescribeFieldResult fieldResult = User_Management__c.Country_Code__c.getDescribe();            
            List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();            
            lstring.add('--Select--');
            for(Schema.picklistEntry f:ple){                
                   lstring.add(f.getLabel());
            } 
            bankdetail.countrycodelist = lstring;
            List<String> countrystring = new List<String>();
            Schema.DescribeFieldResult countryfieldResult = User_Management__c.Country_List__c.getDescribe();            
            List<Schema.picklistEntry> clist = countryfieldResult.getPicklistValues();            
            countrystring.add('--Select--');
            for(Schema.picklistEntry f:clist){                
                   countrystring.add(f.getLabel());
            } 
            bankdetail.countrylist = countrystring;
        }catch (Exception e) { 
            system.debug('ERROR ===== '+e.getMessage());
            //return e.getMessage();
        }
        return bankdetail;
        /*String Btitle = '';
        try {  
            List<Credit_Union_User__c> lstUsers = [SELECT Credit_Union__c,User__c FROM Credit_Union_User__c WHERE User__c =:userinfo.getUserId()];
            Btitle = [SELECT Browser_Title__c FROM Credit_Union__c WHERE Id =: lstUsers[0].Credit_Union__c].Browser_Title__c;
        }
        catch (Exception e) { 
            system.debug('ERROR ===== '+e.getMessage());
        }*/        
    }
    @AuraEnabled
    public static string addCounterPartiesname(String LoggedUserID,Tenant__c saveCreditunion,User_Management__c UM,string PName,string PID,string Flag,String TransId){
        String retValue = '';
        try{
            Organization__c OZ =New Organization__c();
            OZ.Name = saveCreditunion.Tenant_Site_Name__c;
            OZ.CreatedFromOTP__c =true;
            Insert OZ;
            saveCreditunion.Tenant_Type__c = 'Bank';
            saveCreditunion.Tenant_Bank_Type__c = 'Counterparty Bank';
            saveCreditunion.CreatedBy__c = LoggedUserID;
            saveCreditunion.LastModifiedBy__c = LoggedUserID;
            saveCreditunion.Tenant_Short_Name__c= 'admin';//saveCreditunion.Tenant_Site_Name__c.split (' ')[0];
            saveCreditunion.Organization__c = OZ.Id;
            saveCreditunion.CreatedFromOTP__c =true;
            Insert saveCreditunion;
            User_Management__c[] UserName = [SELECT First_Name__c,Last_Name__c FROM User_Management__c WHERE id=:LoggedUserID Limit 1];
            Helper.ActivityLogInsertCallForRecord(LoggedUserID,saveCreditunion.id,'New Tenant',UserName[0].First_Name__c+' '+UserName[0].Last_Name__c+' is Created New Tenant - '+saveCreditunion.Tenant_Site_Name__c,false);
            UM.CreatedFromrapid__c =true;
            UM.Active__c = true;
            UM.User_Name__c = UM.User_Email__c;
            UM.Tenant__c=saveCreditunion.Id;
            UM.Profiles__c =System.label.OBCP_ProfileID;
            UM.User_Profile__c = 'Counterparty Manager';
            UM.CreatedBy__c = LoggedUserID;
            UM.LastModifiedBy__c = LoggedUserID;
            if(Flag =='IC'){
                UM.Invited_By__c = LoggedUserID;
            }
            if(Flag =='TA'){
                UM.Task_Assignee__c = true;
            }
            if(UM.Country_Code__c == '--Select--'){
                UM.Country_Code__c = null;
            }
            Insert UM;
            if(Flag =='TA'){
                Helper.ActivityLogInsertCallForRecord(LoggedUserID,UM.id,'New Tenant User',UserName[0].First_Name__c+' '+UserName[0].Last_Name__c+' is Created New Assignee - '+UM.First_Name__c+' '+UM.Last_Name__c,false);
            }else{
                Helper.ActivityLogInsertCallForRecord(LoggedUserID,UM.id,'New Tenant User',UserName[0].First_Name__c+' '+UserName[0].Last_Name__c+' is Created New User - '+UM.First_Name__c+' '+UM.Last_Name__c,false);
            }
            
              
            Limited_Bidding__c LB =New Limited_Bidding__c();
            LB.Tenant_User__c =UM.id; 
            LB.Transaction__c =TransId;
            LB.CreatedBy__c = LoggedUserID;
            LB.LastModifiedBy__c = LoggedUserID;
            if(Flag =='TA'){
                LB.Assignee_Email__c = true;
            }else{
               LB.Assignee_Email__c = false; 
            }
            Insert LB;
<<<<<<< HEAD
            Helper.ActivityLogInsertCallForRecord(LoggedUserID,TransId,'CP - '+UM.User_Email__c+'- Invited for participation','',false);
=======
            Helper.ActivityLogInsertCallForRecord(LoggedUserID,TransId,'CP - '+UM.User_Email__c+'- Requested For Quote','',false);
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-clientdemo.git

        }
        catch(Exception ex){
            System.debug('addRemoveCounterParty: ' + ex.getMessage());
            retValue = 'ex.getMessage()';
        }
        return retValue;
    }
    @AuraEnabled
    public static string addassignee(String LoggedUserID, String LoggedUserTenantId, Tenant__c saveCreditunion,User_Management__c UM,string PName,string PID,string Flag,String TransId, Boolean TransFlag){
        String retValue = '';
        try{
            system.debug('=====addassignee====='+UM.Country_List__c);
            Organization__c OZ =New Organization__c();
            OZ.Name = saveCreditunion.Tenant_Site_Name__c;
            OZ.CreatedFromOTP__c =true;
            Insert OZ;
            saveCreditunion.Tenant_Type__c = 'Bank';
            saveCreditunion.Tenant_Bank_Type__c = 'Counterparty Bank';
            saveCreditunion.CreatedBy__c = LoggedUserID;
            saveCreditunion.LastModifiedBy__c = LoggedUserID;
            saveCreditunion.Tenant_Short_Name__c= 'admin';//saveCreditunion.Tenant_Site_Name__c.split (' ')[0];
            saveCreditunion.Organization__c = OZ.Id;
            saveCreditunion.CreatedFromOTP__c =true;
            Insert saveCreditunion;
            
            Quotes__c[] closequote = [SELECT Id, CreatedBy__r.Tenant__c from Quotes__c WHERE Transaction__c =: TransId AND Bid_Status__c ='Quote Closed'];
            User_Management__c[] UserName = [SELECT First_Name__c,Last_Name__c FROM User_Management__c WHERE id=:LoggedUserID Limit 1];
            Helper.ActivityLogInsertCallForRecord(LoggedUserID,saveCreditunion.id,'New Tenant',UserName[0].First_Name__c+' '+UserName[0].Last_Name__c+' is Created New Tenant - '+saveCreditunion.Tenant_Site_Name__c,false);
            UM.CreatedFromrapid__c =true;
            UM.Active__c = true;
            UM.User_Name__c = UM.User_Email__c;            
            if(TransFlag == true){
                UM.Tenant__c = closequote[0].CreatedBy__r.Tenant__c;
            }else{
                UM.Tenant__c= saveCreditunion.Id;
            }            
            UM.Profiles__c =System.label.OBCP_ProfileID;
            UM.User_Profile__c = 'Counterparty Manager';
            UM.CreatedBy__c = LoggedUserID;
            UM.LastModifiedBy__c = LoggedUserID;
            if(Flag =='IC'){
                UM.Invited_By__c = LoggedUserID;
            }
            if(Flag =='TA'){
                UM.Task_Assignee__c = true;
            }
            if(UM.Country_Code__c == '--Select--'){
                UM.Country_Code__c = null;
            }
            if(UM.Country_List__c == '--Select--'){
                UM.Country_List__c = null;
            }else{
                UM.Country_List__c = UM.Country_List__c;
            }
            if(TransFlag == true){
                UM.View_Transaction__c = true;                
            }else{
                UM.View_Transaction__c = false;
            }
            UM.SiteName__c = saveCreditunion.Tenant_Site_Name__c;
            Insert UM;
           
            Helper.ActivityLogInsertCallForRecord(LoggedUserID,UM.id,'New Tenant User',UserName[0].First_Name__c+' '+UserName[0].Last_Name__c+' is Created New Assignee - '+UM.First_Name__c+' '+UM.Last_Name__c,false);
            
            
              
            /*Limited_Bidding__c LB =New Limited_Bidding__c();
            LB.Tenant_User__c =UM.id; 
            LB.Transaction__c =TransId;
            LB.CreatedBy__c = LoggedUserID;
            LB.LastModifiedBy__c = LoggedUserID;
            //if(Flag =='TA'){
                LB.Assignee_Email__c = false;
                LB.Don_t_Sent_mail__c = true;
           //}else{
             //  LB.Assignee_Email__c = false; 
           // }
            Insert LB;
            Helper.ActivityLogInsertCallForRecord(LoggedUserID,TransId,'Assignee - '+UM.User_Email__c+'- Requested For Manage Task','',false);*/

        }
        catch(Exception ex){
            System.debug('addRemoveCounterParty: ' + ex.getMessage());
            retValue = 'ex.getMessage()';
        }
        return retValue;
    }
    @AuraEnabled
    public static LightBankListV Filter(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) { 
        //Helper.setSessionValue('sessionselected', selected);
        return ShowTable(counter, sortbyField, sortDirection, selected,Filtervalue,TID); 
    }
    private static LightBankListV ShowTable(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID){ 
        LightBankListV retValue = new LightBankListV();
        try{
            String sessionselectedchk='';
            if((String)Cache.Session.get('sessionselected') != null)  
                sessionselectedchk= Helper.getSessionValue('sessionselected');  
            system.debug('#sessionselected#'+sessionselectedchk);
            String whereCondition = ''; 
            if(sessionselectedchk == 'Active' || sessionselectedchk == ''){
                whereCondition = ' WHERE Tenant_Approved__r.IsActive__c = True';
                
            }
            else if(sessionselectedchk == 'All'){
                whereCondition = ' WHERE (Tenant_Approved__r.IsActive__c = True OR Tenant_Approved__r.IsActive__c = false)';
            }
            else if(sessionselectedchk == 'Inactive'){
                whereCondition = ' WHERE Tenant_Approved__r.IsActive__c = false';
            }
            if(Filtervalue!='All Realms' && Filtervalue!=null)
            {
                String setTids='(';
                if(Filtervalue.startsWith('a0M')){
                    
                    for(Tenant__c T:[SELECT id FROM Tenant__c WHERE Organization__c=:Filtervalue])
                    {
                       setTids =setTids +'\''+T.id+'\',';                        
                    }
                    setTids =setTids+')';
                    if(setTids != '(')
                    {
                        system.debug('##'+setTids);
                     String FinalsetTids = setTids.replace(',)', ')');
                        system.debug('#FinalsetTids#'+FinalsetTids);
                     whereCondition = whereCondition + ' AND id IN'+FinalsetTids;
                        
                    }
                }else{
                whereCondition = whereCondition + ' AND id=\''+Filtervalue+'\'';
                }
             }
            size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'BankList' AND Grid_Name__c =: 'BankList']; 
            
            if(size > 0){
                retValue.list_size = Integer.valueOf([SELECT List_Size__c from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'BankList' AND Grid_Name__c =: 'BankList'][0].List_Size__c);
            }
            else {
                retValue.list_size = 25;   
            }
            
            String query = 'SELECT Tenant_Approved__r.CreatedBy__c,Tenant_Approved__r.Id,Tenant_Approved__r.IsActive__c,Tenant_Approved__r.Name,Tenant_Approved__r.Organization__r.Name,Tenant_Approved__r.Organization__c,Tenant_Approved__r.Primary_Contact_Email__c,Tenant_Approved__r.Primary_Contact_Mobile__c,Tenant_Approved__r.Primary_Contact_Name__c,Tenant_Approved__r.Tenant_Banner_Url__c,Tenant_Approved__r.Tenant_Footer_Message__c,Tenant_Approved__r.Tenant_Logo_Url__c,Tenant_Approved__r.Tenant_News_Message__c,Tenant_Approved__r.Tenant_Short_Name__c,Tenant_Approved__r.Tenant_Site_Name__c,Tenant_Approved__r.Tenant_Swift_Code__c,Tenant_Approved__r.Tenant_Type__c,Tenant_Approved__r.Tenant_User_Count__c,Tenant_Approved__r.Tenant_vision__c,Tenant_Approved__r.Tenant_Website__c,Tenant_Approved__r.User_Activation_Email__c FROM Tenant_Mapping__c';
            system.debug('&&'+UserInfo.getUserId());
            system.debug('&&'+System.Label.Admin_ID);
            
            if(TID  == System.Label.TradeAix_Tenant_Id)
            {
                retValue.Adminchk='A';
                query = query + whereCondition +' AND Selectchk__c=true AND Tenant_Approved__r.Tenant_Type__c=\'Bank\' AND Tenant_Approved__r.Tenant_Bank_Type__c=\'Counterparty Bank\' ORDER BY ' + sortbyField + ' ' + sortDirection + ' LIMIT ' + retValue.list_size + ' offset ' + counter;
           
            }else{
               query = query + whereCondition +' AND Selectchk__c=true AND Tenant__c=\''+TID+'\' AND Tenant_Approved__r.Tenant_Type__c=\'Bank\' AND Tenant_Approved__r.Tenant_Bank_Type__c=\'Counterparty Bank\' ORDER BY ' + sortbyField + ' ' + sortDirection + ' LIMIT ' + retValue.list_size + ' offset ' + counter;
            }
            system.debug('&&'+query);
            
            Set<Id> TMSet = new Set<Id>();
            List<Tenant_Mapping__c> TMList = new List<Tenant_Mapping__c>();
            for (Tenant_Mapping__c s : Database.query(query)) {
                system.debug('&sss&'+s.Tenant_Approved__c);
                if (TMSet.add(s.Tenant_Approved__c)) {
                    TMList.add(s);
                }
                 
            }
              system.debug('&&'+TMSet.size());
            
            retValue.TMresults = TMList;
            //retValue.results = retValue.results;
            retValue.total_size = TMSet.size();
            retValue.counter = counter; 
            retValue.sortbyField = sortbyField; 
            retValue.sortDirection = sortDirection; 
            retValue.total_page = getTotalPages(retValue.list_size, retValue.total_size);
            retValue.selectedItem = (sessionselectedchk == '') ? 'Active': sessionselectedchk;
        } catch(Exception ex){
            System.debug('LightBankList - ShowTable() : ' + ex.getMessage());
            
        }
        
        return retValue;    
    }
    @AuraEnabled
    public static String EditBank(String  ccid) {
        String response = '';
       
        return response; 
    }
    @AuraEnabled
    public static String CheckBank(String bankName, String swiftcode,String email) { 
        String response = '';
        try {  
            Integer count = [SELECT count() FROM Tenant__c WHERE Tenant_Site_Name__c = : bankName AND Tenant_Swift_Code__c =:swiftcode]; 
            Integer countemail = [SELECT count() FROM User_Management__c WHERE User_Email__c = : email]; 
            if(count > 0){
                response = 'Already Used LName';
            }
            else if(countemail >0){
                response = 'Already Used Email';
            }
            else{
                 response = '';
            }
            
        } catch(Exception ex){
            System.debug('LightBankList - CheckBankName() : ' + ex.getMessage());
          
            response = '';
        }
        return response; 
    } 
    @AuraEnabled
    public static LightBankListV getBankList(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID){           
        LightBankListV retValue = new LightBankListV(); 
        try{  
            if((String)Cache.Session.get('sortbyFieldSOL') != null)  
                sortbyField = Helper.getSessionValue('sortbyFieldSOL');  
            
            if((String)Cache.Session.get('sortDirectionSOL') != null)  
                sortDirection = Helper.getSessionValue('sortDirectionSOL'); 
            retValue = ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID);  
        } catch(Exception ex){
            System.debug('LightBankList - ShowTable() : ' + ex.getMessage());
            
        }
        return retValue;
    } 
    
    @AuraEnabled
    public static LightBankListV getBeginning(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) { 
        return ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID); 
    }
    
    @AuraEnabled
    public static LightBankListV Views(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) { 
        Helper.setSessionValue('sessionselected', selected);
        return ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID); 
    }
    
    @AuraEnabled
    public static LightBankListV getPrevious(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) {
        return ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID); 
    }
    
    @AuraEnabled
    public static LightBankListV getNext(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) {  
        return ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID); 
    }
    
    @AuraEnabled
    public static LightBankListV getEnd(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) { 
        return ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID); 
    } 
    
    private static Integer getTotalPages(Integer list_size, Integer total_size) { 
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        }
        else {
            return (total_size/list_size);
        }
    }  
    
    @AuraEnabled
    public static LightBankListV changelist_size(Integer counter, String sortbyField, String sortDirection, Integer list_size, String selected,String Filtervalue,String TID) { 
        LightBankListV retValue = new LightBankListV(); 
        try{
            size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'BankList' AND Grid_Name__c =: 'BankList'];
            if(size > 0){  
                UI_List_Size__c listSize = new UI_List_Size__c(
                    Id = [SELECT Id from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'BankList' AND Grid_Name__c =: 'BankList'][0].Id,
                    List_Size__c = list_size  
                );
                update listSize;
            }
            else {
                UI_List_Size__c listSize = new UI_List_Size__c(
                    List_Size__c = list_size,
                    Name = 'BankList',
                    Grid_Name__c = 'BankList',
                    User__c = UserInfo.getUserId()
                );
                insert listSize;
            }  
            
            retValue = ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID);
            
        } catch(Exception ex){
            
        }
        return retValue;
    }
    
    @AuraEnabled
    public static LightBankListV SortTable(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) { 
        LightBankListV retValue = new LightBankListV(); 
        try{
            if(sortbyField == '') {
                sortbyField = 'LastModifiedDate';
                sortDirection = 'DESC';
            } else {
                if(sortDirection.equals('ASC')){
                    sortDirection = 'DESC';
                } else {
                    sortDirection = 'ASC';
                }
            }
            Helper.setSessionValue('sortbyFieldSOL', sortbyField);
            Helper.setSessionValue('sortDirectionSOL', sortDirection); 
            retValue = ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID); 
        } catch (Exception ex){ 
           
        }
        return retValue;
    } 
    
    @AuraEnabled
    public static LightBankListV changePage(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) {
        return ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID); 
    }
    
    @AuraEnabled
    public static LightBankListV unregistercpusers(String TID){
        LightBankListV retValue = new LightBankListV(); 
        try{
            Set<Id> tIds = new Set<Id>();
            List<Tenant__c> lstTenant = [SELECT Id from Tenant__c WHERE CreatedFromOTP__c = true];
            for(Tenant__c T:lstTenant){
                tIds.add(T.Id);
            }
            retValue.UMresults = [SELECT Id, Name, Tenant__r.Tenant_Site_Name__c,Tenant__r.Tenant_Swift_Code__c,First_Name__c,Last_Name__c,City__c,User_Email__c,Phone__c,Title__c from User_Management__c WHERE Tenant__c =: tIds];
            
        }catch(exception ex){
            system.debug('==unregistercpusers=='+ex.getMessage());
        }
        return retValue; 
    }
    
    @AuraEnabled
    public static LightBankListV getunregisteruserquotes(String userId){
        LightBankListV retValue = new LightBankListV(); 
        try{
            retValue.quoteresults = [SELECT Id, Name, Bid_Amount__c,Bid_Expiration_Date__c,Bid_Service_Fee_bps__c,Bid_Status__c,CreatedById__c,Request_Information_Quote__c,Transaction__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,CounteParty__r.Tenant_Site_Name__c,CreatedDate,Note__c from Quotes__c WHERE CreatedBy__c=: userId];
        }catch(exception ex){
            system.debug('==getunregisteruserquotes=='+ex.getMessage());
        }
        return retValue; 
    }
    
    
    
}
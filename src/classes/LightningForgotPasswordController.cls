global class LightningForgotPasswordController {

    public LightningForgotPasswordController() {

    }
    @AuraEnabled
    public static Userloginv1 getTenant(String TenantId) {
        Userloginv1 ULogin =NEW Userloginv1();
        try{
            
            String RetVal ='';
            ApexPages.PageReference lgn;
            lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', null);
            system.debug('=====login user===='+userinfo.getName()); 
            ULogin.userlists = [SELECT Id,Tenant_Site_Name__c,Tenant_Logo_Url__c,Tenant_Banner_Url__c,Tenant_vision__c,Name from Tenant__c WHERE Tenant_Short_Name__c =:TenantId LIMIT 1];
            
            system.debug('####'+ULogin);
        }
        catch (Exception ex) {
            Helper.ActivityLogInsertCall('','Error-TogetTenant',ex.getMessage(),true);
            //return ex.getMessage();            
        }
        return ULogin;
    }
    /*@AuraEnabled
    public static String forgotStartup() {
        
        system.debug('=====LightningForgotPasswordController -forgotPassword()');
        try {
            String RetVal ='';
            ApexPages.PageReference lgn;
            lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', null);
            system.debug('=====login user===='+userinfo.getName());   
            return RetVal ='Success';
        }
        
        catch (Exception ex) {
            return ex.getMessage();
        }
        
    }*/
    @AuraEnabled
    public static Userloginv1 forgotPassword(String username, String checkEmailUrl, String tenant) {
        Userloginv1 ULogin =NEW Userloginv1();
        system.debug('=====LightningForgotPasswordController -forgotPassword()');
        try {
             String RetVal ='';
            ApexPages.PageReference lgn;
            //lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', null);
            system.debug('=====login user===='+userinfo.getName());           
            
            system.debug('=====LightningForgotPasswordController -forgotPassword()');
            Site.forgotPassword(username);
            ApexPages.PageReference checkEmailRef = new PageReference(checkEmailUrl);
            if(!Site.isValidUsername(username)) {
                ULogin.returnValue =  Label.Site.invalid_email;
                system.debug('=====LightningForgotPasswordController -forgotPassword()');
            }else{
                
                User_Management__c[] email = [SELECT User_Email__c, Id, First_Name__c, Last_Name__c, Tenant__r.Name  from User_Management__c WHERE User_Name__c =: username];
                if(email.size() > 0){
                    Login_History__c[] loginhistory = [SELECT Id, Password_Requested_Date__c, Forgot_Password_Attempt__c from Login_History__c WHERE User_Management__c =: email[0].Id];
                    if(loginhistory.size() < 3){
                        
                        Blob exampleIv = Blob.valueOf('Example of IV123');
                        Blob key = Crypto.generateAesKey(128);
                        Blob data = Blob.valueOf(username);
                        Blob encrypted = Crypto.encrypt('AES128', key, exampleIv, data);
                        String encryptedString = EncodingUtil.base64Encode(encrypted);
                        
                        User_Management__c UM = new User_Management__c();
                        UM.Id = email[0].Id;
                        UM.UserEmail_Encrypt__c = encryptedString;
                        update UM;
                        
                        system.debug('=======Email==='+email[0].User_Email__c);                    
                        // Step 0: Create a master list to hold the emails we'll send
                        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();  
                        
                        // Step 1: Create a new Email
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        
                        // Step 2: Set list of people who should get the email
                        List<String> sendTo = new List<String>();
                        sendTo.add(email[0].User_Email__c);
                        mail.setToAddresses(sendTo);
                        
                        // Step 3: Set who the email is sent from
                        mail.setReplyTo('satheesh@aixchane.co.in');
                        mail.setSenderDisplayName('TradeAix');

                        // Step 4. Set email contents - you can use variables!
                        mail.setSubject('Password Reset for '+username); 
                        String body = 'Hi '+email[0].First_Name__c+' '+email[0].Last_Name__c+', <br/><br/>';
                        body += 'To reset your password for '+email[0].Tenant__r.Name+', please click on the link below: <br/><br/>';
                        body += 'https://clientdemo-stratizantqa.cs17.force.com/tradeaix/s/change-password?username='+ encryptedString+'&tenant='+tenant+'';
                        body += '<br/>Please note that the password reset page will expire after 24 hours.';
                        
                        mail.setHtmlBody(body);
                        
                        // Step 5. Add your email to the master list
                        mails.add(mail);
                        
                        // Step 6: Send all emails in the master list
                        Messaging.sendEmail(mails);
                        //RetVal = 'Success';
                        ULogin.returnValue =  'Success';
                    }else{
                        system.debug('=======ELSE Forgot Password attempt==='+email);
                        //RetVal  = 'Please contact your Administrator';
                        ULogin.returnValue = 'Please contact your Administrator';
                    }
                }else{
                    
                    system.debug('=======ELSE Email==='+email);
                    RetVal  = 'Invalid User';
                    ULogin.returnValue = 'Invalid User';
                }
            }
            Helper.ActivityLogInsertCall(username,'Success - Forgot Password','Forgot Password Email and respective link sent to appropriate email id',false);
            //return RetVal ;
            
        }
        catch (Exception ex) {
            Helper.ActivityLogInsertCall(username,'Error - Forgot Password',ex.getMessage(),true);
            ULogin.returnValue = ex.getMessage();
        }
        return ULogin;
    }
     @AuraEnabled
    public static Boolean getIsUsernamePasswordEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getUsernamePasswordEnabled();
    }
    
    @AuraEnabled
    public static Boolean getIsSelfRegistrationEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getSelfRegistrationEnabled();
    }
    
    @AuraEnabled
    public static String getSelfRegistrationUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        if (authConfig.getSelfRegistrationEnabled()) {
            return authConfig.getSelfRegistrationUrl();
        }
        return null;
    }
    
    @AuraEnabled
    public static String getForgotPasswordUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getForgotPasswordUrl();
    }
    
    @TestVisible
    private static Auth.AuthConfiguration getAuthConfig(){
        Id networkId = Network.getNetworkId();
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(networkId,'');
        return authConfig;
    }

}
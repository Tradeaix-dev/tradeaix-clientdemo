public class TransactionCreateEmailTemp {
    public Id transId {get;set;} 
    public String OTP{get;set;}
    public class OfferDetails { 
        
        public String offerId {get;set;}
        public String offerCreatedUser {get;set;}
        public String prodTemplateName {get;set;}        
        
        public String TransactionRefNumber {get;set;}
        public String OfferType {get;set;}
        public String MarketType {get;set;}
        public String Status {get;set;}
        
        public String CreatedBy {get;set;}
        public String CreatedDate {get;set;}
        public String Title {get;set;}
        public String Email {get;set;}
        public String Phone {get;set;}
        public String firstname {get;set;}
        public String PartyName {get;set;}
        public String ReferenceNumber {get;set;}
        public String City {get;set;}
        public String State {get;set;}
        public String Thanksmsg{get;set;}
        public String logo{get;set;}
        public String logotitle{get;set;} 
        public List<String> Body{get;set;}
        public String LB {get;set;}
        public Boolean onboardingUser {get;set;}
        public String Id{get;set;}
        public String Currencycode{get;set;}
        public String IB{get;set;}
    } 
    
    public OfferDetails email {get;set;}  
    
    public OfferDetails getOffers()
    {
        email = New OfferDetails();
        
        try{
            
            Transaction__c trans = [SELECT CreatedById,CreatedBy__c,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,CreatedBy__r.User_Title__c,CreatedDate,Currency__c,Id,LastActivityDate,LastModifiedById,LastModifiedBy__c,LastModifiedDate,Loan_Type__c,IsPrimary__c,Name,New_Status__c,Notes__c,Offer_Acceptance_Date__c,Product_Template__r.TemplateName__c,Published_By__c,Published_Date__c,Published__c,Status__c,Submitted_Date__c,Tenant__c,TransactionRefNumber__c,RFIChk__c,RFQChk__c,Request_For_Information__c,Issuing_Banks__r.Tenant_Site_Name__c FROM Transaction__c WHERE Id =:transId];
            email.prodTemplateName = trans.Product_Template__r.TemplateName__c;
            email.TransactionRefNumber = trans.TransactionRefNumber__c;            
            email.OfferType = trans.Loan_Type__c;           
            email.Currencycode= trans.Currency__c;
            if(trans.IsPrimary__c == true ){
                email.MarketType = 'Primary';
            }else{
                email.MarketType = 'Secondary';
            }            
            email.IB=trans.Issuing_Banks__r.Tenant_Site_Name__c;
            Datetime myDT = trans.CreatedDate; 
            TimeZone tz = UserInfo.getTimeZone();
            String myDate = myDT.format('dd/MM/YYYY HH:mm:ss', tz.getID()); 
            //String myDate = myDT.format('YYYY-MM-dd HH:mm:ss');
            email.CreatedDate = myDate;
            email.CreatedBy = trans.CreatedBy__r.First_Name__c+' '+trans.CreatedBy__r.Last_Name__c;  
            email.Status = trans.Status__c;
            if(trans.CreatedBy__r.User_Title__c != null){
                email.firstname = trans.CreatedBy__r.User_Title__c+' '+trans.CreatedBy__r.First_Name__c+' '+trans.CreatedBy__r.Last_Name__c; 
            }else{
                email.firstname = trans.CreatedBy__r.First_Name__c+' '+trans.CreatedBy__r.Last_Name__c; 
            }
            
            //Counter Party Information
            List<Quotes__c> lstQuote = [SELECT Id, Name,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,CreatedBy__r.User_Title__c from Quotes__c WHERE Transaction__c =: transId and Bid_Status__c = 'Quote Closed'];
            if(lstQuote.size()>0){
                if(lstQuote[0].CreatedBy__r.User_Title__c != null){
                    email.firstname = lstQuote[0].CreatedBy__r.User_Title__c+' '+lstQuote[0].CreatedBy__r.First_Name__c+' '+lstQuote[0].CreatedBy__r.Last_Name__c;
                }else{
                    email.firstname = lstQuote[0].CreatedBy__r.First_Name__c+' '+lstQuote[0].CreatedBy__r.Last_Name__c;
                }
                
            }            
            //Party Information
            User_Management__c partyUser = [SELECT Id, Tenant__c,Tenant__r.Tenant_Site_Name__c ,Name,First_Name__c,Last_Name__c,User_Name__c,Profiles__r.Name,User_Email__c,Street__c, City__c,State_Province__c,Title__c,Phone__c,Tenant__r.Tenant_Footer_Message__c, Tenant__r.Tenant_Logo_Url__c from User_Management__c WHERE Id =: trans.CreatedBy__c];
            email.CreatedBy = partyUser.First_Name__c + ' '+partyUser.Last_Name__c ;
            email.Title = partyUser.Title__c;
            email.Email = partyUser.User_Email__c;
            email.Phone = partyUser.Phone__c;      
            email.Thanksmsg = partyUser.Tenant__r.Tenant_Footer_Message__c;
            email.PartyName = partyUser.Tenant__r.Tenant_Site_Name__c ;           
            email.City = partyUser.city__c;
            email.State = partyUser.State_Province__c;    
            email.logo = partyUser.Tenant__r.Tenant_Logo_Url__c;
            //email.logotitle=PartyInformation.Browser_Title__c;  
            List<String> emailBody = new List<String>();
            if(trans.RFIChk__c == true && trans.RFQChk__c == false){
                List<String> lstRequestInformation = trans.Request_For_Information__c.split(',');
                for(Transaction_Attributes__c TA:[SELECT Attribute_Name__c,Attribute_Value__c,Transaction__c,Attribute_Type__c FROM Transaction_Attributes__c WHERE Transaction__c =: transId ]){
                    if(!lstRequestInformation.contains(TA.Attribute_Name__c)){
                        if(TA.Attribute_Type__c == 'DateTime'){
                            string a = TA.Attribute_Value__c;
                            string[] b = a.split('/');
<<<<<<< HEAD
                            emailBody.add(TA.Attribute_Name__c +' : '+b[0]+'/'+b[1]+'/'+b[2]);
                        }else{
                            emailBody.add(TA.Attribute_Name__c +' : '+TA.Attribute_Value__c);
                        }
                    }
                }
            }else{
                for(Transaction_Attributes__c TA:[SELECT Attribute_Name__c,Attribute_Value__c,Transaction__c,Attribute_Type__c FROM Transaction_Attributes__c WHERE Transaction__c =: transId ]){
                    if(TA.Attribute_Type__c == 'DateTime'){
                        string a = TA.Attribute_Value__c;
                        string[] b = a.split('/');
                        emailBody.add(TA.Attribute_Name__c +' : '+b[0]+'/'+b[1]+'/'+b[2]);
=======
                            emailBody.add(TA.Attribute_Name__c +' : '+b[2]+'/'+b[1]+'/'+b[0]);
                        }else{
                            emailBody.add(TA.Attribute_Name__c +' : '+TA.Attribute_Value__c);
                        }
                    }
                }
            }else{
                for(Transaction_Attributes__c TA:[SELECT Attribute_Name__c,Attribute_Value__c,Transaction__c,Attribute_Type__c FROM Transaction_Attributes__c WHERE Transaction__c =: transId ]){
                    if(TA.Attribute_Type__c == 'DateTime'){
                        string a = TA.Attribute_Value__c;
                        string[] b = a.split('/');
                        emailBody.add(TA.Attribute_Name__c +' : '+b[2]+'/'+b[1]+'/'+b[0]);
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-clientdemo.git
                    }else{
                        emailBody.add(TA.Attribute_Name__c +' : '+TA.Attribute_Value__c);
                    }                
                }
            }
            email.Body = emailBody;
            system.debug('// emailBody '+ emailBody );
            for(Limited_Bidding__c UM : [SELECT Id,agree__c,CreatedBy__c,Tenant_User__c from Limited_Bidding__c WHERE Transaction__c=:transId Order by createdDate desc Limit 1])
            {
            email.LB  = UM.Id;
            email.Id = UM.Tenant_User__c;
            OTP ='yes';
            //email.onboardingUser = true;
            }
           /* List<CPSelection__c> LstCP = [SELECT id,Tenant_User__c,Tenant_User__r.Profiles__c FROM CPSelection__c WHERE Transaction__c=:transId LIMIT 1];
             
             if(LstCP.Size()>0)
             {if(LstCP[0].Tenant_User__r.Profiles__c == system.label.OBCP_ProfileID){
                 email.onboardingUser = true;
             }else{
                 email.onboardingUser = false;
             }}
             else{
             email.onboardingUser = false;
             } */
            
        } catch(exception e){
            system.debug('//****// '+e.getMessage());
            email.PartyName = e.getMessage()+'::'+e.getLineNumber();  
        } 
        return email;
    }
     
}
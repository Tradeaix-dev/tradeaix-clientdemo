public class BankFields {
    @AuraEnabled 
    public String label {get; set;}
    @AuraEnabled 
    public String value {get; set;} 
    
     public BankFields(String value, String label) {
        this.value = value;
        this.label = label;
       
    }
    
    
}
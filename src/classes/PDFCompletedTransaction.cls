public without sharing class PDFCompletedTransaction {
    public Id transactionId {get;set;} 
    public List<Activity_Log__c> History{get;set;}
    public List<Transaction_Attributes__c> AmendHistory {get;set;}
    public String Amendedatt {get;set;} 
    public class OfferDetails { 
        public String offerId {get;set;}
        public String offerCreatedUser {get;set;}
        public String prodTemplateName {get;set;}
        public String SobjName {get;set;}
        
        public String TransactionRefNumber {get;set;}
        public String OfferType {get;set;}
        public String MarketType {get;set;}
        public String Status {get;set;}
        public String IssuingBankName {get;set;}
        public String TransAmount {get;set;}
        Public String QuoteAmount {get;set;}
        Public String BidderName {get;set;}
        Public String Pricing {get;set;}
        
        public String CreatedBy {get;set;}
        public String CreatedDate {get;set;}
        public String Notes {get;set;}
        
        public String Title {get;set;}
        public String Email {get;set;}
        public String Phone {get;set;}
        public String City {get;set;}
        public String State {get;set;}
        public String PartyName {get;set;}
        
        public String cTitle {get;set;}
        public String cEmail {get;set;}
        public String cPhone {get;set;}
        public String cCity {get;set;}
        public String cState {get;set;}
        public String cPartyName {get;set;}
        public String curr {get;set;}
        
        public String ReferenceNumber {get;set;}
       
        public String Thanksmsg{get;set;}
        public String logo{get;set;}
        public String logotitle{get;set;}
        public List<String> SobjectApiName {get;set;}
        public List<String> SobjectFieldApiName {get;set;}
        public String sellerCreditUnion {get;set;}           
        public List<String> Body{get;set;}  
        public List<String> AmendBody{get;set;}    
       
    } 
    public OfferDetails email {get;set;} 
    
    public OfferDetails getOffers()
    {
        email = New OfferDetails();
        try{
            transactionId = ApexPages.currentPage().getParameters().get('Id');
            system.debug('==transactionId=='+transactionId);
            Transaction__c trans = [SELECT CreatedById,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,CreatedDate,Currency__c,Id,LastActivityDate,LastModifiedById,LastModifiedBy__c,LastModifiedDate,Loan_Type__c,IsPrimary__c,Name,New_Status__c,Notes__c,Offer_Acceptance_Date__c,Product_Template__r.TemplateName__c,Published_By__c,Published_Date__c,Published__c,Status__c,Submitted_Date__c,Tenant__c,TransactionRefNumber__c,Issuing_Banks__r.Tenant_Site_Name__c,Issuing_Banks__r.City__c FROM Transaction__c WHERE Id =:transactionId];
            email.prodTemplateName = trans.Product_Template__r.TemplateName__c;
            email.TransactionRefNumber = trans.TransactionRefNumber__c;            
            email.OfferType = trans.Loan_Type__c;
            email.Notes =trans.Notes__c;
            email.curr =trans.Currency__c;
            if(trans.IsPrimary__c == true ){
                email.MarketType = 'Primary';
            }else{
                email.MarketType = 'Secondary';
            }
            Datetime myDT = trans.CreatedDate; 
            String myDate = myDT.format('dd/MM/YYYY HH:mm:ss');
            email.CreatedDate = myDate;
            email.CreatedBy = trans.CreatedBy__r.First_Name__c+' '+trans.CreatedBy__r.Last_Name__c;
            email.Status = trans.Status__c;
            email.IssuingBankName = trans.Issuing_Banks__r.Tenant_Site_Name__c+' ,'+trans.Issuing_Banks__r.City__c;
            List<String> emailBody = new List<String>();
            for(Transaction_Attributes__c TA:[SELECT Attribute_Name__c,Attribute_Value__c,Transaction__c,Attribute_Type__c FROM Transaction_Attributes__c WHERE Transaction__c =: transactionId AND Amendment__c = false]){
                //if(TA.Attribute_Type__c == 'DateTime'){
                //    string a = TA.Attribute_Value__c;
                //  string[] b = a.split('-');
                //   emailBody.add(TA.Attribute_Name__c +' : '+b[2]+'/'+b[1]+'/'+b[0]);
                // }else{
                if(TA.Attribute_Name__c == 'Amount'){
                    email.TransAmount = TA.Attribute_Value__c;
                }
                emailBody.add(TA.Attribute_Name__c +' : '+TA.Attribute_Value__c);
                // }                
            }
            email.Body = emailBody;
            List<String> emailAmendBody = new List<String>();
            for(Transaction_Attributes__c TA:[SELECT Attribute_Name__c,Attribute_Value__c,Transaction__c,Attribute_Type__c FROM Transaction_Attributes__c WHERE Transaction__c =: transactionId AND Amendment__c = true]){
                if(TA.Attribute_Name__c == 'Amount'){
                    email.TransAmount = TA.Attribute_Value__c;
                }
                emailAmendBody.add(TA.Attribute_Name__c +' : '+TA.Attribute_Value__c);
            }
            email.AmendBody = emailAmendBody;            
            
            //Approved Amended attributes -start
            Set<Id> AID = new Set<Id>(); 
            List<Amend_Attributes__c> lstAA = [SELECT Id, Name FROM Amend_Attributes__c WHERE Transaction__c =: transactionId AND Amend_Status__c = 'Approved'];
            for(Amend_Attributes__c AA : lstAA){
                AID.add(AA.Id);
            }
            AmendHistory = new List<Transaction_Attributes__c>();
            AmendHistory = [SELECT id,Tenant__c,Product_Template__c,Attribute_Name__c,Attribute_Value__c,Display_on_gride__c,Sort_Order__c,Attribute_Type__c,Amend_Number__c,Amend_Attributes__r.Comments__c, CreatedBy__r.First_Name__c, CreatedBy__r.Last_Name__c, CreatedDate, Original_Value__c, Comments__c, CreatedDatePDF__c FROM Transaction_Attributes__c WHERE Transaction__c=:transactionId AND Amend_Attributes__c =: AID];
            
            if(AmendHistory.size()>0){
                Amendedatt = 'Yes';
            }else{
                Amendedatt = 'No'; 
            }
            //Approved Amended attributes -end
            History= new List<Activity_Log__c>();
             
            History = [SELECT id,CreatedDatePDF__c,Action_By__c,Action_By__r.Tenant__r.Tenant_Site_Name__c,Action_By__r.First_Name__c,Action_By__r.Last_Name__c,CreatedDate,Operation__c,Operation_Log__c,RecordID__c FROM Activity_Log__c WHERE RecordID__c=:transactionId+'Task' AND Operation__c like'Changed%' ORDER BY CreatedDate DESC];
            //Party Information
            User_Management__c partyUser = [SELECT Id, Tenant__c,Tenant__r.Tenant_Site_Name__c ,Tenant__r.Tenant_Logo_Url__c,Name,First_Name__c,Last_Name__c,User_Name__c,Profiles__r.Name,User_Email__c,Street__c, City__c,State_Province__c,Title__c,Phone__c from User_Management__c WHERE Tenant__c =: trans.Tenant__c AND Profiles__r.Name ='Admin Manager Community Plus'];
            email.CreatedBy = partyUser.First_Name__c + ' '+partyUser.Last_Name__c ;
            email.Title = partyUser.Title__c;
            email.Email = partyUser.User_Email__c;
            email.Phone = partyUser.Phone__c;            
            email.PartyName = partyUser.Tenant__r.Tenant_Site_Name__c ;           
            email.City = partyUser.city__c;
            email.State = partyUser.State_Province__c;
            email.logo = partyUser.Tenant__r.Tenant_Logo_Url__c;
            
            //Get Counter Party Name
            system.debug('==transactionId=='+transactionId);
            List<Quotes__c> counterPartyUser =[SELECT CreatedBy__c,Bid_Amount__c,CounteParty__r.Tenant_Site_Name__c,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,Bid_Service_Fee_bps__c FROM Quotes__c WHERE Transaction__c =: transactionId  AND Bid_Status__c='Quote Closed'];
            system.debug('===QuoteAmount=='+String.valueOf(counterPartyUser[0].Bid_Amount__c));
            email.QuoteAmount = String.valueOf(counterPartyUser[0].Bid_Amount__c).split('\\.')[0];
            email.Pricing = String.valueOf(counterPartyUser[0].Bid_Service_Fee_bps__c)+' bps';
            email.BidderName = counterPartyUser[0].CreatedBy__r.First_Name__c+' '+counterPartyUser[0].CreatedBy__r.Last_Name__c+' ('+counterPartyUser[0].CounteParty__r.Tenant_Site_Name__c+')';
            User_Management__c CpartyUser = [SELECT Id, Tenant__c,Tenant__r.Tenant_Site_Name__c ,Name,First_Name__c,Last_Name__c,User_Name__c,Profiles__r.Name,User_Email__c,Street__c, City__c,State_Province__c,Title__c,Phone__c from User_Management__c WHERE id=: counterPartyUser[0].CreatedBy__c];
            //String counterPartyUser =[SELECT CreatedBy__c FROM Quotes__c WHERE Transaction__c =: transactionId  AND Bid_Status__c='Quote Closed'].CreatedBy__c;
            //User_Management__c CpartyUser = [SELECT Id, Tenant__c,Tenant__r.Tenant_Site_Name__c ,Name,First_Name__c,Last_Name__c,User_Name__c,Profiles__r.Name,User_Email__c,Street__c, City__c,State_Province__c,Title__c,Phone__c from User_Management__c WHERE id=: counterPartyUser];
            //email.cCreatedBy = CpartyUser.First_Name__c + ' '+CpartyUser.First_Name__c ;
            email.cTitle = CpartyUser.Title__c;
            email.cEmail = CpartyUser.User_Email__c;
            email.cPhone = CpartyUser.Phone__c;            
            email.cPartyName = CpartyUser.Tenant__r.Tenant_Site_Name__c ;           
            email.cCity = CpartyUser.city__c;
            email.cState = CpartyUser.State_Province__c;

            
        }catch(exception ex){
            system.debug('======PDF Completed Transaction Exception======'+ex.getMessage());
             system.debug('======PDF Completed Transaction Exception LIne number======'+ex.getLineNumber());
        }
        
        return email;
        
    }
   
}
global class LightningLoginFormController {
    
    public LightningLoginFormController() {
        
    } 
    @AuraEnabled
    public static String login(String username, String password, String TenantId) {
        try{
            TenantId='JSL';
            String RetVal ='';
            if(TenantId =='JSL')
            { 
                ApexPages.PageReference lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', '/tradeaix/s/dashboard');
                //RetVal = Helper.getLoginUserDetails(username,password,TenantId);
                if(RetVal =='Failed'){
                    return RetVal;
                }else{
                    
                    aura.redirect(lgn);
                }
            }
            
            return RetVal;
            
        }
        catch (Exception ex) {
            return ex.getMessage();            
        }
    }
    @AuraEnabled
    public static Boolean getIsUsernamePasswordEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getUsernamePasswordEnabled();
    }
    
    @AuraEnabled
    public static Boolean getIsSelfRegistrationEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getSelfRegistrationEnabled();
    }
    
    @AuraEnabled
    public static String getSelfRegistrationUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        if (authConfig.getSelfRegistrationEnabled()) {
            return authConfig.getSelfRegistrationUrl();
        }
        return null;
    }
    
    @AuraEnabled
    public static String getForgotPasswordUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getForgotPasswordUrl();
    }
    
    @TestVisible
    private static Auth.AuthConfiguration getAuthConfig(){
        Id networkId = Network.getNetworkId();
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(networkId,'');
        return authConfig;
    }
}
public class BidSubmitEmailTemp {
    public Id quoteId {get;set;} 
    public String OTP{get;set;} 

    public class QuoteDetails { 
        public String CreatedBy {get;set;}
        public String TransactionReference {get;set;}
        public String BidReference {get;set;}
        public String Amount {get;set;}
        public String BidCurrency {get;set;}
        public String Expirationdate {get;set;}
        public String BidStatus {get;set;}
        public String partyName {get;set;}
        public String counterpartyName {get;set;}
        public String firstname {get;set;}
        public String transactioncreatedname {get;set;}
        public String bidFee {get;set;}
        public String logourl {get;set;}
        public String partylogourl {get;set;}
        public String IssuingBank {get;set;}
        public Boolean IssuingBankhasValue {get;set;}
        public Boolean logourlhasvalue {get;set;}
        public String LB {get;set;}
        public Boolean onboardingUser {get;set;}
        public String Id{get;set;}
<<<<<<< HEAD
        public String costoffunds {get;set;}
        public String Email_Guidance_Quote_Withdraw{get;set;}

    }
    public QuoteDetails email {get;set;}  
    
     public QuoteDetails getQuotes(){
         email = New QuoteDetails();
         try{
             Quotes__c quoteRef = [SELECT Id,Name,Bid_Amount__c,Bid_Expiration_Date__c,Bid_Service_Fee_bps__c,Bid_Status__c,Note__c,Reason__c,CreatedBy__r.Tenant__r.Email_Guidance_Quote_Withdraw__c,CreatedBy__r.First_Name__c, CreatedBy__r.Last_Name__c,CreatedBy__r.Tenant__r.Tenant_Site_Name__c,CreatedBy__r.Tenant__r.Tenant_Logo_Url__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Currency__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,Transaction__r.Tenant__r.Tenant_Logo_Url__c,Transaction__r.Issuing_Banks__r.Tenant_Site_Name__c,CreatedBy__r.Profiles__c,Transaction__r.CreatedBy__r.First_Name__c,Transaction__r.CreatedBy__r.Last_Name__c,Transaction__r.CreatedBy__r.User_Title__c,CreatedBy__r.User_Title__c, Cost_of_Funds__c from Quotes__c WHERE Id =: quoteId];
             email.CreatedBy = quoteRef.CreatedBy__r.First_Name__c+' '+quoteRef.CreatedBy__r.Last_Name__c;
             if(quoteRef.CreatedBy__r.User_Title__c != null){
                 email.firstname = quoteRef.CreatedBy__r.User_Title__c+' '+quoteRef.CreatedBy__r.First_Name__c+' '+quoteRef.CreatedBy__r.Last_Name__c;
             }else{
                 email.firstname = quoteRef.CreatedBy__r.First_Name__c+' '+quoteRef.CreatedBy__r.Last_Name__c;
             }             
             email.Email_Guidance_Quote_Withdraw=quoteRef.CreatedBy__r.Tenant__r.Email_Guidance_Quote_Withdraw__c;
             email.TransactionReference = quoteRef.Transaction__r.TransactionRefNumber__c;
             email.BidReference = quoteRef.Name;
             email.Amount = string.ValueOf(quoteRef.Bid_Amount__c);
             email.BidCurrency = quoteRef.Transaction__r.Currency__c;
             email.costoffunds = quoteRef.Cost_of_Funds__c;
=======
        public String Email_Guidance_Quote_Withdraw{get;set;}

    }
    public QuoteDetails email {get;set;}  
    
     public QuoteDetails getQuotes(){
         email = New QuoteDetails();
         try{
             Quotes__c quoteRef = [SELECT Id,Name,Bid_Amount__c,Bid_Expiration_Date__c,Bid_Service_Fee_bps__c,Bid_Status__c,Note__c,Reason__c,CreatedBy__r.Tenant__r.Email_Guidance_Quote_Withdraw__c,CreatedBy__r.First_Name__c, CreatedBy__r.Last_Name__c,CreatedBy__r.Tenant__r.Tenant_Site_Name__c,CreatedBy__r.Tenant__r.Tenant_Logo_Url__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Currency__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,Transaction__r.Tenant__r.Tenant_Logo_Url__c,Transaction__r.Issuing_Banks__r.Tenant_Site_Name__c,CreatedBy__r.Profiles__c,Transaction__r.CreatedBy__r.First_Name__c,Transaction__r.CreatedBy__r.Last_Name__c,Transaction__r.CreatedBy__r.User_Title__c,CreatedBy__r.User_Title__c from Quotes__c WHERE Id =: quoteId];
             email.CreatedBy = quoteRef.CreatedBy__r.First_Name__c+' '+quoteRef.CreatedBy__r.Last_Name__c;
             if(quoteRef.CreatedBy__r.User_Title__c != null){
                 email.firstname = quoteRef.CreatedBy__r.User_Title__c+' '+quoteRef.CreatedBy__r.First_Name__c+' '+quoteRef.CreatedBy__r.Last_Name__c;
             }else{
                 email.firstname = quoteRef.CreatedBy__r.First_Name__c+' '+quoteRef.CreatedBy__r.Last_Name__c;
             }             
             email.Email_Guidance_Quote_Withdraw=quoteRef.CreatedBy__r.Tenant__r.Email_Guidance_Quote_Withdraw__c;
             email.TransactionReference = quoteRef.Transaction__r.TransactionRefNumber__c;
             email.BidReference = quoteRef.Name;
             email.Amount = string.ValueOf(quoteRef.Bid_Amount__c);
             email.BidCurrency = quoteRef.Transaction__r.Currency__c;
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-clientdemo.git
             Datetime myDT = quoteRef.Bid_Expiration_Date__c;             
             TimeZone tz = UserInfo.getTimeZone();
             String myDate = myDT.format('dd/MM/YYYY', tz.getID()); 
             //String myDate = myDT.format('dd/MM/YYYY HH:mm:ss');
             email.Expirationdate = myDate;
             email.BidStatus = quoteRef.Bid_Status__c;
             if(quoteRef.Transaction__r.CreatedBy__r.User_Title__c != null){
                 email.transactioncreatedname = quoteRef.Transaction__r.CreatedBy__r.User_Title__c+' '+quoteRef.Transaction__r.CreatedBy__r.First_Name__c+' '+quoteRef.Transaction__r.CreatedBy__r.Last_Name__c;
             }else{
                 email.transactioncreatedname = quoteRef.Transaction__r.CreatedBy__r.First_Name__c+' '+quoteRef.Transaction__r.CreatedBy__r.Last_Name__c;
             }             
             email.counterpartyName = quoteRef.CreatedBy__r.Tenant__r.Tenant_Site_Name__c;
             email.partyName = quoteRef.Transaction__r.Tenant__r.Tenant_Site_Name__c;
             email.bidFee = string.ValueOf(quoteRef.Bid_Service_Fee_bps__c);
             email.logourl = quoteRef.CreatedBy__r.Tenant__r.Tenant_Logo_Url__c;
             email.partylogourl = quoteRef.Transaction__r.Tenant__r.Tenant_Logo_Url__c;
             email.IssuingBank = quoteRef.Transaction__r.Issuing_Banks__r.Tenant_Site_Name__c;
             if(quoteRef.Transaction__r.Issuing_Banks__r.Tenant_Site_Name__c != null){
                 email.IssuingBankhasValue = true;
             }else{
                 email.IssuingBankhasValue = false;
             }
             if(quoteRef.CreatedBy__r.Tenant__r.Tenant_Logo_Url__c == null){            
                 email.logourl = system.label.TradeAix_Logo;
             }               
             
            for(Limited_Bidding__c UM : [SELECT Id,agree__c,CreatedBy__c,Tenant_User__c from Limited_Bidding__c WHERE Transaction__c=:quoteRef.Transaction__c Order by createdDate desc Limit 1])
            {
            email.LB  = UM.Id;
            email.Id = UM.Tenant_User__c;
            OTP ='yes';            
            }
             if(quoteRef.CreatedBy__r.Profiles__c == system.label.OBCP_ProfileID){
                 email.onboardingUser = true;
             }else{
                 email.onboardingUser = false;
             }
             

         }catch(exception ex){
             system.debug('===BidSubmitEmailTempException==='+ex.getMessage());
         }
         return email;
     }

}
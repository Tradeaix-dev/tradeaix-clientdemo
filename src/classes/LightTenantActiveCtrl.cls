public class LightTenantActiveCtrl {
    @AuraEnabled 
    public static Organization__c[] loadorgs(String username) {
        return Helper.loadorgs(username);
    }
    
    @AuraEnabled
    public static LightAdminProfileV getallLists(string OrgId, string TId, string userId){
        LightAdminProfileV listOrganization = new LightAdminProfileV();
        system.debug('======getallLists====');
        try{ 
            system.debug('####'+userinfo.getUserId());
            system.debug('##'+OrgId);
            Organization__c[] org = [SELECT Id, isPrimary__c from Organization__c WHERE Id =: OrgId];
            system.debug('##'+org);
            listOrganization.Activity_Log =[SELECT id,Action_By__c,Action_By__r.First_Name__c,Action_By__r.Last_Name__c,CreatedDate,Operation__c,Operation_Log__c,RecordID__c FROM Activity_Log__c WHERE RecordID__c=:OrgId AND ErrorCall__c=false ORDER BY CreatedDate DESC LIMIT 100];
            listOrganization.TMCP  =[SELECT id,Tenant_Approved__r.Tenant_Site_Name__c,Tenant_Approved__r.Tenant_Logo_Url__c,Selectchk__c FROM Tenant_Mapping__c WHERE Tenant__c=:TId AND Selectchk__c=true];     
            
            if(org[0].isPrimary__c == true){
                listOrganization.orgLists = [SELECT Id, Name, Employees__c, Industry__c, Phone__c, Primary_Contact_Email__c, Primary_Contact_Name__c, Primary_Contact_Phone__c, Revenue__c, Website__c, Tenant_Counts__c, CreatedDate, LastModifiedDate  from Organization__c WHERE Id !=: system.Label.TradeAix_Org_Id AND CreatedFromOTP__c=false AND isPrimary__c=true order by CreatedDate desc,Name];
                listOrganization.tenantLists = [SELECT Id, Name,Tenant_Logo_Url__c,IsAdminActivated__c,Organization__r.Name, Primary_Contact_Email__c,Tenant_Type__c, Primary_Contact_Mobile__c, Primary_Contact_Name__c, Tenant_Short_Name__c, Tenant_Site_Name__c, Tenant_Swift_Code__c, Tenant_Website__c, Tenant_User_Count__c, IsActive__c, CreatedDate, LastModifiedDate from Tenant__c WHERE Organization__c !=: system.Label.TradeAix_Org_Id AND CreatedFromOTP__c=false  AND Tenant_Type__c='Corporate' order by CreatedDate desc,Organization__r.Name ] ;
                listOrganization.userLists = [SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c, Profiles__r.Name, CreatedDate, LastModifiedDate,Tenant__r.Tenant_Site_Name__c,User_Profile__c,Tenant__r.Organization__r.Name,Password__c  from User_Management__c where (Tenant__c !=: system.Label.TradeAix_Tenant_Id AND Tenant__r.Tenant_Type__c='Corporate') OR (Profiles__c=:System.label.OBCP_ProfileID AND Password__c!='') order by CreatedDate desc,Tenant__r.Tenant_Site_Name__c];
                
            }else{
                listOrganization.orgLists = [SELECT Id, Name, Employees__c, Industry__c, Phone__c, Primary_Contact_Email__c, Primary_Contact_Name__c, Primary_Contact_Phone__c, Revenue__c, Website__c, Tenant_Counts__c, CreatedDate, LastModifiedDate  from Organization__c WHERE Id=:OrgId AND CreatedFromOTP__c=false AND isPrimary__c=true order by CreatedDate desc,Name];
                listOrganization.tenantLists = [SELECT Id,Organization__r.Name,Tenant_Logo_Url__c,IsAdminActivated__c,Name, Primary_Contact_Email__c,Tenant_Type__c, Primary_Contact_Mobile__c, Primary_Contact_Name__c, Tenant_Short_Name__c, Tenant_Site_Name__c, Tenant_Swift_Code__c, Tenant_Website__c, Tenant_User_Count__c, IsActive__c, CreatedDate, LastModifiedDate from Tenant__c WHERE Organization__c =:OrgId AND Id !=: TId  AND CreatedFromOTP__c=false AND Tenant_Type__c='Corporate' order by CreatedDate desc,Organization__r.Name] ;
                Set<Id> tenantId = new Set<Id>();
                for(Tenant__c listtenantId: listOrganization.tenantLists){
                    tenantId.add(listtenantId.Id);
                }       
                system.debug('====ID==='+tenantId); 
                listOrganization.userLists = [SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c, Profiles__r.Name, CreatedDate, LastModifiedDate,Tenant__r.Tenant_Site_Name__c,User_Profile__c,Tenant__r.Organization__r.Name,Password__c  from User_Management__c WHERE (Tenant__c=: tenantId AND CreatedFromrapid__c=false AND Tenant__r.Tenant_Type__c='Corporate') OR (Profiles__c=:System.label.OBCP_ProfileID AND Password__c!='') order by CreatedDate desc,Tenant__r.Tenant_Site_Name__c];
            }
            
            
        }catch(exception ex){
            system.debug('====getallLists Exception==='+ex.getMessage());
        }
        return listOrganization;
    }
    
    @AuraEnabled
    public static LightAdminProfileV getfilterLists(string tenantId){
        LightAdminProfileV listOrganization = new LightAdminProfileV();
        system.debug('======getallLists====');
        try{         
            Organization__c[] orgList = [SELECT Id from Organization__c WHERE Id =: tenantId] ;
            if(orgList.size()>0){
                listOrganization.orgLists = [SELECT Id, Name, Employees__c, Industry__c, Phone__c, Primary_Contact_Email__c, Primary_Contact_Name__c, Primary_Contact_Phone__c, Revenue__c, Website__c, Tenant_Counts__c, CreatedDate, LastModifiedDate  from Organization__c WHERE Id=: tenantId AND CreatedFromOTP__c=false AND isPrimary__c=true];
                listOrganization.tenantLists = [SELECT Id, IsAdminActivated__c,Tenant_Logo_Url__c,Name,Organization__r.Name, Primary_Contact_Email__c,Tenant_Type__c, Primary_Contact_Mobile__c, Primary_Contact_Name__c, Tenant_Short_Name__c, Tenant_Site_Name__c, Tenant_Swift_Code__c, Tenant_Website__c, Tenant_User_Count__c, IsActive__c, CreatedDate, LastModifiedDate from Tenant__c WHERE Organization__c =: tenantId AND CreatedFromOTP__c=false] ;
                set<Id> userIds = new set<Id>();
                for(Tenant__c t: listOrganization.tenantLists){
                    userIds.add(t.Id);
                }
                listOrganization.userLists = [SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c, Profiles__r.Name, CreatedDate, LastModifiedDate,Tenant__r.Tenant_Site_Name__c,User_Profile__c,Tenant__r.Organization__r.Name from User_Management__c where Tenant__c =: userIds AND CreatedFromrapid__c=false AND Tenant__r.Tenant_Type__c='Corporate'];
            }else{
                listOrganization.tenantLists = [SELECT Id, Name,IsAdminActivated__c,Tenant_Logo_Url__c,Organization__c,Organization__r.Name, Primary_Contact_Email__c,Tenant_Type__c, Primary_Contact_Mobile__c, Primary_Contact_Name__c, Tenant_Short_Name__c, Tenant_Site_Name__c, Tenant_Swift_Code__c, Tenant_Website__c, Tenant_User_Count__c, IsActive__c, CreatedDate, LastModifiedDate from Tenant__c WHERE Organization__c =: tenantId AND CreatedFromOTP__c=false AND Tenant_Type__c='Corporate'] ;
                listOrganization.orgLists = [SELECT Id, Name, Employees__c, Industry__c, Phone__c, Primary_Contact_Email__c, Primary_Contact_Name__c, Primary_Contact_Phone__c, Revenue__c, Website__c, Tenant_Counts__c, CreatedDate, LastModifiedDate  from Organization__c WHERE Id=: listOrganization.tenantLists[0].Organization__c AND CreatedFromOTP__c=false AND isPrimary__c=true];
                set<Id> userIds = new set<Id>();
                for(Tenant__c t: listOrganization.tenantLists){
                    userIds.add(t.Id);
                }
                listOrganization.userLists = [SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c, Profiles__r.Name, CreatedDate, LastModifiedDate,Tenant__r.Tenant_Site_Name__c,User_Profile__c,Tenant__r.Organization__r.Name from User_Management__c where Tenant__c =: userIds AND CreatedFromrapid__c=false AND Tenant__r.Tenant_Type__c='Corporate'];
                
            }
            
            
        }catch(exception ex){
            system.debug('====getfilterLists Exception==='+ex.getMessage());
        }
        return listOrganization;
    }
    
    @AuraEnabled
    public static LightAdminProfileV getUserSearch(string seachtext){
        LightAdminProfileV  listProfile = new LightAdminProfileV ();
        try{
            String query ='';
            Boolean txt = false;
            system.debug('=======seachtext===='+seachtext);                     
            if(seachtext != null && !String.isBlank(seachtext)){
                system.debug('======= If seachtext===='+seachtext);
                List<List<User_Management__c>> lstUser = new List<List<User_Management__c>>();
                lstUser = [FIND :'*'+seachtext+'*' IN ALL FIELDS RETURNING User_Management__c(Id)];
                system.debug('=======lstUser===='+lstUser);
                String setId ='';
                for(List<User_Management__c> lstUserId : lstUser){
                    for(User_Management__c UM: lstUserId){
                        setId +='\''+UM.Id+'\',';
                    }                
                }
                String UId = setId.substring(0,setId.length()-1) ;
                system.debug('=======setId===='+seachtext);
                query = 'SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c, Profiles__r.Name, CreatedDate, LastModifiedDate,Tenant__r.Tenant_Site_Name__c,User_Profile__c,Tenant__r.Organization__r.Name from User_Management__c WHERE Id IN ('+UId+') AND CreatedFromrapid__c=false';
                system.debug('=======query===='+query);
            }else{
                query = 'SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c, Profiles__r.Name, CreatedDate, LastModifiedDate,Tenant__r.Tenant_Site_Name__c,User_Profile__c,Tenant__r.Organization__r.Name from User_Management__c WHERE CreatedFromrapid__c=false';
                
            }            
            listProfile.userLists = Database.query(query);
            
        }catch(exception ex){
            system.debug('=======getUserSearch Exception===='+ex.getMessage());
        }
        return listProfile;
        
    }
    
    @AuraEnabled
    public static LightAdminProfileV getTenantSearch(string seachtext){
        LightAdminProfileV  listProfile = new LightAdminProfileV ();
        try{
            String query ='';
            Boolean txt = false;
            system.debug('=======seachtext tenant===='+seachtext);                     
            if(seachtext != null && !String.isBlank(seachtext)){
                system.debug('======= If seachtext tenant===='+seachtext);
                List<List<Tenant__c>> lstUser = new List<List<Tenant__c>>();
                lstUser = [FIND :'*'+seachtext+'*' IN ALL FIELDS RETURNING Tenant__c(Id)];
                system.debug('=======lstUser===='+lstUser);
                String setId ='';
                for(List<Tenant__c> lstUserId : lstUser){
                    for(Tenant__c UM: lstUserId){
                        setId +='\''+UM.Id+'\',';
                    }                
                }
                String UId = setId.substring(0,setId.length()-1) ;
                system.debug('=======setId===='+seachtext);
                query = 'SELECT Id, Name, Primary_Contact_Email__c,Tenant_Type__c, Primary_Contact_Mobile__c, Primary_Contact_Name__c, Tenant_Short_Name__c, Tenant_Site_Name__c, Tenant_Swift_Code__c, Tenant_Website__c, Organization__r.Name, Tenant_User_Count__c, IsActive__c, CreatedDate, LastModifiedDate from Tenant__c WHERE Id IN ('+UId+') AND CreatedFromOTP__c=false';
                system.debug('=======query===='+query);
            }else{
                query = 'SELECT Id, Name, Primary_Contact_Email__c,Tenant_Type__c, Primary_Contact_Mobile__c, Primary_Contact_Name__c, Tenant_Short_Name__c, Tenant_Site_Name__c, Tenant_Swift_Code__c, Tenant_Website__c, Organization__r.Name, Tenant_User_Count__c, IsActive__c, CreatedDate, LastModifiedDate from Tenant__c WHERE CreatedFromOTP__c=false';
                
            }            
            listProfile.tenantLists = Database.query(query);
            
        }catch(exception ex){
            system.debug('===getTenantSearch Exception==='+ex.getMessage());
        }
        return listProfile;
    }
    
    @AuraEnabled
    public static LightAdminProfileV getOrgSearch(string seachtext){
        LightAdminProfileV  listProfile = new LightAdminProfileV ();
        try{
            String query ='';
            Boolean txt = false;
            system.debug('=======seachtext Org===='+seachtext);                     
            if(seachtext != null && !String.isBlank(seachtext)){
                system.debug('======= If seachtext Org===='+seachtext);
                List<List<Organization__c>> lstUser = new List<List<Organization__c>>();
                lstUser = [FIND :'*'+seachtext+'*' IN ALL FIELDS RETURNING Organization__c(Id)];
                system.debug('=======lstUser===='+lstUser);
                String setId ='';
                for(List<Organization__c> lstUserId : lstUser){
                    for(Organization__c UM: lstUserId){
                        setId +='\''+UM.Id+'\',';
                    }                
                }
                String UId = setId.substring(0,setId.length()-1) ;
                system.debug('=======setId===='+seachtext);
                query = 'SELECT Id, Name, Employees__c, Industry__c, Phone__c, Primary_Contact_Email__c, Primary_Contact_Name__c, Primary_Contact_Phone__c, Revenue__c, Website__c, Tenant_Counts__c, CreatedDate, LastModifiedDate  from Organization__c WHERE Id IN ('+UId+') AND CreatedFromOTP__c=false';
                system.debug('=======query===='+query);
            }else{
                query = 'SELECT Id, Name, Employees__c, Industry__c, Phone__c, Primary_Contact_Email__c, Primary_Contact_Name__c, Primary_Contact_Phone__c, Revenue__c, Website__c, Tenant_Counts__c, CreatedDate, LastModifiedDate  from Organization__c WHERE CreatedFromOTP__c=false';
                
            }            
            listProfile.orgLists = Database.query(query);
            
        }catch(exception ex){
            system.debug('=======getOrgSearch Exception===='+ex.getMessage());
        }
        return listProfile;
    }
    
    @AuraEnabled
    public static String Activechk(string Id){
        String res = '';
        try{
            Tenant__c[] T =[SELECT IsActive__c FROM Tenant__c WHERE id=:Id Limit 1];
            if(T[0].IsActive__c)
            {
                res = 'Active';  
            }else{
                res = '';   
            }
            
        }catch(exception ex){
            system.debug('=======updateTenantactive Exception====='+ex.getMessage());
            res = ex.getMessage();
        }
        return res;
        
    }
    @AuraEnabled
    public static String updateTenantactive(string TenantId,String LoggedUserID){
        String res = '';
        try{
            Tenant__c[] T =[SELECT Tenant_Site_Name__c FROM Tenant__c WHERE id=:TenantId Limit 1];
            User_Management__c[] UserName =[SELECT First_Name__c,Last_Name__c FROM User_Management__c WHERE id=:LoggedUserID Limit 1];
            Helper.ActivityLogInsertCallForRecord(LoggedUserID,TenantId,'Tenant Activation',UserName[0].First_Name__c+' '+UserName[0].Last_Name__c+' is Activatied the '+T[0].Tenant_Site_Name__c,false);
            Tenant__c tenant = new Tenant__c();
            tenant.Id = TenantId;
            tenant.IsActive__c = true;
            update tenant;   
            
            
            res = 'Success';
        }catch(exception ex){
            system.debug('=======updateTenantactive Exception====='+ex.getMessage());
            res = ex.getMessage();
        }
        return res;
        
    }
    @AuraEnabled
    public static String Tenantdeactivecall(string TenantId,String LoggedUserID){
        String res = '';
        try{
            Tenant__c[] T =[SELECT Tenant_Site_Name__c FROM Tenant__c WHERE id=:TenantId Limit 1];
            User_Management__c[] UserName =[SELECT First_Name__c,Last_Name__c FROM User_Management__c WHERE id=:LoggedUserID Limit 1];
            Helper.ActivityLogInsertCallForRecord(LoggedUserID,TenantId,'Tenant Deactivation',UserName[0].First_Name__c+' '+UserName[0].Last_Name__c+' is Deactivated the '+T[0].Tenant_Site_Name__c,false);
            
            
            List<User_Management__c> UpdateUMList =NEW List<User_Management__c>();
            List<User_Management__c> UMList=[SELECT id FROM User_Management__c WHERE Tenant__c=:TenantId];
            for(User_Management__c UMobj:UMList)
            {
                User_Management__c UM = NEW User_Management__c();
                UM.Active__c=false;
                UM.id=UMobj.id;
                UpdateUMList.add(UM);
            }
            if(UpdateUMList.size()>0){
                update UpdateUMList; 
            }
            Tenant__c tenant = new Tenant__c();
            tenant.Id = TenantId;
            tenant.IsActive__c = false;
            update tenant;            
            res = 'Success';
        }catch(exception ex){
            system.debug('=======updateTenantactive Exception====='+ex.getMessage());
            res = ex.getMessage();
        }
        return res;
        
    }
    @AuraEnabled
    public static LightAdminProfileV getOrganization(string username,string orgname){
        system.debug('==getOrganization=='+username+'::::::'+orgname);
        LightAdminProfileV listOrganization = new LightAdminProfileV();
        try{
            listOrganization.organization = [SELECT Id, Name,Description__c, Employees__c, Industry__c, Phone__c, Primary_Contact_Email__c, Primary_Contact_Name__c, Primary_Contact_Phone__c, Revenue__c, Website__c, Tenant_Counts__c, CreatedDate, LastModifiedDate,Billing_Street__c,Billing_City__c,Billing_State_Province__c,Billing_Zip_Postal_Code__c,Billing_Country__c,Shipping_Street__c,Shipping_City__c,Shipping_State_Province__c,Shipping_Zip_Postal_Code__c,Shipping_Country__c  from Organization__c WHERE Id =: orgname];
            listOrganization.Activity_Log = [SELECT id,Action_By__c,Action_By__r.First_Name__c,Action_By__r.Last_Name__c,CreatedDate,Operation__c,Operation_Log__c,RecordID__c FROM Activity_Log__c WHERE RecordID__c=: orgname ORDER BY CreatedDate DESC];
            
        }catch(exception ex){
            system.debug('====getOrganization Exception==='+ex.getMessage());
        }        
        return listOrganization;
    }
    
    @AuraEnabled
    public static LightAdminProfileV getTenant(string username,string tenantname){
        system.debug('==getOrganization=='+username+'::::::'+tenantname);
        LightAdminProfileV listTenant = new LightAdminProfileV();
        try{
            listTenant.tenant = [SELECT Id, Name,Organization__r.Name,Tenant_Logo_Url__c, Primary_Contact_Email__c,Tenant_Type__c, Primary_Contact_Mobile__c, Primary_Contact_Name__c, Tenant_Short_Name__c, Tenant_Site_Name__c, Tenant_Swift_Code__c, Tenant_Website__c, Tenant_User_Count__c, IsActive__c, CreatedDate, LastModifiedDate,Tenant_Bank_Type__c,Tenant_vision__c from Tenant__c WHERE Id =: tenantname] ;
            listTenant.Activity_Log = [SELECT id,Action_By__c,Action_By__r.First_Name__c,Action_By__r.Last_Name__c,CreatedDate,Operation__c,Operation_Log__c,RecordID__c FROM Activity_Log__c WHERE RecordID__c=: tenantname ORDER BY CreatedDate DESC];
            listTenant.TMCP  =[SELECT id,Tenant_Approved__r.Tenant_Site_Name__c,Tenant_Approved__r.Tenant_Logo_Url__c,Selectchk__c FROM Tenant_Mapping__c WHERE Tenant__c=:tenantname AND Selectchk__c=true AND Tenant_Approved__r.Tenant_Bank_Type__c='Counterparty Bank'];     
            listTenant.TMIB  =[SELECT id,Tenant_Approved__r.Tenant_Site_Name__c,Tenant_Approved__r.Tenant_Logo_Url__c,Selectchk__c FROM Tenant_Mapping__c WHERE Tenant__c=:tenantname AND Selectchk__c=true AND Tenant_Approved__r.Tenant_Bank_Type__c='Issuing Bank'];     
            
        }catch(exception ex){
            system.debug('====getTenant Exception==='+ex.getMessage());
        }        
        return listTenant;
    }
    
    @AuraEnabled
    public static LightAdminProfileV getOrganizationdetails(string organizationId, string username){
        LightAdminProfileV results = new LightAdminProfileV();
        try{
            results.organization = [SELECT Id, Name, Website__c,Description__c,Revenue__c,isPrimary__c,Phone__c,Industry__c,Employees__c,Primary_Contact_Name__c,Primary_Contact_Email__c,Primary_Contact_Phone__c,Billing_Street__c,Billing_City__c,Billing_State_Province__c,Billing_Zip_Postal_Code__c,Billing_Country__c,Shipping_Street__c,Shipping_City__c,Shipping_State_Province__c,Shipping_Zip_Postal_Code__c,Shipping_Country__c,Organization_Logo_URL__c from Organization__c WHERE Id=: organizationId];
            
        }catch(exception ex){
            system.debug('===getOrganizationdetails Exception==='+ex.getMessage());
        }
        return results;
    }  
    
    @AuraEnabled
    public static String updateOrganization(String LoggedUserID,Organization__c Organization, string username){
        system.debug('====Enter updateOrganization======='+username);
        string retvalue ='';
        try{
            Organization.LastModifiedBy__c = username;
            update Organization;
            retvalue = 'Success';
            User_Management__c[] UM =[SELECT First_Name__c,Last_Name__c FROM User_Management__c WHERE id=:LoggedUserID Limit 1];
            Helper.ActivityLogInsertCallForRecord(LoggedUserID,Organization.Id,'Updated Organization',UM[0].First_Name__c+' '+UM[0].Last_Name__c+' is Updated Organization',false);
            
        }catch(exception ex){
            system.debug('===updateOrganization Exception==='+ex.getMessage());
            retvalue = 'Failure';
        }
        return retvalue;
    }
    
    @AuraEnabled
    public static LightAdminProfileV getTenantdetails(string tenantId, string username){
        LightAdminProfileV results = new LightAdminProfileV();
        try{
            results.tenant = [SELECT Id, Name,Organization__c,Tenant_Type__c,Tenant_Swift_Code__c,Tenant_Bank_Type__c,Data_Visibility__c,Tenant_Short_Name__c,Tenant_Site_Name__c,Tenant_vision__c,Tenant_Website__c,Tenant_Logo_Url__c,Tenant_Banner_Url__c,Tenant_News_Message__c,Tenant_News_Message_URL__c,Tenant_Footer_Message__c,Primary_Contact_Name__c,Primary_Contact_Email__c,Primary_Contact_Mobile__c from Tenant__c WHERE Id=: tenantId];
            results.userLists= [SELECT id,Country_Code__c,CreatedFromrapid__c,Profiles__c,Mobile__c,Title__c,First_Name__c,User_Email__c,Last_Name__c,Street__c,Phone__c,Tenant__r.Tenant_Swift_Code__c,Tenant__r.Tenant_Site_Name__c,City__c,Department__c,State_Province__c,Zip_Postal_Code__c,Country__c,User_Title__c FROM User_Management__c WHERE id=:username Limit 1];
            
        }catch(exception ex){
            system.debug('==getOrganizationdetails Exception=='+ex.getMessage());
        }
        return results;
    }
    
    
    @AuraEnabled
    public static string UpdateRegForm1(String LoggedUserID,User_Management__c UM){
        String retValue = '';
        try{
            UM.Id=LoggedUserID;
            User_Management__c[] UserName = [SELECT First_Name__c,Last_Name__c FROM User_Management__c WHERE id=:LoggedUserID Limit 1];
            Helper.ActivityLogInsertCallForRecord(LoggedUserID,UM.id,'Update Tenant User',UserName[0].First_Name__c+' '+UserName[0].Last_Name__c+' is Updated User - '+UM.First_Name__c+' '+UM.Last_Name__c,false);
            Update UM;
        }
        catch(Exception ex){
            System.debug('addRemoveCounterParty: ' + ex.getMessage());
            retValue = 'ex.getMessage()';
        }
        return retValue;
    }
    @AuraEnabled
    public static String updateTenant(Tenant__c Tenant, string username){
        system.debug('====Enter updateOrganization======='+username);
        string retvalue ='';
        try{
            Tenant.LastModifiedBy__c = username;
            Tenant.IsAdminUpdated__c =true;
            update Tenant;
            User_Management__c[] UM =[SELECT id,Profiles__c,First_Name__c,Last_Name__c FROM User_Management__c WHERE id=:username Limit 1];
            Helper.ActivityLogInsertCallForRecord(username,Tenant.Id,'Updated Tenant',UM[0].First_Name__c+' '+UM[0].Last_Name__c+' is Updated Tenant',false);
            
            /*User_Management__c UM1= New User_Management__c();
UM1.id = UM[0].id;
UM1.Profiles__c = system.label.BMCP_ProfileID;
update UM1;
*/
            /*EmailManager em = new EmailManager();
List<String> CcAddresses = new List<String>();
List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =:username];                 
for( User_Management__c lstStr: listUserEmail){
CcAddresses.add(lstStr.User_Email__c) ;
}
String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'UserActiveNotificationEmailTemp' LIMIT 1].Id;
em.sendMailWithTemplate(listUserEmail[0].User_Email__c , CcAddresses , temaplateId , userinfo.getuserid(), username);
*/
            
            // Step 0: Create a master list to hold the emails we'll send
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            // Step 1: Create a new Email
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            // Step 2: Set list of people who should get the email
            List<String> sendTo = new List<String>();
            sendTo.add(System.label.TradeAix_email_ID);
            mail.setToAddresses(sendTo);
            // Step 3: Set who the email is sent from
            mail.setReplyTo('satheesh@aixchane.co.in');
            mail.setSenderDisplayName('TradeAix');
            // Step 4. Set email contents - you can use variables!
            mail.setSubject(Tenant.Tenant_Site_Name__c+' Active Notification - '+UM[0].First_Name__c+' '+UM[0].Last_Name__c); 
            String body = 'Hi TradeAix Admin,<br/><br/>';                
            body += Tenant.Tenant_Site_Name__c+' has requested you to activate the '+Tenant.Tenant_Site_Name__c+' tenant. <br/><br/>';
            body +='Please click on the link and continue further.<br/><br/>';
            body += 'https://clientdemo-stratizantqa.cs17.force.com/tradeaix/s/admin<br/><br/>';
            body += 'Note: Please go to Administration / Tenant / Click Active Button to Activate the Tenant.  <br/>';
            mail.setHtmlBody(body);
            
            // Step 5. Add your email to the master list
            mails.add(mail);
            
            // Step 6: Send all emails in the master list
            Messaging.sendEmail(mails);
            retvalue = 'Success';
        }catch(exception ex){
            system.debug('===updateOrganization Exception==='+ex.getMessage());
            retvalue = 'Failure';
        }
        return retvalue;
    }  
    private static Boolean isPublished(String creditUnionId,String Tid) {
        List<Tenant_Mapping__c> lstApproved = [SELECT Tenant_Approved__r.Id, Tenant_Approved__r.Tenant_Site_Name__c,Selectchk__c FROM Tenant_Mapping__c WHERE Tenant__c=:creditUnionId AND Tenant_Approved__c=:Tid];
        Boolean retValue = false;
        for(Tenant_Mapping__c appr : lstApproved){ 
            if(appr.Selectchk__c){
                retValue=true;
            }
            else{
                retValue = false;
            }
        }
        
        return retValue;
    }
    @AuraEnabled
    public static List<SelectListItem> getTMapping(string creditUnionId){
        List<SelectListItem> retValues = new List<SelectListItem>();
        try{
            List<Tenant__c> TList = [SELECT id,Tenant_Site_Name__c FROM Tenant__c Where Tenant_Bank_Type__c='Counterparty Bank' AND CreatedFromOTP__c=false];
            for(Tenant__c appr : TList){ 
                retValues.add(new SelectListItem(
                    appr.Id,
                    appr.Tenant_Site_Name__c,
                    isPublished(
                        creditUnionId,appr.id
                    )
                    
                ));
            }  
            
            retValues.sort();            
        } catch(Exception ex){
            System.debug('getApprovedCreditUnions: ' + ex.getMessage());
        }
        return retValues;
    }
    @AuraEnabled
    public static List<SelectListItem> getTMappingIB(string creditUnionId){
        List<SelectListItem> retValues = new List<SelectListItem>();
        try{
            List<Tenant__c> TList = [SELECT id,Tenant_Site_Name__c FROM Tenant__c Where Tenant_Bank_Type__c='Issuing Bank' AND CreatedFromOTP__c=false];
            for(Tenant__c appr : TList){ 
                retValues.add(new SelectListItem(
                    appr.Id,
                    appr.Tenant_Site_Name__c,
                    isPublished(
                        creditUnionId,appr.id
                    )
                    
                ));
            }  
            
            retValues.sort();            
        } catch(Exception ex){
            System.debug('getApprovedCreditUnions: ' + ex.getMessage());
        }
        return retValues;
    }
    
    
    @AuraEnabled
    public static string addRemoveCounterParty(string creditUnionId, boolean forAdd,String TId,string LoggedUserId){
        system.debug('##addRemoveCounterParty##'+creditUnionId);
        system.debug('##forAdd##'+forAdd);
        system.debug('##LoggedUserId##'+LoggedUserId);
        String retValue = '';
        try{
            
            if(forAdd)
            {
                Tenant_Mapping__c TM =NEW Tenant_Mapping__c();
                TM.Tenant_Approved__c=creditUnionId;
                TM.Tenant__c = TId;
                TM.Selectchk__c =True;
                Insert TM;
                system.debug('+'+TM.id);
                Tenant_Mapping__c[] TMChk = [SELECT id,Tenant_Approved__r.Id, Tenant_Approved__r.Tenant_Site_Name__c,Selectchk__c FROM Tenant_Mapping__c WHERE id=:TM.id Limit 1];
                Helper.ActivityLogInsertCallForRecord(LoggedUserId,TId,'Selected','CP - '+TMChk[0].Tenant_Approved__r.Tenant_Site_Name__c+'- Selected',false);
                
            }else{
                Tenant_Mapping__c[] TMChk = [SELECT id,Tenant_Approved__r.Id, Tenant_Approved__r.Tenant_Site_Name__c,Selectchk__c FROM Tenant_Mapping__c WHERE id=:creditUnionId Limit 1];
                TMChk[0].Selectchk__c = forAdd;
                Helper.ActivityLogInsertCallForRecord(LoggedUserId,TId,'Un-Selected','CP - '+TMChk[0].Tenant_Approved__r.Tenant_Site_Name__c+'- Un-Selected',false);
                delete TMChk;
            }
            retValue = 'Success';
        } catch(Exception ex){
            System.debug('addRemoveCounterParty: ' + ex.getMessage());
            retValue = 'ex.getMessage()';
        }
        return retValue;
    }
    
}
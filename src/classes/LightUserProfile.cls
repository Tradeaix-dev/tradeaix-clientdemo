public class LightUserProfile {    
    @AuraEnabled 
    public static Organization__c[] loadorgs(String username) {
        return Helper.loadorgs(username);
    }
     @AuraEnabled
    public static String CheckTenantUserName(String UserName) { 
        String response = '';
        try {  
            Integer count = [SELECT count() FROM User_Management__c WHERE User_Name__c = : UserName]; 
            if(count > 0){
                response = 'Already Used';
            }
            else { 
                response = '';
            }  
        } catch(Exception ex){
            System.debug('Tenant User - CheckTenantUserName() : ' + ex.getMessage());
            response = '';
        }
        return response; 
    } 
    @AuraEnabled
    public static Userloginv1 getTenant(String TenantId) {
        Userloginv1 ULogin =NEW Userloginv1();
        try{
            
            String RetVal ='';
            ApexPages.PageReference lgn;
            lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', null);
            system.debug('=====login user===='+userinfo.getName()); 
            ULogin.userlists = [SELECT Id,Tenant_Site_Name__c,Tenant_Logo_Url__c,Tenant_Banner_Url__c,Tenant_vision__c,Name from Tenant__c Where Tenant_Short_Name__c=:TenantId Limit 1];
            system.debug('####'+ULogin);
        }
        catch (Exception ex) {
            //return ex.getMessage();            
        }
        return ULogin;
    }
    @AuraEnabled
    public static LightUserProfileV getUserprofile(string username){
        LightUserProfileV  listProfile = new LightUserProfileV ();
        try{  
            system.debug('======Login UserId==='+username+'-a0c0x000000tWGXAA2');
            if(system.label.Admin_TradeAix.contains(username)){
               listProfile.tradeaixAdmin = true; 
            }else{
                 listProfile.tradeaixAdmin = false; 
            }
            listProfile.orgLists = [SELECT Id, Name from Organization__c];
            listProfile.tenantLists = [SELECT Id, Tenant_Site_Name__c from Tenant__c];
            system.debug('=====Enter Try===='+username); 
            Profiles__c[] superAdmin = [SELECT Id, Name from Profiles__c WHERE Name IN('Seller Admin Community Plus','Admin Community Plus')];         
            system.debug('=====superAdmin======='+superAdmin);  
            listProfile.userlists = [SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c,Profiles__r.Name,Tenant__c,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,LastModifiedBy__r.First_Name__c,LastModifiedBy__r.Last_Name__c,Tenant__r.Tenant_Site_Name__c,Tenant__r.Organization__r.Name,CreatedDate,LastModifiedDate,User_Profile__c,Time_Zone__c,Locale__c,Language__c, Task_Assignee__c   from User_Management__c WHERE Id =: username  order  by Tenant__r.Organization__r.Name] ;
            system.debug('=====userlists======='+listProfile.userlists);  
            if(listProfile.userlists.Profiles__c == 'a0F6C0000006ruyUAA' && listProfile.userlists.Id != 'a0c0x000000tUyWAAU'){
                system.debug('====Trade Admin Entry===');
                listProfile.userResults=[SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c,Profiles__r.Name,Tenant__c,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,LastModifiedBy__r.First_Name__c,LastModifiedBy__r.Last_Name__c,Tenant__r.Tenant_Site_Name__c,  Tenant__r.Organization__r.Name,CreatedDate,LastModifiedDate,User_Profile__c,Time_Zone__c,Locale__c,Language__c, Task_Assignee__c   from User_Management__c WHERE Task_Assignee__c = true  order by Tenant__r.Organization__r.Name];
            
            }else if(listProfile.userlists.Id == 'a0c0x000000tUyWAAU'){
                listProfile.userResults=[SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c,Profiles__r.Name,Tenant__c,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,LastModifiedBy__r.First_Name__c,LastModifiedBy__r.Last_Name__c,Tenant__r.Tenant_Site_Name__c,  Tenant__r.Organization__r.Name,CreatedDate,LastModifiedDate,User_Profile__c,Time_Zone__c,Locale__c,Language__c, Task_Assignee__c  from User_Management__c   WHERE Tenant__c IN('a0f0x000000XTbc','a0f0x000000XNmB')  order by Tenant__r.Organization__r.Name];
            }
            else{
                system.debug('====Admin Entry===');
                listProfile.userResults=[SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c,Profiles__r.Name,Tenant__c ,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,LastModifiedBy__r.First_Name__c,LastModifiedBy__r.Last_Name__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Organization__r.Name,CreatedDate,LastModifiedDate,User_Profile__c,Time_Zone__c,Locale__c,Language__c, Task_Assignee__c  from User_Management__c WHERE Tenant__c =: listProfile.userlists.Tenant__c  order by Tenant__r.Organization__r.Name];
            }
            if(listProfile.userlists.Profiles__c == superAdmin[0].Id || listProfile.userlists.Profiles__c == superAdmin[1].Id){
                system.debug('=======Profile Id==='+listProfile.userlists.Profiles__c  + superAdmin[0].Id);
                listProfile.isAdmin = true;
            } 
            listProfile.Activity_Log=[SELECT id,Action_By__c,Action_By__r.First_Name__c,Action_By__r.Last_Name__c,CreatedDate,Operation__c,Operation_Log__c,RecordID__c FROM Activity_Log__c WHERE Action_By__c=:username AND ErrorCall__c=false ORDER BY CreatedDate DESC LIMIT 100];
            listProfile.LoginHistory=[SELECT Login_Time__c,BrowserName__c,IP_Address__c,Login_URL__c,Location__c,Logout_Time__c FROM Login_History__c WHERE User_Management__c=:username ORDER BY CreatedDate DESC LIMIT 100];
            listProfile.usernotificationLists = [SELECT Id, Name, Email__c, Notification__c, Tenant_User__c from User_Notification__c WHERE Tenant_User__c =: username];
            listProfile.userResults1=[SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c,Profiles__r.Name,Tenant__c ,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,LastModifiedBy__r.First_Name__c,LastModifiedBy__r.Last_Name__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Organization__r.Name,CreatedDate,LastModifiedDate,User_Profile__c,Time_Zone__c,Locale__c,Language__c, Task_Assignee__c  from User_Management__c WHERE  Task_Assignee__c = true  order by Tenant__r.Organization__r.Name];
            List<String> lstring = new List<String>();
            Schema.DescribeFieldResult fieldResult = User_Management__c.Country_Code__c.getDescribe();            
            List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();            
            for(Schema.picklistEntry f:ple){                
                   lstring.add('+ '+f.getLabel());
            }  
            system.debug('=====listProfile.countrycodelist before======='+lstring); 
            listProfile.countrycodelist = lstring;
            system.debug('=====listProfile.countrycodelist after======='+listProfile.countrycodelist); 
            //Helper.ActivityLogInsertCall(username,'getUserprofile()','getUserprofile()');
        }
        catch(exception ex){
            Helper.ActivityLogInsertCall('','getUserprofile()',ex.getmessage(),true);
            system.debug('=====Exception===='+ex.getmessage());
            system.debug('=====Exception===='+ex.getLineNumber());
        }
        return listProfile ;
    }
    
    @AuraEnabled
    public static String saveprofile(string username, User_Management__c tenant, string orgId, string tenantId){
        system.debug('====Enter saveUserprofile======='+username);
        try{
            Boolean flag=true;
            system.debug('====Org & TenantId==='+orgId+tenantId);
            system.debug('=====Enter Try saveUserprofile===='+tenant.User_Profile__c);
            if(tenant.User_Profile__c != null){
                string profilename = '';
                if(tenant.User_Profile__c == 'Party Inputter' || tenant.User_Profile__c == 'Party Approver'){
                    profilename = 'Seller Associate Community Plus';
                }
                if(tenant.User_Profile__c == 'Party Validator'){
                    profilename = 'Seller Validator Community Plus';
                }
                if(tenant.User_Profile__c == 'Counterparty Validator'){
                    profilename = 'Buyer Validator Community Plus';
                }
                  if(tenant.User_Profile__c == 'Counterparty Manager'){
                    profilename = 'Buyer Manager Community Plus';
                }
                if(tenant.User_Profile__c == 'Party Manager'){
                    profilename = 'Seller Manager Community Plus';
                }
                if(tenant.User_Profile__c == 'Party Admin'){
                    profilename = 'Seller Admin Community Plus';
                }
                system.debug('===Profile Name==='+profilename);
                List<Profiles__c> profileId = [Select Id,Name from Profiles__c WHERE Name =: profilename LIMIT 1];
                List<User_Management__c> TId = [Select Name,First_Name__c,Last_Name__c,Tenant__c,User_Name__c from User_Management__c WHERE Id =:username LIMIT 1];
                system.debug('=====Enter Try profileId===='+profileId + ':'+TId);
                If(tenant.Id == null){
                    if(System.Label.Admin_TradeAix.contains(username)){
                        tenant.Tenant__c = tenantId;
                       tenant.Profiles__c = profileId[0].Id;
                    }else{
                        tenant.Tenant__c = TId[0].Tenant__c;
                        tenant.Profiles__c = profileId[0].Id;
                    }
                    tenant.Active__c=true;
                } 
                //if(tenant.Id != username){
                    tenant.CreatedBy__c = username;
                    tenant.LastModifiedBy__c = username;
//                } 
                system.debug('====tenantIdif===='+tenant.Id);
                system.debug('====tenantIdif===='+TId[0].First_Name__c);
                if(tenant.Id == null)
                { 
                    flag =false;
                    
                }else{
                    system.debug('====tenantIdif===='+tenant.Id);
                    
                    Helper.ActivityLogInsertCallForRecord(username,tenant.Id,'Updated User',TId[0].First_Name__c+' '+TId[0].Last_Name__c+' is Updated User Details - '+tenant.First_Name__c+' '+tenant.Last_Name__c,false);
                    
                }
            }
            system.debug('====User Profile values===='+tenant);
            upsert tenant;
            if(!flag){
                system.debug('====tenantIdElse===='+tenant.Id);
                List<User_Management__c> TId1 = [Select Name,First_Name__c,Last_Name__c,Tenant__c,User_Name__c from User_Management__c WHERE Id =:username LIMIT 1];
                
                Helper.ActivityLogInsertCallForRecord(username,tenant.Id,'New User',TId1[0].First_Name__c+' '+TId1[0].Last_Name__c+' is Created New User - '+tenant.First_Name__c+' '+tenant.Last_Name__c,false);
                
            }
            
        }
        catch(exception ex){
            system.debug('=====Enter Exception saveUserprofile===='+ex.getmessage()+ex.getLinenumber());
            //Helper.ActivityLogInsertCall('','Error-Save Profile',ex.getmessage());
            
        }
        return tenant.Id;
        
    }
    
    @AuraEnabled
    public static String changePassword(string username, string oldpassword, string newpassword){
        string resultmsg = '';
        try{
            system.debug('======username::+oldpassword::+newpassword'+username+'::'+oldpassword+':'+newpassword);
            User_Management__c[] userMgmt = [SELECT Id,User_Name__c, Name , Password__c from User_Management__c WHERE Id =: username AND Password__c =: oldpassword LIMIT 1];
            system.debug('====userMgmt==='+userMgmt);
            if(userMgmt.size() > 0){
                system.debug('========Enter try====='+username+newpassword);
                User_Management__c UM = new User_Management__c();
                UM.Id = username;
                UM.Password__c = newpassword;
                UM.Last_Password_Change_or_Reset__c = system.now();
                update UM;
                resultmsg = 'Success';
                
            }else{
                 resultmsg = 'Old Password does not match.';
            }
            Helper.ActivityLogInsertCall(username,'Password Changed',userMgmt[0].User_Name__c+ 'Got changed password',false);
        }catch(exception ex){
            system.debug('======Exception======'+ex.getMessage());
            resultmsg = ex.getMessage();
            Helper.ActivityLogInsertCall(username,'getUserprofile()',ex.getmessage(),true);
        }
        return resultmsg;
        
    }
    
    @AuraEnabled
    public static LightUserProfileV getUserList(string username){
        LightUserProfileV result = new LightUserProfileV();
        try{
            system.debug('===== Enter Try getUserList====');
            String tenantId = [Select Tenant__c from User_Management__c WHERE Id =: username].Tenant__c;
            result.userResults = [Select Id , Name, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c,Enable_Email__c, Enable_Notification__c from User_Management__c WHERE Id !=: username AND Tenant__c =: tenantId];
            
        }catch(exception ex){
            system.debug('=====Exception getUserList===='+ex.getMessage());
        }
        return result;
    }
    
    @AuraEnabled
    public static String forgotPasswordUpdate(string username, string newpassword){
        ApexPages.PageReference lgn;
        lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', null);
        system.debug('=====login user===='+userinfo.getName());           
        
        system.debug('=====forgotPasswordUpdate');
        String result='';
        try{
            system.debug('=====username===='+username);
            User_Management__c[] userId = [SELECT Id from User_Management__c WHERE UserEmail_Encrypt__c =: username+'='];
            if(userId.size() > 0){
                User_Management__c UM = new User_Management__c();
                UM.Id = userId[0].Id;
                UM.Password__c = newpassword;
                UM.Last_Password_Change_or_Reset__c = system.now();
                update UM;
                
               Login_History__c LH = new Login_History__c();
               LH.User_Management__c = userId[0].Id;
               LH.Forgot_Password_Attempt__c = true;
               LH.Password_Requested_Date__c = system.now();
               insert LH;
                
            }
            result = 'Success';
        }catch(exception ex){
            system.debug('=====Exception forgotPasswordUpdate====='+ex.getMessage());
            result = ex.getMessage();
        }
        return result;
        
    }
    
    @AuraEnabled
    public static String changeNotification(string username, boolean notification, boolean email){
        string results = '';
        try{
            User_Management__c UM = new User_Management__c();
            UM.Id = username;
            UM.Enable_Notification__c = notification;
            UM.Enable_Email__c = email;
            update UM;
            results = 'Success';
            
        }catch(exception ex){
            system.debug('======Exception changeNotification======'+ex.getMessage());
            results = ex.getMessage();
        }
        return results;
    }
    
    @AuraEnabled
    public static LightUserProfileV getUsername(string username){
        
        ApexPages.PageReference lgn;
        lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', null);
        system.debug('=====login user===='+userinfo.getName());   
        LightUserProfileV  listProfile = new LightUserProfileV ();        
        try{
            listProfile.userlists = [SELECT Id, Name, UserEmail_Encrypt__c,Last_Password_Change_or_Reset__c, User_Name__c from User_Management__c WHERE UserEmail_Encrypt__c =: username+'='];
            system.debug('======listProfile.userlists======'+listProfile.userlists);
            
        } catch(exception ex){
            system.debug('======Exception changeNotification======'+ex.getMessage());           
        }
        return listProfile;
    }
    
    @AuraEnabled
    public static LightUserProfileV getUserSearch(string username, string seachtext){
        LightUserProfileV  listProfile = new LightUserProfileV ();
        try{
            String query ='';
            Boolean txt = false;
            system.debug('=======seachtext===='+seachtext);
            String TId = [Select Tenant__c from User_Management__c WHERE Id =: username].Tenant__c; 
            if(String.isBlank(seachtext)){
                system.debug('=======Boolean true===='+seachtext);
                txt = true;
            }
            if(seachtext != null && txt == false){
                system.debug('======= If seachtext===='+seachtext);
                List<List<User_Management__c>> lstUser = new List<List<User_Management__c>>();
                lstUser = [FIND :'*'+seachtext+'*' IN ALL FIELDS RETURNING User_Management__c(Id)];
                system.debug('=======lstUser===='+lstUser);
                String setId ='';
                for(List<User_Management__c> lstUserId : lstUser){
                    for(User_Management__c UM: lstUserId){
                        setId +='\''+UM.Id+'\',';
                    }                
                }
                String UId = setId.substring(0,setId.length()-1) ;
                system.debug('=======setId===='+TId+'::::'+seachtext);
                if(TId == System.Label.TradeAix_Tenant_Id){
                    query = 'SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Organization__r.Name,User_Profile__c,Time_Zone__c,Locale__c,Language__c,CreatedDate from User_Management__c WHERE Id IN ('+UId+')';                    
                }else{
                    query = 'SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Organization__r.Name,User_Profile__c,Time_Zone__c,Locale__c,Language__c,CreatedDate from User_Management__c WHERE Id IN ('+UId+') AND Tenant__c = ';
                    query += '\''+TId+'\'';                   
                }
                system.debug('=======query===='+query);
                
            }else{
                if(TId == System.Label.TradeAix_Tenant_Id){
                    query = 'SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Organization__r.Name,User_Profile__c,Time_Zone__c,Locale__c,Language__c,CreatedDate from User_Management__c';                    
                }else{
                    query = 'SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Organization__r.Name,User_Profile__c,Time_Zone__c,Locale__c,Language__c,CreatedDate from User_Management__c WHERE Tenant__c = ';
                    query += '\''+TId+'\'';
                }
                
            }            
            listProfile.userResults = Database.query(query);
                                                                        
        }catch(exception ex){
            system.debug('=======getUserSearch Exception===='+ex.getMessage());
        }
        return listProfile;        
    }
    @AuraEnabled
    public static LightUserProfileV getTenantlists(string orgname){
        LightUserProfileV result = new LightUserProfileV();
        try{
            if(orgname == '0'){
               result.tenantLists = [SELECT Id, Tenant_Site_Name__c from Tenant__c ];
            }else{
               result.tenantLists = [SELECT Id, Tenant_Site_Name__c from Tenant__c WHERE Organization__c =: orgname]; 
            }
            
        }catch(exception ex){
            system.debug('=======getUserSearch Exception===='+ex.getMessage());
        }
        return result;
    }
    
    @AuraEnabled
    public static String changeMynotifications(string username, String listnotify){
        try{
            Type listArrType = Type.forName('List<string>');
            List<string> wrapperList = (List<string>) JSON.deserialize(listnotify, listArrType);
            system.debug('====wrapperList==='+wrapperList); 
            Map<String, Id> notifyMap = new Map<String,Id>();
            List<User_Notification__c> existingNotify = [SELECT Id,Name from User_Notification__c WHERE Tenant_User__c =: username];
            List<User_Notification__c> lstNotification = new List<User_Notification__c>();
            if(existingNotify.size()>0){
                for(User_Notification__c UNs: existingNotify){
                    notifyMap.put(UNs.Name, UNs.Id);
                }
                for(String ls:wrapperList){
                    User_Notification__c UN = new User_Notification__c();
                    UN.Id = notifyMap.get(ls.Split( ':')[0]);
                    UN.Name = ls.Split( ':')[0];
                    UN.Notification__c = Boolean.valueOf(ls.Split( ':')[1]);
                    UN.Email__c =  Boolean.valueOf(ls.Split( ':')[2]);
                    UN.Tenant_User__c = username;
                    lstNotification.add(UN);
                }
                if(lstNotification.size()>0){
                    update lstNotification;
                    User_Management__c[] UM =[SELECT First_Name__c,Last_Name__c FROM User_Management__c WHERE id=:username Limit 1];
                    Helper.ActivityLogInsertCall(username,'Update Notification',UM[0].First_Name__c+' '+UM[0].Last_Name__c+' is Updated Notification.',false);
            
                }
                
            }else{
                for(String ls:wrapperList){
                    User_Notification__c UN = new User_Notification__c();
                    UN.Name = ls.Split( ':')[0];
                    UN.Notification__c = Boolean.valueOf(ls.Split( ':')[1]);
                    UN.Email__c =  Boolean.valueOf(ls.Split( ':')[2]);
                    UN.Tenant_User__c = username;
                    lstNotification.add(UN);
                }
                system.debug('##'+lstNotification.size());
                if(lstNotification.size()>0){
                    insert lstNotification;
                    User_Management__c[] UM =[SELECT First_Name__c,Last_Name__c FROM User_Management__c WHERE id=:username Limit 1];
                    Helper.ActivityLogInsertCall(username,'Update Notification',UM[0].First_Name__c+' '+UM[0].Last_Name__c+' is Updated Notification.',false);
            
                }
            }
            return 'Success';  
        }catch(exception ex){
            system.debug('====changeMynotifications==='+ex.getMessage());
            Helper.ActivityLogInsertCall(username,'changeMynotifications',ex.getmessage(),true);
            return ex.getMessage();
        }
    }
    
    @AuraEnabled
    public static LightUserProfileV getfilterUserList(string tenantId){
        LightUserProfileV result = new LightUserProfileV();
        try{
            set<Id> userIds = new set<Id>();
            Organization__c[] orgList = [SELECT Id from Organization__c WHERE Id =: tenantId] ;
            if(orgList.size()>0){
                result.tenantLists = [SELECT Id, Name,Organization__r.Name, Primary_Contact_Email__c,Tenant_Type__c, Primary_Contact_Mobile__c, Primary_Contact_Name__c, Tenant_Short_Name__c, Tenant_Site_Name__c, Tenant_Swift_Code__c, Tenant_Website__c, Tenant_User_Count__c, IsActive__c, CreatedDate, LastModifiedDate from Tenant__c WHERE Organization__c =: tenantId] ;
               
                for(Tenant__c t: result.tenantLists){
                    userIds.add(t.Id);
                }
                result.userResults=[SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c,Profiles__r.Name,Tenant__c ,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,LastModifiedBy__r.First_Name__c,LastModifiedBy__r.Last_Name__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Organization__r.Name,CreatedDate,LastModifiedDate,User_Profile__c,Time_Zone__c,Locale__c,Language__c  from User_Management__c WHERE Tenant__c =: userIds order by Tenant__r.Organization__r.Name];
            }else{
                result.tenantLists = [SELECT Id, Name,Organization__r.Name, Primary_Contact_Email__c,Tenant_Type__c, Primary_Contact_Mobile__c, Primary_Contact_Name__c, Tenant_Short_Name__c, Tenant_Site_Name__c, Tenant_Swift_Code__c, Tenant_Website__c, Tenant_User_Count__c, IsActive__c, CreatedDate, LastModifiedDate from Tenant__c WHERE Id =: tenantId] ;
                for(Tenant__c t: result.tenantLists){
                    userIds.add(t.Id);
                }
               result.userResults=[SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c,Profiles__r.Name,Tenant__c ,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,LastModifiedBy__r.First_Name__c,LastModifiedBy__r.Last_Name__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Organization__r.Name,CreatedDate,LastModifiedDate,User_Profile__c,Time_Zone__c,Locale__c,Language__c  from User_Management__c WHERE Tenant__c =: userIds order by Tenant__r.Organization__r.Name];
            }
            
        }catch(exception ex){
            system.debug('===getfilterUserList exception==='+ex.getMessage()+'::'+ex.getLineNumber());
        }
        return result;
    }
    
    @AuraEnabled
    public static List<SelectListItem> getCompletedTransactions(String tenantId) {
        List<SelectListItem> retValue = new List<SelectListItem>(); 
        try {            
            retValue.add(new SelectListItem('', '-- None --')); 
            List<Transaction__c> lstTransactions = new List<Transaction__c>();
            
            lstTransactions = [
                SELECT 
                Id, 
                TransactionRefNumber__c 
                FROM Transaction__c 
                WHERE 
                Tenant__c =: tenantId AND Status__c = 'Completed Transaction'
                ORDER BY CreatedDate DESC
            ];
            if(lstTransactions != null && lstTransactions.size() > 0){
                for(Transaction__c template : lstTransactions){
                    retValue.add(new SelectListItem(template.Id, template.TransactionRefNumber__c)); 
                } 
            }
        } catch(Exception ex) {
            System.debug('getCompletedTransactions: '+ ex.getMessage());
        }
        return retValue;
    }
}
public class AttributesReports {
    public static String query = '';
    public static String query1 = '';
    @AuraEnabled
    public static List<String> objectNames(){
        List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();    
        List<String> options = new List<String>();
        for(SObjectType f : gd){
            options.add(f.getDescribe().getName());
            options.sort();        
        }
        return options;
    }
    @AuraEnabled
    public static List<Tenant_Mapping__c> getCP(){
        List<Tenant_Mapping__c> Tmapping = new List<Tenant_Mapping__c>(); 
        Tmapping =[SELECT Tenant_Approved__r.Id,Tenant_Approved__r.Tenant_Site_Name__c FROM Tenant_Mapping__c];
            return Tmapping;
    }
        
    @AuraEnabled
    public static List<ProductTemplate__c> getPTemplate(String TenantId){ 
        List<ProductTemplate__c> cpList = New List<ProductTemplate__c>();
        //cpList = [SELECT Id,TemplateName__c FROM ProductTemplate__c WHERE Active__c = true AND Created_ByName__c=:TenantId ORDER BY TemplateName__c ASC];
		cpList = [SELECT Id,TemplateName__c FROM ProductTemplate__c WHERE Active__c = true ORDER BY TemplateName__c ASC];

        return cpList;
    }
    
    @AuraEnabled
    public static List<UI_Filter__c> bindfilterres(String UID) 
    { 
        List<UI_Filter__c> filter = new List<UI_Filter__c>(); 
        try { 
            List<UI_Filter__c> filter1 = [SELECT Keywords__c,CounterParty__c,QuoteFAmtFrom__c,QuoteFAmtTo__c,QuoteAmtFrom__c,QuoteAmtTo__c,Quote_Status__c,TransactionRefNumberFrom__c,Transaction_Status__c,TransactionRefNumberTo__c,AmountFrom__c,AmountTo__c,LCNumberFrom__c,LCNumberTo__c,Expired_Date_From__c,Expired_Date_To__c,CreateddateFrom__c,CreateddateTo__c FROM UI_Filter__c WHERE Tenant_User__c=:UID Order by Createddate Desc Limit 1]; 
            if(filter1.size() > 0){
                filter = filter1; 
                return filter;
            } 
        } catch(Exception ex){
            System.debug('LightSellerOfferList - getFilter() : ' + ex.getMessage());
            //Helper.ActivityLogInsertCall('','Error - LightSellerOfferList - getFilter()',ex.getMessage()); 
        }
        return filter; 
    }
     @AuraEnabled
    public static string resetfiltercall(String UID) 
    { 
        UI_Filter__c filter = new UI_Filter__c(); 
        try { 
            List<UI_Filter__c> filter1 = [SELECT id,Keywords__c,TransactionRefNumberFrom__c,TransactionRefNumberTo__c,AmountFrom__c,AmountTo__c,LCNumberFrom__c,LCNumberTo__c,Expired_Date_From__c,Expired_Date_To__c,CreateddateFrom__c,CreateddateTo__c FROM UI_Filter__c WHERE Tenant_User__c=:UID]; 
            
            system.debug('####'+filter1.size());
            if(filter1.size() > 0){
               delete filter1;
                //return filter;
            } 
        } catch(Exception ex){
            System.debug('LightSellerOfferList - getFilter() : ' + ex.getMessage());
            //Helper.ActivityLogInsertCall('','Error - LightSellerOfferList - getFilter()',ex.getMessage()); 
        }
        return null; 
    }
    
    
    @AuraEnabled
    public static UI_Filter__c getFilter() 
    { 
        UI_Filter__c filter = new UI_Filter__c(); 
        try { 
            List<UI_Filter__c> filter1 = [SELECT Id,CounterParty__c,QuoteFAmtFrom__c,QuoteFAmtTo__c,QuoteAmtFrom__c,QuoteAmtTo__c,Quote_Status__c,CreateddateTo__c,CreateddateFrom__c,IsPrimary__c,Loan_Type__c,Transaction_Status__c,TemplateId_c__C, Name FROM UI_Filter__c WHERE Name =: 'QuoteList' ]; 
            if(filter1.size() > 0){
                filter = filter1[0]; 
                return filter;
            } 
        } catch(Exception ex){
            System.debug('LightSellerOfferList - getFilter() : ' + ex.getMessage());
            //Helper.ActivityLogInsertCall('','Error - LightSellerOfferList - getFilter()',ex.getMessage()); 
        }
        return filter; 
    }
    @AuraEnabled
    public static DynamicBindingWrapper Initcall(String selectedValue,String TenantId,String[] PTempt){
         return ShowTable(selectedValue,'',TenantId,'','','','','','','','','','','',PTempt);
    }
    @AuraEnabled
    public static DynamicBindingWrapper Viewcall(String selectedValue,String TenantId,String[] PTempt){
       return ShowTable(selectedValue,'',TenantId,'','','','','','','','','','','',PTempt);
    }
    /*@AuraEnabled
public static DynamicBindingWrapper Searchcall(String selectedValue,String TenantId,String selected){
return ShowTable(selectedValue, TenantId,selected);
}*/
    @AuraEnabled
    public static DynamicBindingWrapper filtercall(String UID,String TenantId,String transrange , String amtrange, String Expdaterange, String lcnumberrange, string createdrange, string SearchKey,string TStatus,string CPName,string QStatus,string Qamtrange,string QFamtrange,String[] PTempt){
        return ShowTable('',UID,TenantId,transrange,amtrange,Expdaterange,lcnumberrange,createdrange,SearchKey,TStatus,CPName,QStatus,Qamtrange,QFamtrange,PTempt);
    }
    @AuraEnabled
    public static DynamicBindingWrapper ShowTable(String selectedValue,String UID,String TenantId,String transrange , String amtrange, String Expdaterange, String lcnumberrange, string createdrange, string SearchKey,string TStatus,string CPName,string QStatus,string Qamtrange,string QFamtrange,String[] PTempt){
        system.debug('<#selectedValue#>'+selectedValue);
        system.debug('<#TenantId#>'+TenantId);
        DynamicBindingWrapper dynamicData = new DynamicBindingWrapper();
        List<fieldDataWrapper> wrapperList =  new List<fieldDataWrapper>();
        List<String> fields = new List<String>();
        Set<Id> TId = new Set<Id>();
        Set<Id> AId = new Set<Id>(); 
        fieldDataWrapper wrapper = new fieldDataWrapper();
        wrapper.label = 'Transaction Ref Number';
        wrapper.apiName = 'Transaction Ref Number';
        wrapperList.add(wrapper);
        
        fieldDataWrapper wrapper1 = new fieldDataWrapper();
        wrapper1.label = 'Transaction Status';
        wrapper1.apiName = 'Transaction Status';
        wrapperList.add(wrapper1);
        
        fieldDataWrapper wrapperPT = new fieldDataWrapper();
        wrapperPT.label = 'Product Template';
        wrapperPT.apiName = 'Product Template';
        wrapperList.add(wrapperPT);
        
        if(selectedValue!=''){
            IF(selectedValue=='Party')
            {
                string PTids='';
                List<Transaction__c> lstT = [SELECT Id, Name, Product_Template__c FROM Transaction__c where Tenant__c=:TenantId];
                for(Transaction__c T: lstT){
                    TId.add(T.Id);
                    PTids =PTids+ '\''+T.Id+'\',';   
                    AId.add(T.Product_Template__c);
                } 
                if(TId.size()>0)
                {
                    query = 'SELECT Id,Quote_Count__c,TransactionRefNumber__c,Product_Template__r.TemplateName__c FROM Transaction__c WHERE Id IN('+PTids.removeEnd(',')+')';
                }
            }
            if(selectedValue=='Counterpartry')
            {
                string PTids='';
                for(Published_Tenant__c PT:[SELECT Transaction__c FROM Published_Tenant__c WHERE Tenant__c=:TenantId])
                {
                    TId.add(PT.Transaction__c);
                    PTids =PTids+ '\''+PT.Transaction__c+'\',';  
                }
                if(TId.size()>0)
                {
                    query = 'SELECT Id,Quote_Count__c, TransactionRefNumber__c,Product_Template__r.TemplateName__c FROM Transaction__c WHERE Id IN('+PTids.removeEnd(',')+')';
                    
                } 
            }
            else
            {
                query = 'SELECT Id,Quote_Count__c, TransactionRefNumber__c,Product_Template__r.TemplateName__c FROM Transaction__c';
            }
        }else{
            string PTids='';
            system.debug('<##>'+transrange);
            UI_Filter__c UI =New UI_Filter__c();
            if(transrange!='undefined:undefined' && transrange!=':')
            {
                UI.TransactionRefNumberFrom__c = transrange.split(':')[0];
                UI.TransactionRefNumberTo__c = transrange.split(':')[1];
                List<Transaction__c> lstT = [select Id, Name, Product_Template__c from Transaction__c where TransactionRefNumber__c >=: transrange.split(':')[0] and TransactionRefNumber__c <=: transrange.split(':')[1]];
                for(Transaction__c T: lstT){
                    
                    PTids =PTids+ '\''+T.Id+'\',';
                    
                }
            }
            
            if(CPName!='' && CPName!='--Select--')
            {
                UI.CounterParty__c=CPName;
                List<Quotes__c> lstT = [select Id,Transaction__c from Quotes__c where CounteParty__c=:CPName];
                for(Quotes__c T: lstT){
                    
                    PTids =PTids+ '\''+T.Transaction__c+'\',';
                    
                }
                
            }
            if(TStatus!='' && TStatus!='--Select--')
            {
                UI.Transaction_Status__c=TStatus;
                List<Transaction__c> lstT = [select Id, Name, Product_Template__c from Transaction__c where Status__c=:TStatus];
                for(Transaction__c T: lstT){
                    
                    PTids =PTids+ '\''+T.Id+'\',';
                    
                }
            }
            for(string  strIB:PTempt)
            {
               //UI.Transaction_Status__c=TStatus;
                List<Transaction__c> lstT = [select Id, Name, Product_Template__c from Transaction__c where Product_Template__c=:strIB];
                for(Transaction__c T: lstT){
                    
                    PTids =PTids+ '\''+T.Id+'\',';
                    
                } 
            }
            if(QStatus!='' && QStatus!='--Select--')
            {
                UI.Quote_Status__c=QStatus;
                List<Quotes__c> lstT = [select Id,Transaction__c from Quotes__c where Bid_Status__c=:QStatus];
                for(Quotes__c T: lstT){
                    
                    PTids =PTids+ '\''+T.Transaction__c+'\',';
                    
                }
            }
            if(Qamtrange!='undefined:undefined' && Qamtrange!=':')
            {
                UI.QuoteAmtFrom__c = Qamtrange.split(':')[0];
                UI.QuoteAmtTo__c = Qamtrange.split(':')[1];
                List<Quotes__c> lstT = [select Id, Name,Transaction__c from Quotes__c where Bid_Amount__c >=: decimal.valueOf(Qamtrange.split(':')[0]) and Bid_Amount__c <=: decimal.valueOf(Qamtrange.split(':')[1])];
                for(Quotes__c T: lstT){
                    
                    PTids =PTids+ '\''+T.Transaction__c+'\',';
                    
                }
            }
            system.debug('<##>'+QFamtrange);
            if(QFamtrange!='undefined:undefined' && QFamtrange!=':')
            {
                UI.QuoteFAmtFrom__c = QFamtrange.split(':')[0];
                UI.QuoteFAmtTo__c = QFamtrange.split(':')[1];
                List<Quotes__c> lstT = [select Id, Name,Transaction__c from Quotes__c where Bid_Service_Fee_bps__c >=: decimal.valueOf(QFamtrange.split(':')[0]) and Bid_Service_Fee_bps__c <=: decimal.valueOf(QFamtrange.split(':')[1])];
                if(lstT.Size()>0){
                for(Quotes__c T: lstT){
                    
                    PTids =PTids+ '\''+T.Transaction__c+'\',';
                    
                }
                }
            }
            system.debug('<#transrange#>'+PTids);
            system.debug('<##>'+createdrange);
            if(createdrange!='undefined:undefined' && createdrange!=':')
            {  
                
                string Fromdate = createdrange.split(':')[0]+' 00:00:00'.replace(' ','T');
                string Todate = createdrange.split(':')[1]+' 00:00:00'.replace(' ','T');
                UI.CreateddateFrom__c=Datetime.valueOf(Fromdate);
                UI.CreateddateTo__c=Datetime.valueOf(Todate);
                query1='select Id, Name, Product_Template__c from Transaction__c WHERE ( CreatedDate >=' + Fromdate+'.000Z AND CreatedDate <='+ Todate+'.000Z )';  
                List<Transaction__c> lstT = Database.query(query1);
                for(Transaction__c T: lstT){
                    PTids =PTids+ '\''+T.Id+'\',';
                }
            }
            system.debug('<#createdrange#>'+PTids);
            system.debug('<#amtrange#>'+amtrange);
            if(amtrange!='undefined:undefined' && transrange!=':')
            {
                UI.AmountFrom__c=amtrange.split(':')[0];
                UI.AmountTo__c = amtrange.split(':')[1];
                List<Transaction_Attributes__c> lstT = [select Id, Name,Attribute_Value__c,Attribute_Name__c,Transaction__c from Transaction_Attributes__c where Product_Template__c!='' AND Attribute_Name__c='Amount' AND ( Attribute_Value_OnlyNumber__c >=: Integer.valueOf(amtrange.split(':')[0]) AND Attribute_Value_OnlyNumber__c <=: Integer.valueOf(amtrange.split(':')[1]))];
                for(Transaction_Attributes__c T: lstT){
                    PTids =PTids+ '\''+T.Transaction__c+'\',';
                }
            }
            if(lcnumberrange!='undefined:undefined' && lcnumberrange!=':')
            {
                UI.LCNumberFrom__c =lcnumberrange.split(':')[0];
                UI.LCNumberTo__c=lcnumberrange.split(':')[1];
                List<Transaction_Attributes__c> lstT = [select Id, Name,Attribute_Value__c,Attribute_Name__c,Transaction__c from Transaction_Attributes__c where Product_Template__c!='' AND  Attribute_Name__c='LC Number' AND ( Attribute_Value_OnlyNumber__c >=: Integer.valueOf(lcnumberrange.split(':')[0]) AND Attribute_Value_OnlyNumber__c <=: Integer.valueOf(lcnumberrange.split(':')[1]))];
                for(Transaction_Attributes__c T: lstT){
                    PTids =PTids+ '\''+T.Transaction__c+'\',';
                }
            }
            if(Expdaterange!='undefined:undefined' && Expdaterange!=':')
            {
                UI.Expired_Date_From__c=Expdaterange.split(':')[0];
                UI.Expired_Date_To__c=Expdaterange.split(':')[1];
                List<Transaction_Attributes__c> lstT = [select Id, Name,Attribute_Value__c,Attribute_Name__c,Transaction__c from Transaction_Attributes__c where Product_Template__c!='' AND  Attribute_Name__c='Date of Expiry' AND ( Attribute_Value__c >=: Expdaterange.split(':')[0] AND Attribute_Value__c <=: Expdaterange.split(':')[1])];
                for(Transaction_Attributes__c T: lstT){
                    PTids =PTids+ '\''+T.Transaction__c+'\',';
                }
            }
            
            system.debug('<#amtrange#>'+PTids);
            system.debug('##'+SearchKey);
            if(SearchKey!=NULL && SearchKey!=''){
                UI.Keywords__c=SearchKey;
                //Transaction_Attributes__c
                List<List<Transaction_Attributes__c >> TA  = new List<List<Transaction_Attributes__c >>();
                TA= [FIND :'*'+SearchKey+'*' IN ALL FIELDS  RETURNING Transaction_Attributes__c (Transaction__c)];
                for(List<Transaction_Attributes__c> syffers : TA) {
                    for (Transaction_Attributes__c synd : syffers) {
                        PTids = PTids + '\'' + synd.Transaction__c + '\',';
                    }
                }
                //Transactions
                List<List<Transaction__c >> Trans  = new List<List<Transaction__c >>();
                Trans= [FIND :'*'+SearchKey+'*' IN ALL FIELDS  RETURNING Transaction__c (Id)];
                for(List<Transaction__c> syffers : Trans) {
                    for (Transaction__c t : syffers) {
                        PTids = PTids + '\'' + t.Id + '\',';
                    }
                }
                
            }
            List<UI_Filter__c> filter1 = [SELECT id,Keywords__c,TransactionRefNumberFrom__c,TransactionRefNumberTo__c,AmountFrom__c,AmountTo__c,LCNumberFrom__c,LCNumberTo__c,Expired_Date_From__c,Expired_Date_To__c,CreateddateFrom__c,CreateddateTo__c FROM UI_Filter__c WHERE Tenant_User__c=:UID Order by Createddate Desc Limit 1]; 
            if(filter1.size() > 0){
                UI.id=filter1[0].id;
                UI.Tenant_User__c=UID;
                update UI;
            }else{
                UI.Tenant_User__c=UID;
                Insert UI;
            }
            if(PTids!=''){
                query = 'SELECT Id,Quote_Count__c,TransactionRefNumber__c,Product_Template__r.TemplateName__c FROM Transaction__c WHERE Id IN('+PTids.removeEnd(',')+')';
            }
        }
        List<Product_Template_Object__c> PTO1 = [SELECT Id,Name,Attributes__r.Name,Attributes__c from Product_Template_Object__c];
        system.debug('====PTO1===='+PTO1);
        map<Id, String> mapPT = new map<Id, String>();    
        for(Product_Template_Object__c PT: PTO1){
            fieldDataWrapper wrapper2 = new fieldDataWrapper();
            wrapper2.label = PT.Attributes__r.Name;
            wrapper2.apiName = PT.Attributes__c;
            if(!mapPT.containsKey(PT.Attributes__c)){
                mapPT.put(PT.Attributes__c, PT.Attributes__r.Name);
                wrapperList.add(wrapper2);
            }
        } 
        fieldDataWrapper wrapper10 = new fieldDataWrapper();
        wrapper10.label = 'Counterparty Name';
        wrapper10.apiName = 'Counterparty Name';
        wrapperList.add(wrapper10);
        
        fieldDataWrapper wrapper11 = new fieldDataWrapper();
        wrapper11.label = 'Quote Amount';
        wrapper11.apiName = 'Quote Amount';
        wrapperList.add(wrapper11);
        
        fieldDataWrapper wrapperPT12 = new fieldDataWrapper();
        wrapperPT12.label = 'Quote Fee (bps)';
        wrapperPT12.apiName = 'Quote Fee (bps)';
        wrapperList.add(wrapperPT12);
        
        fieldDataWrapper wrapperPT13 = new fieldDataWrapper();
        wrapperPT13.label = 'Quote Status';
        wrapperPT13.apiName = 'Quote Status';
        wrapperList.add(wrapperPT13);
        system.debug('<##>'+query);
        if(query=='')
        {
            query = 'SELECT Id, Quote_Count__c,TransactionRefNumber__c,Product_Template__r.TemplateName__c FROM Transaction__c order by Name desc Limit 0';
        }else{
            query=query+' order by Name desc';
        }
        system.debug('<<#query#>>'+query);
        //Id =: TId 
        List<Transaction__c> ListTransaction = new List<Transaction__c>();
        ListTransaction = Database.query(query);
        String sql = ' SELECT Id, Name FROM Transaction__c ORDER BY TransactionRefNumber__c DESC LIMIT 5';
        List<Sobject> objRecords = new List<Sobject>();
        objRecords = Database.Query(sql);
        List<String> lstring = new List<String>();
        string a = '';
        map<string, list<String>> mapAttList = new map<string, list<String>>();    
        
        MAP<Id,Quotes__c> QuoMAP =New MAP<Id,Quotes__c>();
        for(Quotes__c Q:[SELECT Id,Transaction__c,Bid_Amount__c,CounteParty__r.Tenant_Site_Name__c,Bid_Service_Fee_bps__c,Bid_Status__c FROM Quotes__c Order by LastmodifiedDate asc])
        {
            QuoMAP.put(Q.Id,Q);
        }
        
        for(Transaction__c  T:ListTransaction){ 
            system.debug('<##>'+T.Quote_Count__c);
            if(T.Quote_Count__c>=1)
            {
                integer i;
                List<Quotes__c> QList ;
                system.debug('<##>'+Qamtrange);
                system.debug('<##>'+QFamtrange);
                if(QStatus!='' && QStatus!='--Select--')
                {
                    QList=[SELECT Transaction__c,Bid_Amount__c,CounteParty__r.Tenant_Site_Name__c,Bid_Service_Fee_bps__c,Bid_Status__c FROM Quotes__c WHERE Transaction__c=:T.ID AND Bid_Status__c=:QStatus Order by LastmodifiedDate asc];  
                }
                else if(Qamtrange!='undefined:undefined' && Qamtrange!=':' && Qamtrange!='')
                {
                    QList = [select Id, Name,Transaction__c from Quotes__c where Transaction__c=:T.ID AND Bid_Amount__c >=: decimal.valueOf(Qamtrange.split(':')[0]) and Bid_Amount__c <=: decimal.valueOf(Qamtrange.split(':')[1])];
                }
                else if(QFamtrange!='undefined:undefined' && QFamtrange!=':' && QFamtrange!='')
                {  
                    QList = [select Id, Name,Transaction__c from Quotes__c where Transaction__c=:T.ID AND Bid_Service_Fee_bps__c >=: decimal.valueOf(QFamtrange.split(':')[0]) and Bid_Service_Fee_bps__c <=: decimal.valueOf(QFamtrange.split(':')[1])];
                }
                else{
                    QList=[SELECT Transaction__c,Bid_Amount__c,CounteParty__r.Tenant_Site_Name__c,Bid_Service_Fee_bps__c,Bid_Status__c FROM Quotes__c WHERE Transaction__c=:T.ID Order by LastmodifiedDate asc];  
                }
                
            for(Quotes__c Q:QList)
                {
                String atts = '';
                List<String> ls = new List<String>();
            for(Transaction_Attributes__c TA: [SELECT Id, Attribute_Name__c,Attribute_Value__c,Product_Template__r.TemplateName__c,Transaction__r.Status__c,Transaction__r.TransactionRefNumber__c from Transaction_Attributes__c WHERE Product_Template__c!='' AND Transaction__c =: T.Id ORDER BY Transaction__r.Name DESC]){
             
                if(atts == ''){                        
                    atts+= 'Transaction Ref Number:'+TA.Transaction__r.TransactionRefNumber__c+'|';
                    atts+= 'Transaction Status:'+TA.Transaction__r.Status__c+'|';
                    atts+= 'Product Template:'+TA.Product_Template__r.TemplateName__c+'|';
                }
                atts+=TA.Attribute_Name__c+':'+TA.Attribute_Value__c+'|';
                a = TA.Transaction__r.TransactionRefNumber__c;
            }
            system.debug('<##>'+QuoMAP.containsKey(Q.Id));
            if(QuoMAP.containsKey(Q.Id))
            {
                system.debug('<##>'+QuoMAP.get(Q.Id).CounteParty__r.Tenant_Site_Name__c);
                atts+='Counterparty Name:'+QuoMAP.get(Q.Id).CounteParty__r.Tenant_Site_Name__c+'|';
                atts+='Quote Amount:'+QuoMAP.get(Q.Id).Bid_Amount__c+'|';
                atts+='Quote Fee (bps):'+QuoMAP.get(Q.Id).Bid_Service_Fee_bps__c+'|';
                atts+='Quote Status:'+QuoMAP.get(Q.Id).Bid_Status__c+'|';
                
            } 
                    system.debug('<#ls#>'+ls);        
                    ls.add(atts);      
                    system.debug('<#ls#>'+ls);
            //if(!mapAttList.containsKey(T.TransactionRefNumber__c)){
                mapAttList.put(a, ls);
                lstring.add(atts);
                ls.clear();
           // }
            }
            }else{
               String atts = '';
            List<String> ls = new List<String>();
            for(Transaction_Attributes__c TA: [SELECT Id, Attribute_Name__c,Attribute_Value__c,Product_Template__r.TemplateName__c,Transaction__r.Status__c,Transaction__r.TransactionRefNumber__c from Transaction_Attributes__c WHERE Product_Template__c!='' AND Transaction__c =: T.Id ORDER BY Transaction__r.Name DESC]){
            	if(atts == ''){                        
                    atts+= 'Transaction Ref Number:'+TA.Transaction__r.TransactionRefNumber__c+'|';
                    atts+= 'Transaction Status:'+TA.Transaction__r.Status__c+'|';
                    atts+= 'Product Template:'+TA.Product_Template__r.TemplateName__c+'|';
                }
                atts+=TA.Attribute_Name__c+':'+TA.Attribute_Value__c+'|';
                a = TA.Transaction__r.TransactionRefNumber__c;
            }
                
                
            system.debug('<#443#>'+QuoMAP.containsKey(T.Id));
            if(QuoMAP.containsKey(T.Id))
            {
                system.debug('<##>'+QuoMAP.get(T.Id).CounteParty__r.Tenant_Site_Name__c);
                atts+='Counterparty Name:'+QuoMAP.get(T.Id).CounteParty__r.Tenant_Site_Name__c+'|';
                atts+='Quote Amount:'+QuoMAP.get(T.Id).Bid_Amount__c+'|';
                atts+='Quote Fee (bps):'+QuoMAP.get(T.Id).Bid_Service_Fee_bps__c+'|';
                atts+='Quote Status:'+QuoMAP.get(T.Id).Bid_Status__c+'|';
                ls.add(atts);
                
            }else{
                ls.add(atts);
            } 
            //ls.add(atts);              
            if(!mapAttList.containsKey(T.TransactionRefNumber__c)){
                mapAttList.put(a, ls );
                lstring.add(atts);
                ls.clear();
            } 
            }
            
        }      
        System.debug('objRecords--->' + objRecords);
        System.debug('===lstring======'+lstring);
        if(objRecords!=null && objRecords.size()>0){
            dynamicData.sObjectDatastr = lstring;
        }
        else{
            dynamicData.sObjectData = new List<sObject>();
        }
        dynamicData.fieldList = wrapperList;
        System.debug('dynamicData.sObjectData--->' + dynamicData.sObjectData);
        System.debug('dynamicData.fieldList--->' + dynamicData.fieldList);
        System.debug('dynamicData--->' + dynamicData);
        return dynamicData;
    }
    
    
    
    //Class to store the dynamic data and list of related fields
    public class DynamicBindingWrapper{
        @AuraEnabled
        public List<sObject> sObjectData {get; set;}
        @AuraEnabled
        public List<String> sObjectDatastr {get; set;}
        @AuraEnabled
        public List<fieldDataWrapper> fieldList {get; set;}
    }
    
    //Class to store the field information
    public class fieldDataWrapper{
        @AuraEnabled
        public String label {get; set;}
        @AuraEnabled
        public String apiName {get; set;}
    }
}
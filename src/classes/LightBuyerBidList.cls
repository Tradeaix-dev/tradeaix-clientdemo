public class LightBuyerBidList {
    public static String query = '';    
    public static Integer size = 0; 
    @AuraEnabled
    public static LightBuyerBidListV buyerDetails(Integer counter, String sortbyField, String sortDirection,String LoggeduserTenantID,String LoggedUserId,String Filtervalue) {
        
        return ShowTable(counter, sortbyField, sortDirection, NULL, LoggeduserTenantID, LoggedUserId,Filtervalue);
        
    }
    private static LightBuyerBidListV ShowTable(Integer counter, String sortbyField, String sortDirection, string searchText,String LoggeduserTenantID,String LoggedUserId,String Filtervalue)
    {
        system.debug('!'+searchText);
        system.debug('!'+LoggedUserId);
        LightBuyerBidListV offer = new LightBuyerBidListV();
        offer.isFilterA = false;
        try{
            
            string Ids='';
            SET<id> setTids =New SET<id>();
            size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'Bids' AND Grid_Name__c =: 'Bids']; 
            
            if(size > 0){
                offer.list_size = Integer.valueOf([SELECT List_Size__c from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'Bids' AND Grid_Name__c =: 'Bids'][0].List_Size__c);
            }
            else {
                offer.list_size = 25;
            }
            if(Filtervalue!='All Realms' && Filtervalue!=null)
            {  
                if(Filtervalue.startsWith('a0M')){
                    
                    for(Tenant__c T:[SELECT id FROM Tenant__c WHERE Organization__c=:Filtervalue])
                    {
                        setTids.add(T.id);  
                        Ids = '\''+T.id+'\',';
                    }
                    
                }else{
                    setTids.add(Filtervalue); 
                    Ids = '\''+Filtervalue+'\',';
                }
            }
            system.debug('###'+setTids.size());
            if(setTids.size() > 0){               
<<<<<<< HEAD
                query = 'SELECT Id, Name, Bid_Amount__c,Bid_Expiration_Date__c,Bid_Service_Fee_bps__c,Bid_Status__c,CreatedById__c,Request_Information_Quote__c,Transaction__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,CounteParty__r.Tenant_Site_Name__c,CreatedDate,Note__c, Cost_of_Funds__c from Quotes__c WHERE Transaction__r.Tenant__c IN('+Ids.removeEnd(',')+')';
            }
            else
            {
                if(LoggedUserTenantId !=system.label.TradeAix_Tenant_Id){
                    system.debug('########'+LoggedUserTenantId);
                    string PTIds='';
                    List<Tenant__c> ListT =[SELECT id,CreatedFromOTP__c,Tenant_Bank_Type__c FROM Tenant__c WHERE id=:LoggedUserTenantId LIMIT 1];
                    if(ListT[0].Tenant_Bank_Type__c =='Counterparty Bank')
                    {
                        system.debug('########'+ListT[0].Tenant_Bank_Type__c);
                        SET<id> setPTids =New SET<id>();
                        for(Published_Tenant__c PT:[SELECT Tenant__c,Transaction__c FROM Published_Tenant__c WHERE Tenant__c=:LoggedUserTenantId])
                        {
                            if(PT.Transaction__c!=null){
                                setPTids.add(PT.Transaction__c); 
                                PTIds = PTIds+ '\''+PT.Transaction__c+'\',';
                            }
                        }
                        system.debug('########'+setPTids);
                        if(setPTids.size()>0){
                            system.debug('########'+setPTids);
                        }
                        
                        SET<id> setTBids =New SET<id>();
                        for(User_Management__c UM:[SELECT id,Profiles__c FROM User_Management__c WHERE Tenant__c=:LoggedUserTenantId])
                        {
                            if(UM.Profiles__c==system.label.OBCP_ProfileID || ListT[0].CreatedFromOTP__c==true)
                            {
                                setTBids.add(UM.id);
                            }
                        }
                        if(setTBids.size()>0)
                        {
                            for(Limited_Bidding__c LBList:[SELECT Transaction__c FROM Limited_Bidding__c WHERE Tenant_User__c IN :setTBids])
                            {
                                if(LBList.Transaction__c!=null){
                                    setPTids.add(LBList.Transaction__c); 
                                    PTIds = PTIds+'\''+LBList.Transaction__c+'\',';
                                }
                            }
                        }
                        
                        system.debug('########'+setPTids);
                        system.debug('########'+PTIds);
                        if(setPTids.size() > 0){
                            query = 'SELECT Id, Name, Bid_Amount__c,Bid_Expiration_Date__c,Bid_Service_Fee_bps__c,Bid_Status__c,CreatedById__c,Request_Information_Quote__c,Transaction__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,CounteParty__r.Tenant_Site_Name__c,CreatedDate,Note__c, Cost_of_Funds__c from Quotes__c WHERE Transaction__c IN('+PTIds.removeEnd(',')+') AND CreatedBy__c= \''+LoggedUserId+'\'';
                        }else{
                            query = 'SELECT Id, Name, Bid_Amount__c,Bid_Expiration_Date__c,Bid_Service_Fee_bps__c,Bid_Status__c,CreatedById__c,Request_Information_Quote__c,Transaction__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,CounteParty__r.Tenant_Site_Name__c,CreatedDate,Note__c, Cost_of_Funds__c from Quotes__c WHERE Transaction__r.Tenant__c= \''+LoggedUserTenantId+'\'';
                        }
                    }else{
                        query = 'SELECT Id, Name, Bid_Amount__c,Bid_Expiration_Date__c,Bid_Service_Fee_bps__c,Bid_Status__c,CreatedById__c,Request_Information_Quote__c,Transaction__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,CounteParty__r.Tenant_Site_Name__c,CreatedDate,Note__c, Cost_of_Funds__c from Quotes__c WHERE Transaction__r.Tenant__c=\''+LoggedUserTenantId+'\'';
                    }
                }
                else{
                    query = 'SELECT Id, Name, Bid_Amount__c,Bid_Expiration_Date__c,Bid_Service_Fee_bps__c,Bid_Status__c,CreatedById__c,Request_Information_Quote__c,Transaction__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,CounteParty__r.Tenant_Site_Name__c,CreatedDate,Note__c, Cost_of_Funds__c from Quotes__c';
=======
                query = 'SELECT Id, Name, Bid_Amount__c,Bid_Expiration_Date__c,Bid_Service_Fee_bps__c,Bid_Status__c,CreatedById__c,Request_Information_Quote__c,Transaction__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,CounteParty__r.Tenant_Site_Name__c,CreatedDate,Note__c from Quotes__c WHERE Transaction__r.Tenant__c IN('+Ids.removeEnd(',')+')';
            }
            else
            {
                if(LoggedUserTenantId !=system.label.TradeAix_Tenant_Id){
                    system.debug('########'+LoggedUserTenantId);
                    string PTIds='';
                    List<Tenant__c> ListT =[SELECT id,CreatedFromOTP__c,Tenant_Bank_Type__c FROM Tenant__c WHERE id=:LoggedUserTenantId LIMIT 1];
                    if(ListT[0].Tenant_Bank_Type__c =='Counterparty Bank')
                    {
                        system.debug('########'+ListT[0].Tenant_Bank_Type__c);
                        SET<id> setPTids =New SET<id>();
                        for(Published_Tenant__c PT:[SELECT Tenant__c,Transaction__c FROM Published_Tenant__c WHERE Tenant__c=:LoggedUserTenantId])
                        {
                            if(PT.Transaction__c!=null){
                                setPTids.add(PT.Transaction__c); 
                                PTIds = PTIds+ '\''+PT.Transaction__c+'\',';
                            }
                        }
                        system.debug('########'+setPTids);
                        if(setPTids.size()>0){
                            system.debug('########'+setPTids);
                        }
                        
                        SET<id> setTBids =New SET<id>();
                        for(User_Management__c UM:[SELECT id,Profiles__c FROM User_Management__c WHERE Tenant__c=:LoggedUserTenantId])
                        {
                            if(UM.Profiles__c==system.label.OBCP_ProfileID || ListT[0].CreatedFromOTP__c==true)
                            {
                                setTBids.add(UM.id);
                            }
                        }
                        if(setTBids.size()>0)
                        {
                            for(Limited_Bidding__c LBList:[SELECT Transaction__c FROM Limited_Bidding__c WHERE Tenant_User__c IN :setTBids])
                            {
                                if(LBList.Transaction__c!=null){
                                    setPTids.add(LBList.Transaction__c); 
                                    PTIds = PTIds+'\''+LBList.Transaction__c+'\',';
                                }
                            }
                        }
                        
                        system.debug('########'+setPTids);
                        system.debug('########'+PTIds);
                        if(setPTids.size() > 0){
                            query = 'SELECT Id, Name, Bid_Amount__c,Bid_Expiration_Date__c,Bid_Service_Fee_bps__c,Bid_Status__c,CreatedById__c,Request_Information_Quote__c,Transaction__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,CounteParty__r.Tenant_Site_Name__c,CreatedDate,Note__c from Quotes__c WHERE Transaction__c IN('+PTIds.removeEnd(',')+') AND CreatedBy__c= \''+LoggedUserId+'\'';
                        }else{
                            query = 'SELECT Id, Name, Bid_Amount__c,Bid_Expiration_Date__c,Bid_Service_Fee_bps__c,Bid_Status__c,CreatedById__c,Request_Information_Quote__c,Transaction__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,CounteParty__r.Tenant_Site_Name__c,CreatedDate,Note__c from Quotes__c WHERE Transaction__r.Tenant__c= \''+LoggedUserTenantId+'\'';
                        }
                    }else{
                        query = 'SELECT Id, Name, Bid_Amount__c,Bid_Expiration_Date__c,Bid_Service_Fee_bps__c,Bid_Status__c,CreatedById__c,Request_Information_Quote__c,Transaction__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,CounteParty__r.Tenant_Site_Name__c,CreatedDate,Note__c from Quotes__c WHERE Transaction__r.Tenant__c=\''+LoggedUserTenantId+'\'';
                    }
                }
                else{
                    query = 'SELECT Id, Name, Bid_Amount__c,Bid_Expiration_Date__c,Bid_Service_Fee_bps__c,Bid_Status__c,CreatedById__c,Request_Information_Quote__c,Transaction__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,CounteParty__r.Tenant_Site_Name__c,CreatedDate,Note__c from Quotes__c';
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-clientdemo.git
                }
            }
            
            List<UI_Filter__c> filter = [SELECT Id,Markettype__c,IsDeleted,IsPrimary__c,LastModifiedById,LastModifiedDate,Loan_Type__c,Name,OwnerId,SystemModstamp,CreateddateTo__c,CreateddateFrom__c,Transaction_Status__c,TemplateId_c__c,TransactionRefNumber__c FROM UI_Filter__c WHERE Name =: 'QuoteList' AND CreatedById =: UserInfo.getUserId()]; 
            if(filter .size() > 0 && (String)Cache.Session.get('Filter') != null && (String)Cache.Session.get('Filter') != '')
            {
                offer.filter = filter[0];
                if(offer.filter.CreateddateFrom__c!=NULL){
                    string Fromdate = offer.filter.CreateddateFrom__c.format('yyyy-MM-dd hh:mm:ss').replace(' ','T');
                    string Todate = offer.filter.CreateddateTo__c.format('yyyy-MM-dd hh:mm:ss').replace(' ','T');
                    system.debug('@Fromdate@'+offer.filter.CreateddateFrom__c);
                    if(LoggedUserTenantId !=system.label.TradeAix_Tenant_Id){
                        query=query+' AND (( CreatedDate >' + Fromdate+'.000Z'+' AND CreatedDate <'+ Todate+'.000Z ) OR ( CreatedDate >' + Todate+'.000Z'+' AND CreatedDate <'+ Fromdate+'.000Z ))';  
                    }else{
                        query=query+' WHERE (( CreatedDate >' + Fromdate+'.000Z'+' AND CreatedDate <'+ Todate+'.000Z ) OR ( CreatedDate >' + Todate+'.000Z'+' AND CreatedDate <'+ Fromdate+'.000Z ))';  
                        
                    }
                    offer.isFilterA = true;
                }
            }
            if(searchText!=NULL){
                system.debug('#Search#');
                offer.isFilterA = true;
                string getOfferIds='';
                //Quotes__c
                List<List<Quotes__c >> Syndications  = new List<List<Quotes__c >>();
                Syndications= [FIND :'*'+searchText+'*' IN ALL FIELDS  RETURNING Quotes__c (Id)];
                for(List<Quotes__c> syffers : Syndications) {
                    for (Quotes__c synd : syffers) {
                        getOfferIds = getOfferIds + '\'' + synd.Id + '\',';
                    }
                }
                //Transactions
                string getOfferIds1='';
                List<List<Transaction__c >> Trans  = new List<List<Transaction__c >>();
                Trans= [FIND :'*'+searchText+'*' IN Name FIELDS  RETURNING Transaction__c (Id)];
                for(List<Transaction__c> syffers : Trans) {
                    for (Transaction__c t : syffers) {
                        getOfferIds1 = getOfferIds1 + '\'' + t.Id + '\',';
                    }
                }
                
                
                
                if(getOfferIds!=''){
                    system.debug('#getOfferIds#');
                    getOfferIds = getOfferIds.substring(0, getOfferIds.length() - 1);
                    if(LoggedUserTenantId !=system.label.TradeAix_Tenant_Id){
                        query = query + ' AND id IN ('+ getOfferIds +')';
                    }
                    else{
                        query = query + ' WHERE id IN ('+ getOfferIds +')';
                    }
                }
                if(getOfferIds1!=''){
                    system.debug('#getOfferIds1#');
                    getOfferIds1 = getOfferIds1.substring(0, getOfferIds1.length() - 1);
                    if(LoggedUserTenantId !=system.label.TradeAix_Tenant_Id){
                        query = query + ' AND Transaction__c IN ('+ getOfferIds1 +')';
                    }
                    else{
                        query = query + ' WHERE Transaction__c IN ('+ getOfferIds1 +')';
                    }
                }
            }
            
            
            
            query=query+' order by CreatedDate desc '+ 'LIMIT '+offer.list_size+' offset '+counter;
            system.debug('===String Query==='+query);
            offer.results = Database.query(query);
            offer.total_size = offer.results.Size();
            offer.counter = counter; 
            system.debug('==offer.list_size=='+offer.list_size+':::'+offer.total_size);
            offer.total_page = getTotalPages(offer.list_size, offer.total_size);
            system.debug('###'+offer.results);
        }catch(exception ex){
            system.debug('=====LightBuyerBidListV ShowTable===='+ex.getMessage()+'::::'+ex.getLineNumber());
        }
        return offer;
    }
    @AuraEnabled 
    public static Organization__c[] loadorgs(String username) {
        return Helper.loadorgs(username);
    }
    @AuraEnabled
    public static LightBuyerBidListV Filter(Integer counter, String sortbyField, String sortDirection, string searchText,String LoggeduserTenantID,String LoggedUserId,String Filtervalue) { 
        
        return ShowTable(counter, sortbyField, sortDirection, searchText, LoggeduserTenantID, LoggedUserId,Filtervalue);
    }
    @AuraEnabled
    public static LightBuyerBidListV saveFilter(Integer counter, String sortbyField, String sortDirection,string filter,String LoggeduserTenantID,String LoggedUserId,String Filtervalue) 
    { 
        LightBuyerBidListV retValue = new LightBuyerBidListV();
        try { 
            
            retValue = ShowTable(counter, sortbyField, sortDirection, filter, LoggeduserTenantID, LoggedUserId,Filtervalue); 
        } catch(Exception ex){
            System.debug('LightSellerOfferList - saveFilter() : ' + ex.getMessage());
            //Helper.ActivityLogInsertCall('','Error - LightSellerOfferList - saveFilter()',ex.getMessage()); 
        }  
        return retValue; 
    }
    @AuraEnabled
    public static LightBuyerBidListV alyFilter(Integer counter, String sortbyField, String sortDirection, String selected, UI_Filter__c filter,String LoggeduserTenantID,String LoggedUserId,String Filtervalue) 
    { 
        LightBuyerBidListV retValue = new LightBuyerBidListV(); 
        try { 
            List<UI_Filter__c> filter1 = [SELECT Id,Name FROM UI_Filter__c WHERE Name =:'QuoteList' AND CreatedById =: UserInfo.getUserId()]; 
            if(filter1.size() > 0){
               // filter1.Id = filter1[0].Id;
                //system.debug('@F@'+filter);
                filter1[0] = filter;
                update filter1; 
            }
            else {
                filter.Name = 'QuoteList';
                insert filter; 
            }   
            Helper.setSessionValue('Filter', 'QuoteList');
            retValue = ShowTable(counter, sortbyField, sortDirection, NULL, LoggeduserTenantID, LoggedUserId,Filtervalue);
        } catch(Exception ex){
            System.debug('LightSellerOfferList - saveFilter() : ' + ex.getMessage());
            //Helper.ActivityLogInsertCall('','Error - LightSellerOfferList - saveFilter()',ex.getMessage()); 
        }  
        return retValue; 
    }
    @AuraEnabled
    public static UI_Filter__c getFilter() 
    { 
        UI_Filter__c filter = new UI_Filter__c(); 
        try { 
            List<UI_Filter__c> filter1 = [SELECT Id,CreateddateTo__c,CreateddateFrom__c,IsPrimary__c,Loan_Type__c,Transaction_Status__c,TemplateId_c__C, Name FROM UI_Filter__c WHERE Name =: 'QuoteList' AND CreatedById =: UserInfo.getUserId()]; 
            if(filter1.size() > 0){
                filter = filter1[0]; 
                return filter;
            } 
        } catch(Exception ex){
            System.debug('LightSellerOfferList - getFilter() : ' + ex.getMessage());
            //Helper.ActivityLogInsertCall('','Error - LightSellerOfferList - getFilter()',ex.getMessage()); 
        }
        return filter; 
    }
    private static Integer getTotalPages(Integer list_size, Integer total_size) { 
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        }
        else {
            return (total_size/list_size);
        }
    }  
    
    @AuraEnabled
    public static LightBuyerBidListV getNext(Integer counter, String sortbyField, String sortDirection, string searchText,String LoggeduserTenantID,String LoggedUserId,String Filtervalue) {  
        return ShowTable(counter, sortbyField, sortDirection, NULL, LoggeduserTenantID, LoggedUserId,Filtervalue); 
    }
    @AuraEnabled
    public static LightBuyerBidListV getEnd(Integer counter, String sortbyField, String sortDirection, string searchText,String LoggeduserTenantID,String LoggedUserId,String Filtervalue) {  
        return ShowTable(counter, sortbyField, sortDirection, NULL, LoggeduserTenantID, LoggedUserId,Filtervalue); 
    }
    @AuraEnabled
    public static LightBuyerBidListV getBeginning(Integer counter, String sortbyField, String sortDirection, string searchText,String LoggeduserTenantID,String LoggedUserId,String Filtervalue) {  
        return ShowTable(counter, sortbyField, sortDirection, NULL, LoggeduserTenantID, LoggedUserId,Filtervalue); 
    }
    @AuraEnabled
    public static LightBuyerBidListV getPrevious(Integer counter, String sortbyField, String sortDirection, string searchText,String LoggeduserTenantID,String LoggedUserId,String Filtervalue) {  
        return ShowTable(counter, sortbyField, sortDirection, NULL, LoggeduserTenantID, LoggedUserId,Filtervalue); 
    }
    
    
}
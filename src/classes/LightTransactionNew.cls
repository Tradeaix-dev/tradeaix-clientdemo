public without sharing class LightTransactionNew {   
    @AuraEnabled
    public static String getloggeduserBrowserTitle() { 
        String Btitle = '';
        
        return '';
    }
    @AuraEnabled
    public static List<Tenant__c> getCP(){ 
        List<Tenant__c> cpList = New List<Tenant__c>();
        for(Tenant__c cpaList :[SELECT ID,Tenant_Site_Name__c,City__c FROM Tenant__c WHERE  Tenant_Bank_Type__c='Issuing Bank' AND IsActive__c=true])
        {
            Tenant__c cu = new Tenant__c();
            cu.Tenant_Site_Name__c =cpaList.Tenant_Site_Name__c+', '+cpaList.City__c;
            cu.Id =cpaList.ID;
            cpList.add(cu); 
        } 
        return cpList;
    }
    @AuraEnabled
    public static List<SelectListItem> getProductTemplates(String Tid) {
        List<SelectListItem> retValue = new List<SelectListItem>(); 
        try {
            List<User_Management__c> lstmId =[Select Id,name FROM User_Management__c WHERE Tenant__c=:Tid  AND (Profiles__c=:System.label.SACP_ProfileID OR Profiles__c=:System.label.AMCP)  LIMIT 1];
            boolean ispri; 
            ispri=true;
            system.debug('##'+lstmId[0].Id);
            retValue.add(new SelectListItem('', '-- None --')); 
            List<ProductTemplate__c> lstProductTemplates = new List<ProductTemplate__c>();
            
            lstProductTemplates = [
                SELECT 
                Id, 
                TemplateName__c 
                FROM ProductTemplate__c 
                WHERE 
                Active__c = true AND IsPrimary__c=:ispri AND Created_ByName__c=:lstmId[0].Id
                ORDER BY TemplateName__c ASC
            ];
            if(lstProductTemplates != null && lstProductTemplates.size() > 0){
                for(ProductTemplate__c template : lstProductTemplates){
                    retValue.add(new SelectListItem(template.Id, template.TemplateName__c)); 
                } 
            }
        } catch(Exception ex) {
            System.debug('getProductTemplates: '+ ex.getMessage());
        }
        return retValue;
    }
    @AuraEnabled
    public static String saveSellerOffersItems(Transaction__c saveSellerOffers,String[] IB,String RID) { 
        system.debug('#IssuingBank#'+IB.Size());
        Type idArrType = Type.forName('List<string>');
        List<string> wrapperList = (List<string>) JSON.deserialize(RID, idArrType);
        system.debug('#wrapperList#'+wrapperList);
        
        String response = '';
        String Groupid='';
        Integer rndnum = Math.round(Math.random()*1000000000);
        if(string.valueOf(rndnum).length() == 4){
            Groupid ='91'+string.valueOf(rndnum);
        }  else if(string.valueOf(rndnum).length() == 5)  {
            Groupid ='9'+string.valueOf(rndnum);
        }else{
            Groupid =string.valueOf(rndnum);
        }   
        if(IB.Size()>1)
        {
            response ='GT'+Groupid;
        }
        String ProID =saveSellerOffers.Product_Template__c;
        try {  
            for(string  strIB:IB)
            {
                List<Transaction__c> ListOffer = New List<Transaction__c>();
                
                Transaction__c  offerobj = new Transaction__c();
                system.debug('#IssuingBank-INside#'+IB);
                
                offerobj.Status__c= 'Transaction Created';
                offerobj.Loan_Type__c=saveSellerOffers.Loan_Type__c;
                offerobj.Tenant__c=saveSellerOffers.Tenant__c;
                offerobj.CreatedBy__c =saveSellerOffers.CreatedBy__c;
                offerobj.LastModifiedBy__c = saveSellerOffers.LastModifiedBy__c;
                offerobj.Notes__c=saveSellerOffers.Notes__c;
                offerobj.Product_Template__c=saveSellerOffers.Product_Template__c;
                offerobj.Currency__c =saveSellerOffers.Currency__c;
                offerobj.Issuing_Banks__c= strIB;
                offerobj.GroupID__c ='GT-'+Groupid;
                offerobj.IsPrimary__c = true;
                ListOffer.add(offerobj);
                
                If(ListOffer.size()>0)
                {
                    system.debug('##List'+ListOffer);
                    Insert ListOffer;
                    List<Notification__c> ListNoti=New List<Notification__c>();
                    List<Transaction__c> ListOfferupdate = New List<Transaction__c>();
                    
                    for(Transaction__c OffInfo: ListOffer){
                        response =OffInfo.Id;
                        Transaction__c updateoff = new Transaction__c();
                        
                        
                        String market ='P';
                        string loanType = (OffInfo.Loan_Type__c=='Funded') ? 'F' : 'U';
                        updateoff.id=OffInfo.id;
                        Transaction__c[] Off = [SELECT id,createdbyId,CreatedBy__c,Tenant__r.Tenant_Site_Name__c,Tenant__r.Tenant_Swift_Code__c,TransactionRefNumber__c,createdby.Name,Name,Product_Template__r.TemplateName__c From Transaction__c WHERE id=:OffInfo.id LIMIT 1];
                        String TransactionRefNumber = Off[0].Tenant__r.Tenant_Site_Name__c.substring(0, 4)+ string.valueOf(Off[0].Tenant__r.Tenant_Swift_Code__c).substring(0, 3) + market + loanType + Off[0].Product_Template__r.TemplateName__c.substring(0, 3) + Off[0].Name;
                        updateoff.TransactionRefNumber__c= TransactionRefNumber;
                        updateoff.New_Status__c = 'Approved2'; 
                        updateoff.Approved__c = true;
                        updateoff.Status__c='Transaction Approved';
                        updateoff.Approvedate__c=system.Now();
                        ListOfferupdate.add(updateoff); 
                        Helper.NotificationInsertCall(Off[0].CreatedBy__c,Off[0].CreatedBy__c,'Transaction Created','',TransactionRefNumber,OffInfo.id);
                        Helper.NotificationInsertCall(Off[0].CreatedBy__c,Off[0].CreatedBy__c,'Transaction Approved','',TransactionRefNumber,OffInfo.id);
                        
                        response = OffInfo.Id;
                        Helper.ActivityLogInsertCallForRecord(OffInfo.CreatedBy__c,OffInfo.Id,'Transaction Created','Transaction Successfully Created.',false);
                    }
                    List<Transaction_Attributes__c> ListTAobj = New List<Transaction_Attributes__c>();
                    for(string WList : wrapperList)
                    {
                        Transaction_Attributes__c TAobj = New Transaction_Attributes__c();
                        TAobj.Attribute_Name__c= WList.split(':')[0];
                        String AttValues= WList.split(':')[1];
                        if(AttValues.contains('-')){
                            TAobj.Attribute_Value__c= AttValues.split('-')[2]+'/'+AttValues.split('-')[1]+'/'+AttValues.split('-')[0];
                        }else{
                            TAobj.Attribute_Value__c= WList.split(':')[1];
                        }
                        TAobj.Attribute_Type__c= WList.split(':')[2];
                        system.debug('<####>'+WList.split(':')[2]);
                        if(WList.split(':')[2] =='Number')
                        {
                            system.debug('<##IN##>'+WList.split(':')[2]);
                            TAobj.Attribute_Value_OnlyNumber__c=Integer.valueOf(WList.split(':')[1]);
                            system.debug('<##OUT##>'+WList.split(':')[2]);
                        }
                        TAobj.Transaction__c = response;
                        TAobj.Product_Template__c = ProID;//saveSellerOffers.Product_Template__c;
                        ListTAobj.add(TAobj);
                    } 
                    system.debug('##size'+ListTAobj.Size());
                    if(ListTAobj.Size()>0){
                        Insert ListTAobj;
                    }
                    
                    IF(ListNoti.size()>0)
                    {
                        Insert ListNoti;
                        
                    }
                    if(ListOfferupdate.size()>0){
                        update ListOfferupdate;
                    }
                    
                    
                }
            }
        }
        catch (Exception e) { 
            //Logger.LogException('Transaction Created-Error', 'Exception : ' + e.getMessage());   
            response = '';
        }
        if(IB.Size()>1)
        {
            try{
                response ='GT-'+Groupid;
                Transaction__c[] Trans = [SELECT id,Tenant__c,CreatedBy__r.User_Title__c,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,createdbyId,CreatedBy__c,Tenant__r.Tenant_Footer_Message__c,Tenant__r.Tenant_Site_Name__c,Tenant__r.Tenant_Swift_Code__c,TransactionRefNumber__c,createdby.Name,Name,Product_Template__r.TemplateName__c From Transaction__c WHERE GroupID__c=:'GT-'+Groupid LIMIT 1];
                String Amt = [SELECT Attribute_Name__c,Attribute_Value__c,Transaction__c,Transaction__r.TransactionRefNumber__c   from Transaction_Attributes__c WHERE Transaction__c =:Trans[0].Id AND Attribute_Name__c='Amount' Limit 1].Attribute_Value__c; 
                // Step 0: Create a master list to hold the emails we'll send
                List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();  
                // Step 1: Create a new Email
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                // Step 2: Set list of people who should get the email
                List<User_Management__c> lstUM  = [SELECT User_Email__c from User_Management__c WHERE Tenant__c=: Trans[0].Tenant__c AND (Profiles__c=:system.label.SMCP_ProfileId OR Profiles__c=:system.label.AMCP)] ;
                string email = lstUM[0].User_Email__c ; 
                List<String> sendTo = new List<String>();
                sendTo.add(email);
                mail.setToAddresses(sendTo);
                List<String> GmailList = New List<String>();
                String[] Groupmail = System.label.GroupEmail.split(',');
                for (String str : Groupmail) {
                    GmailList.add(str);
                }
                mail.setCcAddresses(GmailList);
                // Step 3: Set who the email is sent from
                mail.setReplyTo('tradeaixa@gmail.com');
                mail.setSenderDisplayName('TradeAix');
                // Step 4. Set email contents - you can use variables!
                Datetime myDT = system.now();   
                String myDate = myDT.format('YYYY-MM-dd HH:mm:ss');
                string mailSubject =IB.Size()+ ' Transactions Created ('+Trans[0].Tenant__r.Tenant_Site_Name__c+')';
                mail.setSubject(mailSubject); 
                String body='';
                if(Trans[0].CreatedBy__r.User_Title__c!=null){
                     body = Trans[0].CreatedBy__r.User_Title__c+' '+Trans[0].CreatedBy__r.First_Name__c+' '+Trans[0].CreatedBy__r.Last_Name__c+', <br/><br/>';
                }else
                {
                     body =Trans[0].CreatedBy__r.First_Name__c+' '+Trans[0].CreatedBy__r.Last_Name__c+', <br/><br/>';
                    
                }
                body += 'Please find the transaction details below,<br/>';
                body +='<br/>';
                body +='<table>';
                body += '<tr><td>Issuing Banks</td><td>-</td><td>Transaction Ref Number</td><td>-</td><td>Currency Amount</td></tr>';
                body += '<tr> <td><hr></hr></td><td><hr></hr></td><td><hr></hr></td><td><hr></hr></td><td><hr></hr></td></tr>';
                for(Transaction__c t : [Select Id,CreatedBy__c,Name,Tenant__c,Issuing_Banks__r.Tenant_Site_Name__c,TransactionRefNumber__c,Issuing_Banks__r.City__c,Currency__c FROM Transaction__c Where GroupID__c=:'GT-'+Groupid]){ 
                    body += '<tr><td>'+t.Issuing_Banks__r.Tenant_Site_Name__c+', '+t.Issuing_Banks__r.City__c+'</td><td>-</td><td>'+t.TransactionRefNumber__c+'</td><td>-</td><td>'+t.Currency__c+' '+Amt+'</td></tr>';
                }
                body +='</table>';
                body +='<br/><br/>';
             
                body +='Thanks<br/>';
                body +=Trans[0].Tenant__r.Tenant_Footer_Message__c;
                mail.setHtmlBody(body);
                // Step 5. Add your email to the master list
                mails.add(mail);
                // Step 6: Send all emails in the master list
                Messaging.sendEmail(mails);
            }    
            catch(exception ex){
                system.debug('===TransactionTrigger==='+ex.getMessage());
            }            
        }else{
            system.debug('##response##'+response);
            EmailManager em = new EmailManager();
            for(Transaction__c t : [Select Id,CreatedBy__c,Name,Status__c,Tenant__c FROM Transaction__c Where Id=:response]){  
                try{
                    List<String> CcAddresses = new List<String>(); 
                    List<String> bCcAddresses = new List<String>(); 
                    set<Id> tenantId = new set<Id>();
                    String toEmail = [SELECT User_Email__c from User_Management__c WHERE Tenant__c=: t.Tenant__c AND (Profiles__c=:system.label.SMCP_ProfileId OR Profiles__c=:system.label.AMCP)].User_Email__c ;
                    
                    List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: t.CreatedBy__c];                 
                    for( User_Management__c lstStr: listUserEmail){
                        CcAddresses.add(lstStr.User_Email__c) ;
                    }
                    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TransactionCreateEmailTemp' LIMIT 1].Id;
                    em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(t.Id));
                    
                }catch(exception ex){
                    system.debug('===TransactionTrigger==='+ex.getMessage());
                }
                
            } 
        }
        system.debug('============response===== '+response);
        return response; 
    } 
    
    @AuraEnabled
    public static ProductTemplateNewv1 getTemplateDetail(String templateId){
        ProductTemplateNewv1 retValue= new ProductTemplateNewv1();
        try{
            List<ProductTemplate__c> lstProductTemplates = [
                SELECT 
                Id, 
                TemplateName__c,
                Transactiondetailfields__c,
                IsPrimary__c,ProductType__c
                FROM ProductTemplate__c
                WHERE ID = :templateId
            ];
            if(lstProductTemplates != null && lstProductTemplates.size()>0){
                retValue.PT = lstProductTemplates.get(0);
            }
            retValue.strList = [SELECT Attributes__r.Name,Attributes__r.Attribute_Type__c,Attributes__r.Mandatory__c FROM Product_Template_Object__c WHERE Product_Template__c=:templateId];
            
        } 
        catch(Exception ex){ }
        return retValue;
    }
}
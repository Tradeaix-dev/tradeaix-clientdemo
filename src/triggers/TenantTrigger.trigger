trigger TenantTrigger on Tenant__c (after insert, after update) {
    system.debug('====TenantTrigger Inside====');
    EmailManager em = new EmailManager();  
    if(Trigger.isAfter){
        
        system.debug('====TenantTrigger isBefore Inside====');
        for (Tenant__c tenant: Trigger.new) {
            if(Trigger.isInsert){
                system.debug('====TenantTrigger isBefore If Condition====');
                if(tenant.Tenant_Type__c == 'Bank' && tenant.IsActive__c == false && tenant.Issuing_Bank_Approved__c == false){
                    system.debug('====TenantTrigger isBefore Inside If Condition====');
                    Helper.NotificationInsertCall(tenant.CreatedBy__c,system.label.Admin_TradeAix.split(';')[1],'Issuing Bank Created','',tenant.Tenant_Site_Name__c,tenant.Id);
                    String toEmail = system.label.TradeAix_email_ID;
                    List<String> CcAddresses = new List<String>();
                    CcAddresses.add(toEmail);   
                    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'IssuingBankCreatedEmailTemp' LIMIT 1].Id;
                    em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(tenant.Id));
                }
            }
            
            
            if (Trigger.isUpdate) {               
                
                if(tenant.IsActive__c == true && tenant.Issuing_Bank_Status__c == 'Approved' && Trigger.oldMap.get(tenant.Id).Issuing_Bank_Status__c != 'Approved'){
                    Helper.NotificationInsertCall(tenant.LastModifiedBy__c,tenant.CreatedBy__c,'Issuing Bank Approved',tenant.Approve_Reject_Reason__c,tenant.Tenant_Site_Name__c,tenant.Id);
                    system.debug('====TenantTrigger update Approved condition====');
                    String toEmail = [SELECT User_Email__c from User_Management__c WHERE Id=: tenant.CreatedBy__c].User_Email__c;
                    List<String> CcAddresses = new List<String>();
                    CcAddresses.add(system.label.TradeAix_email_ID);   
                    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'IssuingBankApproveRejectEmailTemp' LIMIT 1].Id;
                    em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(tenant.Id));
                } 
                if(tenant.IsActive__c == false && tenant.Issuing_Bank_Status__c == 'Rejected' && Trigger.oldMap.get(tenant.Id).Issuing_Bank_Status__c != 'Rejected'){
                    Helper.NotificationInsertCall(tenant.LastModifiedBy__c,tenant.CreatedBy__c,'Issuing Bank Rejected',tenant.Approve_Reject_Reason__c,tenant.Tenant_Site_Name__c,tenant.Id);
                    system.debug('====TenantTrigger update Rejected condition====');
                    String toEmail = [SELECT User_Email__c from User_Management__c WHERE Id=: tenant.CreatedBy__c].User_Email__c;
                    List<String> CcAddresses = new List<String>();
                    CcAddresses.add(system.label.TradeAix_email_ID);   
                    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'IssuingBankApproveRejectEmailTemp' LIMIT 1].Id;
                    em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(tenant.Id));
                }  
                if(tenant.IsActive__c == true && tenant.User_Activation_Email__c == false){
                    system.debug('====TenantTrigger update condition====');
                    UserActiveNotification.usernotification(tenant.Id, tenant.Tenant_Short_Name__c);
                }
            }
        }
       
        
    }
    
}
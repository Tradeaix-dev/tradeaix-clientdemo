trigger OTPHistoryTrigger  on OTP_History__c (before insert,after insert) {
    EmailManager em = new EmailManager();  
    system.debug('====OTP_History__c Inside====');
    
    try{
<<<<<<< HEAD
        
        for (OTP_History__c tenantuser: Trigger.new) {
            if(Trigger.isbefore){
                tenantuser.OTP_Generated_On__c = System.Now();
                tenantuser.OTP_Expired_On__c = System.Now().AddMinutes(Integer.valueOf(System.Label.OTPTimeinterval));
            }
            if(Trigger.isafter){
                Limited_Bidding__c UM = [SELECT Id,agree__c,Transaction__r.RFQChk__c,Transaction__r.RFIChk__c,CreatedBy__c,Tenant_User__c from Limited_Bidding__c WHERE Id=:tenantuser.Limited_Bidding__c];
                
                system.debug('##'+tenantuser);
                system.debug('##'+UM .agree__c);
                if(UM.agree__c) {
                    //&& (UM.Transaction__r.RFQChk__c || UM.Transaction__r.RFIChk__c)){
                    List<String> CcAddresses = new List<String>(); 
                    List<String> CcAddresses1 = new List<String>(); 
                    String toEmail = [SELECT User_Email__c from User_Management__c WHERE id=:UM.Tenant_User__c].User_Email__c ;
                    system.debug('======isInsert=====');      
                    CcAddresses1.add(toEmail);
                    List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =:UM.CreatedBy__c];                 
                    for( User_Management__c lstStr: listUserEmail){
                        //CcAddresses.add(lstStr.User_Email__c) ;
                        CcAddresses.add(toEmail) ;
                    }
                    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'OTPEmailTemp' LIMIT 1].Id;
                    //em.sendMailWithTemplate(toEmail , CcAddresses1 , temaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
                    // Dynamic Email OTP Notification - START 
                    Limited_Bidding__c LB = [SELECT Id,Transaction__c,Transaction_Reference__c,Tenant_User__c from Limited_Bidding__c WHERE Id=:tenantuser.Limited_Bidding__c];
                    User_Management__c UM1 = [SELECT Id,First_Name__c,Last_Name__c,Name,CreatedBy__c,User_Name__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Tenant_Logo_Url__c, Tenant__r.Tenant_Footer_Message__c FROM User_Management__c WHERE id=:LB.Tenant_User__c];
                    system.debug('##'+UM1);
                    User_Management__c UM2 = [SELECT Id,First_Name__c,Last_Name__c,Name,CreatedBy__c,User_Name__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Tenant_Logo_Url__c, Tenant__r.Tenant_Footer_Message__c FROM User_Management__c WHERE id=:UM1.CreatedBy__c];
                    
                    // Step 0: Create a master list to hold the emails we'll send
                    List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                    // Step 1: Create a new Email
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    // Step 2: Set list of people who should get the email
                    List<String> sendTo = new List<String>();
                    sendTo.add(toEmail);
                    mail.setToAddresses(sendTo);
                    // Step 3: Set who the email is sent from
                    //mail.setReplyTo('satheesh@aixchane.co.in');
                    mail.setSenderDisplayName('MNC');
                    // Step 4. Set email contents - you can use variables!
                    mail.setSubject('OTP for Transaction : '+tenantuser.Transaction_Ref_Number__c+' - '+System.Now()+' IST'); 
                    // Dynamic Email OTP Notification - END
                    String body = '<div style="width:50%;background-color:#fafafa"><div class="adM"><br></div><hr style="border:3px solid #2aaee4;margin-left:10px;margin-right:10px"><br>';
                    body += '<img id="m_4611592398463120175j_id0:emailTemplate:j_id3:j_id4:j_id6:j_id7:theImage" src="'+UM2.Tenant__r.Tenant_Logo_Url__c+'" style="margin-left:10px" width="15%" class="CToWUd">';
                    body += '<br><hr style="border:3px solid #2aaee4;margin-left:10px;margin-right:10px"><br><div style="margin-left:10px">';
                    body += 'Dear '+UM1.First_Name__c;
                    body += '</div><br><div style="margin-left:10px">As per your request, the One Time Password (OTP) is <b>'+tenantuser.OTP__c;
                    body += '</b>.<br> <br> Please use this OTP to participate in the transaction.<br> <br> ';
                    body += '<a href="https://clientdemo-stratizantqa.cs17.force.com/tradeaix/s/transactionviewdetails?transactionId='+LB.Transaction__c+'" target="_blank"';
                    body += '>Click here to view the Transaction</a><br><br>Note: OTP will expire in 5 Minutes.';
                    body += '</div><hr style="border:3px solid #2aaee4;margin-left:10px;margin-right:10px"><div style="margin-left:10px">';                  
                    body += '<br> '+UM2.Tenant__r.Tenant_Footer_Message__c+' </div><hr style="border:3px solid #2aaee4;margin-left:10px;margin-right:10px"><br> </div>';
                    mail.setHtmlBody(body);
                    
                    // Step 5. Add your email to the master list
                    mails.add(mail);
                    
                    // Step 6: Send all emails in the master list
                    Messaging.sendEmail(mails);
                }
            }
=======
   
        for (OTP_History__c tenantuser: Trigger.new) {
        if(Trigger.isbefore){
        tenantuser.OTP_Generated_On__c = System.Now();
        tenantuser.OTP_Expired_On__c = System.Now().AddMinutes(Integer.valueOf(System.Label.OTPTimeinterval));
        }
        if(Trigger.isafter){
            Limited_Bidding__c UM = [SELECT Id,agree__c,Transaction__r.RFQChk__c,Transaction__r.RFIChk__c,CreatedBy__c,Tenant_User__c from Limited_Bidding__c WHERE Id=:tenantuser.Limited_Bidding__c];

       system.debug('##'+tenantuser);
        system.debug('##'+UM .agree__c);
        if(UM.agree__c) {
        //&& (UM.Transaction__r.RFQChk__c || UM.Transaction__r.RFIChk__c)){
        List<String> CcAddresses = new List<String>(); 
        String toEmail = [SELECT User_Email__c from User_Management__c WHERE id=:UM.Tenant_User__c].User_Email__c ;
        system.debug('======isInsert=====');                   
        List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =:UM.CreatedBy__c];                 
        for( User_Management__c lstStr: listUserEmail){
        CcAddresses.add(lstStr.User_Email__c) ;
        }
        String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'OTPEmailTemp' LIMIT 1].Id;
        em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
        }
        }
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-clientdemo.git
        }
    }
    catch(exception ex){
        system.debug('=======Exception======='+ex.getMessage());
    }
    }
trigger TransactionTrigger on Transaction__c (after insert, after update) {
    TransactionTriggerHandler.SentMail(Trigger.new,Trigger.oldMap);
}
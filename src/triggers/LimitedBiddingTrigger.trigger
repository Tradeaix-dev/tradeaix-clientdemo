trigger LimitedBiddingTrigger  on Limited_Bidding__c (after insert, after update) {
    EmailManager em = new EmailManager();  
    system.debug('====TenantUserTrigger Inside====');
    if(Trigger.isAfter){
<<<<<<< HEAD
        
        try{
            List<OTP_History__c> OTPListt = NEW List<OTP_History__c>();
            for (Limited_Bidding__c tenantuser: Trigger.new) {
                if(Trigger.isInsert)
                {
                    List<String> CcAddresses = new List<String>(); 
                    String toEmail = [SELECT User_Email__c from User_Management__c WHERE id=:tenantuser.Tenant_User__c].User_Email__c ;
                    
                    system.debug('======isInsert=====');                   
                    List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: tenantuser.CreatedBy__c];                 
                    for( User_Management__c lstStr: listUserEmail){
                        CcAddresses.add(lstStr.User_Email__c) ;
                    }
                    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'CPInvitationEmailTemp' LIMIT 1].Id; 
                    String assigneetemaplateId = [SELECT Id from EmailTemplate WHERE Name = 'AssigneeEmailTemp' LIMIT 1].Id;  
                    system.debug('======LIMITED BIDDING TRIGGER assigneetemaplateId======'+assigneetemaplateId);
                    if(tenantuser.Don_t_Sent_mail__c == false && tenantuser.Assignee_Email__c == false){   
                        system.debug('======LIMITED BIDDING TRIGGER======');
                        //em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
                        // Dynamic Email Transaction Participation - START 
                        //Limited_Bidding__c UM = [SELECT Id,Transaction_Reference__c,Tenant_User__c from Limited_Bidding__c WHERE Id=:tenantuserId];
                        //system.debug('##'+UM);
                        User_Management__c UM1 = [SELECT Id,First_Name__c,Invited_By__c,Last_Name__c,Name,CreatedBy__c,User_Name__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Tenant_Logo_Url__c, Tenant__r.Tenant_Footer_Message__c, Task_Assignee__c FROM User_Management__c WHERE id=:tenantuser.Tenant_User__c];
                        system.debug('##'+UM1);
                        User_Management__c UM2 = [SELECT Id,First_Name__c,Last_Name__c,Name,CreatedBy__c,User_Name__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Tenant_Logo_Url__c, Tenant__r.Tenant_Footer_Message__c, Task_Assignee__c FROM User_Management__c WHERE id=:UM1.CreatedBy__c];
                        // Step 0: Create a master list to hold the emails we'll send
                        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                        // Step 1: Create a new Email
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        // Step 2: Set list of people who should get the email
                        List<String> sendTo = new List<String>();
                        sendTo.add(toEmail);
                        mail.setToAddresses(sendTo);
                        // Step 3: Set who the email is sent from
                        //mail.setReplyTo('satheesh@aixchane.co.in');
                        mail.setSenderDisplayName('MNC');
                        mail.setSubject(tenantuser.Transaction_Reference__c+' : Transaction Participation Invite'); 
                        // Step 4. Set email contents - you can use variables!
                        String body = '<div style="width:50%;background-color:#fafafa"><div class="adM"><br></div><hr style="border:3px solid #2aaee4;margin-left:10px;margin-right:10px"><br>';
                        body += '<img id="m_-5463468053937946419j_id0:emailTemplate:j_id3:j_id4:j_id6:j_id7:theImage" src="'+UM2.Tenant__r.Tenant_Logo_Url__c+'" style="margin-left:10px" width="15%" class="CToWUd">';
                        body += '<br><hr style="border:3px solid #2aaee4;margin-left:10px;margin-right:10px"><br><div style="margin-left:10px">Dear '+UM1.First_Name__c+',</div> <br>';
                        body += '<div style="margin-left:10px"><span id="m_-5463468053937946419j_id0:emailTemplate:j_id3:j_id4:j_id6:j_id7:j_id12">You have been invited by <b>'+UM2.First_Name__c+' '+UM2.Last_Name__c+'</b> to participate in a Trade Finance transaction. The transaction reference is <b>';
                        body += tenantuser.Transaction_Reference__c+'</b>.</span><br><br>';
                        body += 'Please <a href="https://clientdemo-stratizantqa.cs17.force.com/tradeaix/s/onboarding?id='+UM1.Id+'&amp;cp='+tenantuser.Id+'&amp;i='+UM1.Invited_By__c+'" target="_blank">click here </a> to get started.<br><br> ';
                        body += '</div><hr style="border:3px solid #2aaee4;margin-left:10px;margin-right:10px"><div style="margin-left:10px">';                  
                        body += '<br>'+UM2.Tenant__r.Tenant_Footer_Message__c+'</div><hr style="border:3px solid #2aaee4;margin-left:10px;margin-right:10px"><br></div>';
                        mail.setHtmlBody(body);                
                        // Step 5. Add your email to the master list
                        mails.add(mail);                
                        // Step 6: Send all emails in the master list
                        Messaging.sendEmail(mails);               
                        // Dynamic Email Transaction Participation - END 
                    } 
                    if(tenantuser.Assignee_Email__c == true){            
                        em.sendMailWithTemplate(toEmail , CcAddresses , assigneetemaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
                    }   
                    system.debug('======isInsert=====');
                    
                }
                //if(tenantuser.agree__c == true && tenantuser.Don_t_Sent_mail__c == false){ 
                if(tenantuser.agree__c){
                    system.debug('====Limited_Bidding__c====AGREE=='+tenantuser.agree__c);
                    //Limited_Bidding__c oldlimiting = Trigger.oldMap.get(tenantuser.Id);
                    if(tenantuser.agree__c == true && tenantuser.Don_t_Sent_mail__c == false){ 
                        String aEmail = [SELECT User_Email__c from User_Management__c WHERE id=:tenantuser.Tenant_User__c].User_Email__c ; 
                        Helper.ActivityLogInsertCallForRecord(tenantuser.Tenant_User__c,tenantuser.Transaction__c,'CP - '+aEmail+'- Accepted to participate','',false);
                    }
                    Integer rndnum = Math.round(Math.random()*1000000);
                    
                        OTP_History__c OH =NEW OTP_History__c();
                        if(string.valueOf(rndnum).length() == 4){
                            OH.OTP__c ='91'+string.valueOf(rndnum);
                        }  else if(string.valueOf(rndnum).length() == 5)  {
                            OH.OTP__c ='9'+string.valueOf(rndnum);
                        }else{
                            OH.OTP__c =string.valueOf(rndnum);
                        }   
                        OH.isActive__c =true;
                        OH.Limited_Bidding__c =tenantuser.Id;
                        OH.OTP_Time_Intervals__c= string.valueOf(System.Now().getTime());
                        //OH.OTP_Time_Interval__c = Integer.valueOf(System.Label.OTPTimeinterval);
                        OH.OTP_Generated_On__c = System.Now();
                        OH.OTP_Expired_On__c = System.Now().AddMinutes(Integer.valueOf(System.Label.OTPTimeinterval));
                        
                        OTPListt.add(OH);
                   
                }
                
                if(tenantuser.Do_Not_Agree__c){  
                    List<String> CcAddresses = new List<String>();
                    system.debug('======isInsert=====');                   
                    String toEmail1 = [SELECT User_Email__c from User_Management__c WHERE id=:tenantuser.Tenant_User__c].User_Email__c ;
                    List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: tenantuser.CreatedBy__c];                 
                    for( User_Management__c lstStr: listUserEmail){
                        CcAddresses.add(lstStr.User_Email__c) ;
                    }
                    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'OTPNotificationdisagreeEmailTemp' LIMIT 1].Id;
                    em.sendMailWithTemplate(toEmail1 , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
                    
                    
                }
                if(tenantuser.Not_the_Apropriate_User__c){ 
                    List<String> CcAddresses = new List<String>(); 
                    system.debug('======isInsert=====');                   
                    String toEmail1 = [SELECT User_Email__c from User_Management__c WHERE id=:tenantuser.Tenant_User__c].User_Email__c ;
                    List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: tenantuser.CreatedBy__c];                 
                    for( User_Management__c lstStr: listUserEmail){
                        CcAddresses.add(lstStr.User_Email__c) ;
                    }
                    Helper.ActivityLogInsertCallForRecord(tenantuser.Tenant_User__c,tenantuser.Transaction__c,'CP - '+toEmail1+'- Declined to participate','',false);
                    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'OTPNotificationnotuserEmailTemp' LIMIT 1].Id;
                    em.sendMailWithTemplate(toEmail1 , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
                    
                    
                }
            }
            if(OTPListt.Size()>0){
                Insert OTPListt;
            }
        }
        catch(exception ex){
            system.debug('=======Exception======='+ex.getMessage());
        }
    }
}
=======
    
    try{
    List<OTP_History__c> OTPListt = NEW List<OTP_History__c>();
    for (Limited_Bidding__c tenantuser: Trigger.new) {
    if(Trigger.isInsert)
    {
    List<String> CcAddresses = new List<String>(); 
    String toEmail = [SELECT User_Email__c from User_Management__c WHERE id=:tenantuser.Tenant_User__c].User_Email__c ;
    
    system.debug('======isInsert=====');                   
    List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: tenantuser.CreatedBy__c];                 
    for( User_Management__c lstStr: listUserEmail){
    CcAddresses.add(lstStr.User_Email__c) ;
    }
        String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'CPInvitationEmailTemp' LIMIT 1].Id; 
        String assigneetemaplateId = [SELECT Id from EmailTemplate WHERE Name = 'AssigneeEmailTemp' LIMIT 1].Id;  
        system.debug('======LIMITED BIDDING TRIGGER assigneetemaplateId======'+assigneetemaplateId);
        if(tenantuser.Don_t_Sent_mail__c == false && tenantuser.Assignee_Email__c == false){   
            system.debug('======LIMITED BIDDING TRIGGER======');
            em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
        } 
        if(tenantuser.Assignee_Email__c == true){            
            em.sendMailWithTemplate(toEmail , CcAddresses , assigneetemaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
        }   
    system.debug('======isInsert=====');
    
    }
    if(tenantuser.agree__c){    
    Integer rndnum = Math.round(Math.random()*1000000);
        
        OTP_History__c OH =NEW OTP_History__c();
        if(string.valueOf(rndnum).length() == 4){
            OH.OTP__c ='91'+string.valueOf(rndnum);
        }  else if(string.valueOf(rndnum).length() == 5)  {
             OH.OTP__c ='9'+string.valueOf(rndnum);
        }else{
             OH.OTP__c =string.valueOf(rndnum);
        }   
        OH.isActive__c =true;
        OH.Limited_Bidding__c =tenantuser.Id;
        OH.OTP_Time_Intervals__c= string.valueOf(System.Now().getTime());
        //OH.OTP_Time_Interval__c = Integer.valueOf(System.Label.OTPTimeinterval);
        OH.OTP_Generated_On__c = System.Now();
        OH.OTP_Expired_On__c = System.Now().AddMinutes(Integer.valueOf(System.Label.OTPTimeinterval));

        OTPListt.add(OH);
    }
       
    if(tenantuser.Do_Not_Agree__c){  
    List<String> CcAddresses = new List<String>();
    system.debug('======isInsert=====');                   
    String toEmail1 = [SELECT User_Email__c from User_Management__c WHERE id=:tenantuser.Tenant_User__c].User_Email__c ;
    List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: tenantuser.CreatedBy__c];                 
    for( User_Management__c lstStr: listUserEmail){
    CcAddresses.add(lstStr.User_Email__c) ;
    }
    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'OTPNotificationdisagreeEmailTemp' LIMIT 1].Id;
    em.sendMailWithTemplate(toEmail1 , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
    
    
    }
    if(tenantuser.Not_the_Apropriate_User__c){ 
    List<String> CcAddresses = new List<String>(); 
    system.debug('======isInsert=====');                   
    String toEmail1 = [SELECT User_Email__c from User_Management__c WHERE id=:tenantuser.Tenant_User__c].User_Email__c ;
    List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: tenantuser.CreatedBy__c];                 
    for( User_Management__c lstStr: listUserEmail){
    CcAddresses.add(lstStr.User_Email__c) ;
    }
    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'OTPNotificationnotuserEmailTemp' LIMIT 1].Id;
    em.sendMailWithTemplate(toEmail1 , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
    
    
    }
    }
    if(OTPListt.Size()>0){
    Insert OTPListt;
    }
    }
    catch(exception ex){
    system.debug('=======Exception======='+ex.getMessage());
    }
    }
    }
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-clientdemo.git

trigger QuoteTrigger on Quotes__c (before insert,after insert, after update) {
    EmailManager em = new EmailManager();  
    for(Quotes__c a : Trigger.New){    
        system.debug('===Trigger call===');     
            try{
            if(Trigger.isbefore && Trigger.isinsert){
            TimeZone tz = UserInfo.getTimeZone();
            a.CreatedDatePDF__c = System.Now().format('dd/MM/YYYY HH:mm:ss', tz.getID());
            }else{
            string actionTo ='';
            Transaction__c[] tranCreated = [SELECT Tenant__c,CreatedBy__c from Transaction__c WHERE Id =: a.Transaction__c];
            String toEmail = [SELECT User_Email__c from User_Management__c WHERE Tenant__c=: tranCreated[0].Tenant__c AND (Profiles__c=:system.label.SMCP_ProfileId OR Profiles__c=:system.label.AMCP)].User_Email__c ;
            List<User_Management__c> listUserEmail= [SELECT Id,Tenant__r.Tenant_Short_Name__c,User_Email__c from User_Management__c WHERE Id =: a.CreatedBy__c];                 
            actionTo = listUserEmail[0].Id;
            List<String> CcAddresses = new List<String>();
            for( User_Management__c lstStr: listUserEmail){
                CcAddresses.add(lstStr.User_Email__c) ;               
            }
            if(a.Bid_Status__c.equals('Quote Closed')){
                Helper.NotificationInsertCall(tranCreated[0].CreatedBy__c,actionTo,'Quote Closed','',a.Name,a.id);
                //String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'BidClosedEmailTemp' LIMIT 1].Id;
                //em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(a.Id));
            }
            if(a.Bid_Status__c.equals('Quote Accepted')){
                Helper.NotificationInsertCall(tranCreated[0].CreatedBy__c,actionTo,'Quote Accepted','',a.Name,a.id);
                String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'BidAcceptEmailTemp' LIMIT 1].Id;
                em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(a.Id)); 
            }
            if(a.Bid_Status__c.equals('Quote Rejected')){
                Helper.NotificationInsertCall(tranCreated[0].CreatedBy__c,actionTo,'Quote Rejected','',a.Name,a.id);
                String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'BidRejectEmailTemp' LIMIT 1].Id;
                em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(a.Id)); 
            }
            if(a.Bid_Status__c.equals('Quote Withdrawn')){                
                Helper.NotificationInsertCall(actionTo,tranCreated[0].CreatedBy__c,'Quote Withdrawn','',a.Name,a.id);
                String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'BidwithdrawnTemp' LIMIT 1].Id;
                em.sendMailWithTemplateQuote(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(a.Id),listUserEmail[0].Tenant__r.Tenant_Short_Name__c); 
            }
            if(a.Bid_Status__c.equals('Quote Inactivated')){
                Helper.NotificationInsertCall(actionTo,tranCreated[0].CreatedBy__c,'Quote Inactivated',a.Name+' - Auto Inactivated',a.Name,a.id);
                String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'BidInactivateEmailTemp' LIMIT 1].Id;
                em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(a.Id)); 
            }            
            if(a.Bid_Status__c.equals('Quote Submitted')){
                Helper.NotificationInsertCall(actionTo,tranCreated[0].CreatedBy__c,'Quote Submitted','',a.Name,a.id);
                system.debug('==If submit bid=='+userinfo.getuserid());
                String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'BidCreateEmailTemp' LIMIT 1].Id;               
                //String[] CcAddresses = Helper.getCCMail(usrList, lstUsers, sellerUser.Id); 
                em.sendMailWithTemplateQuote(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(a.Id),listUserEmail[0].Tenant__r.Tenant_Short_Name__c); 
            } }
            
        }catch(exception ex){
            system.debug('==Quote Trigger Exception=='+ex.getMessage());
        }
    }
}